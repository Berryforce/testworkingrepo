<messaging:emailTemplate recipientType="Contact" 
  relatedToType="Directory_Edition__c"
  subject="New Directory Dates - {!relatedTo.Year__c} Edition of {!relatedTo.Directory__r.Name} ({!relatedTo.Directory_Code__c})">

<messaging:htmlEmailBody >

  <p>{!Month(TODAY())}-{!Day(TODAY())}-{!Year(TODAY())}</p>
  <p>Dear {!recipient.name},</p>
  <p>The following are key dates that have been established for the directory listed above.  Please be advised these dates
  are subject to change and dates bolded below represent a change from the previous schedule letter sent.</p>

    <apex:panelgrid columns="3" id="dates" frame="box" border="2">
        <apex:outputtext value="EAS Change Close:" />
        <apex:outputfield value="{!relatedTo.EAS_CHANGE_CLOSE__c}" />
        <apex:outputtext value="Any changes, addition or deletions to Extended Area Service (EAS)/Courtesy sections must be received by your Publishing Project Manager by this date." />
        
        <apex:outputtext value="DCR Close:" />
        <apex:outputfield value="{!relatedTo.DCR_Close__c}" />
        <apex:outputtext value="Last date for Sales to initiate a Directory Change Request (DCR). Final DCR's are due Telco this date." />
        
        <apex:outputtext value="Service Order Close:" />
        <apex:outputfield value="{!relatedTo.BOC__c}" />
        <apex:outputtext value="Also known as Business Office Close (BOC), final date for the Telephone Company to complete service orders to appear in upcoming issue." />
        
        <apex:outputtext value="Final Service Orders Due Berry:" />
        <apex:outputfield value="{!relatedTo.Final_Service_Order_Due_to_Berry__c}" />
        <apex:outputtext value="All Service Orders are to be at The Berry Company by this date. Service Orders received after this date will be held until the next issue." />
        
        <apex:outputtext value="CATS/Manuscript Due Berry:" />
        <apex:outputfield value="{!relatedTo.MANUSCRIPT_DUE_BERRY__c}" />
        <apex:outputtext value="The Annual Load File or Manuscript must be to Berry by this date." />
        
<!--    <apex:outputtext value="Pre-Galley Due Telco:" />
        <apex:outputfield value="{!relatedTo.ALPHA_GALLEY_PULL__c}" />
        <apex:outputtext value="Date when Berry needs to send the Pre-Galley to the Telco companies." />
-->        
        <apex:outputtext value="Alpha Galley Due Telco:" />
        <apex:outputfield value="{!relatedTo.Galleys_Due_Telco__c}" />
        <apex:outputtext value="Berry will provide a list of all white page listings for verification. Listing information will reflect activity up to Service Order Close date only. These pages are for proofing only. No new additions or changes should be made to the galley." />

        <apex:outputtext value="Alpha Galley Due Berry:" />
        <apex:outputfield value="{!relatedTo.Galleys_Due_Berry__c}" />
        <apex:outputtext value="Galley corrections need to be noted and returned to Berry no later than this date." />

        <apex:outputtext value="Mailing Labels Due Berry:" />
        <apex:outputfield value="{!relatedTo.MAIL_LABELS_BERRY__c}" />
        <apex:outputtext value="Mailing Labels should be received by Berry no later than this date." />
        
        <apex:outputtext value="Alpha Page Proofs Due Telco:" rendered="{!IF(OR(relatedTo.Directory__r.ALPHA_PROOFS__c = 'No', relatedTo.Directory__r.ALPHA_PROOFS__c = 'N/A', relatedTo.Directory__r.ALPHA_PROOFS__c = ''),false,true)}" />
        <apex:outputfield value="{!relatedTo.ALPHA_PROOFS_DUE__c}" rendered="{!IF(OR(relatedTo.Directory__r.ALPHA_PROOFS__c = 'No', relatedTo.Directory__r.ALPHA_PROOFS__c = 'N/A', relatedTo.Directory__r.ALPHA_PROOFS__c = ''),false,true)}" />
        <apex:outputtext value="Telephone Company receives their set of alpha proofs for verification." rendered="{!IF(OR(relatedTo.Directory__r.ALPHA_PROOFS__c = 'No', relatedTo.Directory__r.ALPHA_PROOFS__c = 'N/A', relatedTo.Directory__r.ALPHA_PROOFS__c = ''),false,true)}" />
        
        <apex:outputtext value="Alpha Proof Corrections Due Berry:" rendered="{!IF(OR(relatedTo.Directory__r.ALPHA_PROOFS__c = 'No', relatedTo.Directory__r.ALPHA_PROOFS__c = 'N/A', relatedTo.Directory__r.ALPHA_PROOFS__c = ''),false,true)}" />
        <apex:outputfield value="{!relatedTo.ALPHA_PROOFS_DUE_BERRY__c}" rendered="{!IF(OR(relatedTo.Directory__r.ALPHA_PROOFS__c = 'No', relatedTo.Directory__r.ALPHA_PROOFS__c = 'N/A', relatedTo.Directory__r.ALPHA_PROOFS__c = ''),false,true)}" />
        <apex:outputtext value="Telephone Company must notify Berry of all corrections by this date." rendered="{!IF(OR(relatedTo.Directory__r.ALPHA_PROOFS__c = 'No', relatedTo.Directory__r.ALPHA_PROOFS__c = 'N/A', relatedTo.Directory__r.ALPHA_PROOFS__c = ''),false,true)}" />
        
        <apex:outputtext value="Class Page Proofs Due Telco:" rendered="{!IF(OR(relatedTo.Directory__r.CLASS_PROOFS__c = 'No', relatedTo.Directory__r.CLASS_PROOFS__c = ''),false,true)}" />
        <apex:outputfield value="{!relatedTo.CLASS_PROOFS_DUE__c}" rendered="{!IF(OR(relatedTo.Directory__r.CLASS_PROOFS__c = 'No', relatedTo.Directory__r.CLASS_PROOFS__c = ''),false,true)}" />
        <apex:outputtext value="Telephone Company receives their set of class proofs for verification." rendered="{!IF(OR(relatedTo.Directory__r.CLASS_PROOFS__c = 'No', relatedTo.Directory__r.CLASS_PROOFS__c = ''),false,true)}" />
        
        <apex:outputtext value="Class Proof Corrections Due Berry:" rendered="{!IF(OR(relatedTo.Directory__r.CLASS_PROOFS__c = 'No', relatedTo.Directory__r.CLASS_PROOFS__c = ''),false,true)}" />
        <apex:outputfield value="{!relatedTo.CLASS_PROOFS_DUE_BERRY__c}" rendered="{!IF(OR(relatedTo.Directory__r.CLASS_PROOFS__c = 'No', relatedTo.Directory__r.CLASS_PROOFS__c = ''),false,true)}" />
        <apex:outputtext value="Telephone Company must notify Berry of all corrections by this date." rendered="{!IF(OR(relatedTo.Directory__r.CLASS_PROOFS__c = 'No', relatedTo.Directory__r.CLASS_PROOFS__c = ''),false,true)}" />

        <apex:outputtext value="Printer Ship Date:" />
        <apex:outputfield value="{!relatedTo.Ship_Date__c}" />
        <apex:outputtext value="Printer will ship the directories for distribution." />

        <apex:outputtext value="Billing Information Due Telco:" />
        <apex:outputfield value="{!relatedTo.EBD__c}" />
        <apex:outputtext value="Telephone Company will receive the list of businesses on this date. If this date does not meet your requirements please contact us immediately." />
    </apex:panelgrid>

  <p>In closing, please share this information with the appropriate team members in your organization. If schedule changes
  are required please contact Rita Ganan at 855-447-0423 or Rita.Ganan@theberrycompany.com at your earliest convenience.</p>
<br/>
  <p>Thank you,<br/>
  {!$User.FirstName} {!$User.LastName}<br/>
  Directory Schedule Coordinator<br/>
  {!$User.Email}</p>
</messaging:htmlEmailBody>
</messaging:emailTemplate>