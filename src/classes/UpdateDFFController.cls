public with sharing class UpdateDFFController {
    
    String dffId {get;set;}
    String accountId {get;set;}
    public String profileId {get;set;}
    public boolean hideButton {get;set;}
    map<Id, Fulfillment_Profile__c> mapFProfile {get;set;}
    list<Digital_Product_Requirement__c> lstDFF {get;set;}
    public list<wrapperProfileClass> lstWrapper {get;set;}
    
    
    public UpdateDFFController(ApexPages.StandardController controller) {
        dffId = Apexpages.currentPage().getParameters().get('id');      
    }
    
    public Pagereference onLoad() {
        hideButton = false;
        lstWrapper = new list<wrapperProfileClass>();
        lstDFF = DigitalFulfillmentFormSOQLMethods.getDFFbyId(new set<Id>{dffId});
        system.debug('DFF Record : '+ lstDFF.size());
        if(lstDFF.size() > 0) {
            accountId = lstDFF[0].Account__c;
            if(String.isNotEmpty(accountId)) {
                mapFProfile = FulfillmentProfileSOQLMethods.getFulFillmentProfileByAccountId(new set<Id>{accountId});
                System.debug('##### '+mapFProfile.size());
                if(mapFProfile.size() > 0) {
                    for(Fulfillment_Profile__c iterator : mapFProfile.values()) {
                        lstWrapper.add(new wrapperProfileClass(false, iterator));
                    }
                }
                else {
                    CommonMethods.addError(CommonMessages.fulfillmentProfileisEmpty);                   
                    hideButton = true;
                }
            }
            else {
                CommonMethods.addError(CommonMessages.dffAccountIsEmpty);
                hideButton = true;
            }
        }
        return null;
    }
    
    public Pagereference updateDFFNew() {
        if(validatingNewUpdate()) {
            if(lstDFF != null && lstDFF.size() > 0) {
                Fulfillment_Profile__c objFProfile = mapFProfile.get(profileId);
                system.debug('DFF Record : '+ lstDFF.size());
                system.debug('DFF Record : '+ lstDFF);
                Digital_Product_Requirement__c modifyDFF = CommonMethods.copyValuesFromProfileToDFF(lstDFF[0], objFProfile, null);
                update modifyDFF;
                return new Pagereference('/'+modifyDFF.Id);
            }
        }
        return null;
    }
        
    public Pagereference cancelDFF() {
        return new Pagereference('/'+dffId);
    }
    
    private boolean validatingNewUpdate() {
        integer iCount = 0;
        for(wrapperProfileClass iterator : lstWrapper) {
            if(iterator.bFlag == true) {
                iCount++;
                profileId = iterator.objProfile.Id;
            }
        }
        if(iCount == 0) {
            CommonMethods.addError('Please select at least one record.');
            return false;
        }
        else if(iCount > 1) {
            CommonMethods.addError('Please select only one fulfillment profile');
            profileId = null;
            return false;
        }
        return true;
    }
    
    public class wrapperProfileClass {
        public boolean bFlag {get;set;}
        public Fulfillment_Profile__c objProfile {get;set;}
        public wrapperProfileClass(boolean bFlag, Fulfillment_Profile__c objProfile) {
            this.bFlag = bFlag;
            this.objProfile = objProfile;
        }
    }
    
    @IsTest(seeAlldata=true)
    static void UpdateDFFControllertest() {
        
        Profile p = [select id from profile where name='Sales Rep' limit 1];
        //User u= [Select id, phone, Email from user where profileid = :p.Id and isActive = true and phone != null and Email !=null limit 1];
        //UserRole role=[SELECT Id,Name FROM UserRole where name='Account Manager' limit 1];
        //User u = [SELECT Id,Name FROM User WHERE UserRoleId = :role.id limit 1];

        //system.runAs(u) 
        //{
            Canvass__c c= new Canvass__c(Name='Test Canvas');
            insert c;
            Account a = new Account(Name='Test Account', TalusAccountId__c='222222', TalusPhoneId__c='2222', Primary_Canvass__c=c.id, Phone='(425)533-3099');
            insert a;
            Test.startTest();
            Apexpages.Standardcontroller contr = new Apexpages.Standardcontroller(new Digital_Product_Requirement__c());
            
            RecordType r=[select id,name from RecordType where recordtype.DeveloperName ='iYP_Gold' limit 1 ];
            Digital_Product_Requirement__c DFF = new Digital_Product_Requirement__c(Account__c = a.Id,URN_number__c=23,RecordTypeId=r.id);
            insert DFF;
            Apexpages.currentPage().getParameters().put('id',DFF.Id);            
            UpdateDFFController dffCntl = new UpdateDFFController(contr);
            dffCntl.onLoad();
            dffCntl = new UpdateDFFController(contr);
            dffCntl.onLoad();
            Fulfillment_Profile__c FP = new Fulfillment_Profile__c(Account__c = a.Id);
            insert FP;
            Fulfillment_Profile__c FP1 = new Fulfillment_Profile__c(Account__c = a.Id);
            insert FP1;
            dffCntl.onLoad();
            dffCntl.updateDFFNew();
            System.assertEquals(null, dffCntl.updateDFFNew());
            for(wrapperProfileClass iterator : dffCntl.lstWrapper) {
                iterator.bFlag = true;
            }
            dffCntl.updateDFFNew();
            System.assertEquals(null, dffCntl.updateDFFNew());
            integer trueCount = 0;
            for(wrapperProfileClass iterator : dffCntl.lstWrapper) {
                if(iterator.bFlag == true) {
                    iterator.bFlag = false;
                }
                if(trueCount == 0) {
                    iterator.bFlag = true;
                    trueCount++;
                }
            }
            dffCntl.updateDFFNew();
            dffCntl.cancelDFF();
            Test.stopTest();
        //}
    }
}