@isTest(seealldata=true)
public class TelcoLOBReport_v1Test
{
    public static testMethod void testLOBReportv1v1() 
    {
      
      //Test Utility
      Canvass__c c=TestMethodsUtility.generateCanvass();
      c.Billing_Entity__c='CENTURY';
      insert c;
      list<Account> lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        lstAccount.add(TestMethodsUtility.generateAccount('customer'));
        lstAccount.add(TestMethodsUtility.generateAccount('publication'));
        insert lstAccount;  
        Account newAccount = new Account();
        Account newPubAccount = new Account();
        Account newTelcoAccount = new Account();
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        for(Account iterator : lstAccount) {
        if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
        newAccount = iterator;
        }
        else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
        newPubAccount = iterator;
        }
        else {
        newTelcoAccount = iterator;
        }
        }
        system.assertNotEquals(newAccount.ID, null);
        system.assertNotEquals(newPubAccount.ID, null);
        system.assertNotEquals(newAccount.Primary_Canvass__c, null);
        system.assertNotEquals(newTelcoAccount.ID, null);
        Telco__c objTelco = TestMethodsUtility.createTelco(newTelcoAccount.id);
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
             List<String> telcosList=new List<String>();
             
              for( Telco__c Telcolst:[Select Id, Name from Telco__c where Account__c =:newTelcoAccount.Id])
                {
                   if(Telcolst.Id!=null)
                   telcosList.add(String.valueof(Telcolst.get('Id')));
                  
                }
            
        system.assertNotEquals(newTelcoAccount.ID, null);
      
        Division__c objDiv = TestMethodsUtility.createDivision();
        Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Provider__c = objTelco.Id;
        objDir.Canvass__c = newAccount.Primary_Canvass__c;        
        objDir.Publication_Company__c = newPubAccount.Id;
        objDir.Division__c = objDiv.Id;
        insert objDir;
      
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
        insert objDirEd;
      
        Digital_Telco_Scheduler__c  dts=new Digital_Telco_Scheduler__c (); 
        dts.Billing_Entity__c='CENTURY';
        dts.Invoice_From_Date__c=date.parse('01/01/2013');
        dts.Bill_Prep_Date__c=date.parse('04/04/2013');
        dts.Invoice_To_Date__c=date.parse('12/12/2013');
        dts.Sent_Telco_File__c=false;
        dts.Telco_Bill_Date__c=date.parse('04/4/2013');
        dts.Telco_Receives_EFile__c=false;
        dts.Telco__c=objTelco.Id;
        dts.XML_Output_Total_Amount__c=400.00;
        insert dts;
        
        Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        
        Product2 newProduct = TestMethodsUtility.generateproduct();
        newProduct.Family = 'Print';
        insert newProduct;
        
        Product2 objProd = new Product2();
        objProd.Name = 'Test';
        objProd.Product_Type__c = 'Digital';
        objProd.ProductCode = 'WLCSH';
        objProd.Print_Product_Type__c='Display';
        insert  objProd;
        
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        insert newOpportunity;
        
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
        
        Order_Line_Items__c objOrderLineItem = new Order_Line_Items__c(Billing_Partner__c='Hawain Telecom',
        Account__c=newAccount.Id, Billing_Contact__c=newContact.id, Opportunity__c=newOpportunity.id, 
        Order_Group__c=newOrderSet.id,Order__c=newOrder.id,Product2__c=objProd.Id,is_p4p__c=false,media_type__c='Print',
        Directory_Edition__c = objDirEd.Id,Directory__c=Objdir.Id,canvass__c=c.id,UnitPrice__c=200,Payment_Duration__c=12,
        Payment_Method__c='Telco Billing',Package_ID__c='pkgid_12',Payments_Remaining__c=11,Successful_Payments__c=1);
        insert objOrderLineItem;
        
         c2g__codaInvoice__c invoice =TestMethodsUtility.generateSalesInvoice(newAccount,newOpportunity);
        invoice.Customer_Name__c=newAccount.id;
        invoice.c2g__InvoiceStatus__c='In Progress';
        insert invoice;
        c2g__codaAccountingCurrency__c currencyrec=[SELECT Id from c2g__codaAccountingCurrency__c WHERE name='USD' limit 1];
        
        c2g__codaCreditNote__c SCN=TestMethodsUtility.generateSalesCreditNote(invoice, newAccount);
        SCN.c2g__InvoiceDate__c=date.parse('01/06/2013');
        insert SCN;
        
        c2g__codaDimension1__c  dimension1=TestMethodsUtility.createDimension1(objDir);
        c2g__codaDimension2__c  dimension2=TestMethodsUtility.createDimension2(objProd);
        c2g__codaDimension4__c  dimension4=TestMethodsUtility.createDimension4();
        c2g__codaDimension3__c  dimension3=TestMethodsUtility.createDimension3(objOrderLineItem);
        
        c2g__codaInvoiceLineItem__c invoiceLi=TestMethodsUtility.createSalesInvoiceLineItem(objOrderLineItem ,invoice,objProd,dimension1,dimension2,dimension3,dimension4 );
       
        c2g__codaCreditNoteLineItem__c scliDigital=TestMethodsUtility.generateSalesCreditNoteLineItem(SCN,objProd);
        scliDigital.Sales_Invoice_Line_Item__c=invoiceLi.id;
        scliDigital.c2g__Dimension1__c=dimension1.id;
        scliDigital.c2g__Dimension2__c=dimension2.id;
        scliDigital.c2g__Dimension3__c= dimension3.id;
        scliDigital.c2g__Dimension4__c=dimension4.id;
        insert scliDigital;
       
        // Account objAcc = TestMethodsUtility.generateAccount();
       // insert objAcc;
        /*
        Contact newContact = TestMethodsUtility.createContact(newTelcoAccount.Id);
        insert newContact;
     
        
        Product2 objProd = new Product2();
        objProd.Name = 'Test';
        objProd.Product_Type__c = 'Print';
        
        insert objProd;
        
        Opportunity  objopp= TestMethodsUtility.createOpportunity(newTelcoAccount,newContact);
         insert objopp;
        c2g__codaInvoice__c objc2ginvoice =TestMethodsUtility.generateSalesInvoice(newTelcoAccount,objopp);
        insert objc2ginvoice;
      c2g__codaInvoiceLineItem__c objcodainvoicelitem =TestMethodsUtility.generateSalesInvoiceLineItem(objc2ginvoice,objProd);
      insert objcodainvoicelitem;
      */
      //positive test
      test.StartTest();
      PageReference pageRef = Page.TelcoLOBReport_v1;
      Test.setCurrentPage(pageRef);
      TelcoLOBReport_v1 TLOBR_I=new TelcoLOBReport_v1 ();
      TLOBR_I.setReporttype('Digital');
      TLOBR_I.setentity('Test');
      TLOBR_I.getTelcoDate();
      
      TelcoLOBReport_v1 TLOBR_P=new TelcoLOBReport_v1 ();
      
     
      TLOBR_P.getdirectory();
      TLOBR_P.setdirectories(String.valueof(objDir.id));
      TLOBR_P.getReporttypeOptions();
      TLOBR_P.setReporttype('Digital');
      TLOBR_P.getReporttype();
      TLOBR_P.getbillingEntity();
      TLOBR_P.getentity();
      TLOBR_P.setentity('CENTURY');
      TLOBR_P.getTelcoDate();
      TLOBR_P.startdatename='01/01/2013';
      TLOBR_P.enddatename='12/31/2013';
      TLOBR_P.chk();
      TLOBR_P.populateEdition();
      TLOBR_P.getdirectories();
      TLOBR_P.setdirectoriesEdition(String.valueof(objDirEd.id));
      TLOBR_P.getdirectoriesEdition();
      TLOBR_P.getbillingpartners();
      TLOBR_P.setpartners(telcosList);
      TLOBR_P.getpartners();
      TLOBR_P.IsIncludeNational2=false;
      TLOBR_P.IsIncludeCredit2=true;
      TLOBR_P.generateReport();
      Apexpages.currentpage().getparameters().put('en','CENTURY');
      Apexpages.currentpage().getparameters().put('sdate','01/01/2013');
      Apexpages.currentpage().getparameters().put('edate','12/31/2013');
      TLOBR_P.generateDigitalfromURL();
      
      //negative test case for Digital
      TelcoLOBReport_v1 TLOBR_N0=new TelcoLOBReport_v1 ();
      TLOBR_N0.getdirectory();
      TLOBR_N0.setdirectories(String.valueof(objDir.id));
      TLOBR_N0.getReporttypeOptions();
      TLOBR_N0.setReporttype('Digital');
      TLOBR_N0.getReporttype();
      TLOBR_N0.startdatename='01/01/2013';
      TLOBR_N0.enddatename='12/31/2013';
      TLOBR_N0.chk();
      TLOBR_N0.populateEdition();
      TLOBR_N0.getdirectories();
      TLOBR_N0.getdirectoriesEdition();
      TLOBR_N0.getbillingpartners();
      TLOBR_N0.setpartners(telcosList);
      TLOBR_N0.getpartners();
      TLOBR_N0.IsIncludeNational2=false;
      TLOBR_N0.IsIncludeCredit2=false;
      TLOBR_N0.selectedReporttype='Print';
      TLOBR_N0.partners=new string[]{};
      TLOBR_N0.generateReport();
      //negative test case for Digital
      TelcoLOBReport_v1 TLOBR_N1=new TelcoLOBReport_v1 ();
      
      TLOBR_N1.getdirectory();
      TLOBR_N1.setdirectories(String.valueof(objDir.id));
      TLOBR_N1.getReporttypeOptions();
      TLOBR_N1.setReporttype('NONE');
      TLOBR_N1.getReporttype();
      TLOBR_N1.startdatename='01/01/2013';
      TLOBR_N1.enddatename='12/31/2013';
      TLOBR_N1.chk();
      TLOBR_N1.populateEdition();
      TLOBR_N1.getdirectories();
      TLOBR_N1.getdirectoriesEdition();
      TLOBR_N1.getbillingpartners();
      TLOBR_N1.setpartners(telcosList);
      TLOBR_N1.getpartners();
      TLOBR_N1.IsIncludeNational2=false;
      TLOBR_N1.IsIncludeCredit2=false;
      TLOBR_N1.generateReport();
     
      //negative test case for Print
      TelcoLOBReport_v1 TLOBR_N=new TelcoLOBReport_v1 ();
      TLOBR_N.getdirectory();
      TLOBR_N.setdirectories(String.valueof(objDir.id));
      TLOBR_N.getReporttypeOptions();
      TLOBR_N.setReporttype('Print');
      TLOBR_N.getReporttype();
      TLOBR_N.startdatename='01/01/2013';
      TLOBR_N.enddatename='12/31/2013';
      TLOBR_N.chk();
      TLOBR_N.populateEdition();
      TLOBR_N.getdirectories();
      TLOBR_N.getdirectoriesEdition();
      TLOBR_N.getbillingpartners();
      TLOBR_N.setpartners(telcosList);
      TLOBR_N.getpartners();
      TLOBR_N.IsIncludeNational2=true;
      TLOBR_N.IsIncludeCredit2=false;
      TLOBR_N.selecteddirectories=objDir.id;
      Directory_Mapping__c dirMap=new Directory_Mapping__c ();
      dirMap.Telco__c=objTelco.Id;
      dirMap.Name='DIRMAPTEST';
      dirMap.Directory__c=objDir.id;
      insert dirMap;
      TLOBR_N.getbillingpartners();
      TLOBR_N.setpartners(telcosList);
      TLOBR_N.getpartners();
      TLOBR_N.selectedReporttype='Print';
      TLOBR_N.generateReport();
      test.StopTest();
    }
    
    public static testMethod void testPrintLOBReportv1() 
    {
      PageReference pageRef = Page.PrintLOBReport_v1;
      Test.setCurrentPage(pageRef);
      
      //Test Utility
      //Telco__c objTelco=[Select id from Telco__c where Name like '%Windstream Communications Inc%' Limit 1];
      list<Account> lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        lstAccount.add(TestMethodsUtility.generateAccount('customer'));
        lstAccount.add(TestMethodsUtility.generateAccount('publication'));
        insert lstAccount;  
        Account newAccount = new Account();
        Account newPubAccount = new Account();
        Account newTelcoAccount = new Account();
        for(Account iterator : lstAccount) {
        if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
        newAccount = iterator;
        }
        else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
        newPubAccount = iterator;
        }
        else {
        newTelcoAccount = iterator;
        }
        }
        system.assertNotEquals(newAccount.ID, null);
        system.assertNotEquals(newPubAccount.ID, null);
        system.assertNotEquals(newAccount.Primary_Canvass__c, null);
        system.assertNotEquals(newTelcoAccount.ID, null);
        Telco__c objTelco = TestMethodsUtility.createTelco(newTelcoAccount.Id);
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
        
      
             List<String> telcosList=new List<String>();
             
              for( Telco__c Telcolst:[Select Id, Name from Telco__c where Account__c =:newTelcoAccount.Id])
                {
                   if(Telcolst.Id!=null)
                   telcosList.add(String.valueof(Telcolst.get('Id')));
                  
                }
        system.assertNotEquals(newTelcoAccount.ID, null);
      
        Division__c objDiv = TestMethodsUtility.createDivision();
        
        Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Provider__c = objTelco.Id;
        objDir.Canvass__c = newAccount.Primary_Canvass__c;        
        objDir.Publication_Company__c = newPubAccount.Id;
        objDir.Division__c = objDiv.Id;
        insert objDir;
      
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
        insert objDirEd;
      
      //positive test
      
      Apexpages.currentpage().getparameters().put('telcoProduct',String.valueof(objTelco.id));
      Apexpages.currentpage().getparameters().put('directoryId',String.valueof(objDir.id));
      Apexpages.currentpage().getparameters().put('Edition',String.valueof(objDirEd.id));
      Apexpages.currentpage().getparameters().put('IsNational','true');
      PrintLOBReport_v1 TLOBR_P=new PrintLOBReport_v1();
     // PrintLOBReport_v1.PrintLOBReport_v1();
       TLOBR_P.doGenerateCSV_email(telcosList,String.valueof(objDir.id),String.valueof(objDirEd.id));
            TLOBR_P.doGenerateCSV_email(telcosList,'','');
      TLOBR_P.cancel();
     
    }
          
}