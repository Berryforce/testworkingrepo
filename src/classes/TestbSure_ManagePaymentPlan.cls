/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(seeAllData=true)
private class TestbSure_ManagePaymentPlan {

    static testMethod void myUnitTest() {
        // TO DO: implement unit test
        
        //sObject objtest = Schema.getGlobalDescribe().get('Account').newSObject();
        //Account objtest=new sObject();
      /*  Canvass__c c= new Canvass__c(Name='Test Canvas');
        insert c;
        test.startTest();
        Account objtest=new Account();
       // objtest.put('AR_Balance__c', 100); 
        objtest.put('Name', 'testAccount');
        objtest.put('BillingStreet', '3785 s las vegs av');
        objtest.put('BillingCity', 'los vegos');
        objtest.BillingState='NV';
        objtest.Do_Not_Validate_Address__c=true;
        objtest.put('BillingPostalCode', '90210');
        objtest.put('BillingCountry', 'US');
        objtest.Primary_Canvass__c=c.id;
        
        insert objtest;
        List<AsyncApexJob> aj = [Select status from AsyncApexJob where status in ('Preparing' ,'Processing','Completed','Queued')];
        for (AsyncApexJob a : aj)
        {
            System.abortJob(a.Id);
        }
        test.stopTest();
        
        Vertex_Berry__BSureC_Customer_Basic_Info__c objCust=new Vertex_Berry__BSureC_Customer_Basic_Info__c();
        objCust.Vertex_Berry__ExternalId__c=String.valueof(objtest.id).substring(0,15);
        objCust.Vertex_Berry__Customer_Name__c='testAccount';
        insert objCust;
        
        
        Contact cnt = new Contact(AccountId=objtest.id, FirstName='Test FName', Primary_Contact__c=false,
                          MailingCity='Test Mailing City', MailingState='CA',
                          MailingStreet='Test Mailing Street', MailingPostalCode='94538',
                          LastName='Test LName', Email='test.test@test.com', Phone='(422)552-4422');
        insert cnt;
        
        pymt__PaymentX__c objpyment=new pymt__PaymentX__c();
        objpyment.pymt__Account__c=objtest.id;
        objpyment.pymt__Amount__c=500;
        insert objpyment;
        
        test.startTest();
        Vertex_Berry__BSure_Payment_Plan__c objPlan=new Vertex_Berry__BSure_Payment_Plan__c();
        objPlan.Vertex_Berry__External_ID__c=String.valueof(objtest.id).subString(0,15);
        objPlan.Vertex_Berry__Total_Paid_Amount__c=100;
        objPlan.Vertex_Berry__Monthly_EMI__c=50;
        ObjPlan.Vertex_Berry__Customer_Information__c=objCust.id;
        objPlan.Vertex_Berry__Payment_Plan_Status__c='Active';
        objPlan.Vertex_Berry__Amount_Due__c=100;
        objPlan.Vertex_Berry__Amount_Paid__c=100;
        objPlan.Vertex_Berry__Financial_Fees__c=10;
        objPlan.Vertex_Berry__Repayment_Over__c='3';
        objPlan.Vertex_Berry__Payment_Date__c=system.today()+1;
        insert objPlan;
        test.stopTest();
        List<AsyncApexJob> ajc = [Select status from AsyncApexJob where status in ('Preparing' ,'Processing','Completed','Queued')];
        for (AsyncApexJob ac : ajc)
        {
            System.abortJob(ac.Id);
        }
        */
        test.startTest();
        try{
            database.executebatch(new BSure_ManagePaymentPlan());
        }catch(Exception e){}
        
        test.stopTest();
        
    }
    
    static testMethod void myUnitTest2() {
        Canvass__c c= new Canvass__c(Name='Test Canvas');
        insert c;
        
        Account objtest=new Account();
       // objtest.put('AR_Balance__c', 100); 
        objtest.put('Name', 'testAccount');
        objtest.put('BillingStreet', '3785 s las vegs av');
        objtest.put('BillingCity', 'los vegos');
        objtest.BillingState='NV';
        objtest.Do_Not_Validate_Address__c=true;
        objtest.put('BillingPostalCode', '90210');
        objtest.put('BillingCountry', 'US');
        objtest.Primary_Canvass__c=c.id;
        objtest.Phone = '(989)448-4989';        
        insert objtest;
        
        pymt__PaymentX__c objpyment=new pymt__PaymentX__c();
        objpyment.pymt__Account__c=objtest.id;
        objpyment.pymt__Amount__c=500;
        insert objpyment;        
        
        Opportunity objopp=new Opportunity();
        objopp.AccountId=objtest.id;
        objopp.Close_Date__c=system.today()+1;
        objopp.CloseDate=system.today()+1;
        objopp.Name='testOpp';
        objopp.StageName='Attempting to contact';
        insert objopp;
        
        Vertex_Berry__BSureC_Customer_Basic_Info__c objCust=new Vertex_Berry__BSureC_Customer_Basic_Info__c();
        objCust.Vertex_Berry__ExternalId__c=String.valueof(objtest.id).substring(0,15);
        objCust.Vertex_Berry__Customer_Name__c='testAccount';
        insert objCust; 
        
        Profile pfl = [select id from profile where name='Standard User'];
        User testUser = new User(alias = 'u1', email='u1@testorg.com',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = pfl.Id,  country='United States', CommunityNickname = 'u1',
            timezonesidkey='America/Los_Angeles', username='u1@testorg.com');
        
        Vertex_Berry__PIA_Credit_Review__c objRev=new Vertex_Berry__PIA_Credit_Review__c();
        objRev.Vertex_Berry__Opportunity_ID__c=objopp.id;
        objRev.Vertex_Berry__Customer_Information__c=objCust.id;
        objRev.Vertex_Berry__Analyst__c=testUser.id;
        objRev.Vertex_Berry__Current_PIA__c=12;
        objRev.Vertex_Berry__Former_PIA__c=10;
        objRev.Vertex_Berry__Status__c='Completed';
        insert objRev;
    }
    
    static testMethod void myUnitTest3() {
        Profile pfl2 = [select id from profile where name='Standard User'];
        User testUser2 = new User(alias = 'u1', email='u1@testorg.com',
            emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
            localesidkey='en_US', profileid = pfl2.Id,  country='United States', CommunityNickname = 'u1',
            timezonesidkey='America/Los_Angeles', username='u1@testorg.com');
        Canvass__c c2= new Canvass__c(Name='Test Canvas');
        insert c2;
        
        Account objtest2=new Account();
       // objtest.put('AR_Balance__c', 100); 
        objtest2.put('Name', 'testAccount');
        objtest2.put('BillingStreet', '3785 s las vegs av');
        objtest2.put('BillingCity', 'los vegos');
        objtest2.BillingState='NV';
        objtest2.Do_Not_Validate_Address__c=true;
        objtest2.put('BillingPostalCode', '90210');
        objtest2.put('BillingCountry', 'US');
        objtest2.Primary_Canvass__c=c2.id;
        objtest2.phone = '(989)448-4989';        
        insert objtest2;
        
        pymt__PaymentX__c objpyment=new pymt__PaymentX__c();
        objpyment.pymt__Account__c=objtest2.id;
        objpyment.pymt__Amount__c=500;
        insert objpyment;         
        
        Opportunity objopp2=new Opportunity();
        objopp2.AccountId=objtest2.id;
        objopp2.Close_Date__c=system.today()+1;
        objopp2.CloseDate=system.today()+1;
        objopp2.StageName='Attempting to contact';
        objopp2.Name='testOpp2';
        insert objopp2;
        
        
        BsureCreditApplication__c objCA2=new BsureCreditApplication__c();
        objCA2.Status__c='Completed';
        objCA2.Opportunity__c=String.valueof(objopp2.id).substring(0,15);
        objCA2.PIA__c=10;
        insert objCA2;
        
        objCA2.PIA__c=12;
        update objCA2;
        
        
        Vertex_Berry__BsureCreditApplication__c objCA3=new Vertex_Berry__BsureCreditApplication__c();
        objCA3.Vertex_Berry__ExternalId__c=String.valueof(objopp2.id).substring(0,15);
        objCA3.Vertex_Berry__Buisness_Name__c='Completed';
        objCA3.Vertex_Berry__Number_of_Employees__c=10;
        objCA3.Vertex_Berry__Billing_City__c='test';
        objCA3.Vertex_Berry__Billing_State__c='NV';
        objCA3.Vertex_Berry__Billing_Street__c='abcd';
        objCA3.Vertex_Berry__Billing1_Zip__c='123';
        objCA3.Vertex_Berry__Customer_Account_Number__c='2323232';
        
        objCA3.Vertex_Berry__Contact_Number__c ='1111';
        
        objCA3.Vertex_Berry__Legal_Business_Name__c ='111';
        
        objCA3.Vertex_Berry__Listed_Business_Name__c ='test';
        
        objCA3.Vertex_Berry__Telephone_Numbers__c ='11111';
        
        objCA3.Vertex_Berry__Annual_Estimated_Income__c ='1111';
        
        objCA3.Vertex_Berry__Year_Business_Established__c =system.today()-10;
        
        objCA3.Vertex_Berry__Physical_Address__c ='tet';
        
        objCA3.Vertex_Berry__Physical_City__c ='US';
        
        objCA3.Vertex_Berry__Physical1_State__c ='us';
        
        objCA3.Vertex_Berry__Physical_Street__c  ='us';
        
        objCA3.Vertex_Berry__Physical1_Zip__c ='111';
        
        objCA3.Vertex_Berry__Owner_Officer_information__c='test'; 
        
        objCA3.Vertex_Berry__SS1__c ='12345678';
        
        objCA3.Vertex_Berry__Title1__c  ='test';
        
        objCA3.Vertex_Berry__Telephone_Number1__c='1234567890'; 
        
        objCA3.Vertex_Berry__Residence_Address1__c ='test';
        
        objCA3.Vertex_Berry__Sales_Representative_Name__c=testUser2.id; 
        
        objCA3.Vertex_Berry__Owner_Officer_information2__c ='test';
        
        objCA3.Vertex_Berry__Title2__c ='title 2';
        
        objCA3.Vertex_Berry__SS2__c ='12121211';
        
        objCA3.Vertex_Berry__Residence_Address2__c='test'; 
        
        objCA3.Vertex_Berry__Telephone_Number2__c ='12331323';
        
        objCA3.Vertex_Berry__Salesperson__c =testUser2.id;
        
        objCA3.Vertex_Berry__Sales_Manager__c =testUser2.id;
             
        objCA3.Vertex_Berry__BCS__c ='500';
        
        objCA3.Vertex_Berry__PCS__c ='700';
        
        objCA3.Vertex_Berry__EquifaxId__c='132313313'; 
        
        objCA3.Vertex_Berry__SIC_Code__c ='123123';
        
        objCA3.Vertex_Berry__NumberOfBankruptcies__c ='1';
        
        objCA3.Vertex_Berry__Number_Of_Liens__c ='2';
        
        objCA3.Vertex_Berry__Available_Credit__c ='3';
        
        objCA3.Vertex_Berry__Payment_Index_Industry__c='11'; 
        
        objCA3.Vertex_Berry__Payment_Index_Business__c ='11';
        
        objCA3.Vertex_Berry__Other_Business_Names__c ='21';
        
        objCA3.Vertex_Berry__Business_Structure__c ='121';
        
        objCA3.Vertex_Berry__Date_of_Incorporation__c =system.today()-5;
        
        objCA3.Vertex_Berry__State_of_Incorporation__c ='test';
        
        objCA3.Vertex_Berry__Federal_Tax_ID_Number__c ='212121';
        
        objCA3.Vertex_Berry__Cell_Phone__c='121'; 
        
        objCA3.Vertex_Berry__Status__c='Completed';
        objCA3.Vertex_Berry__PIA__c =50;
        
        insert objCA3;
        
        objCA3.Vertex_Berry__Number_of_Employees__c=12;
        update objCA3;
        
    }
}