public with sharing class natManInputEdit {

    public String OrderSetId;
    public National_Staging_Line_Item__c NatLinObj{get; set;}
    public National_Staging_Order_Set__c NatHdrObj{get; set;}
    public List <National_Staging_Line_Item__c> NatLinLstObj{get; set;}
    public String HeaderId{get;Set;}
    public Integer rowNum{get;set;}
    public String UDAC{get;Set;} 
    private map<String, Account> mapClientAccount;
    private map<String, Directory_Edition__c> mapLSADirEdition;
    Id clientRTID;  
    public RecordType ManualRT; 
    public natManInputEdit(ApexPages.StandardController controller) 
    {
       OrderSetId=ApexPages.CurrentPage().getParameters().get('id');
       ManualRT= CommonMethods.getRecordTypeDetailsByDeveloperName('National_Staging_Order_Set__c','Manual_NS_RT');
       if(OrderSetId!=null)
       {
       NatHdrObj=[SELECT Auto_Number__c,ChildReadyForProcess__c,Child_Total__c,Client_Name_Text__c,Client_Name__c,Client_Number__c,CMRClientCombo__c,CMR_Name__c,CMR_Notice__c,
       CMR_Number__c,CORE_Migration_ID__c,CreatedById,CreatedDate,Date__c,Directory_Edition_Number__c,Directory_Edition__c,Directory_Number__c,Directory_Version__c,Directory__c,
       From_Number__c,From_Type__c,Header_Error_Description__c,Id,Is_Converted__c,Is_Ready__c,Name,NAT_Client_Id__c,NAT__c,Non_Elite_CMR__c,OwnerId,
       Publication_Code__c,Publication_Company__c,Publication_Date__c,RAC_Date__c,Ready_for_Process__c,RecordTypeId,Reference_Date__c,SAC_Date__c,State__c,To_Number__c,To_Type__c,Transaction_ID__c,
       Transaction_Unique_ID__c,Transaction_Version_ID__c,Transaction_Version__c,TRANS_Code__c,(SELECT Action__c,Advertising_Data__c,Auto_Number__c,BAS__c,CORE_Migration_ID__c,DAT__c,Delete__c,
       Directory_Heading__c,Discount__c,Full_Rate_f__c,Id,Line_Error_Description__c,Line_Number__c,Listing__c,
       Name,National_Staging_Header__c,Parent_ID__c,Product2__c,Ready_for_Processing__c,Row_Type__c,Sales_Rate__c,Scoped_Listing__c,SEQ__c,SPINS__c,
       Transaction_Id__c,Transaction_Line_Id__c,Transaction_Unique_ID__c,Transaction_Version_ID__c,Type__c,UDAC__c FROM National_Staging_Line_Items__r) 
       FROM National_Staging_Order_Set__c where id=:OrderSetId];
       if(NatHdrObj.National_Staging_Line_Items__r.size()>0)
       NatLinLstObj=NatHdrObj.National_Staging_Line_Items__r;
       }
       
       if(NatHdrObj.Date__c == null) {
        NatHdrObj.Date__c = system.today();
       }
    }
    
    public pageReference Cancel() {
        pageReference pr;
        pr = new pageReference('/'+OrderSetId);
        pr.setRedirect(true);
        return pr;
    }

    public void addLineItems()
    {
        National_Staging_Line_Item__c NatLine = new National_Staging_Line_Item__c();
        if(HeaderId != ''){
            NatLine.National_Staging_Header__c= HeaderId ;   
        }
        NatLinLstObj.add(NatLine);
    }



    public pageReference save() 
    {
        pageReference pr;
        boolean bFlag = true;
        if(NatHdrObj.CMR_Name__c == null) {
            CommonMethods.addError('Please enter valid CMR name');
            bFlag = false;
        }

        system.debug(NatHdrObj.Client_Name__c);
        if(NatHdrObj.Client_Name__c == null && String.isBlank(NatHdrObj.Client_Name_Text__c)) {
            CommonMethods.addError('Please enter client name');
            bFlag = false;
        }

        if(bFlag) {
            boolean bFlagChild = true;
            set<String> setUDAC = new set<String>();
            for(National_Staging_Line_Item__c Natli : NatLinLstObj){
                if(String.isNotBlank(Natli.UDAC__c)) {
                    setUDAC.add(Natli.UDAC__c);
                }
            }
            
            list<Product2> lstUDAC = Product2SOQLMethods.getProductByUDAC(setUDAC);
            map<String, Id> mapUDAC = new map<String, Id>();
            for(Product2 iterator : lstUDAC) {
                mapUDAC.put(iterator.ProductCode, iterator.Id);
            }
            
            for(National_Staging_Line_Item__c Natli : NatLinLstObj)
            {                
                if(String.isNotBlank(Natli.Line_Number__c)) {
                    if(Natli.Line_Number__c.length() < 5) {
                        Natli.Line_Number__c.addError('Line # should be 5 digit number.');
                        bFlagChild = false;
                    }
                }               
                if(String.isNotBlank(Natli.UDAC__c)) {
                    if(mapUDAC.get(Natli.UDAC__c) != null) {
                        Natli.Product2__c = mapUDAC.get(Natli.UDAC__c);
                    }
                    else {
                        Natli.UDAC__c.addError('UDAC is not valid.');
                        bFlagChild = false;
                    }
                }
            }
            
            if(bFlagChild) {
                if(NatHdrObj.Client_Name__c == null && String.isNotBlank(NatHdrObj.Client_Name_Text__c) && String.isNotBlank(NatHdrObj.Client_Number__c) ) {
                    createNewAccount(NatHdrObj);
                }
                NatHdrObj.RecordTypeId=ManualRT.id;
                update NatHdrObj;
                HeaderId = NatHdrObj.id;
                List <National_Staging_Line_Item__c> NSLIL = new List <National_Staging_Line_Item__c>();
                for(National_Staging_Line_Item__c Natli : NatLinLstObj) {
                    Natli.National_Staging_Header__c = NatHdrObj.id;
                    Natli.Transaction_Id__c = NatHdrObj.Transaction_ID__c;
                    NSLIL.add(Natli);
                }
                upsert NSLIL;
                pr = new pageReference('/'+NatHdrObj.id );
                pr.setRedirect(true);
                return pr;
            }
        }
        return null;
    }
    
    public void createNewAccount(National_Staging_Order_Set__c  NatHdrObj)
    {
       Account accNew = new Account ();
       accNew.Name = NatHdrObj.Client_Name_Text__c+' '+NatHdrObj.Client_Number__c;
       accNew.Client_Number__c = NatHdrObj.Client_Number__c;
       accNew.CMR_Number__c = NatHdrObj.CMR_Number__c;
       // Modified by : Ankit Task: BFTHREE-1295 Added Record type in SOQL for National Account
       // Commented ParentId as CMR for NewAccount would be null in order to avoid duplicate records and relation 
       // accNew.ParentId = NatHdrObj.CMR_Name__c;
       accNew.RecordTypeId= CommonMethods.getRedordTypeIdByName('National Account', 'Account');
       insert accNew;
       NatHdrObj.Client_Name__c = accNew.Id;
    }
    
    public List<SelectOption> getNatList() 
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('A','A - National'));
        options.add(new SelectOption('B','B - Local'));
        options.add(new SelectOption('E','E - Emerging'));
        options.add(new SelectOption('R','R - Regional'));
        return options;
    }

    public List<SelectOption> getToFromList() 
    {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('C','C - CMR'));
        options.add(new SelectOption('P','P - Publisher'));
        options.add(new SelectOption('Y','Y - YPA Elite'));
        return options;
    }

    public List<SelectOption> getTransList() {
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('',' --None--'));
        options.add(new SelectOption('A','A - Advice'));
        options.add(new SelectOption('C','C - Change Order'));
        options.add(new SelectOption('D','D - Delete Entire Order'));
        options.add(new SelectOption('F','F - Forward'));
        options.add(new SelectOption('H','H - Header Change'));
        options.add(new SelectOption('I','I - New Insert Order'));
        options.add(new SelectOption('K','K - Order Compare'));
        options.add(new SelectOption('L','L - Renumber Order'));
        options.add(new SelectOption('M','M - Memo'));
        options.add(new SelectOption('N','N - CMR Transfer (no-standing order)'));
        options.add(new SelectOption('Q','Q - Query'));
        options.add(new SelectOption('R','R - Resend Graphic Request'));
        options.add(new SelectOption('T','T - CMR Transfer (with standing order)'));
        options.add(new SelectOption('V','V - Verification of Order'));
        options.add(new SelectOption('X','X - View YPA Elite Standing Order'));
        return options;
    }

    public void fetchCMRbyNumber() {
        system.debug('CMR Number : ' + NatHdrObj.CMR_Number__c);
        system.debug(NatHdrObj.CMR_Number__c);
        mapClientAccount = new map<String, Account>();
        NatHdrObj.CMR_Name__c = null;
        ID cmrAccRTID = CommonMethods.getRedordTypeIdByName(CommonMessages.accountCMRRT, CommonMessages.accountObjectName);
        if(String.isNotBlank(NatHdrObj.CMR_Number__c)) {
            list<Account> lstCMRAcc = AccountSOQLMethods.getCMRAccountByNumber(NatHdrObj.CMR_Number__c, cmrAccRTID, clientRTID);
            if(lstCMRAcc.size() > 0) {
                for(Account iterator : lstCMRAcc) {
                    NatHdrObj.CMR_Name__c = iterator.Id;
                    for(Account childIterator : iterator.ChildAccounts) {
                        if(String.isNotBlank(childIterator.Client_Number__c)) {
                            mapClientAccount.put(childIterator.Client_Number__c, childIterator);
                        }
                    }
                }
            }           
            if(NatHdrObj.CMR_Name__c == null) {
                //CommonMethods.addError('Entered CMR Number is not valid.');
                NatHdrObj.CMR_Number__c.addError('Entered CMR Number is not valid.');
            }
        }
    }

   /// fix by chris roberts to find the client id  the map does not contain any child records because that is not how they are stored anymore
    public void fetchClientbyNumber() {
        // test numbers 6083 for the CMR and 9987 for the Client # should yeld the account 001Z000000gHaDC
        // select Id,  Name, Type, RecordTypeId, ParentId, CMR_Number__c,Client_Number__c from Account WHERE Client_Number__c = '9987' AND CMR_Number__c ='6083' 
        // record type=national account
        fetchCMRbyNumber();
        if(NatHdrObj.CMR_Number__c == null) {
            NatHdrObj.CMR_Number__c.addError('CMR Number Value is required to get client id'); 
            NatHdrObj.Client_Number__c.addError('CMR Number Value is required to get client id CMR # is:'+NatHdrObj.CMR_Number__c);
        }
        system.debug(NatHdrObj.Client_Number__c);
       // mapClientAccount = new map<String, Account>();
        Account acc = new account();
        acc = AccountSOQLMethods.getClientNatAcctByNumbers(NatHdrObj.Client_Number__c, NatHdrObj.CMR_Number__c);
        if(acc.id != Null){
            mapClientAccount.put(NatHdrObj.Client_Number__c, acc);
        }
        NatHdrObj.Client_Name__c = null;
        if(mapClientAccount.size() > 0) 
        {
            if(mapClientAccount.get(NatHdrObj.Client_Number__c) != null) {
                NatHdrObj.Client_Name__c = mapClientAccount.get(NatHdrObj.Client_Number__c).Id;
                NatHdrObj.Client_Name_Text__c=mapClientAccount.get(NatHdrObj.Client_Number__c).name;
            }           
            if(NatHdrObj.Client_Name__c == null) {
                //CommonMethods.addError('Entered Client Number is not valid. If you want to create a new client account please enter the client name and proceed.');
                NatHdrObj.Client_Number__c.addError('Entered Client Number is not valid. If you want to create a new client account please enter the client name and proceed.');
            }
        } 
        else 
        {
            NatHdrObj.Client_Number__c.addError('Entered Client Number is not valid');
        }
    }

    public void fetchDirectoryByCode() {
        NatHdrObj.Directory__c = null;
        NatHdrObj.State__c = null;
        NatHdrObj.Publication_Date__c = null;
        NatHdrObj.Directory_Edition__c = null;
        if(String.isNotBlank(NatHdrObj.Directory_Number__c)) {
            mapLSADirEdition = new map<String, Directory_Edition__c>();
            list<Directory__c> lstDir = DirectorySOQLMethods.getDirectoryByCode(new set<String>{NatHdrObj.Directory_Number__c});
            if(lstDir != null && lstDir.size() > 0) {
                for(Directory__c iterator : lstDir) {
                    NatHdrObj.Directory__c = iterator.id;
                    NatHdrObj.State__c = iterator.State__c;
                    for(Directory_Edition__c iteratorDE : iterator.Directory_Editions__r) {
                        if(String.isNotBlank(iteratorDE.LSA_Directory_Version__c)) {
                            mapLSADirEdition.put(iteratorDE.LSA_Directory_Version__c, iteratorDE);
                        }
                    }
                }
            }
            if(String.isNotBlank(NatHdrObj.Directory_Version__c) && mapLSADirEdition.size() > 0) {
                if(mapLSADirEdition.containsKey(NatHdrObj.Directory_Edition_Number__c)) {
                    NatHdrObj.Publication_Date__c = mapLSADirEdition.get(NatHdrObj.Directory_Edition_Number__c).Pub_Date__c;
                    NatHdrObj.Directory_Edition__c = mapLSADirEdition.get(NatHdrObj.Directory_Edition_Number__c).Id;
                }                                
            }
        }
    }

    public void fetchDEByLSANo(){
        NatHdrObj.Publication_Date__c = null;
        NatHdrObj.Directory_Edition__c = null;
        if(String.isNotBlank(NatHdrObj.Directory_Version__c)) {
            if(mapLSADirEdition != null && mapLSADirEdition.size() > 0) {
                if(mapLSADirEdition.get(NatHdrObj.Directory_Edition_Number__c) != null) {
                    NatHdrObj.Publication_Date__c = mapLSADirEdition.get(NatHdrObj.Directory_Version__c).Pub_Date__c;
                    NatHdrObj.Directory_Edition__c = mapLSADirEdition.get(NatHdrObj.Directory_Version__c).Id;                
                }
            }
            else {
                if(NatHdrObj.Directory_Number__c != null){
                    list<Directory_Edition__c> lstDE = DirectoryEditionSOQLMethods.getDirEditionByLSACodeDirNo(NatHdrObj.Directory_Version__c, NatHdrObj.Directory_Number__c);
                    for(Directory_Edition__c iterator : lstDE) {
                        if(iterator.Book_Status__c == 'NI') {
                            NatHdrObj.Publication_Date__c = iterator.Pub_Date__c;
                            NatHdrObj.Directory_Edition__c = iterator.Id;
                            if(NatHdrObj.Directory__c == null){
                                NatHdrObj.Directory__c = iterator.Directory__c;
                                NatHdrObj.State__c = iterator.Directory__r.State__c;
                                NatHdrObj.Directory_Number__c = iterator.Directory__r.directory_code__c;
                            }                            
                        }
                    }
                }
                
            }

            /*if(NatHdrObj.Directory__c == null) {
                list<Directory_Edition__c> lstDE = DirectoryEditionSOQLMethods.getDirEditionByLSACode(new set<String>{NatHdrObj.Directory_Edition_Number__c});
                for(Directory_Edition__c iterator : lstDE) {
                    if(iterator.Book_Status__c == 'NI') {
                        NatHdrObj.Publication_Date__c = iterator.Pub_Date__c;
                        NatHdrObj.Directory_Edition__c = iterator.Id;
                        NatHdrObj.Directory__c = iterator.Directory__c;
                        NatHdrObj.State__c = iterator.Directory__r.State__c;
                        NatHdrObj.Directory_Number__c = iterator.Directory__r.directory_code__c;
                    }
                }
            }*/
        }
    }

    public void removeEntry()
    {
        rowNum = Integer.valueOf(apexpages.currentpage().getparameters().get('index'));
        
        if(rowNum!=null && NatLinLstObj[rowNum].id!=null)
        {
        National_Staging_Line_Item__c  NatLI=new National_Staging_Line_Item__c (id=NatLinLstObj[rowNum].id);
        delete NatLI;
        }
        NatLinLstObj.remove(rowNum);
    }

    public void productByUDAC(){
        rowNum = Integer.valueOf(apexpages.currentpage().getparameters().get('index'));
        Product2 prod = new Product2();
        if( [SELECT count() FROM Product2 WHERE ProductCode= :UDAC LIMIT 1] ==1){
            prod = [SELECT Id, Name, ProductCode FROM Product2 WHERE ProductCode= :UDAC LIMIT 1];
            NatLinLstObj[rowNum].Product2__c = prod.Id;
        } else {
            //raise error 
        }
    }
}