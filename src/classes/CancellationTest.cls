@isTest(seealldata=true)                    
public class CancellationTest {

    public static testMethod void Cancellation_close() {
        Case cancelCase=TestMethodsUtility.generateCancellation();
        insert cancelCase;      
        PageReference pageRef = Page.CancellationCloseCase;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', cancelCase.id);
        Cancellation_close_case_controller obj =new Cancellation_close_case_controller(new ApexPages.StandardController(cancelCase));
        obj.closeCaseValidation();
        cancelCase.Zero_Cancel_Fee_Requested__c=true;
        update cancelCase;
        obj.closeCaseValidation();
        
    }
    public static testMethod void cancellationLetter(){
        Test.startTest();
        Canvass__c c=TestMethodsUtility.createCanvass();
        c = [SELECT Name, Canvass_Code__c, Id FROM Canvass__c WHERE Id =: c.Id];
        Division__c division=TestMethodsUtility.createDivision();
        Account newaccount = new Account(Primary_Canvass__c = c.Id, BillingState = 'AB', BillingCountry = 'US', BillingCity = 'California', Name = 'Test Account', Phone = '(999) 999-9999', Billing_Phone__c='(999) 999-9999', Lead_Source__c = 'Testing', Conversion_Status__c = 'Yes');
        insert newaccount ;
        Contact newContact = TestMethodsUtility.createContact(newAccount);
        insert newContact ;
        Opportunity opp =TestMethodsUtility.createOpportunity(newaccount,newContact );
        insert opp;
        Directory__c dir=new Directory__c(Division__c=division.id,Name = 'testDirectory, KY', Directory_Code__c = '111111',Canvass__c=c.id);
        insert dir;
        
        Directory_Edition__c objDirE = new Directory_Edition__c();
        objDirE.Name = 'Test DirE1';
        objDirE.Directory__c = dir.Id;
        objDirE.Letter_Renewal_Stage_1__c = system.today();
        objDirE.Sales_Lockout__c=Date.today().addDays(30);
        objDirE.book_status__c='NI';
        objDirE.Pub_Date__c =Date.today().addMonths(1);
        insert objDirE;
        
        Directory_Edition__c objDirE1 = new Directory_Edition__c();
        objDirE1.Name = 'Test DirE2';
        objDirE1.Directory__c = dir.Id;
        objDirE1.Letter_Renewal_Stage_1__c = system.today();
        objDirE1.Sales_Lockout__c=Date.today().addDays(30);
        objDirE1.book_status__c='BOTS';
        objDirE1.Pub_Date__c =Date.today();
        
        insert objDirE1;                  
        
        Directory_Section__c objDirSec = new Directory_Section__c();
        objDirSec.Directory__c = dir.id;
        objDirSec.Name = 'Test Dir Section';
        objDirSec.Section_Page_Type__c = 'WP';
        objDirSec.Section_Code__c = '111111';
        insert objDirSec; 
           
        Telco__c newTelco = TestMethodsUtility.createTelco(newAccount.Id);
        Pricebook2 newPriceBook = [Select Id from Pricebook2 where isStandard=true limit 1];
        Product2 newProduct = TestMethodsUtility.generateproduct();
        newProduct.Family = 'Print';
        newProduct.RGU__c = 'Print';
        insert newProduct;
                Product2 objProd = new Product2();
        objProd.Name = 'Test';
        objProd.Product_Type__c = 'Print';
        objProd.ProductCode = 'WLCSH';
        objProd.Print_Product_Type__c='Display';
        objProd.RGU__c = 'Print';         
        insert  objProd;
        Product2 objProd1 = new Product2();
        objProd1.Name = 'Test';
        objProd1.Product_Type__c = 'Print Graphic';
        objProd1.ProductCode = 'GC50';
        objProd1.Print_Product_Type__c='Specialty';          
        objProd1.RGU__c = 'Print';
        insert objProd1;
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        insert newOpportunity;
        Case cancelCase=TestMethodsUtility.generateCancellation();
        insert cancelCase;      
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
       // Order_Line_Items__c newOrLI = TestMethodsUtility.createOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet);
        Order_Line_Items__c objOrderLineItem = new Order_Line_Items__c(Case__c=cancelCase.id,Successful_Payments__c=5,Directory_Section__c=objDirSec.id ,Billing_Partner__c='THE BERRY COMPANY',Account__c=newAccount.Id, Billing_Contact__c=newContact.id, Opportunity__c=newOpportunity.id, Order_Group__c=newOrderSet.id,Order__c=newOrder.id,Product2__c=objProd.Id,is_p4p__c=false,media_type__c='Print',Directory_Edition__c = objDirE1.Id,Directory__c=dir.id,canvass__c=c.id,UnitPrice__c=200,Payment_Duration__c=12,Payment_Method__c='Telco Billing',Package_ID__c='pkgid_12',Billing_Frequency__c='Monthly',Payments_Remaining__c=2,Service_End_Date__c=System.today().addMonths(12),Talus_Go_Live_Date__c=System.today(),Service_Start_Date__c=System.today(),Order_Anniversary_Start_Date__c=System.today().addMOnths(2));
        insert objOrderLineItem;
        Order_Line_Items__c objOrderLineItem1 = new Order_Line_Items__c(Case__c=cancelCase.id,Successful_Payments__c=4,Directory_Section__c=objDirSec.id ,Billing_Partner__c = 'THE BERRY COMPANY', Account__c=newAccount.Id, Billing_Contact__c=newContact.id, Opportunity__c=newOpportunity.id, Order_Group__c=newOrderSet.id,Order__c=newOrder.id,Product2__c=objProd.Id,is_p4p__c=false,media_type__c='Print',Directory_Edition__c = objDirE.Id,Directory__c=dir.id,canvass__c=c.id,UnitPrice__c=100,Payment_Duration__c=12,Payment_Method__c='Telco Billing',Billing_Frequency__c='Single Payment',Package_ID__c='pkgid_12',Payments_Remaining__c=2,Service_End_Date__c=System.today().addMonths(12),Talus_Go_Live_Date__c=System.today(),Service_Start_Date__c=System.today(),Order_Anniversary_Start_Date__c=System.today().addMonths(2));
        insert objOrderLineItem1;
        Order_Line_Items__c objOrderLineItem2 = new Order_Line_Items__c(Case__c=cancelCase.id,Successful_Payments__c=4,Directory_Section__c=objDirSec.id ,Billing_Partner__c = 'THE BERRY COMPANY1', Account__c=newAccount.Id, Billing_Contact__c=newContact.id, Opportunity__c=newOpportunity.id, Order_Group__c=newOrderSet.id,Order__c=newOrder.id,Product2__c=objProd.Id,is_p4p__c=false,media_type__c='Digital',Directory_Edition__c = objDirE.Id,Directory__c=dir.id,canvass__c=c.id,UnitPrice__c=100,Payment_Duration__c=12,Payment_Method__c='Telco Billing',Billing_Frequency__c='Single Payment',Package_ID__c='pkgid_12',Payments_Remaining__c=2,Service_End_Date__c=System.today().addMonths(12),Talus_Go_Live_Date__c=System.today(),Service_Start_Date__c=System.today(),Order_Anniversary_Start_Date__c=System.today().addMonths(2));
        insert objOrderLineItem2;
        Test.stopTest();
        PageReference pageRef = Page.CancellationLetter;
        Test.setCurrentPage(pageRef);
        ApexPages.currentPage().getParameters().put('id', cancelCase.id);       
        cancellationConfirmation obj =new cancellationConfirmation();
        obj.caseIdFromUrl=cancelCase.id;  
        obj.getCaseObj1();  
        CancellationLetterController obj1=new CancellationLetterController();
        obj1.caseId=cancelCase.id;
    }
}