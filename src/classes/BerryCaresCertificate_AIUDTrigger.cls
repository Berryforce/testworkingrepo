public class BerryCaresCertificate_AIUDTrigger {
      /*
      Created by: Magulan D
      Modified by: Aakrit
      Note(Very Important) :Kindly take a backup of this controller before editing because
      Blob.toPDF() causes exception error.
      */
        public static void createCertificate(List<Berry_Cares_Certificate__c> BCCList) {
            Map<String,DocumentURLs__c> DocURLs = DocumentURLs__c.getAll();
            system.debug('**** DocumentURL Value' + DocURLs );
            List<Attachment> CertificateList = new List<Attachment>();
            List<Messaging.SingleEmailMessage> MailList = new List<Messaging.SingleEmailMessage>();
            List<Case> CaseBCCUpdate = new List<Case>();
            List<EmailMessage> listEmailMessage = new list<EmailMessage>();
            list<Attachment> listAttachment = new list<Attachment>();
            
            
            Set<Id> CaseIDs = new Set<Id>();
           //For ATP-2963
            map<id,Case>CaseMap=new map<id,Case>([SELECT Id,owner.name from case Where Id IN (select Case__c from Berry_Cares_Certificate__c where id IN : BCCList)]);
            
           map<id,string>caseBCC = new map<id,String>();
            for(Berry_Cares_Certificate__c BCC : BCCList) {
                Attachment Certificate = new Attachment();
                //Certificate.Type = 'pdf';
                Certificate.Name = 'Certificate on ' + system.today();  
               // String CertificateBody = '<html><table border=\"0\" cellpadding=\"0\" cellspacing=\"0\"><tr><td valign=\"top\">';
                String CertificateBody = '<html>';
                /*
                if(DocURLs.containsKey(BCC.Branding__c)) {
                    CertificateBody = CertificateBody + '<div><img src=\"' + DocURLs.get(BCC.Branding__c).URL__c + '\"/></div>';
                }
                */
                if(BCC.Branding__c == CommonMessages.berryCare) {  
                  if(DocURLs.containsKey(BCC.Branding__c)) { 
          
                      CertificateBody = CertificateBody + '<body><br/><br/><br/><br/><br/><img src=\"' + DocURLs.get(BCC.Branding__c).URL__c + '\"/>';
                  }
                } 
                else if(BCC.Branding__c == CommonMessages.berryCareElite) {
                 system.debug('**** inside BElite' );
                  if(DocURLs.containsKey(BCC.Branding__c)) {
                      system.debug('**** inside BElite Document ');
                      CertificateBody = CertificateBody + '<body><br/><br/><br/><br/><br/><img src=\"' + DocURLs.get(BCC.Branding__c).URL__c + '\" />';
                  }
                } else if(BCC.Branding__c == CommonMessages.frontierCare) { 
                  if(DocURLs.containsKey(BCC.Branding__c)) {
                      CertificateBody = CertificateBody + '<body><br/><br/><br/><br/><br/><img src=\"' + DocURLs.get(BCC.Branding__c).URL__c + '\"/>';
                  }
                } else if(BCC.Branding__c == CommonMessages.frontierCareElite) {
                  if(DocURLs.containsKey(BCC.Branding__c)) {
                      CertificateBody = CertificateBody + '<body><br/><br/><br/><br/><br/><img src=\"' + DocURLs.get(BCC.Branding__c).URL__c + '\"/>';
                  }
                } 
                
                if(BCC.Branding__c == CommonMessages.berryCare || BCC.Branding__c == CommonMessages.berryCareElite) {
                    CertificateBody = CertificateBody + '<table bgcolor=\"#83BB4E\" width=\"100%\" height=\"2px\"><tr><td></td></tr></table>';
                } else if(BCC.Branding__c == CommonMessages.frontierCare || BCC.Branding__c == CommonMessages.frontierCareElite) {
                    CertificateBody = CertificateBody + '<table bgcolor=\"#A90426\" width=\"100%\" height=\"2px\"><tr><td></td></tr></table>';
                }
    
                if(BCC.Branding__c == CommonMessages.berryCare || BCC.Branding__c == CommonMessages.berryCareElite) {
                    CertificateBody = CertificateBody + '<table border=\"0\" width=\"100%\" style=\"font-size:10px;margin:0px auto;text-align:justify;\"><tr><td>Berry is committed to delivering our customers the latest print and digital advertising solutions, designed to drive customers to your door. We take pride in providing solutions that meet and exceed our customers’ expectations. Since we have fallen short of that goal, we want you to know that we sincerely apologize and truly value your business.</td></tr><br/></table>';
                } else if(BCC.Branding__c == CommonMessages.frontierCare || BCC.Branding__c == CommonMessages.frontierCareElite) {
                    CertificateBody = CertificateBody + '<table border=\"0\" width=\"100%\" style=\"font-size:10px;margin:0px auto;text-align:justify;\"><tr><td>Berry is committed to delivering our customers the latest print and digital advertising solutions, designed to drive customers to your door. We take pride in providing solutions that meet and exceed our customers’ expectations. Since we have fallen short of that goal, we want you to know that we sincerely apologize and truly value your business.</td></tr><br/></table>';
                }
                
                CertificateBody = CertificateBody + '<table border=\"1\" BORDERCOLOR=\"#D3D3D3\" width=\"100%\" style=\"margin:0px auto;\">';
                CertificateBody = CertificateBody + '<tr><td font-size:11px;text-align:right;><b>Certificate Number</b></td><td colspan="2">' + BCC.Certificate_Number__c + '</td></tr>';
                CertificateBody = CertificateBody + '<tr><td font-size:11px;text-align:right;><b>Date Of Issue</b></td><td colspan="2">' + BCC.Date_of_Issue__c + '</td></tr>';
                CertificateBody = CertificateBody + '<tr><td rowspan="2" font-size:11px;text-align:right;><b>Monthly Certificate Amount</b></td><td colspan="2">' + BCC.BCC_Sub_Amount__c + '&nbsp;(per month for 12 months)</td></tr>';
                CertificateBody = CertificateBody + '<tr><td font-size:11px;text-align:right;><b>Customer ID</b></td><td colspan="2">' + BCC.Customer_ID__c + '</td></tr>';
                CertificateBody = CertificateBody + '<tr><td font-size:11px;text-align:right;><b>Account Name</b></td><td colspan="2">' + BCC.Account_Name__c + '</td></tr>';
                CertificateBody = CertificateBody + '<tr><td font-size:11px;text-align:right;><b>Primary Telephone Number</b></td><td colspan="2">' + BCC.Primary_Telephone_Number__c + '</td></tr>';
                CertificateBody = CertificateBody + '<tr><td font-size:11px;text-align:right;><b>Client Name</b></td><td>' + BCC.Client_Contact_First_Name__c + '</td><td font-size:11px;text-align:right;>' + BCC.Client_Contact_Last_Name__c + '</td></tr>';
                CertificateBody = CertificateBody + '<tr><td font-size:11px;text-align:right;><b>Issued By</b></td><td>' + BCC.CSR_First_Name__c +  '</td><td font-size:11px;text-align:right;>' + BCC.CSR_Alias__c + '</td></tr>';
                CertificateBody = CertificateBody + '</table>';
                CertificateBody = CertificateBody + '<br/>';   
                if(BCC.Branding__c == CommonMessages.berryCare || BCC.Branding__c == CommonMessages.berryCareElite) {
                    CertificateBody = CertificateBody + '<table bgcolor=\"#83BB4E\" width=\"100%\" height=\"2px\"><tr><td></td></tr></table>';
                } else if(BCC.Branding__c == CommonMessages.frontierCare || BCC.Branding__c == CommonMessages.frontierCareElite) {
                    CertificateBody = CertificateBody + '<table bgcolor=\"#A90426\" width=\"100%\" height=\"2px\"><tr><td></td></tr></table>';
                }
                
                CertificateBody = CertificateBody + '<table border=\"0\" width=\"100%\" style=\"margin-bottom:10px auto;text-align:justify;color:#0D3082;\"><tr><td><b>IMPORTANT:</b>Please retain this certificate for presentation to your Berry Sales Representative.</td></tr></table>';
                
                if(BCC.Branding__c == CommonMessages.berryCare || BCC.Branding__c == CommonMessages.berryCareElite) {
                    CertificateBody = CertificateBody + '<table bgcolor=\"#83BB4E\" width=\"100%\" height=\"4px\"><tr><td></td></tr></table>';
                } else if(BCC.Branding__c == CommonMessages.frontierCare || BCC.Branding__c == CommonMessages.frontierCareElite) {
                    CertificateBody = CertificateBody + '<table bgcolor=\"#A90426\" width=\"100%\" height=\"4px\"><tr><td></td></tr></table>';
                }
    
                if(BCC.Branding__c == CommonMessages.berryCare || BCC.Branding__c == CommonMessages.berryCareElite) {
                    CertificateBody = CertificateBody + '<table border=\"0\" width=\"100%\" style=\"color:#ADB1AD;font-size:10px;margin:0px auto;text-align:justify;\"><tr><td><b><i>Restrictions:</i></b> This <b>Berry</b> Cares Product Certificate (this “Certificate”) must begin redemption of the Certificate within twelve (12) months of the Date of Issue specified above.  The Certificate credit amount must match the amount associated with the Certificate Number as maintained in Berry systems.  This Certificate may only be utilized towards the purchase of additional advertising above and beyond existing billable advertising at the time the certificate is issued, and in accordance with Berry’s then-current <b>Berry</b> Cares Certificate Policy; it is not redeemable for cash.  Client’s account must be in good standing to utilize the Certificate. Qualified advertising includes most digital and print advertising offered by Berry, except certain premium items such as covers, tabs, edge, gatefolds, tip-ons and other similar specialty items.  See your Berry Sales Representative for details. The Monthly Certificate Amount specified above must be applied towards new monthly recurring charges incurred through a single purchase of additional advertising.  Any excess credit amount not utilized through such single purchase shall be forfeited.  No excess credit amount shall be carried forward or otherwise available for use in subsequent purchases. For example - (i) if a Certificate for $10 is utilized to purchase additional advertising with a value of $8, or(ii) if a Certificate for $10 is utilized to purchase additional advertising with a value of $10 with a product term of only 6 months, the entire Certificate in both instances would be deemed fully utilized by the respective purchase. This Certificate is non-transferable and otherwise subject to the terms and conditions of an Advertising Order.</tr></td></table>';
                } else if(BCC.Branding__c == CommonMessages.frontierCare || BCC.Branding__c == CommonMessages.frontierCareElite) {
                    CertificateBody = CertificateBody + '<table border=\"0\" width=\"100%\" style=\"color:#ADB1AD;font-size:10px;margin:0px auto;text-align:justify;\"><tr><td><b><i>Restrictions:</i></b> This <b>Frontier</b> Cares Product Certificate (this “Certificate”) must begin redemption of the Certificate within twelve (12) months of the Date of Issue specified above.  The Certificate credit amount must match the amount associated with the Certificate Number as maintained in Berry systems.  This Certificate may only be utilized towards the purchase of additional advertising above and beyond existing billable advertising at the time the certificate is issued, and in accordance with Berry’s then-current <b>Frontier</b> Cares Certificate Policy; it is not redeemable for cash.  Client’s account must be in good standing to utilize the Certificate. Qualified advertising includes most digital and print advertising offered by Berry, except certain premium items such as covers, tabs, edge, gatefolds, tip-ons and other similar specialty items.  See your Berry Sales Representative for details. The Monthly Certificate Amount specified above must be applied towards new monthly recurring charges incurred through a single purchase of additional advertising.  Any excess credit amount not utilized through such single purchase shall be forfeited.  No excess credit amount shall be carried forward or otherwise available for use in subsequent purchases. For example - (i) if a Certificate for $10 is utilized to purchase additional advertising with a value of $8, or(ii) if a Certificate for $10 is utilized to purchase additional advertising with a value of $10 with a product term of only 6 months, the entire Certificate in both instances would be deemed fully utilized by the respective purchase. This Certificate is non-transferable and otherwise subject to the terms and conditions of an Advertising Order.</tr></td></table>';
                }  
                
                if(BCC.Branding__c == CommonMessages.berryCare || BCC.Branding__c == CommonMessages.berryCareElite) {
                    CertificateBody = CertificateBody + '<br/><table bgcolor=\"#83BB4E\" width=\"100%\" height=\"4px\"><tr><td></td></tr></table>';
                } else if(BCC.Branding__c == CommonMessages.frontierCare || BCC.Branding__c == CommonMessages.frontierCareElite) {
                    CertificateBody = CertificateBody + '<br/><table bgcolor=\"#A90426\" width=\"100%\" height=\"4px\"><tr><td></td></tr></table>';
                } 
                
                if(BCC.Branding__c == CommonMessages.frontierCare || BCC.Branding__c == CommonMessages.frontierCareElite) {
                  if(DocURLs.containsKey(CommonMessages.frontierCareSign)) {
                      CertificateBody = CertificateBody + '<br/><br/><br/><br/><br/><img width=\"80\" src=\"' + DocURLs.get(CommonMessages.frontierCareSign).URL__c + '\"/>';
                  }
                }        
                
                  CertificateBody = CertificateBody + '</body></html>';
                // CertificateBody = CertificateBody + '</table></html>';
                
                system.debug('Body is ' + CertificateBody);
                Certificate.Body = Blob.toPDF(CertificateBody);            
                Certificate.ParentId = BCC.Id;
                Certificate.ContentType  = 'application/pdf';
                
                CertificateList.add(Certificate);
                
                if(BCC.Client_Contact_Email__c!=null)
                {
                    Messaging.EmailFileAttachment attach = new Messaging.EmailFileAttachment();
                      attach.setContentType('application/pdf');
                      attach.setFileName('Certificate.pdf');
                      attach.Body = Blob.toPDF(CertificateBody);
                  
                    Messaging.SingleEmailMessage Mail = new Messaging.SingleEmailMessage();
                    Mail.setSubject('Berry Cares Certificate');
                    Mail.setToAddresses(new String[] { BCC.Client_Contact_Email__c });
                    //ATP-2963
                    //String Body = 'Hi, <br/>Kindly find the attached Berry Cares Certificate';
                    
                    String  Body = '<html><body>Dear '+BCC.Client_Contact_First_Name__c+' '+BCC.Client_Contact_Last_Name__c+',';                                                
                            Body += '<br><br>'+'Attached please find the '+BCC.Branding__c+' Care Certificate that we had discussed.A copy of this certificate has also been made available to your Sales Representative.';
                            Body += 'Should you have any questions, please contact me at (877)-557-8221.';
                            Body += '<br><br>'+'Sincerely,';
                            Body += '<br>'+CaseMap.get(BCC.case__c).owner.name+'</body></html>';
                            
                    
                            
                    Mail.setHTMLBody(Body);
                    Mail.setFileAttachments(new Messaging.EmailFileAttachment[] { attach });
                    MailList.add(Mail);
                    //ATP-3798
                    EmailMessage emailMsgObj = new EmailMessage();
                    emailMsgObj.parentid =BCC.case__c;
                    emailMsgObj.status='3';
                    emailMsgObj.HtmlBody =Body;                    
					emailMsgObj.Subject = 'Berry Cares Certificate';
					emailMsgObj.ToAddress = BCC.Client_Contact_Email__c;
					emailMsgObj.MessageDate=date.today();
					listEmailMessage.add(emailMsgObj);					
					caseBCC.put(BCC.case__c,CertificateBody);
					
					
					/*Attachment attachmentObj = new Attachment();
  					attachmentObj.Body =  Blob.toPDF(CertificateBody);
  					attachmentObj.Name = String.valueOf('Berry Cares Certificate');
  					attachmentObj.ParentId = emailMsgObj.id; 
  					system.debug('--------'+emailMsgObj.id);
  					listAttachment.add(attachmentObj);
  					*/
					
					
                  }
                else
                {}
    
            }
            insert CertificateList;
            if(!MailList.isEmpty())
            {
                Messaging.sendEmail(MailList);
                insert listEmailMessage;
                
                
                for(EmailMessage emailObj:listEmailMessage)
                {
                	Attachment attachmentObj = new Attachment();
  					attachmentObj.Body =  Blob.toPDF(caseBCC.get(emailObj.parentid));
  					attachmentObj.Name = String.valueOf('Berry Cares Certificate');
  					attachmentObj.ParentId = emailObj.id;
  					listAttachment.add(attachmentObj);
  					
                }
                
                insert listAttachment;
                
                
                
            }
            
            	
            	
           
        }
    }