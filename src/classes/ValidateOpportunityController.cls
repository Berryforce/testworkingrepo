public class ValidateOpportunityController {
    Id opportunityID;
    public Boolean bFlag {get;set;}
    String terminalURL;
    Id strPaymentId;
    Opportunity objOpportunity = new Opportunity();
    list<OpportunityLineItem> lstOPPLIP4P = new list<OpportunityLineItem>();
        
    public ValidateOpportunityController(ApexPages.StandardController controller) {
        opportunityID = ApexPages.currentPage().getParameters().get('Id');
        bFlag = false;
        objOpportunity = OpportunitySOQLMethods.setOpportunityByIdForLockCloneInvoicePayment(new set<Id>{opportunityID})[0];
    }
    
    public void opptyValidation() {                
        //Account & Billing contact address validation
        if(!Test.isRunningTest()) {
            if(CommonMethods.checkOpportunityandBMIQuoteBeforeCreateOrder(objOpportunity, null)) {
            	if(String.isNotBlank(objOpportunity.modifyid__c)) {
                	cloneYPCModifyOpportunityLineItem();
            	}
                bFlag = true;
            }
        } else {
            bFlag = false;
        }
    }
    
    public PageReference closeOppty() {
        if(bFlag == true) {
            lockCloneInvoiceandPaymentController_v6 lockClone = new lockCloneInvoiceandPaymentController_v6(opportunityID);
            return lockClone.showTerminalForOpptyVal();
        }
        return null;
    }
    
    public Pagereference backToOpportunity() {
        return new Pagereference('/'+opportunityID);
    }
    
    private void cloneYPCModifyOpportunityLineItem() {
        list<OpportunityLineItem> updateOppLI = new list<OpportunityLineItem>();
        map<String, list<OpportunityLineItem>> mapOppLIAddon = new map<String, list<OpportunityLineItem>>();
        set<Id> setOPPLIID = new set<Id>();
        set<Id> setOPPLIIDClone = new set<Id>();
        set<Id> setProductID = new set<Id>();
 		list<OpportunityLineItem> deleteOppLI = new list<OpportunityLineItem>();
        for(OpportunityLineItem iterator : objOpportunity.OpportunityLineItems) {
            if(String.isNotBlank(iterator.Parent_ID__c) && iterator.OPPLI_YPC_isModified__c && iterator.Renewals_Action__c == 'Modify') {
                setOPPLIID.add(iterator.Id);
                if(iterator.Original_Line_Item_ID__c != null) {
	                setOPPLIIDClone.add(iterator.Id);
	                setProductID.add(iterator.Original_Line_Item_ID__r.Product2__c);
                }
                mapOppLIAddon.put(iterator.Parent_ID__c, new list<OpportunityLineItem>());
                iterator.Renewals_Action__c = 'Cancel';
                //iterator.Product2Id = iterator.Original_Line_Item_ID__r.Product2__c;
                updateOppLI.add(iterator);
            }
            else if(String.isNotBlank(iterator.Parent_ID_of_Addon__c) && iterator.Renewals_Action__c != 'Cancel') {
                if(mapOppLIAddon.size() > 0) {
                    if(mapOppLIAddon.containsKey(iterator.Parent_ID_of_Addon__c)) {
                        setOPPLIID.add(iterator.Id);
                        iterator.Renewals_Action__c = 'Cancel';
                        if(iterator.Renewals_Action__c != 'New') {
                        	deleteOppLI.add(iterator);
                        }
                        else {
                        	updateOppLI.add(iterator);
                        }
                    }
                }
            }
        }
        
        if(setOPPLIID.size() > 0) {
        	
            String strOPPLIQuery = CommonMethods.getCreatableFieldsSOQL('OpportunityLineItem');
            strOPPLIQuery = strOPPLIQuery.replace('TotalPrice,','');
            strOPPLIQuery = strOPPLIQuery + ' where ID IN:setOPPLIID';
            list<OpportunityLineItem> lstOppLI = Database.Query(strOPPLIQuery);
            if(lstOppLI != null && lstOppLI.size() > 0) {
                list<OpportunityLineItem> lstClonedOppLI = lstOppLI.deepClone(false, false, false);
                if(lstClonedOppLI.size() > 0) {
                    for(OpportunityLineItem iterator : lstClonedOppLI) {
                        iterator.Renewals_Action__c = 'New';
                        iterator.BigMachines__Synchronization_Id__c = iterator.BigMachines__Synchronization_Id__c + '-1';
                        iterator.Original_Line_Item_ID__c = null;
                        if(String.isNotBlank(iterator.Parent_ID__c)) {
                            iterator.Parent_ID__c = iterator.Parent_ID__c + '1';
                        }
                        if(String.isNotBlank(iterator.Parent_ID_of_Addon__c)) {
                            iterator.Parent_ID_of_Addon__c = iterator.Parent_ID_of_Addon__c + '1';
                        }
                    }
                    updateOppLI.addAll(lstClonedOppLI);
                    if(updateOppLI.size() > 0) {
                        upsert updateOppLI;
                        delete deleteOppLI;
                    }
                }
            }
            
            if(setOPPLIIDClone.size() > 0) {
            	list<PricebookEntry> lstPBE = [select Id, Product2Id from PricebookEntry where Product2Id IN:setProductID];
	        	map<Id, Id> mapPriceBookId = new map<Id, Id>();
	        	for(PricebookEntry iterator : lstPBE) {
	        		mapPriceBookId.put(iterator.Product2Id, iterator.Id);
	        	}
            	list<OpportunityLineItem> insertOppLI = new list<OpportunityLineItem>();
            	list<OpportunityLineItem> deleteCloneOppLI = new list<OpportunityLineItem>();
            	String strOPPLIQueryClone = CommonMethods.getCreatableFieldsSOQL('OpportunityLineItem');
	            strOPPLIQueryClone = strOPPLIQueryClone.replace('TotalPrice,','Original_Line_Item_ID__r.Product2__c,');
	            strOPPLIQueryClone = strOPPLIQueryClone + ' where ID IN:setOPPLIIDClone';
	            list<OpportunityLineItem> lstOppLIClone = Database.Query(strOPPLIQueryClone);
	            if(lstOppLIClone != null && lstOppLIClone.size() > 0) {
                	list<OpportunityLineItem> lstClonedOppLI = lstOppLIClone.deepClone(false, false, false);
                	if(lstClonedOppLI.size() > 0) {
	                    for(OpportunityLineItem iterator : lstClonedOppLI) { 
	                    	iterator.PricebookEntryId = mapPriceBookId.get(iterator.Original_Line_Item_ID__r.Product2__c);
	                    	iterator.BigMachines__Synchronization_Id__c = iterator.BigMachines__Synchronization_Id__c + '-Delete1';
	                    	insertOppLI.add(iterator);
	                    }
                	}
                	
                	if(insertOppLI.size() > 0) {
                		insert insertOppLI;
                		delete lstOppLIClone;
                	}
	            }
            }
        }
    }
}