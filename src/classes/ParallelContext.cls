/**
 * FinancialForce.com, inc. claims copyright in this software, its screen display designs and
 * supporting documentation. FinancialForce and FinancialForce.com are trademarks of FinancialForce.com, inc.
 * Any unauthorized use, copying or sale of the above may constitute an infringement of copyright and may
 * result in criminal or other legal proceedings.
 *
 * Copyright FinancialForce.com, inc. All rights reserved.
 * Created by Agustina Garcia
 */

public with sharing class ParallelContext
{
	private static c2g.CODAAPICommon_6_0.Context apiV6Context = null;
	private static c2g.CODAAPICommon_8_0.Context apiV8Context = null;

	public static c2g.CODAAPICommon_6_0.Context getv6ApiContext()
    {
        if( apiv6Context == null )
        {
            String apiToken = EncodingUtil.convertToHex(Crypto.generateDigest('SHA1', Blob.valueOf('0000000000000000C000000000000046' + UserInfo.getUserId() + 'PROBABLEMENTE EL MEJOR SOFTWARE DE CONTABILIDAD EN EL MUNDO')));
            apiv6Context = new c2g.CODAAPICommon_6_0.Context();
            apiv6Context.Token = apiToken;
        }

        return apiv6Context;
    }

    public static c2g.CODAAPICommon_8_0.Context getv8ApiContext()
    {
        if( apiv8Context == null )
        {
            String apiToken = EncodingUtil.convertToHex(Crypto.generateDigest('SHA1', Blob.valueOf('0000000000000000C000000000000046' + UserInfo.getUserId() + 'PROBABLEMENTE EL MEJOR SOFTWARE DE CONTABILIDAD EN EL MUNDO')));
            apiv8Context = new c2g.CODAAPICommon_8_0.Context();
            apiv8Context.Token = apiToken;
        }

        return apiv8Context;
    }
}