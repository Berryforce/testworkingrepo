@isTest
public class PushLeadstoQueueTestClass {
    public static testMethod void LeadQueueBatch() {
     Test.startTest();
    
    Database.BatchableContext bc;
     Canvass__c objCanvss = new Canvass__c();
        objCanvss.Name = 'Unit Test Canvass';
        objCanvss.co_brand_name__c = 'Test';
        objCanvss.IsActive__c = true;
        insert objCanvss;
     
     List<Lead> LstLeads = new List<Lead>();
     Lead objLead = new Lead();
        objLead.LastName = 'Test LastName';
        objLead.Company='Fry And Sons';
        objLead.Primary_Canvass__c = objCanvss.id;
        objLead.Phone = '(201) 679-1555';
        objLead.Status = 'Open';
        objLead.Rating = 'Hot';
         
        LstLeads.add(objLead);
        insert LstLeads;

    
    AssignmentRule AR = new AssignmentRule();
    AR = [select id from AssignmentRule where SobjectType = 'Lead' and Active = true limit 1];
    
        Database.DMLOptions dmlOpts = new Database.DMLOptions();
        dmlOpts.assignmentRuleHeader.assignmentRuleId= AR.id;
        LstLeads[0].setOptions(dmlOpts);
    
    PushLeadstoQueue batchObj = new PushLeadstoQueue ();
        batchObj.start(bc);
        batchObj.execute(bc,LstLeads);     
        batchObj.finish(bc);
        map<Date, DateTime> mapFinalHolidays = PushLeadstoQueue.getHolidayList();
        PushLeadstoQueue.DateCalculatenew(system.today(),mapFinalHolidays ,2);
     Test.stopTest();
    
    
    }
}