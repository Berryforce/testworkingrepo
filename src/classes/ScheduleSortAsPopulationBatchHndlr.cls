global class ScheduleSortAsPopulationBatchHndlr implements ScheduleSortAsPopulationBatch.ScheduleSortAsPopulationBatchInterface {
    global void execute(SchedulableContext SC) {
        SortAsPopulationBatch obj = new SortAsPopulationBatch();
        Database.executeBatch(obj);
    }  
}