public class NewSpecArtRequestOverriddenExt {
    public Id accountId;
    Spec_Art_Request__c SAR;
    Account SARAccount;
    //Contact SARContact;
    //User Usr;
    public NewSpecArtRequestOverriddenExt(Apexpages.Standardcontroller controller) {
        SAR = (Spec_Art_Request__c)controller.getRecord();
        accountId = SAR.Account__c;
        SARAccount = new Account();
        //SARContact = new Contact();
        //Usr = new User();
        system.debug('Account id is ' + accountId);
    }
    public Pagereference onLoad() {
    	if(accountId != null) {
	        PageReference newPage = new PageReference('/' + System.Label.New_Spec_Art + '/e?');
	        if(String.isNotBlank(accountId)){
	            NewSpecArtRequestAutoPopulateField__c ids = NewSpecArtRequestAutoPopulateField__c.getValues('Ids');
	            //Usr = [SELECT Phone, Id, FirstName, LastName FROM User WHERE Id = : UserInfo.getUserId()];
	            SARAccount = [SELECT ID, Name, Website, OwnerId, Owner.Phone, Owner.Name, BillingStreet, BillingCity, BillingState, BillingPostalCode, Phone, BillingCountry FROM Account WHERE Id = : accountId];
	            /*system.debug('Account is ' + SARAccount);
	            List<Contact> contactList = new List<Contact>();
	            contactList = [SELECT AccountId, FirstName, LastName, Email FROM Contact WHERE AccountId = : accountId AND Primary_Contact__c = true];
	            
	            if(!contactList.isEmpty()) {
	                SARContact = contactList.get(0);
	                system.debug('Contact is ' + SARContact);
	                
	                newPage.getParameters().put(ids.Advertised_Business_Email__c, SARContact.Email);
	                newPage.getParameters().put(ids.First_Name__c, SARContact.FirstName);
	                newPage.getParameters().put(ids.Last_Name__c, SARContact.LastName);
	            }   */                   
	            
	            for(String keyValue : ApexPages.currentPage().getParameters().keySet()){
	                if(!keyValue.contains('override') ) {
	                    newPage.getParameters().put(keyValue, ApexPages.currentPage().getParameters().get(keyValue));
	                }
	            }
	            newPage.getParameters().remove('save_new');
	            
	            newPage.getParameters().put(ids.Account_Name__c, SARAccount.Name);
	            newPage.getParameters().put(ids.Account_ID__c, SARAccount.Id);
	            newPage.getParameters().put(ids.Advertised_Business_Postal_Code__c, SARAccount.BillingPostalCode);
	            newPage.getParameters().put(ids.Advertised_Listed_Address__c, SARAccount.BillingStreet);
	            newPage.getParameters().put(ids.Advertised_Listed_Area_Code__c, String.valueOf(SARAccount.Phone).subString(1,4));
	            newPage.getParameters().put(ids.Advertised_Listed_City__c, SARAccount.BillingCity);
	            newPage.getParameters().put(ids.Advertised_Listed_Phone_Number__c, SARAccount.Phone);
	            newPage.getParameters().put(ids.Advertised_Listed_State__c, SARAccount.BillingState);
	            newPage.getParameters().put(ids.Advertised_Listed_Country__c, SARAccount.BillingCountry);
	            newPage.getParameters().put(ids.Listed_Name__c, SARAccount.Name);
	            newPage.getParameters().put(ids.Sales_Rep__c, SARAccount.Owner.Name);
	            newPage.getParameters().put(ids.Sales_Rep_ID__c, SARAccount.Owner.Id);
	            newPage.getParameters().put(ids.Sales_Rep_Phone__c, SARAccount.Owner.Phone);
	            newPage.getParameters().put(ids.Telephone_Number__c, String.valueOf(SARAccount.Phone).subString(6));
	            newPage.getParameters().put(ids.Telephone_Prefix__c, '1');
	            newPage.getParameters().put(ids.Web_Address__c, SARAccount.Website);
	        }
	        newPage.getParameters().put('nooverride','1');
	        newPage.setRedirect(true);
	        return newPage;
        }
        return null;
    }
    
    public pagereference gotoAccount() {
    	return new PageReference('/001/o');
    }
}