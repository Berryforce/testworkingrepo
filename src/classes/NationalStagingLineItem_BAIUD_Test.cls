@isTest
private class NationalStagingLineItem_BAIUD_Test {

    static testMethod void myUnitTest() {
        Test.startTest();
        Directory__c dir = TestMethodsUtility.generateDirectory();
    	dir.Months_for_National__c = 12;
    	insert dir;
        National_Staging_Order_Set__c newNSOS1 = TestMethodsUtility.generateNationalStagingOrderSet('Elite RT');
        newNSOS1.Directory__c = dir.Id;
        insert newNSOS1;
        National_Staging_Line_Item__c newNSLI1 = TestMethodsUtility.generateNationalStagingLineItemCreation(newNSOS1);
        newNSLI1.Transaction_Version_ID__c = 'TestABC';
        newNSLI1.Discount__c ='X15';
        newNSLI1.UDAC__c = 'Test123';
        insert newNSLI1;
        National_Staging_Order_Set__c newNSOS2 = TestMethodsUtility.generateNationalStagingOrderSet('Elite RT');
        newNSOS2.Directory__c = dir.Id;
        insert newNSOS2;
        National_Staging_Line_Item__c newNSLI2 = TestMethodsUtility.generateNationalStagingLineItemCreation(newNSOS2);
        newNSLI2.Discount__c ='X15';
        insert newNSLI2;
        newNSLI1.Transaction_Version_ID__c = 'Test123';
        newNSLI1.UDAC__c = 'TestABC';
        update newNSLI1;
        Test.stopTest();
    }
}