@isTest(SeeAllData=True)
public class SalesInvoiceSOQLMethodsTest{
    static testMethod void SalesInvoiceSOQLMethodsTest(){
        Set<Id> setAccountIds = new Set<Id>();
        Set<Id> setInvoiceIds = new Set<Id>();
        Set<Id> setOpportunityIds = new Set<Id>();
        String AccountIds;
        Account newAccount = TestMethodsUtility.createAccount('customer');
        setAccountIds.add(newAccount.Id);
        AccountIds = newAccount.Id;
        Opportunity newOpportunity = TestMethodsUtility.createOpportunity('new', newAccount);
        setOpportunityIds.add(newOpportunity.Id);
        c2g__codaInvoice__c newSalesInvoice = new c2g__codaInvoice__c(c2g__InvoiceDate__c = System.Today(), c2g__DueDate__c = System.Today()+30, c2g__Account__c = newAccount.Id, c2g__Opportunity__c = newOpportunity.Id);
        newSalesInvoice.c2g__Period__c = System.Label.TestFFPeriods;
        newSalesInvoice.c2g__InvoiceCurrency__c = System.Label.AccountingCurrencyId;
        newSalesInvoice.Customer_Name__c = newAccount.id;
        insert newSalesInvoice;
        setInvoiceIds.add(newSalesInvoice.Id);
        System.assertNotEquals(null, SalesInvoiceSOQLMethods.getMapSalesInvoiceLineItemsPaymentsbyInvoiceId(setInvoiceIds));
        System.assertNotEquals(null, SalesInvoiceSOQLMethods.getMapSalesInvoiceLineItemsPaymentsbyInvoiceIds(setInvoiceIds));
        System.assertNotEquals(null, SalesInvoiceSOQLMethods.getSalesInvoiceByInvoiceID(setInvoiceIds));        
    }
    }