global with sharing class BSure_ScheduleManagePaymentPlan implements Schedulable {
	global void execute(SchedulableContext sc) {
      bSure_ManagePaymentPlan objBatchPayment = new bSure_ManagePaymentPlan(); 
      database.executebatch(objBatchPayment);
   }
}