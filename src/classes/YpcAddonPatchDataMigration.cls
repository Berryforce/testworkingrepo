/***********************************************************
Batch Class to send YPC Add-on patches to Fulfillment Vendor
Created By: Reddy(reddy.yellanki@theberrycompany.com)
Updated Date: 08/14/2015
$Id$
************************************************************/
global class YpcAddonPatchDataMigration implements Database.Batchable < sObject > , Database.AllowsCallouts {
    global String query;

    global YpcAddonPatchDataMigration(String query) {

        this.query = query;

    }

    global Database.Querylocator start(Database.BatchableContext BC) {

        return Database.getQueryLocator(query);

    }

    global void execute(Database.BatchableContext BC, List < Digital_Product_Requirement__c > scope) {

        List < Digital_Product_Requirement__c > lstDffs = scope;
        System.debug('************' + lstDffs.size());

        Set < String > cstlkItms = new Set < String > {
            'CSTLK', 'CST5', 'RBL', 'RB5', 'YPCP', 'YPC5', 'PVID', 'PVID5', 'VID1', 'VID15', 'VIDA', 'VIDA5'
        };
        Set < String > pointsItms = new Set < String > {
            'VCTLK', 'VCTL5', 'VRB', 'VRP5', 'VCP', 'VCP5', 'VVP', 'VP5', 'VAA', 'VA5', 'VRB5'
        };
        Set < String > ypcCpn = new Set < String > {
            'YPCP', 'YPC5'
        };
        Set < String > ypcCstlk = new Set < String > {
            'CSTLK', 'CST5'
        };

        String cstVals;
        String pntVals;
        String testVal;
        String fUrl;
        //System.debug('************You are Testing this************: ' + UserInfo.getFirstName());
        string url = Label.Talus_Nigel_Url;
        String cpnDesc;
        String cpnUrl;
        String CstlkTxt;
        String CstlkUrl;
        String txt1;
        String txt2;
        String txt3;

        List < Digital_Product_Requirement__c > updtDffs = new List < Digital_Product_Requirement__c > ();
        List < Track_DFF_Update__c > trkDffs = new List < Track_DFF_Update__c > ();

        Map < Digital_Product_Requirement__c, List < YPC_AddOn__c > > mpAddons = new Map < Digital_Product_Requirement__c, List < YPC_AddOn__c > > ();
        List < YPC_AddOn__c > ypcAdn = new List < YPC_AddOn__c > ();
        List < YPC_AddOn__c > delYpcAdn = new List < YPC_AddOn__c > ();
        Map < Id, YPC_AddOn__c > delYpdAdnMap = new Map < Id, YPC_AddOn__c > ();

        for (Digital_Product_Requirement__c dff: scope) {

            List < YPC_AddOn__c > spVals = new List < YPC_AddOn__c > ();
            List < YPC_AddOn__c > spVals1 = dff.YPC_AddOns__r;
            if (spVals1.size() > 0) {
                for (YPC_AddOn__c iterator: spVals1) {
                    if (iterator.Effective_Date__c <= system.Today()) {
                        spVals.add(iterator);
                    }
                }
            }

            cstVals = '';
            pntVals = '';
            if (spVals.size() > 0) {

                if (String.isBlank(dff.add_ons__c)) {
                    cstVals = '';
                } else(cstVals = dff.add_ons__c);

                if (String.isBlank(dff.udac_points__c)) {
                    pntVals = '';
                } else(pntVals = dff.udac_points__c);
                for (YPC_AddOn__c val: spVals) {
                    //System.debug('************' + val.Action_Type__c + '************' + val.Effective_Date__c + '************' + val.Name);
                    if (val.Action_Type__c == 'New' && val.Effective_Date__c <= system.Today()) {
                        if (cstlkItms.contains(val.Name) && (!cstVals.contains(val.Name))) {
                            cstVals += ';' + val.Name;
                            if (ypcCpn.contains(val.Name)) {
                                //cpnDesc = val.coupon_description__c;
                                //dff.coupon_description__c = val.coupon_description__c;
                                //cpnUrl = String.Valueof(val.coupon_url__c);
                                //dff.coupon_url__c = String.Valueof(val.coupon_url__c);
                                //txt1 = val.coupon_text_offer_1__c;
                                //dff.coupon_text_offer_1__c = val.coupon_text_offer_1__c;
                                //txt2 = val.coupon_text_offer_2__c;
                                //dff.coupon_text_offer_2__c = val.coupon_text_offer_2__c;
                                //txt3 = val.coupon_text_offer_3__c;
                                //dff.coupon_text_offer_3__c = val.coupon_text_offer_3__c;

                                //System.debug('************CpnVals************' + cpnDesc + '************************' + cpnUrl);
                            }
                            if (ypcCstlk.contains(val.Name)) {
                                //CstLkTxt = val.cslt_text__c;
                                //dff.cslt_text__c = val.cslt_text__c;
                                //CstLkUrl = val.cslt_url__c;
                                //dff.cslt_url__c = val.cslt_url__c;
                                //System.debug('************CstlkVals************' + CstLkTxt + '************************' + CstLkUrl);
                            }
                        } else if (pointsItms.contains(val.Name) && (!pntVals.contains(val.Name))) {
                            pntVals += ';' + val.Name;
                        }
                    } else if (val.Action_Type__c == 'Cancel' && val.Effective_Date__c <= system.Today()) {
                        if (cstlkItms.contains(val.Name)) {
                            cstVals = cstVals.replace(val.Name, '');
                            if (val.Name == 'CSTLK' || val.Name == 'CST5') {
                                dff.cslt_url__c = '';
                                dff.cslt_text__c = '';
                            }
                            if (val.Name == 'YPCP' || val.Name == 'YPC5') {
                                dff.coupon_description__c = '';
                                dff.coupon_url__c = '';
                                dff.coupon_text_offer_1__c = '';
                                dff.coupon_text_offer_2__c = '';
                                dff.coupon_text_offer_3__c = '';
                            }
                            if (cstVals.contains('CSTLK') || cstVals.contains('CST5')) {
                                CstlkUrl = dff.cslt_url__c;
                                CstlkTxt = dff.cslt_text__c;
                            }
                            if (cstVals.contains('YPCP') || cstVals.contains('YPC5')) {
                                cpnDesc = dff.coupon_description__c;
                                cpnUrl = dff.coupon_url__c;
                                txt1 = dff.coupon_text_offer_1__c;
                                txt2 = dff.coupon_text_offer_2__c;
                                txt3 = dff.coupon_text_offer_3__c;
                            }
                        } else if (pointsItms.contains(val.Name)) {
                            pntVals = pntVals.replace(val.Name, '');
                        }
                    }
                }
                //System.debug('************CstLnkVals************' + cstVals + '************PntVals************' + pntVals);
                List < String > fnlVals1 = cstVals.split(';');
                cstVals = '';
                for (String s: fnlVals1) {
                    if (String.isNotBlank(s) && (!cstVals.Contains(s))) {
                        cstvals += s + ';';
                    }
                }

                List < String > fnlVals2 = pntVals.split(';');
                pntVals = '';
                for (String s: fnlVals2) {
                    if (String.isNotBlank(s) && (!pntVals.Contains(s))) {
                        pntVals += s + ';';
                    }
                }

                if (cstVals.endsWith(';')) {
                    cstVals = cstVals.removeEnd(';');
                }
                if (pntVals.endsWith(';')) {
                    pntVals = pntVals.removeEnd(';');
                }

                dff.add_ons__c = cstVals;
                dff.udac_points__c = pntVals;

                //System.debug('************DFF_Addons: ' + dff.add_ons__c + '************DFF_UdacPoints: ' + dff.udac_points__c + '************DFF_CustomLinkInfo: ' + dff.cslt_url__c + '----' + dff.cslt_text__c + '************DFF_CouponInfo: ' + dff.coupon_description__c + '----' + dff.coupon_url__c + '----' + dff.coupon_text_offer_1__c + '----' + dff.coupon_text_offer_2__c + '----' + dff.coupon_text_offer_3__c + '************DFF_CoreOpptyId: ' + dff.Core_Opportunity_Line_ID__c);

                updtDffs.add(dff);

                delYpcAdn.addall(spVals);
                //trkDffs.add(new Track_DFF_Update__c(Data_Fulfillment_Form__c = dff.Id, Sent_Date__c = system.today(), Status__c = 'Applied', Updated_Info__c = 'add_ons__c: ' + cstVals + '\n' + 'udac_points__c: ' + pntVals, Add_Ons__c = true));

            }
        }

        if (updtDffs.size() > 0) {
            System.debug('************DFFsToUPdate************' + updtDffs);
            //update updtDffs;
        }

        if (trkDffs.size() > 0) {
            //insert trkDffs;
        }

        if (delYpcAdn.size() > 0) {
            for (YPC_AddOn__c adn: delYpcAdn) {
                delYpdAdnMap.put(adn.Id, adn);
            }
            //delete delYpdAdnMap.values();
        }
    }

    global void finish(Database.BatchableContext BC) {

        AsyncApexJob a = [Select Id, Status, ExtendedStatus, NumberOfErrors, JobItemsProcessed,
            TotalJobItems, CreatedBy.Email
            from AsyncApexJob where Id = : BC.getJobId()
        ];

        //Send email to Apex job submitter notifying of job completion 
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        List < String > admEmails = Label.Fulfillment_Batch_Notification_Emails.split(',');
        //admEmail.add(a.CreatedBy.Email);
        mail.setToAddresses(admEmails);
        mail.setSubject('Batch Job For Data Migrated YPC Add-on Patch has ' + a.Status + ' in BQA1');
        mail.setPlainTextBody('Batch Job has processed Total of ' + a.TotalJobItems +
            ' batches with ' + a.NumberOfErrors + ' failures');
        Messaging.sendEmail(new Messaging.SingleEmailMessage[] {
            mail
        });

    }
}