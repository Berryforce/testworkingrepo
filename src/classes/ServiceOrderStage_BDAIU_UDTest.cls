@isTest (SeeAllData=true)
public without sharing class ServiceOrderStage_BDAIU_UDTest{   
    private static void testTrigger() {
        CRMfusionDBR101.DB_Globals.triggersDisabled = true;
        sObject testData = CRMfusionDBR101.DB_TriggerHandler.createTestData( Listing__c.getSObjectType() );
        Test.startTest();
        insert testData;
        update testData;
        CRMfusionDBR101.DB_Globals.generateCustomTriggerException = true;
        update testData;
        delete testData;
        Test.stopTest();
    }
}