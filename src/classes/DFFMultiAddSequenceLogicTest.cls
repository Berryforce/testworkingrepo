@isTest
public class DFFMultiAddSequenceLogicTest {
    static testMethod void DFFMultiAddSequenceTest() {
        
        Database.BatchableContext bc;        
        Account acct = TestMethodsUtility.generateCustomerAccount();
        insert acct;
        Contact cnt = TestMethodsUtility.createContact(acct.Id);
        Order__c ord = TestMethodsUtility.createOrder(acct.Id);
        Opportunity oppty = TestMethodsUtility.createOpportunity(acct, cnt);
        insert oppty;
        Order_Group__c og = TestMethodsUtility.createOrderSet(acct, ord, oppty);
        Order_Line_Items__c oln = TestMethodsUtility.createOrderLineItem(acct, cnt, oppty, ord, og);
        Digital_Product_Requirement__c objDFF=TestMethodsUtility.generatePrintGraphic();
        objDFF.OrderLineItemID__c=oln.Id;
        objDFF.Sequence__c=1;
        objDFF.URN_number__c=1234;
        insert objDFF;
        list<Order_Group__c> ordersetLst = new list<Order_Group__c>();
        ordersetLst = [Select Id from Order_Group__c where Id=:og.Id];
        Test.startTest();
        DFFMultiAddSequenceLogic obj = new DFFMultiAddSequenceLogic();
        obj.start(bc);
        obj.execute(bc,ordersetLst);
        obj.finish(bc);
        Test.stopTest();
    }
    
    
}