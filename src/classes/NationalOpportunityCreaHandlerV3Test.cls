@IsTest
public class NationalOpportunityCreaHandlerV3Test{
    public static testmethod void NationalOpportunityCreationHandlerTest(){
        
        
        Canvass__c c=TestMethodsUtility.generateCanvass();
        c.Billing_Entity__c='Test';
        insert c;
        
        list<Account> lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        lstAccount.add(TestMethodsUtility.generateAccount('customer'));
        lstAccount.add(TestMethodsUtility.generateAccount('publication'));
        insert lstAccount;  
        Account newAccount = new Account();
        Account newPubAccount = new Account();
        Account newTelcoAccount = new Account();
        for(Account iterator : lstAccount) {
            if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
                newAccount = iterator;
            }
            else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
                newPubAccount = iterator;
            }
            else {
                newTelcoAccount = iterator;
            }
        }
        Telco__c objTelco =TestMethodsUtility.createTelco(newTelcoAccount.Id);
        //Telco__c objTelco = [Select Id, Name from Telco__c where Account__c =:newTelcoAccount.Id Limit 1];
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
        
        Account objCMRAcc = TestMethodsUtility.generateCMRAccount();
        objCMRAcc.Is_Active__c = true;
        insert objCMRAcc;
        
        system.assertNotEquals(newTelcoAccount.ID, null);
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Division__c objDiv = TestMethodsUtility.createDivision();
        
        Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Recives_Electronice_File__c=true;
        objDir.Telco_Provider__c = objTelco.Id;
        objDir.Canvass__c = newAccount.Primary_Canvass__c;        
        objDir.Publication_Company__c = newPubAccount.Id;
        objDir.Division__c = objDiv.Id;
        objDir.Directory_Code__c = '100000';
        insert objDir;
        
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
        Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
        insert objSHM;
        
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
        objDirEd .New_Print_Bill_Date__c=date.today();
        objDirEd .Bill_Prep__c=date.parse('01/01/2013');
        objDirEd.XML_Output_Total_Amount__c=100;
        objDirEd.Pub_Date__c =Date.today().addMonths(1);
        insert objDirEd;
        
        Directory_Edition__c objDirE = new Directory_Edition__c();
        objDirE.Name = 'Test DirE1';
        objDirE.Directory__c = objDir.Id;
        objDirE.Letter_Renewal_Stage_1__c = system.today();
        objDirE.Sales_Lockout__c=Date.today().addDays(30);
        objDirE.book_status__c='NI';
        objDirE.Pub_Date__c =Date.today().addMonths(2);
        insert objDirE;
        
        Product2 objProd = new Product2();
        objProd.Name = 'Test';
        objProd.Product_Type__c = 'Print';
        objProd.ProductCode = 'WLCSH';
        objProd.Print_Product_Type__c='Display';
        insert  objProd;
        
        Product2 objProd1 = new Product2();
        objProd1.Name = 'Test';
        objProd1.Product_Type__c = 'Print';
        objProd1.ProductCode = 'GC50';
        objProd1.Print_Product_Type__c='Specialty';          
        insert objProd1;
        
        Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        insert newOpportunity;
        
        Product2 newProduct = TestMethodsUtility.generateproduct();
        newProduct.Product_Type__c = CommonMessages.oliPrintProductType;
        newProduct.Inventory_Tracking_Group__c = 'YP Leader Ad';
        newProduct.ProductCode='SS';
        insert newProduct;
        
        PricebookEntry pbe = new PricebookEntry(UnitPrice = 0, Product2Id = newProduct.ID, Pricebook2Id = newPriceBook.id, IsActive = true);
        insert pbe;
        
        National_Staging_Order_Set__c objNSOS = TestMethodsUtility.generateManualNSRT();
        objNSOS.CMR_Name__c = objCMRAcc.id;
        
        objNSOS.Is_Ready__c = true;
        objNSOS.Directory__c = objDir.id;
        insert objNSOS;
        
        List<National_Staging_Line_Item__c> ListNSLI=new List<National_Staging_Line_Item__c>();
        National_Staging_Line_Item__c objNSLI = TestMethodsUtility.generateNationalStagingLineItemCreation(objNSOS);
        objNSLI.Ready_for_Processing__c = true;
        objNSLI.UDAC__c='SS';
        objNSLI.Standing_Order__c=true;
        objNSLI.National_Staging_Header__c = objNSOS.id;
        objNSLI.Is_Changed__c=true;
        objNSLI.Product2__c=newProduct.Id;
        objNSLI.Sales_Rate__c=100.0;
        ListNSLI.add(objNSLI);
        
        National_Staging_Line_Item__c objNSLI1 = TestMethodsUtility.generateNationalStagingLineItemCreation(objNSOS);
        objNSLI1.Ready_for_Processing__c = true;
        objNSLI1.UDAC__c='SS';
        objNSLI1.Standing_Order__c=true;
        objNSLI1.National_Staging_Header__c = objNSOS.id;
        objNSLI1.Is_Changed__c=true;
        objNSLI1.Product2__c=newProduct.Id;
        objNSLI.Sales_Rate__c=101.0;
        ListNSLI.add(objNSLI1);
        
        National_Staging_Line_Item__c objNSLI2 = TestMethodsUtility.generateNationalStagingLineItemCreation(objNSOS);
        objNSLI2.Ready_for_Processing__c = true;
        objNSLI2.New_Transaction__c =true;
        objNSLI2.Standing_Order__c=true;
        objNSLI2.National_Staging_Header__c = objNSOS.id;
        objNSLI2.Is_Changed__c=true;
        objNSLI2.Product2__c=newProduct.Id;
        objNSLI.Sales_Rate__c=102.0;
        ListNSLI.add(objNSLI2);
       
        insert ListNSLI;
        OpportunityLineItem ol1 = new OpportunityLineItem ();
        ol1.OpportunityId = newOpportunity.Id; 
        ol1.PricebookEntryId = pbe.Id;
        ol1.Quantity = 1;
        ol1.UnitPrice = 1;
        ol1.Discount=0.0;
        ol1.UDAC__c='STR';
        ol1.Section_Code__c='010';
        ol1.Type__c='Caption';
        ol1.Heading_Code__c='0123';
        ol1.Advertising_Data__c='TEST';
        ol1.Action__c='I';
        ol1.Heading__c='TEXT';
        ol1.Distribution_Area__c='US';
        ol1.Directory_Heading__c=objDH.Id;
        ol1.CMR_Number__c='TEST';
        ol1.CMR_Name__c=objCMRAcc.Id;
        ol1.Directory__c=objDir.Id;
        ol1.Directory_Edition__c=objDirE.Id;
        ol1.Full_Rate__c=100.00;
        ol1.Line_Number__c='100';
        ol1.NAT__c='N';
        ol1.Publication_Date__c=Date.today();
        ol1.Trans__c='I';
        ol1.DAT__c='N';
        ol1.SPINS__c='A';
        ol1.National_Staging_Line_Item__c=ListNSLI[0].Id;
        
        insert ol1;
        
        OpportunityLineItem ol2 = new OpportunityLineItem ();
        ol2.OpportunityId = newOpportunity.Id; 
        ol2.PricebookEntryId = pbe.Id;
        ol2.Quantity = 1;
        ol2.UnitPrice = 1;
        ol2.Discount=0.0;
        ol2.UDAC__c='STR';
        ol2.Section_Code__c='010';
        ol2.Type__c='Caption';
        ol2.Heading_Code__c='0123';
        ol2.Advertising_Data__c='TEST';
        ol2.Action__c='I';
        ol2.Heading__c='TEXT';
        ol2.Distribution_Area__c='US';
        ol2.Directory_Heading__c=objDH.Id;
        ol2.CMR_Number__c='TEST';
        ol2.CMR_Name__c=objCMRAcc.Id;
        ol2.Directory__c=objDir.Id;
        ol2.Directory_Edition__c=objDirE.Id;
        ol2.Full_Rate__c=100.00;
        ol2.Line_Number__c='100';
        ol2.NAT__c='N';
        ol2.Publication_Date__c=Date.today();
        ol2.Trans__c='I';
        ol2.DAT__c='N';
        ol2.SPINS__c='A';
        ol2.National_Staging_Line_Item__c=ListNSLI[0].Id;
        ol2.Trade_Mark_Parent_Id__c=ol1.Id;
        
        insert ol2;
        Test.startTest(); 
        NationalOpportunityCreationHandler_v3.NationalOrderProcessByManually(new Set<id>{objNSOS.id});
        NationalOpportunityCreationHandler_v3.NationalOrderProcessByCron();
        Test.stopTest(); 
    }
}