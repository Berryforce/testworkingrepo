public with sharing class v_DirectoryProductMappingSOQLMethods {
    public static List<Directory_Product_Mapping__c> getDirProdMapByProdId(set<Id> prodIds){
        return [SELECT Id, Directory__c, Product2__c FROM Directory_Product_Mapping__c WHERE Product2__c IN : prodIds];
    }
    
    public static List<Directory_Product_Mapping__c> getDirProdMapByDirCode(String dirCode){
        return [SELECT Id, Quantity_Remaining__c, Quantity__c, Product2__r.Name, Product2__r.ProductCode 
                FROM Directory_Product_Mapping__c WHERE
                Requires_Inventory_Tracking__c = TRUE AND Inventory_Tracking_Group__c != 'YP Leader Ad' AND Directory__r.Directory_Code__c =: dirCode];
    }
    
    public static List<Directory_Product_Mapping__c> getDirProdMapByProdIdAndDirId(set<Id> prodIds, set<Id> dirIds){
        return [SELECT Id, Directory__c, Product2__c, Quantity_Remaining__c,FullRate__c, 
                Foreign_EAS_WP_Ban_Bill_Remaining__c, Requires_Inventory_Tracking__c, strProductInventoryCombo__c
                FROM Directory_Product_Mapping__c WHERE Directory__c IN : dirIds AND Product2__c IN : prodIds];
    }
    
    public static List<Directory_Product_Mapping__c> getDirProdMapByUdacAndDirId(set<string> setStrUdac, set<Id> setDirIds){
        return [SELECT Id, Directory__c, Product2__c, Product_Code_UDAC__c
                FROM Directory_Product_Mapping__c WHERE Directory__c IN : setDirIds AND Product_Code_UDAC__c IN : setStrUdac];
    }
    
    public static list<Directory_Product_Mapping__c> fetchDPMByDirectoryId(set<Id> setDirID) {
        return [Select Product2__r.LOY__c,Product2__r.AllowX5Discount__c, Product2__r.AllowX10Discount__c, Product2__r.AllowX75Discount__c, Product2__r.AllowX80Discount__c, Product2__r.AllowX85Discount__c, Product2__r.AllowX90Discount__c, Product2__r.AllowX95Discount__c,Product2__r.AllowX70Discount__c, Product2__r.AllowX65Discount__c, Product2__r.AllowX60Discount__c, 
        Product2__r.Allow_X20_Discount__c,Product2__r.AllowX55Discount__c, Product2__r.AllowX50Discount__c, Product2__r.AllowX45Discount__c, Product2__r.AllowX40Discount__c, 
        Product2__r.AllowX35Discount__c, Product2__r.AllowX30Discount__c, Product2__r.AllowX25Discount__c, Product2__r.AllowX15Discount__c, 
        Product2__r.IsActive, Product2__r.ProductCode, Product2__r.Name, Product2__c, Name, Id, National_Full_Rate__c, FullRate__c, 
        Directory__r.Publication_Company_Code__c, Directory__r.Publication_Company__c, Directory__r.Publication_Company_Name__c, 
        Product2__r.Print_Product_Type__c,Product2__r.Description, Product2__r.Product_Type__c, Product2__r.Trademark_Product__c,Local_Only__c,                 //Added New Field :: Kishore_vertex
        National_Only__c,Grandfathered__c,is_Active__c,Directory__r.IsCompanion__c,Exclude_National_Upcharge__c,
        Directory__r.Directory_Code__c, Directory__r.Name, Directory__c,Product_Code_UDAC__c, Directory__r.National_Rate_Up_for_Companion__c From Directory_Product_Mapping__c 
        where is_Active__c = true and Directory__c  IN:setDirID order by Product2__r.ProductCode, Directory__c];
    }  
    
     public static list<Directory_Product_Mapping__c> LocalfetchDPMByDirectoryId(set<Id> setDirID) {
        return [Select Product2__r.LOY__c,Product2__r.AllowX5Discount__c, Product2__r.AllowX10Discount__c, Product2__r.AllowX75Discount__c, Product2__r.AllowX80Discount__c, Product2__r.AllowX85Discount__c, Product2__r.AllowX90Discount__c, Product2__r.AllowX95Discount__c,Product2__r.AllowX70Discount__c, Product2__r.AllowX65Discount__c, Product2__r.AllowX60Discount__c, 
        Product2__r.Allow_X20_Discount__c,Product2__r.AllowX55Discount__c, Product2__r.AllowX50Discount__c, Product2__r.AllowX45Discount__c, Product2__r.AllowX40Discount__c, 
        Product2__r.AllowX35Discount__c, Product2__r.AllowX30Discount__c, Product2__r.AllowX25Discount__c, Product2__r.AllowX15Discount__c, 
        Product2__r.IsActive, Product2__r.ProductCode, Product2__r.Name, Product2__c, Name, Id, National_Full_Rate__c, FullRate__c, 
        Directory__r.Publication_Company_Code__c, Directory__r.Publication_Company__c, Directory__r.Publication_Company_Name__c, 
        Product2__r.Print_Product_Type__c,Product2__r.Description, Product2__r.Product_Type__c, Product2__r.Trademark_Product__c,Local_Only__c,                 //Added New Field :: Kishore_vertex
        National_Only__c,Grandfathered__c,is_Active__c,Directory__r.IsCompanion__c,Exclude_National_Upcharge__c,
        Directory__r.Directory_Code__c, Directory__r.Name, Directory__c,Product_Code_UDAC__c,Directory__r.National_Rate_Up_for_Companion__c From Directory_Product_Mapping__c 
        where is_Active__c = true and Directory__c  IN:setDirID and ((Local_Only__c=true  and  National_Only__c=false) OR ( Local_Only__c=false and  National_Only__c=false)) order by Product2__r.ProductCode, Directory__c];
    }  
    
     public static list<Directory_Product_Mapping__c> NationalfetchDPMByDirectoryId(set<Id> setDirID) {
        return [Select Product2__r.LOY__c,Product2__r.AllowX5Discount__c, Product2__r.AllowX10Discount__c, Product2__r.AllowX75Discount__c, Product2__r.AllowX80Discount__c, Product2__r.AllowX85Discount__c, Product2__r.AllowX90Discount__c, Product2__r.AllowX95Discount__c,Product2__r.AllowX70Discount__c, Product2__r.AllowX65Discount__c, Product2__r.AllowX60Discount__c, 
        Product2__r.Allow_X20_Discount__c,Product2__r.AllowX55Discount__c, Product2__r.AllowX50Discount__c, Product2__r.AllowX45Discount__c, Product2__r.AllowX40Discount__c, 
        Product2__r.AllowX35Discount__c, Product2__r.AllowX30Discount__c, Product2__r.AllowX25Discount__c, Product2__r.AllowX15Discount__c, 
        Product2__r.IsActive, Product2__r.ProductCode, Product2__r.Name, Product2__c, Name, Id, National_Full_Rate__c, FullRate__c, 
        Directory__r.Publication_Company_Code__c, Directory__r.Publication_Company__c, Directory__r.Publication_Company_Name__c, 
        Product2__r.Print_Product_Type__c,Product2__r.Description, Product2__r.Product_Type__c, Product2__r.Trademark_Product__c,Local_Only__c,                 //Added New Field :: Kishore_vertex
        National_Only__c,Grandfathered__c,is_Active__c,Directory__r.IsCompanion__c,Exclude_National_Upcharge__c,
        Directory__r.Directory_Code__c, Directory__r.Name, Directory__c,Product_Code_UDAC__c,Directory__r.National_Rate_Up_for_Companion__c From Directory_Product_Mapping__c 
        where is_Active__c = true and Directory__c  IN:setDirID and ((Local_Only__c=false and  National_Only__c=true) OR ( Local_Only__c=false and  National_Only__c=false)) order by Product2__r.ProductCode, Directory__c];
    }  
    
   
   
    public static map<id,Directory_Product_Mapping__c> getDirProdMapByDirIdAndProducts(set<id> setProduct, Id idDir){
        return new map<id,Directory_Product_Mapping__c>([select id,Product2__c from Directory_Product_Mapping__c where Directory__c = :idDir and Product2__c in :setProduct]);
    }
    
    
    
     // for Local PDF  start  
      public static list<Directory_Product_Mapping__c> fetchDPMByDirectoryId_Local(set<Id> setDirID) {
        return [Select Product2__r.LOY__c, Product2__r.AllowX5Discount__c, Product2__r.AllowX10Discount__c, Product2__r.AllowX75Discount__c, Product2__r.AllowX80Discount__c, Product2__r.AllowX85Discount__c, Product2__r.AllowX90Discount__c, Product2__r.AllowX95Discount__c, Product2__r.AllowX70Discount__c, Product2__r.AllowX65Discount__c, Product2__r.AllowX60Discount__c, 
        Product2__r.AllowX55Discount__c, Product2__r.AllowX50Discount__c, Product2__r.AllowX45Discount__c, Product2__r.AllowX40Discount__c, 
        Product2__r.AllowX35Discount__c, Product2__r.AllowX30Discount__c, Product2__r.AllowX25Discount__c, Product2__r.AllowX15Discount__c,Product2__r.Allow_X20_Discount__c, 
        Product2__r.IsActive, Product2__r.ProductCode, Product2__r.Name, Product2__c, Name, Id, National_Full_Rate__c, FullRate__c, 
        Directory__r.Publication_Company_Code__c, Directory__r.Publication_Company__c, Directory__r.Publication_Company_Name__c, 
        Product2__r.Print_Product_Type__c,Product2__r.Description, Product2__r.Product_Type__c, Product2__r.Trademark_Product__c,Local_Only__c,Exclude_National_Upcharge__c,                 
        National_Only__c,Grandfathered__c,is_Active__c,Directory__r.Directory_Code__c, Directory__r.Name, Directory__c ,Directory__r.IsCompanion__c,Directory__r.National_Rate_Up_for_Companion__c From Directory_Product_Mapping__c 
        where is_Active__c = true and Grandfathered__c= false and  ((National_Only__c= false and Local_Only__c= true) OR (National_Only__c= false and Local_Only__c= false))
        
        and Directory__c  IN:setDirID order by Product2__r.ProductCode, Directory__c];
    }
   // End
    
    
    // for Nation PDF Start
     public static list<Directory_Product_Mapping__c> fetchDPMByDirectoryId_National(set<Id> setDirID) {
        return [Select Product2__r.LOY__c,Product2__r.AllowX5Discount__c, Product2__r.AllowX10Discount__c, Product2__r.AllowX75Discount__c, Product2__r.AllowX80Discount__c, Product2__r.AllowX85Discount__c, Product2__r.AllowX90Discount__c, Product2__r.AllowX95Discount__c, Product2__r.AllowX70Discount__c, Product2__r.AllowX65Discount__c, Product2__r.AllowX60Discount__c, 
        Product2__r.AllowX55Discount__c, Product2__r.AllowX50Discount__c, Product2__r.AllowX45Discount__c, Product2__r.AllowX40Discount__c, 
        Product2__r.AllowX35Discount__c, Product2__r.AllowX30Discount__c, Product2__r.AllowX25Discount__c, Product2__r.AllowX15Discount__c,Product2__r.Allow_X20_Discount__c,
        Product2__r.IsActive, Product2__r.ProductCode, Product2__r.Name, Product2__c, Name, Id, National_Full_Rate__c, FullRate__c, 
        Directory__r.Publication_Company_Code__c, Directory__r.Publication_Company__c, Directory__r.Publication_Company_Name__c, 
        Product2__r.Print_Product_Type__c,Product2__r.Description, Product2__r.Product_Type__c, Product2__r.Trademark_Product__c,Local_Only__c,                
        National_Only__c,Grandfathered__c,is_Active__c,Directory__r.IsCompanion__c,Exclude_National_Upcharge__c,
        Directory__r.Directory_Code__c, Directory__r.Name, Directory__c,Product_Code_UDAC__c,Directory__r.National_Rate_Up_for_Companion__c From Directory_Product_Mapping__c 
        where is_Active__c = true and Grandfathered__c= false and  ((National_Only__c= true and Local_Only__c= false) OR (National_Only__c= false and Local_Only__c= false)) 
        and Directory__c  IN:setDirID order by Product2__r.ProductCode, Directory__c];
    }
    // End
    
    
}