public with sharing class TestCheckbox {

    //Getter and Setters
    public String dffId {
        get;
        set;
    }
    public String orderSetId {
        get;
        set;
    }
    public String mOrderSetId {
        get;
        set;
    }
    public String accountId {
        get;
        set;
    }
    public String fpId {
        get;
        set;
    }
    public String profileId {
        get;
        set;
    }    
    public Integer fpCnt {
        get;
        set;
    }    
    public List < Digital_Product_Requirement__c > allDFFs {
        get;
        set;
    }
    public Map < Id, Fulfillment_Profile__c > mapFProfile {
        get;
        set;
    }
    public list < wrapperFpClass > lstFpWrapper {
        get;
        set;
    }    
    private Digital_Product_Requirement__c dff;
    
    public TestCheckbox(ApexPages.StandardController controller) {
        if (!Test.isRunningTest()) {
            List < String > flds = new List < String > {
                'Id', 'Name', 'Test_PackageId__c', 'OrderLineItemID__r.Order_Group__c', 'ModificationOrderLineItem__r.Order_Group__c', 'Account__c', 'Fulfillment_Profile__c'
            };
            controller.addFields(flds);
        }
        this.dff = (Digital_Product_Requirement__c) Controller.getRecord();
    }
    
    public pageReference fpLoad(){

        dffId = dff.Id;
        accountId = dff.Account__c;
        fpId = dff.Fulfillment_Profile__c;
        lstFpWrapper = new list < wrapperFpClass > ();
        
        if (!Test.isRunningTest()) {
            orderSetId = dff.OrderLineItemID__r.Order_Group__c;
            mOrderSetId = dff.ModificationOrderLineItem__r.Order_Group__c;
        } else {
            Digital_Product_Requirement__c dprRcd = [SELECT Id, OrderLineItemID__r.Order_Group__c, ModificationOrderLineItem__r.Order_Group__c, Test_PackageId__c from Digital_Product_Requirement__c where id = : dffId];
            orderSetId = dprRcd.OrderLineItemID__r.Order_Group__c;
            mOrderSetId = dff.ModificationOrderLineItem__r.Order_Group__c;
        }

        allDffs = CommonUtility.DFFFieldLabelNames(orderSetId, mOrderSetId);    

        if (allDffs.size() > 0) {
            system.debug('************DFFs************' + allDffs.size());

            if (String.isNotEmpty(accountId)) {
                mapFProfile = FulfillmentProfileSOQLMethods.getFulFillmentProfileByAccountId(new set < Id > {
                    accountId
                });
                if (mapFProfile.size() > 0) {
                System.debug('************FPList************' + mapFProfile.size());
                    for (Fulfillment_Profile__c iterator: mapFProfile.values()) {
                        if (iterator.id == fpId) {
                        System.debug('************FP************' + iterator);
                            lstFpWrapper.add(new wrapperFpClass(true, iterator));
                        } else {
                        System.debug('************FP************' + iterator);
                            lstFpWrapper.add(new wrapperFpClass(false, iterator));
                        }
                    }
                } else {
                    CommonMethods.addError(CommonMessages.fulfillmentProfileisEmpty);
                    //hideButton = true;
                }
            } else {
                CommonMethods.addError(CommonMessages.dffAccountIsEmpty);
                //hideButton = true;
            }
        }
        fpCnt = lstFpWrapper.size();    
        return null;  
    }
    
    public pageReference updateDff() {
        return null;
    }
    
    public pageReference updateDffFp() {
        if (fpCheck()) {
            Fulfillment_Profile__c objFProfile = mapFProfile.get(profileId);
            Digital_Product_Requirement__c UpdateCurrentDFF = new Digital_Product_Requirement__c(id = dffId, Fulfillment_Profile__c = profileId);
            try {
                update UpdateCurrentDFF;
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Confirm, 'DFF Fulfillment Profile is successfully updated');
                ApexPages.addMessage(myMsg);

            } catch (Exception ex) {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error, 'Error occurred while updating DFF: ' + ex);
                ApexPages.addMessage(myMsg);
            }
        }
        return null;
    }

    private boolean fpCheck() {
        integer iCount = 0;
        for (wrapperFpClass iterator: lstFpWrapper) {
            if (iterator.bFlag == true) {
                iCount++;
                profileId = iterator.objProfile.Id;
            }
        }
        if (iCount == 0) {
            CommonMethods.addError('Please select at least one Fulfillment Profile record');
            return false;
        } else if (iCount > 1) {
            CommonMethods.addError('Please select only one fulfillment profile record');
            profileId = null;
            return false;
        }
        return true;
    }
    
    public class wrapperFpClass {
        public boolean bFlag {
            get;
            set;
        }
        public Fulfillment_Profile__c objProfile {
            get;
            set;
        }
        public wrapperFpClass(boolean bFlag, Fulfillment_Profile__c objProfile) {
            if (objProfile.Default_Profile__c == true) {
                this.bFlag = bFlag;
                this.objProfile = objProfile;
            } else {
                this.bFlag = bFlag;
                this.objProfile = objProfile;
            }
        }
    }
    
}