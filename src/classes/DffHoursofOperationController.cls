/******************************************************************
Apex Controller Class for users to select custom Hours of Operation
Author: Reddy(reddy.yellanki@theberrycompany.com)
Updated Date: 06/22/2015
*******************************************************************/
public with sharing class DffHoursofOperationController {

    //Getter and Setters
    public String fromHrs {
        get;
        set;
    }
    public String toHrs {
        get;
        set;
    }
    public Boolean mnDy {
        get;
        set;
    }
    public Boolean tsDy {
        get;
        set;
    }
    public Boolean weDy {
        get;
        set;
    }
    public Boolean thDy {
        get;
        set;
    }
    public Boolean frDy {
        get;
        set;
    }
    public Boolean stDy {
        get;
        set;
    }
    public Boolean snDy {
        get;
        set;
    }
    public Boolean refreshPage {
        get;
        set;
    }
    public Boolean disableCopyBtn {
        get;
        set;
    }
    public Boolean cpyAll {
        get;
        set;
    }
    
    public String mFromDflt;
    public String mToDflt;
    public String tFromDflt;
    public String tToDflt;
    public String wFromDflt;
    public String wToDflt;
    public String thFromDflt;
    public String thToDflt;
    public String fFromDflt;
    public String fToDflt;
    public String sFromDflt;
    public String sToDflt;
    public String snFromDflt;
    public String snToDflt;
    public Set<String> dffRTNames = new Set<String>{'Spotzer Website', 'Web.com', 'LocalVox', 'Yodle_Website'};
                            
    public string searchText {
        get {
            if (searchText == null) searchText = 'Acme'; // prefill the serach box for ease of use
            return searchText;
        }
        set;
    }
    public List<Case> searchResults {get;set;}    

    private Digital_Product_Requirement__c dff;
    
    //Constructor
    public DffHoursofOperationController(ApexPages.StandardController controller) {
        if (!Test.isRunningTest()) {
            List < String > flds = new List < String > {
                'Id', 'Name', 'Test_PackageId__c', 'OrderLineItemID__r.Order_Group__c', 'ModificationOrderLineItem__r.Order_Group__c', 'MF__c', 'MT__c', 'DFF_RecordType_Name__c',
                'TF__c', 'TT__c', 'WF__c', 'WT__c', 'ThF__c', 'ThT__c', 'FF__c', 'FT__c', 'SF__c', 'ST__c', 'SnF__c', 'SnT__c', 'is_open_247__c', 'hours__c'
            };
            controller.addFields(flds);
        }
        this.dff = (Digital_Product_Requirement__c) Controller.getRecord();
        refreshPage = false;
        
        mFromDflt = dff.MF__c; mToDflt = dff.MT__c; tFromDflt = dff.TF__c; tToDflt = dff.TT__c; wFromDflt = dff.WF__c; wToDflt = dff.WT__c; 
        thFromDflt = dff.ThF__c; thToDflt = dff.ThT__c; fFromDflt = dff.FF__c; fToDflt = dff.FT__c; sFromDflt = dff.SF__c; sToDflt = dff.ST__c;
        snFromDflt = dff.SnF__c; snToDflt = dff.SnT__c; 
        
        //Hide copy values button if isOpen24/7 is checked on page load
        if(!dff.is_open_247__c){
            disableCopyBtn = true;
        }else{
            disableCopyBtn = false;
        }
    }
    
    //Method to copy hours to user selected days
    public void copyHours() {
        if(cpyAll){
            if (mnDy) {
                dff.MF__c = fromHrs;
                dff.MT__c = toHrs;
                mnDy = false;
            }
            if (tsDy) {
                dff.TF__c = fromHrs;
                dff.TT__c = toHrs;
                tsDy = false;
            }
            if (weDy) {
                dff.WF__c = fromHrs;
                dff.WT__c = toHrs;
                weDy = false;
            }
            if (thDy) {
                dff.ThF__c = fromHrs;
                dff.ThT__c = toHrs;
                thDy = false;
            }
            if (frDy) {
                dff.FF__c = fromHrs;
                dff.FT__c = toHrs;
                frDy = false;
            }
            if (stDy) {
                dff.SF__c = fromHrs;
                dff.ST__c = toHrs;
                stDy = false;
            }
            if (snDy) {
                dff.SnF__c = fromHrs;
                dff.SnT__c = toHrs;
                snDy = false;
            }
            cpyAll = false;        
        } else if (mnDy || tsDy || weDy || thDy || frDy || stDy || snDy) {
            dff.is_open_247__c = false;
            if (mnDy) {
                dff.MF__c = fromHrs;
                dff.MT__c = toHrs;
                mnDy = false;
            }
            if (tsDy) {
                dff.TF__c = fromHrs;
                dff.TT__c = toHrs;
                tsDy = false;
            }
            if (weDy) {
                dff.WF__c = fromHrs;
                dff.WT__c = toHrs;
                weDy = false;
            }
            if (thDy) {
                dff.ThF__c = fromHrs;
                dff.ThT__c = toHrs;
                thDy = false;
            }
            if (frDy) {
                dff.FF__c = fromHrs;
                dff.FT__c = toHrs;
                frDy = false;
            }
            if (stDy) {
                dff.SF__c = fromHrs;
                dff.ST__c = toHrs;
                stDy = false;
            }
            if (snDy) {
                dff.SnF__c = fromHrs;
                dff.SnT__c = toHrs;
                snDy = false;
            }
        } else {
            CommonUtility.msgInfo('Please select days to copy values');
        }
    }
    
    //Method to save user selected values and refresh detail page upon save
    public void dffSave() {
        try {
            update dff;
            refreshPage = true;
        } catch (exception e) {
            CommonUtility.msgError(String.valueof(e.getMessage()));
        }
    }
    
    //Method to instantiate From and To values on custom input hours on UI
    public List < SelectOption > getItems() {
        List < SelectOption > options = new List < SelectOption > ();
        
        Schema.DescribeFieldResult fieldResult =
        Digital_Product_Requirement__c.MF__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        options.add(new SelectOption('', ''));
        
        for( Schema.PicklistEntry f : ple)
        {
            options.add(new SelectOption(f.getLabel(), f.getValue()));
        }       

        return options;
    }
    
    //Method to disable copy values button and blank all hours if isOpen24/7 check box is selected
    public void checkBoxDsbl() {
        if (dff.is_open_247__c) {
            mnDy = false;
            dff.MF__c = '';
            dff.MT__c = '';
            tsDy = false;
            dff.TF__c = '';
            dff.TT__c = '';
            weDy = false;
            dff.WF__c = '';
            dff.WT__c = '';
            thDy = false;
            dff.ThF__c = '';
            dff.ThT__c = '';
            frDy = false;
            dff.FF__c = '';
            dff.FT__c = '';
            stDy = false;
            dff.SF__c = '';
            dff.ST__c = '';
            snDy = false;
            dff.SnF__c = '';
            dff.SnT__c = '';
            disableCopyBtn = false;
            cpyall = false;
        }else{
            dff.MF__c = mFromDflt;
            dff.MT__c = mToDflt;
            dff.TF__c = tFromDflt;
            dff.TT__c = tToDflt;
            dff.WF__c = wFromDflt;
            dff.WT__c = wToDflt;
            dff.ThF__c = thFromDflt;
            dff.ThT__c = thToDflt;
            dff.FF__c = fFromDflt;
            dff.FT__c = fToDflt;
            dff.SF__c = sFromDflt;
            dff.ST__c = sToDflt;
            dff.SnF__c = snFromDflt;
            dff.SnT__c = snToDflt;
            disableCopyBtn =true;
            cpyAll = false;
        }
    }
    
    public void copyAllValidate(){
        if(cpyAll & !dff.is_open_247__c){
            mnDy = true;
            tsDy = true;
            weDy = true;
            thDy = true;
            frDy = true;
            stDy = true;
            snDy = true;
        }else{
            mnDy = false;
            tsDy = false;
            weDy = false;
            thDy = false;
            frDy = false;
            stDy = false;
            snDy = false;        
        }
    }
    
    public void openAllDay(){
        if (mnDy) {
            dff.MF__c = '12:00 AM';
            dff.MT__c = '11:59 PM';
            mnDy = false;
        }
        if (tsDy) {
            dff.TF__c = '12:00 AM';
            dff.TT__c = '11:59 PM';
            tsDy = false;
        }
        if (weDy) {
            dff.WF__c = '12:00 AM';
            dff.WT__c = '11:59 PM';
            weDy = false;
        }
        if (thDy) {
            dff.ThF__c = '12:00 AM';
            dff.ThT__c = '11:59 PM';
            thDy = false;
        }
        if (frDy) {
            dff.FF__c = '12:00 AM';
            dff.FT__c = '11:59 PM';
            frDy = false;
        }
        if (stDy) {
            dff.SF__c = '12:00 AM';
            dff.ST__c = '11:59 PM';
            stDy = false;
        }
        if (snDy) {
            dff.SnF__c = '12:00 AM';
            dff.SnT__c = '11:59 PM';
            snDy = false;
        }        
    }
    
    public void dffHoursReqr(){
        if(String.isBlank(dff.hours__c)){
            if(dffRTNames.contains(dff.DFF_RecordType_Name__c) && (String.isBlank(dff.MF__c) && String.isBlank(dff.TF__c) && String.isBlank(dff.WF__c) && String.isBlank(dff.ThF__c) && String.isBlank(dff.FF__c) && String.isBlank(dff.SF__c) && String.isBlank(dff.SnF__c))){
                CommonUtility.msgWarning('Hours of Operation are required for this Product, please go ahead and select individual Hours or choose Is Open 24/7');
            } else if(String.isNotBlank(dff.MF__c) || String.isNotBlank(dff.TF__c) || String.isNotBlank(dff.WF__c) || String.isNotBlank(dff.ThF__c) || String.isNotBlank(dff.FF__c) || String.isNotBlank(dff.SF__c) || String.isNotBlank(dff.SnF__c)){
                CommonUtility.msgWarning('Plese make sure to input All Days for Hours of Operation');
            }        
        }
    }
    
    public PageReference search(){
        if (searchResults == null) {
            searchResults = new List<Case>(); // init the list if it is null
        } else {
            //searchResults.clear(); // clear out the current results if they exist
        }
        searchResults = [SELECT Id, Status, Description, Subject, CaseNumber from Case where Digital_Product_Requirement__c=:dff.Id and Status=:searchText];
        System.debug('************' + searchResults);
        return null;
    }

}