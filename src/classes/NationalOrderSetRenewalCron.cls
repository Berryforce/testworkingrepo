global class NationalOrderSetRenewalCron implements Schedulable,Database.Batchable <sObject>{
    
     global String dtBD;
     global NationalOrderSetRenewalCron(Date FOSDate){
       dtBD = String.valueOf(FOSDate);
       System.debug('Testing Future Order Set Start Date:=='+FOSDate);
    }
    
    global Database.queryLocator start(Database.BatchableContext bc){
        String SOQL='Select id,Name,directory__c from Directory_Edition__c where Future_Order_Start_Date__c='+dtBD+' AND Book_Status__c =\'BOTS\'';
        return Database.getQueryLocator(SOQL);
    }
    
    global void execute(SchedulableContext sc){
        NationalOrderSetRenewalCron  obj = new NationalOrderSetRenewalCron(Date.today());
        Database.executeBatch(obj);
    }
  
    global void execute(Database.BatchableContext bc, list<Directory_Edition__c> DEList) {
        List<Directory_Edition__c> ListDE=new List<Directory_Edition__c>();
        
        if(DEList.size()>0) {
           for(Directory_Edition__c iterator: [Select id,Name,directory__c,(Select Id from National_Staging_Order_Sets__r) from Directory_Edition__c where Id in: DEList]){
               ListDE.add(iterator);
           }
        }
        
      if(ListDE.size()>0)  NationalOrderSetRenewalHandler.RenewNationalOrder(ListDE);
    }
    global void finish(Database.BatchableContext bc) {
      AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email, ExtendedStatus from AsyncApexJob where Id =:BC.getJobId()];
      Set<String> emailaddrSet=new Set<String>();
      emailaddrSet.add(a.CreatedBy.Email);
      List<String> emailaddrLst=new List<String>();
      emailaddrLst.addall(emailaddrSet);
      String strErrorMessage = '';
      if(a.NumberOfErrors > 0){
          strErrorMessage = a.ExtendedStatus;
        }
        CommonEmailUtils.sendHTMLEmail(emailaddrLst, 'NationalOrderSetRenewalCron is ' + a.Status, 'The batch Apex job processed ' + a.TotalJobItems +
          ' batches with '+ a.NumberOfErrors + ' failures.'+strErrorMessage+'.');
  
   }
}