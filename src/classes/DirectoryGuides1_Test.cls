@isTest

private class DirectoryGuides1_Test
{
    static testMethod void DirectoryGuides1()
    {
        Test.StartTest();
            
        Directory_Guide__c objDirGuide = new Directory_Guide__c(Name = 'Test Guide');
        insert objDirGuide;
        
        List<Directory_Guide__c> LstDirguide = new List<Directory_Guide__c>();
        
        for(integer i=0; i<20; i++) 
        {
            LstDirguide.add(new Directory_Guide__c(Name = 'Test Guide'));
        }
        insert LstDirguide;
        
        DirectoryGuides1 pagingclassobj=new DirectoryGuides1();
        pagingclassobj.DirGuideLst.Name='Test Guide';
        pagingclassobj.Search();
        pagingclassobj.previous();
        pagingclassobj.next();
        boolean b1=pagingclassobj.hasNext;
        boolean b2=pagingclassobj.hasPrevious;
        integer i1 = pagingclassobj.pageNumber;

        Test.StopTest();  
    }
}