/******************************************************
Apext Class to invoke Team Track SOAP Webservice Method
Created By: Reddy(reddy.yellanki@theberrycompany.com)
Updated Date: 11/12/2014
$Id$
*******************************************************/
Public class InvokeTTSOAPService1 {

    //Getter and setters
    public boolean result {
        get;
        set;
    }
    private final Case cs;
    public String CaseId {
        get;
        set;
    }
    public List<Digital_Case_History__c> lstDgtlCase = new List<Digital_Case_History__c>();

    //Constructor
    public InvokeTTSOAPService1(ApexPages.StandardController controller) {

        this.cs = (Case) controller.getRecord();

    }

    //Method to Invoke Team Track webservices to submit Case response 
    public pageReference invokeService1() {

        CaseId = this.cs.Id;
        Case csrd = [SELECT Id, Resolution_Note__c, Subject, Status, TeamTrack_tableIdItemId__c, Case_Reason__c, CreatedById, CreatedBy.Name, CreatedBy.Email, Description, Reason FROM Case WHERE Id = : CaseID LIMIT 1];

        if (String.isNotBlank(csrd.TeamTrack_tableIdItemId__c)) {

            //Create the stub  
            localhost80GsoapSbmappservices72Wsdl.sbmappservices72 invkSrvs = new localhost80GsoapSbmappservices72Wsdl.sbmappservices72();

            sbmappservices72.Auth ath = new sbmappservices72.Auth();
            ath.userId = 'bsfdc';
            ath.password = 'B3rrySfdc01!';

            sbmappservices72.ItemIdentifier itemId = new sbmappservices72.ItemIdentifier();
            //ItemId.tableIdItemId='1027:29895';
            itemId.tableIdItemId = csrd.TeamTrack_tableIdItemId__c;

            sbmappservices72.UserIdentifier userIdt = new sbmappservices72.UserIdentifier();
            userIdt.loginId = 'bsfdc';

            sbmappservices72.NoteAttachmentContents ntctn = new sbmappservices72.NoteAttachmentContents();
            ntctn.title = csrd.Subject;
            ntctn.body = csrd.Resolution_Note__c;
            ntctn.accessType = 'ATTACHACCESS-DEFAULT';

            //System.debug('**********' + ath +'------'+ itemId +'------'+ userIdt +'------'+ ntctn);

            try {

                //Track status in a String field, which is coming back from Team Track   
                result = invkSrvs.CreateNoteAttachment(ath, itemId, userIdt, ntctn);

                CommonUtility.msgConfirm(CommonUtility.ttCaseRplySent);

                System.debug('************RESULT************' + result);
                String test = invksrvs.GetVersion();

                //Upon success response, updated Case statu and priority
                csrd.Status = 'Closed';
                csrd.Priority = 'None';

                //Create Digital case history record
                lstDgtlCase.add(new Digital_Case_History__c(Case__c=csrd.Id, Case_Reason__c=csrd.Reason, Description__c=csrd.Description, Resolution_Notes__c=csrd.Resolution_Note__c, subject__c=csrd.Subject, Sent_Date__c=system.Today()));

            } catch (exception e) {

                CommonUtility.msgFatal('Exception: ' + String.valueof(e.getMessage()));

            }

        } else {
            
            if(String.isNotBlank(csrd.Resolution_Note__c)){

            //Update Case status to closed and send email to Case owner
            csrd.Status = 'Closed';

            //Create Digital case history record
            lstDgtlCase.add(new Digital_Case_History__c(Case__c=csrd.Id, Case_Reason__c=csrd.Reason, Description__c=csrd.Description, Resolution_Notes__c=csrd.Resolution_Note__c, subject__c=csrd.Subject, Sent_Date__c=system.Today()));            
            
            string Url = System.URL.getSalesforceBaseUrl().toExternalForm()+'/'+csrd.Id;
            
            CommonUtility.msgConfirm(CommonUtility.ttCaseClosed);

            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {
                csrd.CreatedBy.Email
            };
            mail.setToAddresses(toAddresses);
            mail.setSubject('IC Rep Query Case Reply');
            mail.setPlainTextBody('Dear Case Owner, ' + '\n' + '\n' + 'Your Case has been successfully closed. Please use below link to review your Case' + '\n' + '\n' + 'Link To Case: ' + Url + '\n' + '\n' + '\n' + 'Thank you');
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] {
                mail
            });
                        
            } else {

                CommonUtility.msgFatal(CommonUtility.ttInputResNotes);            
            
            }

        }

        update csrd;

        if(lstDgtlCase.size()>0){
            insert lstDgtlCase;
        }

        return null;

    }

}