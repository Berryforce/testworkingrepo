global class ScheduleLOBRecordDeleteBatch Implements Schedulable {
    global void execute(SchedulableContext sc) {
        LOBRecordDeleteBatch obj = new LOBRecordDeleteBatch();
        Database.executeBatch(obj);
    }
}