//common variables test class
@isTest(SeeAllData=True)
public class CommonVariablesTest{
static testmethod void CVtest(){

Test.starttest();
List<Directory_Listing__c> lstdirlist = new List<Directory_Listing__c> ();
CommonVariables.setLstDirListing(lstdirlist);
CommonVariables.strId = 'Test';

CommonVariables.getLstDirListing();
CommonVariables.setBScopting();

boolean getbscope = CommonVariables.getBScoping();
CommonVariables.reSetBScoping();
CommonVariables.setBOpportunityProductBundleRecursive();
boolean getbopbr = CommonVariables.getBOpportunityProductBundleRecursive();
CommonVariables.reSetBOpportunityProductBundleRecursive();
CommonVariables.setBAccountBulkBillRecursive();
boolean getbabbr = CommonVariables.getBAccountBulkBillRecursive();
CommonVariables.reSetBAccountBulkBillRecursive();
CommonVariables.setOLIRecursive();
boolean gor = CommonVariables.getOLIRecursive();
CommonVariables.reSetOLIRecursive();
CommonVariables.setOLIDoAccountPartnerRecursive();
boolean godapr = CommonVariables.getOLIDoAccountPartnerRecursive();
CommonVariables.reSetOLIDoAccountPartnerRecursive();
CommonVariables.setOLIaccountCanvassLogicRecursive();
boolean goaclr = CommonVariables.getOLIaccountCanvassLogicRecursive();
CommonVariables.reSetOLIaccountCanvassLogicRecursive();
CommonVariables.setOLINextBillingAndProrationRecursive();
boolean goanpr = CommonVariables.getOLINextBillingAndProrationRecursive();
CommonVariables.reSetOLINextBillingAndProrationRecursive();
CommonVariables.setOLIInventoryTrackingRecursive();
boolean gotr = CommonVariables.getOLIInventoryTrackingRecursive();
CommonVariables.reSetOLIInventoryTrackingRecursive();

Test.stoptest();
}
}