@isTest
private Class SOLstSLDPDeleteBatchTest {
    static testmethod void TestSOLstSLDPDeleteBatch() {
        Database.BatchableContext bc;
        List<service_order_stage__c> objSOLst = new List<service_order_stage__c>();
        Service_order_stage__c objSO = TestMethodsUtility.generateSOrecord('Annual Listings');
        insert objSO;
        listing__c objLst = new listing__c(Name ='Test SO',Lst_Last_Name_Business_Name__c='Test SO',Phone__c='(444)442-2244',recordtypeId=system.label.TestListingMainRT,Service_order_stage_item__c= objSO.Id);
        insert objLst;
        directory_listing__c objdirLst = new directory_listing__c(Name ='Test SO',SL_Last_Name_Business_Name__c='Test SO',Phone_Number__c='(444)442-2244',Service_Order_Stage__c=objSO.Id,listing__c=objLst.Id);
        insert objdirLst;
        directory_pagination__c objDP = new directory_pagination__c(Reference_Listing__c=objdirLst.id,DP_Listing__c=objLst.Id);
        insert objDP;
        list<service_order_stage__c> soLst = [SELECT Id,Batch_ID__c,Name,(Select Id from Listings__r),(Select Id from Directory_Listings__r)from service_order_stage__c where Id=:objSO.Id];
        Test.StartTest();
            SOLstSLDPDeleteBatch objSODelBatch = new SOLstSLDPDeleteBatch(objSO.Batch_ID__c);
            database.executebatch(objSODelBatch);
        Test.StopTest();  
   }
}