public with sharing class TestCheckbox1 {

    //Getter and Setters
    public String dffId {
        get;
        set;
    }
    public String orderSetId {
        get;
        set;
    }
    public String mOrderSetId {
        get;
        set;
    }
    public String accountId {
        get;
        set;
    }
    public String fpId {
        get;
        set;
    }
    public String profileId {
        get;
        set;
    }    
    public Integer fpCnt {
        get;
        set;
    }
    public boolean closeStatus {
        get;
        set;
    }        
    public List < Digital_Product_Requirement__c > allDFFs {
        get;
        set;
    }
    public List < Digital_Product_Requirement__c > lstAllDFFs {
        get;
        set;
    }    
    public Map < Id, Fulfillment_Profile__c > mapFProfile {
        get;
        set;
    }
    public Map < Id, Digital_Product_Requirement__c > UpdateDFFMap {
        get;
        set;
    }
       
    private Digital_Product_Requirement__c dff;
    
    public TestCheckbox1(ApexPages.StandardController controller) {
        if (!Test.isRunningTest()) {
            List < String > flds = new List < String > {
                'Id', 'Name', 'Test_PackageId__c', 'OrderLineItemID__r.Order_Group__c', 'ModificationOrderLineItem__r.Order_Group__c', 'Account__c', 'Fulfillment_Profile__c'
            };
            controller.addFields(flds);
        }
        this.dff = (Digital_Product_Requirement__c) Controller.getRecord();
        lstAllDFFs = new List<Digital_Product_Requirement__c>();
        closeStatus = false;
    }
    
    public pageReference fpDffs(){

        dffId = dff.Id;
        accountId = dff.Account__c;
        fpId = dff.Fulfillment_Profile__c;
        
        if (!Test.isRunningTest()) {
            orderSetId = dff.OrderLineItemID__r.Order_Group__c;
            mOrderSetId = dff.ModificationOrderLineItem__r.Order_Group__c;
        } else {
            Digital_Product_Requirement__c dprRcd = [SELECT Id, OrderLineItemID__r.Order_Group__c, ModificationOrderLineItem__r.Order_Group__c, Test_PackageId__c from Digital_Product_Requirement__c where id = : dffId];
            orderSetId = dprRcd.OrderLineItemID__r.Order_Group__c;
            mOrderSetId = dff.ModificationOrderLineItem__r.Order_Group__c;
        }

        allDffs = CommonUtility.DFFFpLabelNames(orderSetId, mOrderSetId);
        
        if(allDffs.size()>0){

            mapFProfile = FulfillmentProfileSOQLMethods.getFulfillmentProfilebyId(new Set < Id > {
                fpId
            });
            Fulfillment_Profile__c objFProfile = mapFProfile.get(fpId);
            List<Digital_Product_Requirement__c> fnlLst = new List<Digital_Product_Requirement__c>();
            UpdateDFFMap = new Map < Id, Digital_Product_Requirement__c > ();
            
            for(Digital_Product_Requirement__c iterator: allDffs){
                if(iterator.Fulfillment_Profile__c == fpId){
                    fnlLst.add(iterator);
                }
            }
            
            if(fnlLst.size()>0){
                for(Digital_Product_Requirement__c iterator: fnlLst){
                    Digital_Product_Requirement__c modifyDFF = CommonMethods.copyValuesFromProfileToDFF(iterator, objFProfile, null);
                    //System.debug('************INSIDE FOR************' + modifyDFF);
                    UpdateDFFMap.put(modifyDFF.id, modifyDFF);
                }            
            }
            
            if (UpdateDFFMap.size() > 0) {
                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info, 'Below DFFs are utilizing this Fuflillment Profile (' + objFProfile.Name + ') and may be impacted by this change. Please review & submit changes as needed.');
                ApexPages.addMessage(myMsg);

                lstAllDFFs.addall(UpdateDFFMap.values());
                System.debug('************INSIDE IF: allDFFs.size()************' + UpdateDFFMap.keyset());
            } else {
                ApexPages.Message myMsg1 = new ApexPages.Message(ApexPages.Severity.Error, 'There are no DFF records associated with this Fulfillment Profile');
                ApexPages.addMessage(myMsg1);
            }
                    
        }
        
        return null;    
    }
    
    public pageReference updateDffs() {
    if(lstAllDFFs.size()>0){
        try {
            update lstAllDFFs;
            closeStatus = true;
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Info, 'Changes applied successfully');
            ApexPages.addMessage(myMsg);
        } catch (Exception ex) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error, 'Error occurred on updating DFFs: ' + ex);
            ApexPages.addMessage(myMsg);
        }    
    }
        return null;            
    }
}