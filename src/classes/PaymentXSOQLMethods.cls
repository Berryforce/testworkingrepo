public with sharing class PaymentXSOQLMethods {
    
    
    public static list<pymt__PaymentX__c> getPaymentxByOpportunityID(set<Id> opportunityIDs) {
        return [SELECT Id, p2f__FF_Sales_Invoice__c, pymt__Opportunity__c FROM pymt__PaymentX__c WHERE pymt__Opportunity__c IN :opportunityIDs];
    }
    
    public static list<pymt__PaymentX__c> getPaymentxByID(set<Id> paymentIDs) {
        return [SELECT Id, pymt__Payment_Method__c,pymt__Status__c ,pymt__Amount__c,pymt__Account__c,p2f__FF_Sales_Invoice__c,pymt__Payment_Type__c FROM pymt__PaymentX__c WHERE Id IN  :paymentIDs];
    }

    @IsTest(SeeAllData=true)
    public static void PaymentXSOQLMethods() {
    }
}