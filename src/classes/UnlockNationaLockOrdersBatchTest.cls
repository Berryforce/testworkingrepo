@isTest
private class UnlockNationaLockOrdersBatchTest{

public static String CRON_EXP = '0 0 0 15 3 ? 2022';
    static testmethod void UnlockNationaLockOrdersBatch() {
       // The query used by the batch job.
       String SOQL = 'SELECT Id, Is_Locked__c FROM Locked_National_Order__c WHERE Is_Locked__c = true';
       
       Test.StartTest();
        Account CMRAcct = TestMethodsUtility.generateCMRAccount();
        CMRAcct.accountnumber='1234';
        CMRAcct.National_Credit_Status__c='Approved';
        CMRAcct.Is_Active__c=true;
        insert CMRAcct;
        Account ClientAcct = TestMethodsUtility.generateNationalAccount();
        ClientAcct.accountnumber='2345';
        ClientAcct.parent=CMRAcct;
        insert ClientAcct;
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        Directory__c dir = TestMethodsUtility.createDirectory();
        Directory_Product_Mapping__c dirProdMap = TestMethodsUtility.createDirectoryProductMapping(dir);
        Directory_Section__c dirSec = TestMethodsUtility.createDirectorySection(dir);
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(dir);
        objDirEd.LSA_Directory_Version__c = '1234';
        objDirEd.Book_Status__c = 'NI';
        insert objDirEd;
        Listing__c listing = TestMethodsUtility.createListing('Main Listing');
        Product2 prod = TestMethodsUtility.createproduct();
        Directory_Product_Mapping__c objDPM = TestMethodsUtility.generateDirectoryProductMapping(dir);
        objDPM.Product2__c = prod.id;
        insert objDPM;
        
        National_Staging_Order_Set__c NSOS = TestMethodsUtility.generateNationalStagingOrderSet('Manual NS RT');
        NSOS.CMR_Name__c = CMRAcct.id;
        NSOS.Client_Name__c = ClientAcct.id;
        NSOS.Client_Name_Text__c = ClientAcct.Name;
        NSOS.Client_Number__c = ClientAcct.Client_Number__c;
        NSOS.CMR_Number__c = CMRAcct.CMR_Number__c;
        NSOS.Directory_Edition_Number__c = '4444';
        NSOS.Directory_Version__c = '1234';
        NSOS.Directory__c = dir.Id;
        NSOS.Directory_Number__c = dir.Directory_Code__c;
        NSOS.Is_Ready__c=false;
        insert NSOS;
        
        
        National_Staging_Line_Item__c NSLI = TestMethodsUtility.generateNationalStagingLineItemCreation(NSOS);
        NSLI.Line_Number__c = '11122';
        NSLI.UDAC__c = prod.ProductCode;
        NSLI.National_Graphics_ID__c='GRAPH1234567';
        NSLI.Is_Changed__c=true;
        insert NSLI;

        Locked_National_Order__c lockNSOS=new Locked_National_Order__c ();
        lockNSOS.Is_Locked__c =true;
        lockNSOS.Locked_By__c=UserInfo.getUserId();
        lockNSOS.National_Staging_Order_Set__c=NSOS.Id;
        insert lockNSOS;
        
       UnlockNationaLockOrdersBatch c = new UnlockNationaLockOrdersBatch();
       ID batchprocessid = Database.executeBatch(c);
       
       String jobId = System.schedule('ScheduleApexClassTest',
                        CRON_EXP, 
                        new UnlockNationaLockOrdersBatch());
       
       
       CronTrigger ct = [SELECT Id, CronExpression, TimesTriggered, NextFireTime FROM CronTrigger WHERE id = :jobId];

      // Verify the expressions are the same
         System.assertEquals(CRON_EXP, ct.CronExpression);
         
       Test.stopTest();

       Locked_National_Order__c lock = [SELECT id,Is_Locked__c FROM Locked_National_Order__c where National_Staging_Order_Set__c=:NSOS.Id];
       System.assertEquals(false,lock.Is_Locked__c);
    }
}