@RestResource(urlMapping='/attachCancellationLetter/*')
Global class attachCancellationLetterToCase{
     @HttpPost
    global static void attatchPdf(List<Id> caseIdList) {
        List<Attachment> attachListForInsert =new List<Attachment>(); 
        for(Id recordId:caseIdList)
        {
            PageReference pg = Page.CancellationLetter;
            pg.getParameters().put('caseid', recordId);
            Blob b;
            if(Test.isRunningTest())
                b=Blob.valueOf('Testing this class');
            else
                b = pg.getContentAsPDF();
            attachListForInsert.add(new Attachment(Name='Cancellation Letter-'+Date.today()+'.pdf',ParentId=recordId,body=b));
        }
        if(attachListForInsert.size()>0)
        {
            insert attachListForInsert;
        }

    }
}