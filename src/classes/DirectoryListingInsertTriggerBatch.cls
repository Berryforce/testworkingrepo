global class DirectoryListingInsertTriggerBatch implements Database.Batchable<sObject>{
    
    global string filtercondition;
    
    global DirectoryListingInsertTriggerBatch(String condition){
        filtercondition = condition;
    }
    
     global Database.QueryLocator start(Database.BatchableContext bc) {
        String query = 'Select Id, IsDeleted, SL_Telco_Sort_Order__c,SL_Under_Caption_Listing__c,Name, RecordTypeId, CreatedDate, CreatedById, LastModifiedDate, LastModifiedById, SystemModstamp, LastActivityDate, LastViewedDate, LastReferencedDate, Listing__c, CLEC_Provider__c, Directory_Heading__c, Directory_Section__c, Directory__c, Disconnected_Parent_Listing__c, Free_WP_Directory_Listing__c, Product__c, Service_Order_Stage__c, Suppressed_By_OLI__c, Telco_Provider__c, Under_Caption__c, Under_Sub_Caption__c, Address_Override__c, Address__c, Area_Code_WF__c, Area_Code__c, Bus_Main_or_Additional__c, Bus_Res_Gov_Indicator__c, CORE_Migration_ID__c, Caption_Display_Text__c, Caption_Header__c, Caption_Member__c, Caption_Type__c, Created_by_Batch__c, Cross_Reference_Indicator__c, Cross_Reference_Text__c, DataFeedType__c, Designation__c, Directory_Code__c, Directory_Listing_External_ID__c, Directory_Section_Code__c, Disconnect_Reason__c, Disconnected_Via_BOC_Purge__c, Disconnected__c, Effective_Date__c, Exchange_WF__c, Exchange__c, First_Name__c, Honorary_Title__c, Include_Monitoring_Symbol__c, Indent_Level__c, Indent_Order__c, IsAddress__c, IsName__c, IsPhoneNumber__c, L_City__c, L_Country__c, L_PO_Box__c, L_Postal_Code__c, L_State__c, L_Street_Number__c, L_Street__c, Left_Telephone_Phrase__c, Line_WF__c, Line__c, Lineage_Title__c, Listing_City__c, Listing_Country__c, Listing_Locality__c, Listing_PO_Box__c, Listing_Postal_Code__c, Listing_State__c, Listing_Street_Number__c, Listing_Street__c, Manual_Sort_As_Override__c, Normalized_Designation__c, Normalized_First_Name__c, Normalized_Honorary_Title__c, Normalized_Last_Name_Business_Name__c, Normalized_Lineage_Title__c, Normalized_Listing_City__c, Normalized_Listing_PO_Box__c, Normalized_Listing_Postal_Code__c, Normalized_Listing_Street_Name__c, Normalized_Listing_Street_Number__c, Normalized_Phone__c, Normalized_Secondary_Surname__c, Omit_Address_OAD__c, Original_Product__c, Phone_For_Ext_ID__c, Phone_Number_Unformatted__c, Phone_Number__c, Phone_Override_Indicator__c, Phone_Override__c, Phone_Type__c, Product_Code_UDAC__c, Right_Telephone_Phrase__c, Salutation__c, Secondary_Surname__c, Section_Page_Type_Lookup__c, Section_Page_Type__c, Service_Order__c, Sort_As__c, StrBOCDisconnect__c, Street_For_Ext_ID__c, StringDLExistMatch__c, Suppressed__c, Telco_Sort_Order__c, Updated_From__c, strDLcombo__c, strSOSCombination__c, strSuppressionCheck__c, Caption_Address__c, Current_NI_Directory_Edition_SFDC_ID__c, CASESAFEID__c, Caption_Data__c, Data_Feed_Type_2__c, Workflow_Fire_Field__c, ABD_Pending_Review__c, Listing_Full_Name__c, CORE_Caption_Migration_Group_ID__c, Do_Not_Suppress__c, DL_DM_isTriggerExecuted__c, SL_strScopedlistingMatched__c, Multi_Scoped_Appearance__c, Sort_As_Full__c, Do_Not_Create_YP__c, Omit_Phone_OTN__c, strAreaCodeExchange__c, Caption_Header_Enhanced_By__c, Enhanced_By_OLI_Art_URN__c, Enhanced_By_OLI_Con__c, WP_Logo_Product__c, SL_StrPhoneDLDS__c, Follows_Listing_Record2__c, ScopedListing_Unique_ID__c, SL_Scoped_Listing_Number__c, ListingNumber__c, Order_Line_Item_Caption_Header__c, NOSO_CAIN_Sort_As__c, SL_DirTelcoProvider__c, SL_Directory_Heading_Code__c, SL_Directory_Heading_Name__c, SL_Is_Multi_Scoped__c, SL_OLI_Caption_Header_UDAC__c, SL_Print_Product_Type__c, SL_Print_Speciality_Product_Type__c, SL_Under_Caption_UDAC__c, Section_Heading_Mapping_Not_Found__c, SL_Original_Scoped_Listing_Id__c, SL_Last_Name_Business_Name__c, SL_Do_Not_Create_Pagination_Record__c, Follows_Listing_Record__c, Directory_Heading__r.Name,Directory_Heading__r.code__c,Directory_Section__r.Section_Code__c,Product__r.Print_Specialty_Product_Type__c,Product__r.ProductCode, Product__r.Print_Product_Type__c FROM Directory_Listing__c '+filtercondition;
        return Database.getQueryLocator(query);  
     }
     
     global void execute(Database.BatchableContext bc, List<Directory_Listing__c> dirListingList) {
        Savepoint sp = Database.setSavepoint();
       
        try{
             List<Directory_Listing__c> reqdDirListingList=new List<Directory_Listing__c>();
             Map<Id,Directory_Listing__c> dirListingMap=new Map<Id,Directory_Listing__c>();
             dirListingMap.putAll(dirListingList);
             List<Directory_Listing__c> dirListWithDPList=[select id,Disconnected__c,Suppressed__c,(select id from Directory_Paginations__r) from Directory_Listing__c where id IN :dirListingList];
             for(Directory_Listing__c dirList:dirListWithDPList){
                if(dirList.Disconnected__c==false && dirList.Suppressed__c==false && (dirList.Directory_Paginations__r==null || dirList.Directory_Paginations__r.size()==0)){
                    reqdDirListingList.add(dirListingMap.get(dirList.Id));
                }
             }
            if(reqdDirListingList!=null && reqdDirListingList.size()>0){
                List<Directory_Listing__c> dirListingListUpdt = new List<Directory_Listing__c>();
                for(Directory_Listing__c reqdDirList:reqdDirListingList){
                    reqdDirList.DL_DM_isTriggerExecuted__c=true;
                    
                        /*if(dirList.Phone_Number__c != null && dirList.Directory__c != null && dirList.Directory_Section__c != null){                        
                            dirList.SL_StrPhoneDLDS__c = dirList.Phone_Number__c +''+String.valueOf(dirList.Directory__c).substring(0, 15)+''+String.valueOf(dirList.Directory_section__c).substring(0, 15);
                        }
                        //dirList.SL_strScopedlistingMatched__c = dirList.Directory__c+''+dirList.Directory_Section__c+''+dirList.Name+''+dirList.Phone_Number__c+''+dirList.Listing_City__c+''+dirList.Listing_PO_Box__c+''+dirList.Caption_Header__c+''+dirList.Caption_Member__c;
                        dirList.SL_strScopedlistingMatched__c = dirList.Directory__c+''+dirList.Directory_Section__c+''+dirList.Name+''+dirList.Phone_Number__c+''+ dirList.Listing_PO_Box__c+''+dirList.Caption_Header__c+''+dirList.Caption_Member__c;
                        */
                  
                   dirListingListUpdt.add(reqdDirList);  
                    
                }
                if(dirListingListUpdt.size() > 0) {
                   update dirListingListUpdt;
                }
                DirectoryListingHandlerController.createDirectoryPagination(dirListingListUpdt,null);
            }
        }catch(Exception e){
            System.debug('The exception is'+e);
            Database.rollback(sp);
            futureCreateErrorLog.createErrorRecordBatch('Error Type : '+e.getTypename()+'. Error Message : '+e.getMessage(), e.getStackTraceString(), 'Batch update of migrated Opportunity');
        }   
     } 
      
    global void finish(Database.BatchableContext bc) {
        AsyncApexJob a = AsyncApexJobSOQLMethods.getBatchDetails(BC.getJobId());
        /*String[] toAddresses = new String[] {a.CreatedBy.Email,'Mythreyee.Kumar@theberrycompany.com'};*/
        CommonEmailUtils.sendHTMLEmailForTargetObject(a.CreatedById, 'Scoped Listing batch process status : ' + a.Status, 'The Apex batch job picked '+a.TotalJobItems+' batches and processed ' + a.JobItemsProcessed +
            ' batches with '+ a.NumberOfErrors + ' failures. Please check Exception records for any other errors that might have occured while processing.');
    }
}