/*
This should be scheduled end of every month. Not every day.
*/
global class P4PBillingHistoryMonthlyScheduler implements Schedulable {
	global void execute(SchedulableContext SC) {
		Date currentDate = Date.today();
		Date startDate = currentDate.toStartofMonth();
		Date endDate = currentDate.addMonths(1).toStartofMonth().addDays(-1);
		String strQuery = 'Select id,P4P_Price_Per_Click_Lead__c,(SELECT Total_Billable_Calls__c FROM P4P_Transactions__r WHERE Date_of_Click_Call_Lead__c >=: StartMonthDate AND Date_of_Click_Call_Lead__c <=: dateEnd) from Order_Line_Items__c where (Is_P4P__c=true OR P4P_Billing__c=true) AND IsCanceled__c = false AND Talus_Go_Live_Date__c != Null and Payments_Remaining__c >= 1 and Payments_Remaining__c != null';
		if(currentDate == endDate) {
			ID batchprocessid = Database.executeBatch(new P4PBillingHistoryMonthlyBatch(strQuery));
		}
	}
}