global class ListingstoSLDFFUpdateBatchController implements database.batchable<sObject>{
	global string lstngId;
	global ListingstoSLDFFUpdateBatchController(string lstId){
		lstngId=lstId;
	}
    global database.QueryLocator start(database.batchablecontext bc){
    	String strQuery = 'Select Id, Telco_Provider__c, Service_Order__c, Phone_Type__c, Phone_Override__c,Phone_Number__c, Omit_Address_OAD__c, Normalized_Phone__c, '+
                'Normalized_Listing_Street_Number__c, Normalized_Listing_Street_Name__c,Normalized_Last_Name_Business_Name__c, Normalized_Designation__c, Name,SL_Last_Name_Business_Name__c, '+
                'Manual_Sort_As_Override__c, Lineage_Title__c, Left_Telephone_Phrase__c,ABD_Pending_Review__c,Honorary_Title__c, First_Name__c, Effective_Date__c, Disconnected__c,Disconnected_Via_BOC_Purge__c, '+ 
                'Disconnect_Reason__c, Cross_Reference_Text__c, CLEC_Provider__c,Bus_Res_Gov_Indicator__c,Follows_Listing_Record2__c, Bus_Main_or_Additional__c, '+
                'Listing__c From Directory_Listing__c Where Follows_Listing_Record2__c=\'' + true + '\' AND Listing__c = :lstngId';
    	 system.debug('SL Query'+strQuery);
    	 return database.getQueryLocator(strQuery);
    }
    global void execute(database.batchablecontext bc,List<Directory_listing__c> slLst){
    	system.debug('Scoped Listing list:'+slLst.size());
    	list<directory_listing__c> slUpdateLst = new list<directory_listing__c>();
    	map<Id,Listing__c> listingMap = new map<Id,Listing__c>();
    	if(string.isNotBlank(lstngId)){
    		listingMap = ListingSOQLMethods.fetchListingforSLDFFupdate(lstngId);
    	}
    	if(slLst.size()>0) {
    		for(directory_listing__c iteratorSL:slLst){
    			if(listingMap.get(iteratorSL.Listing__c) != null) {
	    			listing__c iteratorLst = listingMap.get(iteratorSL.Listing__c);
	    			iteratorSL.Bus_Res_Gov_Indicator__c = iteratorLst.Bus_Res_Gov_Indicator__c;
					iteratorSL.Cross_Reference_Text__c = iteratorLst.Cross_Reference_Text__c;
					iteratorSL.Normalized_Designation__c = iteratorLst.Normalized_Designation__c;
					iteratorSL.Disconnect_Reason__c = iteratorLst.Disconnect_Reason__c;
					iteratorSL.Disconnected__c = iteratorLst.Disconnected__c;
					iteratorSL.Effective_Date__c = iteratorLst.Effective_Date__c;
					iteratorSL.First_Name__c = iteratorLst.First_Name__c;
					iteratorSL.Honorary_Title__c = iteratorLst.Honorary_Title__c;
					iteratorSL.SL_Last_Name_Business_Name__c = iteratorLst.Lst_Last_Name_Business_Name__c;
					iteratorSL.Left_Telephone_Phrase__c = iteratorLst.Left_Telephone_Phrase__c;
					iteratorSL.Lineage_Title__c = iteratorLst.Lineage_Title__c;
					iteratorSL.Bus_Main_or_Additional__c = iteratorLst.Listing_Type__c;
					iteratorSL.Manual_Sort_As_Override__c = iteratorLst.Manual_Sort_As_Override__c;
					iteratorSL.Normalized_Last_Name_Business_Name__c = iteratorLst.Normalized_Last_Name_Business_Name__c;
					iteratorSL.Normalized_First_Name__c = iteratorLst.Normalized_First_Name__c;
					iteratorSL.Normalized_Listing_Street_Name__c = iteratorLst.Normalized_Listing_Street_Name__c;
					iteratorSL.Normalized_Listing_Street_Number__c = iteratorLst.Normalized_Listing_Street_Number__c;
					iteratorSL.Normalized_Phone__c = iteratorLst.Normalized_Phone__c;
					iteratorSL.Omit_Address_OAD__c = iteratorLst.Omit_Address_OAD__c;
					iteratorSL.Phone_Override__c = iteratorLst.Phone_Override__c;
					iteratorSL.Phone_Type__c = iteratorLst.Phone_Type__c;
					iteratorSL.Phone_Number__c = iteratorLst.Phone__c;
					iteratorSL.Service_Order__c = iteratorLst.Service_Order__c;
					iteratorSL.Telco_Provider__c = iteratorLst.Telco_Provider__c;
					iteratorSL.Listing_street_number__c = iteratorLst.Listing_Street_Number__c;
					iteratorSL.Listing_Street__c = iteratorLst.Listing_Street__c;
					iteratorSL.Listing_City__c = iteratorLst.Listing_City__c;
					iteratorSL.Listing_PO_Box__c = iteratorLst.Listing_PO_Box__c;
					iteratorSL.Listing_State__c = iteratorLst.Listing_State__c;
					iteratorSL.Listing_Postal_Code__c = iteratorLst.Listing_Postal_Code__c;
					iteratorSL.Listing_Country__c = iteratorLst.Listing_Country__c;
					iteratorSL.Right_Telephone_Phrase__c = iteratorLst.Right_Telephone_Phrase__c;
					iteratorSL.Designation__c = iteratorLst.Designation__c;
					iteratorSL.Telco_Sort_Order__c = iteratorLst.Telco_Sort_Order__c;
					iteratorSL.Normalized_Honorary_Title__c = iteratorLst.Normalized_Honorary_Title__c;
					iteratorSL.Normalized_Lineage_Title__c = iteratorLst.Normalized_Lineage_Title__c;
					iteratorSL.Normalized_Secondary_Surname__c = iteratorLst.Normalized_Secondary_Surname__c;
					iteratorSL.Disconnected_Via_BOC_Purge__c = iteratorLst.Disconnected_Via_ABD__c;
					iteratorSL.ABD_Pending_Review__c = iteratorLst.ABD_Pending_Review__c;
					slUpdateLst.add(iteratorSL);
    			}
    		}
    		system.debug('SL List Update'+slUpdateLst);
    		if(slUpdateLst.size()>0){
    			update slUpdateLst;
    		}
    	}
    }
    global void finish(Database.BatchableContext bc){
    	String strErrorMessage = '';
        AsyncApexJob a = AsyncApexJobSOQLMethods.getBatchDetails(BC.getJobId());
        String[] toAddresses = new String[] {a.CreatedBy.Email};
        if(a.NumberOfErrors > 0){
            strErrorMessage = a.ExtendedStatus;
        }
        CommonEmailUtils.sendHTMLEmailForTargetObject(a.CreatedById, 'Listing to scoped listing sync is '+ a.Status, 'The Batch Apex Job processed ' + a.JobItemsProcessed +
          ' batches with '+ a.NumberOfErrors +' failures.');
          
    	ListingstoDFFUpdateBatchController lstDFFBatch = new ListingstoDFFUpdateBatchController(lstngId);
    	database.executeBatch(lstDFFBatch);
    }
    
}