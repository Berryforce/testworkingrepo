@IsTest(SeeAllData=true)
public class YPCGraphicHandler_V2Test {
    static testMethod void TestYPCGraphicHandler_V2 () {
        
        
         recordtype r = [SELECT Id FROM RecordType WHERE Name = 'iYP Silver' and sObjectType = :'Digital_Product_Requirement__c'];
         Digital_Product_Requirement__c  dff = TestMethodsUtility.generatePrintGraphic();
         dff.Production_Notes_for_PrintAdOrderGraphic__c= '124';
         dff.Production_Notes_for_Logo__c = '23463';
         //dff.recordtypeid=r.id;
         insert dff;
         
         recordtype rt = [SELECT Id FROM RecordType WHERE Name = 'iYP Gold' and sObjectType = :'Digital_Product_Requirement__c'];
         Digital_Product_Requirement__c  dff3 = TestMethodsUtility.generatePrintGraphic();
         dff3.Production_Notes_for_PrintAdOrderGraphic__c= '1246';
         dff3.Production_Notes_for_Banner_Graphic__c = '234653';
         //dff.recordtypeid=r.id;
         insert dff3;
         
         recordtype rt1 = [SELECT Id FROM RecordType WHERE Name = 'iYP Diamond' and sObjectType = :'Digital_Product_Requirement__c'];
         Digital_Product_Requirement__c  dff45 = TestMethodsUtility.generatePrintGraphic();
         dff45.Production_Notes_for_PrintAdOrderGraphic__c= '1246';
         dff45.Production_Notes_for_Diamond_Graphic__c = '234653';
         //dff45.recordtypeid=rt1.id;
         //dff.recordtypeid=r.id;
         insert dff45;
         
         
        /* Digital_Product_Requirement__c  dff1 = TestMethodsUtility.generatePrintGraphic();
        
         dff1.Production_Notes_for_PrintAdOrderGraphic__c= '122';
         insert dff1; */
          
        // Digital_Product_Requirement__c dff2 =  dff;
        // list<Digital_Product_Requirement__c> dfflist1 = new list<Digital_Product_Requirement__c>();
        // dffList1.add(dff1); 
        // dffList1.add(dff);
         
         YPC_Graphics__c ypc3 = TestMethodsUtility.generateYPCGraphics(dff);
         ypc3.YPC_Graphics_type__C = 'PAO';
         ypc3.name = '5';
         insert ypc3;
         
          YPC_Graphics__c ypc32 = TestMethodsUtility.generateYPCGraphics(dff);
         ypc32.YPC_Graphics_type__C = 'Logo Distribution';
         ypc32.name = '5';
         
         insert ypc32;
         YPC_Graphics__c ypc12 = TestMethodsUtility.generateYPCGraphics(dff);
         ypc12.YPC_Graphics_type__C = 'Logo';
         ypc12.name = '5';
         
         insert ypc12;
         
         dff.Production_Notes_for_PrintAdOrderGraphic__c= '129';
         dff.recordtypeid=r.id;
         dff.Production_Notes_for_Logo__c='45656512';
         upsert dff;
         
         
         
         YPC_Graphics__c ypc02 = TestMethodsUtility.generateYPCGraphics(dff3);
         ypc02.YPC_Graphics_type__C = 'Banner Distribution';
         ypc02.name = '5';
         insert ypc02;
         YPC_Graphics__c ypc123 = TestMethodsUtility.generateYPCGraphics(dff3);
         ypc123.YPC_Graphics_type__C = 'Banner';
         ypc123.name = '5';
         insert ypc123;
         
         dff3.Production_Notes_for_PrintAdOrderGraphic__c= '12934';
         dff3.recordtypeid=rt.id;
         dff3.Production_Notes_for_Banner_Graphic__c='456565';
         upsert dff3;
         
         
         
         YPC_Graphics__c ypc5 = TestMethodsUtility.generateYPCGraphics(dff45);
         ypc5.YPC_Graphics_type__C = 'Diamond Logo';
         ypc5.name = '5';
         insert ypc5;
         YPC_Graphics__c ypc4 = TestMethodsUtility.generateYPCGraphics(dff45);
         ypc4.YPC_Graphics_type__C = 'Diamond Distribution';
         ypc4.name = '5';
         insert ypc4;
         dff45.Production_Notes_for_PrintAdOrderGraphic__c= '12934';
         dff45.recordtypeid=rt1.id;
         dff45.Production_Notes_for_Diamond_Graphic__c='456565';
         upsert dff45;
         
         
         
         
        // dff2.Production_Notes_for_PrintAdOrderGraphic__c= '120';
         
         YPC_Graphics__c ypc1 = TestMethodsUtility.generateYPCGraphics(dff);
         ypc1.name = '5';
         ypc1.YPC_Graphics_type__C = 'PAO';
         insert ypc1;
         /*YPC_Graphics__c ypc2 = TestMethodsUtility.generateYPCGraphics(dff1); 
         ypc2.YPC_Graphics_type__C = 'PAO';
         insert ypc2;*/
         
         test.startTest();
         
         
         //map<Id,String> mapRecordTypes = new map<Id,String>();
         //mapRecordTypes.put(dff.recordtypeid,'Diamond');
        // mapRecordTypes.put(dff.recordtypeid,'Silver');
         //mapRecordTypes.put(dff1.recordtypeid,'Diamond');
        // System.assertEquals('test'+String.valueof(dffList[0].id), String.valueof(dfflist1[0].id));
        // System.assertEquals('new test'+dffList[0].Production_Notes_for_PrintAdOrderGraphic__c, dfflist1[0].Production_Notes_for_PrintAdOrderGraphic__c);
       //  YPCGraphicHandler_V2.createChildern(dfflist,dfflist1,2,mapRecordTypes);  
         test.stopTest();
    }
}