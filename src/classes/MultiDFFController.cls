public class MultiDFFController {
    private id dffId;
    public Id getdffId() {
        return dffId;
    }
    
    public void setdffId(String value) {
        if(dffId != value) {
            dffId = value;
            onLoad();
        }
    }
    
    public list<Digital_Product_Requirement__c> lstDFFCompleted;
    public list<Digital_Product_Requirement__c> getlstDFFCompleted() {
        return lstDFFCompleted;
    }
    public void setlstDFFCompleted(list<Digital_Product_Requirement__c> value) {
        lstDFFCompleted = value;
    }
    public list<Digital_Product_Requirement__c> lstDFF {get;set;}
    
    public MultiDFFController() {
        //system.debug(strId);
        //system.debug(Apexpages.currentPage().getParameters().get('Id'));
    }
    
    public void onLoad() {
        lstDFF = [Select Id,Name,Account__c,OrderLineItemID__c,OrderLineItemID__r.Name,OrderLineItemID__r.Canvass__r.Name,ProductID__c,UDAC__c,URN_number__c,Issue_Number__c,DFF_Proof_Contact_Email_Address__c,Previous_URN_Number__c,Paper_or_Email__c,Proof_Contact_Title__c,Heading__c,Directory_Edition__c,Directory_Section_Columns__c,Directory_Edition__r.Name,Directory_Name__c,OrderLineItemID__r.Directory_Section__c,OrderLineItemID__r.Directory_Section__r.Name,CreatedDate,Canvass__c,Proof__c,Proof_Contact__c,DFF_Proof_Contact_Mailing_Address__c from Digital_Product_Requirement__c where Id = :dffId limit 1];
        set<Id> actId = new set<Id>();
        set<String> setUdac = new set<String>();
        set<string> setcanvassId = new set<string>();
        String dirSecCols;
        decimal URN;
        String editionCode;
        
        if(lstDFF.size() > 0) {
            for(Digital_Product_Requirement__c objDFF : lstDFF) {
                if(objDFF.Account__c!= null) {
                    actId.add(objDFF.Account__c);
                }
                if(objDFF.UDAC__c != null) {
                    setUdac.add(objDFF.UDAC__c);
                }
                if(objDFF.URN_number__c != null) {
                    URN = objDFF.URN_number__c ;
                }
                if(objDFF.Directory_Section_Columns__c != null){
                    dirSecCols = objDFF.Directory_Section_Columns__c;
                }
                if(objDFF.Canvass__c != null){
                    setcanvassId.add(objDFF.Canvass__c);
                }
                if(objDFF.Issue_Number__c != null){
                    editionCode = objDFF.Issue_Number__c;
                }
            }
        }
        
        if(actId.size()>0) {
            lstDFFCompleted = [Select Id,Name,Account__c,URN_number__c,Previous_URN_Number__c,Is_Multied__c,OrderLineItemID__c,OrderLineItemID__r.Name,OrderLineItemID__r.Product_Type__c,Directory_Section_Columns__c,OrderLineItemID__r.Canvass__r.Name,ProductID__c,UDAC__c,Heading__c,Directory_Edition__c,Directory_Edition__r.Name,Directory_Name__c,OrderLineItemID__r.Directory_Section__c,OrderLineItemID__r.Directory_Section__r.Name,CreatedDate,Canvass__c,sequence__c from Digital_Product_Requirement__c where Issue_Number__c =:editionCode AND DFF_Cancel_Status__c = false and Id NOT IN:lstDFF AND ((Account__c IN :actId and UDAC__c IN:setUdac and Directory_Section_Columns__c =:dirSecCols and Canvass__c IN:setcanvassId and Is_Multied__c = false) OR (Is_Multied__c = True and URN_number__c =: URN )) ];
        }
    }
    
    public pagereference saveSelectedDFF() {
        Digital_Product_Requirement__c objDFF = new Digital_Product_Requirement__c();
        //list<Digital_Product_Requirement__c> lstUpdateDFF = new list<Digital_Product_Requirement__c>();
        map<Id,Digital_Product_Requirement__c> mapUpdateDFF = new map<Id,Digital_Product_Requirement__c>();
        Integer iCount = 1;
        for(Digital_Product_Requirement__c iterator : lstDFF) {
            iterator.Sequence__c = iCount;
            objDFF = iterator;
            iterator.Is_Multied__c = true;
            if(!mapUpdateDFF.containsKey(iterator.Id)){
                mapUpdateDFF.put(iterator.Id,iterator);
            }
            //lstUpdateDFF.add(iterator);
            system.debug('Step S1');
        }
        system.debug(lstDFFCompleted.size());
        for(Digital_Product_Requirement__c iterator : lstDFFCompleted) { 
            system.debug('Step S2 : '+ iterator.Is_Multied__c);
            if(iterator.Is_Multied__c) {
                system.debug('Step S2');
                iCount++;
                if(iterator.Previous_URN_Number__c == null) {
                    iterator.Previous_URN_Number__c = iterator.URN_number__c;
                }
                iterator.URN_number__c = objDFF.URN_number__c;
                iterator.Production_Notes__c = 'This DFF is a multi of ' + objDFF.URN_number__c ;
                iterator.RecordtypeId = system.label.TestDFFMultiAdRT;
                iterator.Proof__c = objDFF.Proof__c;
                iterator.Paper_or_Email__c = objDFF.Paper_or_Email__c;
                iterator.Proof_Contact__c = objDFF.Proof_Contact__c;
                iterator.Proof_Contact_Title__c = objDFF.Proof_Contact_Title__c;
                iterator.Sequence__c = iCount;
                if(!mapUpdateDFF.containsKey(iterator.Id)){
                    mapUpdateDFF.put(iterator.Id,iterator);
                }
                //lstUpdateDFF.add(iterator);
            }
        }
        
        if(mapUpdateDFF.size() > 0) {
            update mapUpdateDFF.values();
            return new pagereference('/'+dffId);
        }
        return null;
    }
}