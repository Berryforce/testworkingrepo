public with sharing class LetterRenewalReportManualSchedule {

    public pagereference doRun() {
        database.executebatch(new  LetterRenewalBatchController ());
        return (new pagereference('/apex/LetterRenewalReportManualSchedule').setredirect(true));
    }
}