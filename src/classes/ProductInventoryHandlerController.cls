public with sharing class ProductInventoryHandlerController {
	public static void onAfterInsert(List<Product_Inventory__c> productInventoriesList, Map<Id, Product_Inventory__c> oldMap){ 
		updateNationalOrNonNationLeaderAds(productInventoriesList);
	}  
	
	public static void onBeforeInsert(List<Product_Inventory__c> productInventoriesList){ 
		populateDirSection(productInventoriesList);
	}
	
	public static void updateNationalOrNonNationLeaderAds(List<Product_Inventory__c> productInventoriesList){
		set<Id> dirSecHeadMapIds = new set<Id>();
		List<Section_Heading_Mapping__c> secHeadMappingList = new List<Section_Heading_Mapping__c>();
		Map<Id, Section_Heading_Mapping__c> secHeadMappingMap = new Map<Id, Section_Heading_Mapping__c>();
		
		for(Product_Inventory__c PI : productInventoriesList){
			if(PI.Section_Heading_Mapping__c != null){
				dirSecHeadMapIds.add(PI.Section_Heading_Mapping__c);
			}
		}
		
		secHeadMappingList = SectionHeadingMappingSOQLMethods.getSecHeadMappingsByIds(dirSecHeadMapIds);
		
		for(Section_Heading_Mapping__c SHM : secHeadMappingList){
			secHeadMappingMap.put(SHM.Id, SHM);
		}
		
		for(Product_Inventory__c PI : productInventoriesList){
			if(PI.Section_Heading_Mapping__c != null){
				if(PI.National_Ad__c){
					secHeadMappingMap.get(PI.Section_Heading_Mapping__c).National_Leader_Ads_Sold__c += 1;						
				} else {
					secHeadMappingMap.get(PI.Section_Heading_Mapping__c).Leader_Ads_Sold__c += 1;
				}
			}
		}
		
		update secHeadMappingMap.values();
	}
	
	public static void populateDirSection(List<Product_Inventory__c> productInventoriesList){
		set<Id> OLIIds = new set<Id>();
		List<Order_Line_Items__c> orderLineItemList = new List<Order_Line_Items__c>();
		Map<Id, Order_Line_Items__c> OLIMap = new Map<Id, Order_Line_Items__c>();
		
		for(Product_Inventory__c PI : productInventoriesList){
			if(PI.Directory_Section__c == null && PI.Order_Line_Item__c != null){
				OLIIds.add(PI.Order_Line_Item__c);
			}
		}
		
		orderLineItemList = OrderLineItemSOQLMethods.getOLIById(OLIIds);
		
		if(orderLineItemList.size() > 0){
			for(Order_Line_Items__c OLI : orderLineItemList){
				OLIMap.put(OLI.Id, OLI);
			}
			
			for(Product_Inventory__c PI : productInventoriesList){
				if(PI.Directory_Section__c == null && PI.Order_Line_Item__c != null){
					PI.Directory_Section__c = OLIMap.get(PI.Order_Line_Item__c).Directory_Section__c;
				}
			}
		}
	}
}