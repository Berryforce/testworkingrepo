@IsTest(SeeAllData=true)
public class Rev_Case_SCN_controllerTest{

    public static testMethod void Rev_Case_SCN_controllerTest01(){

        //string caseID = '500Z0000004xHt5';
        
        Case objCase = TestMethodsUtility.generateCSClaimClosed();
        insert objCase;
        
       Order_Line_Item_Discount__c order1 = new Order_Line_Item_Discount__c();
     
        order1.case__c = objCase.id;
        order1.total_Credit_amount__c = '12';
        insert order1; 
        
        Account newAccount = TestMethodsUtility.createAccount('customer');
        //setAccountIds.add(newAccount.Id);
        //AccountIds = newAccount.Id;
        Opportunity newOpportunity = TestMethodsUtility.createOpportunity('new', newAccount);
        //setOpportunityIds.add(newOpportunity.Id);
        Directory__c dir =  TestMethodsUtility.generateDirectory();
        dir.Directory_Code__c = '1545458';
        insert dir;
        c2g__codaDimension1__c dim1 = TestMethodsUtility.createDimension1(Dir);
        
        Product2 Product = TestMethodsUtility.createproduct();
        c2g__codaDimension2__c dim2 = TestMethodsUtility.generateDimension2(Product);
        dim2.c2g__ReportingCode__c = '374874';
        insert dim2;
        Contact con = TestMethodsUtility.createContact(newAccount.id);
        Order__c order = TestMethodsUtility.createOrder(newAccount.id);
        Order_Group__c gr = TestMethodsUtility.createOrderSet(newAccount, Order,newOpportunity);
        Order_Line_Items__c orderline =  TestMethodsUtility.createOrderLineItem(newAccount,Con, newOpportunity,Order,gr);
        
        c2g__codaDimension3__c dim3 =  TestMethodsUtility.generateDimension3(orderline ); 
        dim3.c2g__ReportingCode__c = '374877';
        insert dim3;
        
        c2g__codaDimension4__c dim4 = TestMethodsUtility.generateDimension4();
        dim4.c2g__ReportingCode__c = '374870';
        insert dim4;
        c2g__codaInvoice__c newSalesInvoice = TestMethodsUtility.createSalesInvoice(newAccount, newOpportunity);
        
      /* c2g__codaTransaction__c tr = new c2g__codaTransaction__c();
       tr.c2g__Account__c = newAccount.id;
       tr.c2g__ExternalId__c='111';
       insert tr;
        
       c2g__codaTransactionLineItem__c trLine = new c2g__codaTransactionLineItem__c();
       trLine.c2g__Transaction__c = tr.id;
       insert trLine;*/
        
       c2g__codaCreditNote__c crdit = TestMethodsUtility.generateSalesCreditNote(newSalesInvoice, newAccount);
       crdit.Case__C= objCase.id;
       crdit.c2g__CreditNoteStatus__c='In Progress';
       crdit.Customer_Name__c=newAccount.id;
       
       // crdit.c2g__Transaction__c=tr.id;
       insert crdit;
         
        
        c2g__codaCreditNoteLineItem__c line = TestMethodsUtility.createSalesCreditNoteLineItem(crdit,Product,orderline,dim1,dim2,dim3,dim4);
         
        //c2g__codaCreditNoteLineItem__c line = TestMethodsUtility.createSalesCreditNoteLineItem(crdit, Product);
         c2g__codaCreditNoteLineItem__c line1 = TestMethodsUtility.createSalesCreditNoteLineItem(crdit,Product,orderline,dim1,dim2,dim3,dim4);
         line.is_billed__c=true;
            update line;
        system.debug('status is'+crdit.c2g__PaymentStatus__c+crdit.c2g__NetTotal__c);
        test.startTest();
      
       PageReference pageRef1 = new PageReference('/apex/revcasescn?caseId='+objCase.id);
        Test.setCurrentPage(pageRef1);
        ApexPages.StandardController con1 = new ApexPages.StandardController(new case());                     
        Rev_Case_SCN_controller ctl = new Rev_Case_SCN_controller(con1); 
        ctl.doomedLIinf.add(crdit.Id);
       
        ctl.caseID = objCase.id;
        
            
            
            ctl.RevClaim(); 
            
           
            
        Test.stopTest();
    }      


}