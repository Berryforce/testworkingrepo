public class NationalBillingSendISS {	
    public String strPubCodeSendISS{get;set;}
    public String strEditionCodeSendISS{get;set;}
    public Integer intISSCount {get;set;}
    public boolean bolFetchDir{get;set;}
    public Decimal TaxAmt {get;set;}
    public Decimal NetAmt {get;set;}
    public Decimal ISSAmt {get;set;}
    public Decimal ISSTaxAmt {get;set;}
    public Decimal ISSNetAmt {get;set;}
    public Decimal TotalCommission {get;set;}
    public Decimal TotalADJAmount {get;set;}
    public boolean chkISSComplete {get;set;}
    public ID SACRACreportId {get;set;}    
    public ID ISSToCMRReportId {get;set;}
    List<National_Billing_Status__c> listNatBill = new List<National_Billing_Status__c>();
    Public National_Billing_Status__c natBilling;    
    public Boolean balancedBool {get;set;} 
    public Boolean sendToEliteBool {get;set;}
    public Boolean natInvCompleteBool {get;set;}
    public boolean bolISS {get;set;}
    Public list<ISS__c> lstISS {get;set;}
	List<ISS_Line_Item__c> lstISSLI = new List<ISS_Line_Item__c>();
	public Decimal totalISSADJAmount {get;set;}
    public Decimal totalISSTaxAmount {get;set;}
    public Decimal totalISSGrossAmount {get;set;}
    public Decimal totalISSCommissionAmount {get;set;}
    public Decimal totalISSNetAmount {get;set;}
    public List<SelectOption> dirList {get;set;}
    public String selectedDirCode {get;set;}
    Map<Id, String> mapDirIdDirCode = new Map<Id, String>();
    map<Id, String> mapDirIdName = new map<Id, String>();
    map<String, Id> mapDirNameId = new map<String, Id>();
    public Id dirId {get;set;}
    
    public NationalBillingSendISS() {
		lstISS = new list<ISS__c>();
		sendToEliteBool = false;
		natBilling  = new National_Billing_Status__c();
		ISSToCMRReportId = SACRACReportId = National_Billing_Reports__c.getInstance('ISSCMRReport').Report_Id__c;
        SACRACreportId = SACRACReportId = National_Billing_Reports__c.getInstance('SACRACTransLog').Report_Id__c;
        dirList = new List<SelectOption>();
    }
    
    public void fetchDirs() {
        dirList = new List<SelectOption>();
        if(String.isNotBlank(strPubCodeSendISS) && String.isNotBlank(strEditionCodeSendISS)) {
            list<Directory_Edition__c> dirEditionList = new list<Directory_Edition__c>(); 
            dirEditionList = DirectoryEditionSOQLMethods.getDirEdByDirPubCodeAndEdCode(strPubCodeSendISS, strEditionCodeSendISS);     
            system.debug('Directory Edition list is ' + dirEditionList);             
            if(dirEditionList.size() > 0) {
                mapDirNameId = new map<String, Id>();
                mapDirIdName = new map<Id, String>();
                for(Directory_Edition__c DE : dirEditionList) {
                    mapDirNameId.put(DE.Directory__r.Name, DE.Directory__c);
                    mapDirIdName.put(DE.Directory__c, DE.Directory__r.Name);
                    mapDirIdDirCode.put(DE.Directory__c, DE.Directory__r.Directory_Code__c);
                }
                List<String> dirNames = new List<String>(mapDirNameId.keySet());
                dirNames.sort();
                for(String str : dirNames) {
                    dirList.add(new SelectOption(mapDirNameId.get(str), str));
                }                  
            }
        }
    } 
    
    public void fetchISS() {
		lstISS = new list<ISS__c>();
		sendToEliteBool = false;
        selectedDirCode = mapDirIdDirCode.get(dirId);  
        fetchStatusTable();
        if(listNatBill.size() == 0) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'No record found in Status Table'));
            bolFetchDir = false;
        } else {    
            set<id> setISS = new set<id>();
            if(natBilling.RSAP_Send_Remittance_Started__c) {
            	lstISSLI = ISSLineItemSOQLMethods.getISSLIForSendISSWithRemSheet(strPubCodeSendISS, strEditionCodeSendISS);
            } else {
            	lstISSLI = ISSLineItemSOQLMethods.getISSLIForSendISSWithoutRemSheet(strPubCodeSendISS, strEditionCodeSendISS);
            }
            ISS__c objISS;
            Decimal TaxAmtTemp;
            Decimal CommisionTemp;
            Decimal GrossAmt;
            ISSAmt = 0;
            ISSTaxAmt = 0;
            ISSNetAmt = 0;
            intISSCount = 0;
            TaxAmt = 0;
            TotalCommission = 0;
            TotalADJAmount = 0;
            bolFetchDir = true;
            System.debug('Testingg fetchISS lstISSLI '+lstISSLI);
            System.debug('Testingg fetchISS lstISSLI.size() '+lstISSLI.size());
            for(ISS_Line_Item__c objISSLI : lstISSLI) {
                System.debug('Testingg objISSLI '+objISSLI);
                System.debug('Testingg setISS before add '+setISS);
                if(!setISS.contains(objISSLI.ISS__c)) {
                    CommisionTemp = 0;
                    setISS.add(objISSLI.ISS__c);
                    objISS = objISSLI.ISS__r;
                    System.debug('Testingg setISS added '+setISS);
                    TaxAmtTemp = 0;
                    GrossAmt = 0;
                    if(objISS.Total_Tax__c != null){
                        TaxAmtTemp = objISS.Total_Tax__c;
                        ISSTaxAmt  = ISSTaxAmt  + TaxAmtTemp;
                    }
                    if(objISS.Total_Gross_Amount__c != null){
                        ISSAmt = ISSAmt + objISS.Total_Gross_Amount__c;
                    }
                    if(objISS.Total_Gross_Amount__c != null){
                        GrossAmt = objISS.Total_Gross_Amount__c;
                    }
                    if(objISS.TotalCommission__c != null){
                       CommisionTemp = objISS.TotalCommission__c;
                    }
                    
                    ISSNetAmt = ISSNetAmt + ((GrossAmt + TaxAmtTemp) - CommisionTemp);
                    
                    TotalCommission += CommisionTemp;
                    
                    if(objISS.Total_ADJ_Amount__c != null) {
                    	TotalADJAmount += objISS.Total_ADJ_Amount__c;
                    }
                }
            }
            intISSCount = setISS.size();
            System.debug('Testingg intISSCount '+intISSCount);
        }
        assignValuesFromNatBill();
    }
    
    public void updateStatusTable() {
        if(listNatBill.size() > 0) {
    		for(National_Billing_Status__c natBill : listNatBill) {    
		        natBill.ISS_Send_ISS_Started__c = true;
		        natBill.ISS_Balanced__c = balancedBool;
    		}
    		update listNatBill;
    	}
    }
    
    public void assignValuesFromNatBill() {
        sendToEliteBool = natBilling.ISS_SEND_TO_ELITE_INITIATE_INFORMATICA__c;
        balancedBool = natBilling.ISS_Balanced__c;
        natInvCompleteBool = natBilling.ISS_COMPLETE_EOJ_FROM_INFORMATICA__c;
        
        if(natBilling.ISS_Transmission_Date__c != null || natBilling.ISS_SEND_TO_ELITE_INITIATE_INFORMATICA__c) {
        	sendToEliteBool = true;
        }
    }
    
    public void fetchStatusTable() {
        listNatBill = NationalBillingStatusSOQLMethods.getNatBillByPubEditionCode(strPubCodeSendISS, strEditionCodeSendISS);
        system.debug('National Billing status size is ' + listNatBill.size());
        if(listNatBill.size() > 0) {
            natBilling = listNatBill.get(0);
        }
    }
    
    public void displayISS() {
    	if(lstISSLI.size() > 0) {
    		Set<Id> ISSIds = new Set<Id>();
    		
    		for(ISS_Line_Item__c ISSLI : lstISSLI) {
    			ISSIds.add(ISSLI.ISS__c);
    		}
    		
    		lstISS = ISSSOQLMethods.getISSByISSIds(ISSIds);
    	}
		calcualteTotalForISS();
        bolISS = true;
    }
    
    public void hideISS() {
        bolISS = false;
    }
    
    public void sendToElite() {
    	if(listNatBill.size() > 0) {
    		for(National_Billing_Status__c natBill : listNatBill) {    			
    			natBill.ISS_SEND_TO_ELITE_INITIATE_INFORMATICA__c = true;
    		}
    	}
    	sendToEliteBool = true;
        update listNatBill;
    }                    
    
    public void calcualteTotalForISS() {
        totalISSADJAmount = 0;
        totalISSTaxAmount = 0;
        totalISSGrossAmount = 0;
        totalISSCommissionAmount = 0;
        totalISSNetAmount = 0;
        
        if(lstISS.size() > 0) {
            for(ISS__c ISS : lstISS) {                
                if(ISS.Total_ADJ_Amount__c != null) {
                    totalISSADJAmount += ISS.Total_ADJ_Amount__c;
                }
                
                if(ISS.Total_Tax__c != null) {
                    totalISSTaxAmount += ISS.Total_Tax__c;
                }
                
                if(ISS.Total_Gross_Amount__c != null) {
                    totalISSGrossAmount += ISS.Total_Gross_Amount__c;
                }
                
                if(ISS.TotalCommission__c != null) {
                    totalISSCommissionAmount += ISS.TotalCommission__c;
                }
                
                if(ISS.NetAmount__c != null) {
                    totalISSNetAmount += ISS.NetAmount__c;
                }
            }
        }
    }
}