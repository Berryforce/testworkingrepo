@isTest
public class ISSSOQLMethodsTest {
	static testMethod void ISSSOQLTest() {
		Canvass__c newCanvass = TestMethodsUtility.createCanvass();
        Account newAccount = TestMethodsUtility.generateAccount('customer', false);
        newAccount.Primary_Canvass__c = newCanvass.Id;
        insert newAccount;

        Directory__c newDirectory1 = TestMethodsUtility.createDirectory();        
        Directory_Mapping__c newDirectoryMapping = TestMethodsUtility.createDirectoryMapping();
        
        Telco__c newTelco = TestMethodsUtility.createTelco(newAccount.Id);
        Directory_Edition__c newDirectoryEdition = TestMethodsUtility.generateDirectoryEdition(newDirectory1.Id, newAccount.Primary_Canvass__c, newTelco.Id, newDirectoryMapping.Id);        
        newDirectoryEdition.Book_Status__c='NI';        
        insert newDirectoryEdition;  
        ISS__c isscreate = TestMethodsUtility.generateISS();
        insert isscreate;
        
        ISSSOQLMethods.getISSByISSId(new set<Id> {isscreate.Id});
        ISSSOQLMethods.getISSByCMRId(new set<Id> {newAccount.Id});
        ISSSOQLMethods.getISSByISS(new List<ISS__c> {isscreate});
        ISSSOQLMethods.getISSByISSIds(new set<Id> {isscreate.Id});
	}
}