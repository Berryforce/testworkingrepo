/***********************************************
Apex Class to create all package product records
Created By: Reddy(pyellanki2@csc.com)
Updated Date: 10/07/2014
$Id$
************************************************/
public with sharing class CreateUDACPackageProducts_2 {

    public static List < Package_Product__c > createPkgPrdt(Id pkgId) {

        String RequestUrl;
        Package_ID__c pkgidrecd = [Select Name, Description__c, Resource_URI__c, Product_Information__c from Package_ID__c where id = : pkgId limit 1];
        //System.debug('************Debug1************' + pkgidrecd);

        //Form the endpoint url for pulling package products
        //RequestUrl = 'https://nigel.stg.taluslabs.com' + pkgidrecd.Resource_URI__c;
        RequestUrl = Label.Talus_Basic_Url + pkgidrecd.Resource_URI__c;

        String responseString;
       
        //Make GET call to retrieve package products
        if(!Test.isRunningTest()){
         responseString= TalusRequestUtilityGET.initiateRequest(Requesturl);
        }else{
          responseString='{"description":"Trumpia Package","end_date":"2099-01-01","name":"Trumpia Package","products":[{"code":"TRMP_BASE","cycle":"monthly","default_quantity":0,"max_quantity":1,"min_quantity":0,"product":{"billing_cycle":"monthly","code":"TRUMPIA_BASE","description":"Trumpia MCM","name":"Trumpia MCM","properties":[{"name":"username","optional":false,"type":"string","value":null},{"name":"plan_id","optional":false,"type":"int","value":null},{"name":"promo_code","optional":true,"type":"string","value":""},{"name":"signup_source","optional":false,"type":"int","value":null},{"name":"industry","optional":false,"type":"int","value":null},{"name":"time_zone","optional":false,"type":"string","value":null},{"name":"sms_short_code","optional":true,"type":"int","value":null},{"name":"password","optional":false,"type":"string","value":null},{"name":"signup_url","optional":false,"type":"string","value":null}],"sku":"MCM0001_BASE","unit_of_measure":"flat"}},{"code":"TRMP_ADD","cycle":"monthly","default_quantity":0,"max_quantity":1,"min_quantity":0,"product":{"billing_cycle":"monthly","code":"TRUMPIA_ADD","description":"MCM add-on pack","name":"MCM add-on pack","properties":[{"name":"username","optional":false,"type":"string","value":null},{"name":"plan_id","optional":false,"type":"int","value":null},{"name":"promo_code","optional":true,"type":"string","value":""},{"name":"signup_source","optional":false,"type":"int","value":null},{"name":"industry","optional":false,"type":"int","value":null},{"name":"time_zone","optional":false,"type":"string","value":null},{"name":"sms_short_code","optional":true,"type":"int","value":null},{"name":"password","optional":false,"type":"string","value":null},{"name":"signup_url","optional":false,"type":"string","value":null}],"sku":"MCM0001_ADD","unit_of_measure":"flat"}}],"resource_uri":"/catalog/api/v2/packages/PKG_MCM0001/","sku":"PKG_MCM0001","start_date":"2012-01-01","status":"active"}';
        }

        system.debug('************Debug2************' + responseString + RequestUrl);

        List < Package_Product__c > lstpkgids = new List < Package_Product__c > ();

        if (responseString != null && (!responseString.contains('error_message'))) {

            //Deserialize the response string
            AllPackageProductsWrapper pkgrecrds = (AllPackageProductsWrapper) System.JSON.deserialize(responseString, AllPackageProductsWrapper.class);

            List < AllPackageProductsWrapper.products > prdlst = pkgrecrds.products;

            List < Product_Properties__c > finalpkgprpts = new List < Product_Properties__c > ();

            //System.debug('************TalusPackageProducts************' + prdlst.size() + prdlst);

            for (AllPackageProductsWrapper.products prdt: prdlst) {

                //System.debug('************SingleProduct************' + prdt);

                //Create new package product record for each package
                Package_Product__c pk = new Package_Product__c();
                pk.Package_ID__c = pkgId;
                pk.Name = string.valueof(prdt.Product.sku);
                pk.Package_Product_ID__c = string.valueof(prdt.Product.sku);
                pk.Product_Description__c = string.valueof(prdt.product.description);
                pk.Product_Name__c = string.valueof(prdt.product.name);
                pk.Product_Code__c = string.valueof(prdt.product.code);
                pk.Max_Quantity__c = string.valueof(prdt.max_quantity);
                pk.Min_Quantity__c = string.valueof(prdt.min_quantity);

                lstpkgids.add(pk);

            }

        }
        
        return lstpkgids;
    }
}