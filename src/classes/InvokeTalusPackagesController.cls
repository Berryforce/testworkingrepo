/****************************************************
Controller Class to pull Fulfillment Packages Info
Created By: Reddy(reddy.yellanki@theberrycompany.com)
Updated Date: 04/14/2015
*****************************************************/
public with sharing class InvokeTalusPackagesController {

    //Getters and Setter
    public static String selectedPkg {
        get;
        set;
    }
    public String selectedUrl {
        get;
        set;
    }
    public String endPointInfo {
        get;
        set;
    }

    public static Package_ID__c flmntPkg;
    public static String Requesturl = Label.Talus_Basic_URL;
    public static String responseString;
    public static List < Package_ID__c > lstPkgs = new List < Package_ID__c > ();
    public static List < Package_Product__c > lstPkgPrdts = new List < Package_Product__c > ();
    public static List < Package_Product__c > lstPkgPrdtsDel = new List < Package_Product__c > ();
    public static List < Product_Properties__c > lstPkgPrdtPrpts = new List < Product_Properties__c > ();
    public static String rspnBody;
    public static Map<String, Object> FnlJSON = new Map<String, Object>();
    
    public InvokeTalusPackagesController(ApexPages.StandardController controller) {
        endPointInfo = 'Below are some sample endpoints: <br/> 1. Package catalog Endpoint: https://stg-nigel.colonylogic.com/catalog/api/v2/packages/ <br/> 2. Pcakage Level Endpoint: https://stg-nigel.colonylogic.com/catalog/api/v2/packages/{package_id}/ <br/> 3. Account Level Endpoint: https://stg-nigel.colonylogic.com/subscription/api/v2/accounts/{account_id}/ <br/> 4. Subscriptin Level Endpoint: https://stg-nigel.colonylogic.com/subscription/api/v2/accounts/{account_id}/subscriptions/{subscription_id}/ <br/> 5. Subscription Product Level Endpoint: ';
    }

    public pageReference invkPkgs() {

        CreateUDACPackages.invokeGET();

        String query = 'Select id, Name from Package_ID__c where Name!=null';
        CreateUDACProducts obj = new CreateUDACProducts(query);
        database.executebatch(obj, 10);

        PageReference pageRef = new PageReference('/apex/TalusPackagesSuccessfullyCreated');
        return pageRef;

    }

    public static void invkSnglPkg() {

        System.debug('************SELECTED Package************' + selectedPkg);

        try {

            if (String.isNotBlank(selectedPkg)) {

                flmntPkg = [Select Id, Name, Description__c, Resource_URI__c, Product_Information__c, (SELECT Id from Package_Products__r) from Package_ID__c where Name = : selectedPkg];

            if (flmntPkg != null) {

                lstPkgPrdtsDel = flmntPkg.Package_Products__r;

                responseString = TalusRequestUtilityGET.initiateRequest(Requesturl + flmntPkg.Resource_URI__c);

                if (responseString != null && (!responseString.contains('error_message'))) {

                    //Deserialize the response string
                    AllPackageProductsWrapper pkgrecrds = (AllPackageProductsWrapper) System.JSON.deserialize(responseString, AllPackageProductsWrapper.class);

                    List < AllPackageProductsWrapper.products > prdlst = pkgrecrds.products;

                    System.debug('************TalusPackageProducts************' + prdlst.size() + prdlst);

                    for (AllPackageProductsWrapper.products prdt: prdlst) {

                        //Create new package product record for each package
                        Package_Product__c pk = new Package_Product__c();
                        pk.Package_ID__c = flmntPkg.Id;
                        pk.Name = string.valueof(prdt.Product.sku);
                        pk.Package_Product_ID__c = string.valueof(prdt.Product.sku);
                        pk.Product_Description__c = string.valueof(prdt.product.description);
                        pk.Product_Name__c = string.valueof(prdt.product.name);
                        pk.Product_Code__c = string.valueof(prdt.product.code);
                        pk.Max_Quantity__c = string.valueof(prdt.max_quantity);
                        pk.Min_Quantity__c = string.valueof(prdt.min_quantity);

                        lstPkgPrdts.add(pk);

                    }
                    if (lstPkgPrdts.size() > 0) {
                        System.debug('************PackageProducts************' + lstPkgPrdts);
                        System.debug('************PackageProductsToDelete************' + lstPkgPrdtsDel);
                        delete lstPkgPrdtsDel;
                        insert lstPkgPrdts;

                        for (AllPackageProductsWrapper.products prdt: prdlst) {
                            List < AllPackageProductsWrapper.Properties > prdpkglst = prdt.product.properties;

                            for (Package_Product__c pkprdt: lstPkgPrdts) {
                                if (String.valueof(prdt.product.sku) == String.valueof(pkprdt.Package_Product_ID__c)) {

                                    if (prdpkglst.size() > 0) {
                                        for (AllPackageProductsWrapper.Properties p1: prdpkglst) {

                                            Product_Properties__c pkgprpt = new Product_Properties__c();
                                            pkgprpt.Package_Product__c = pkprdt.id;
                                            pkgprpt.Name__c = string.valueof(p1.name);
                                            pkgprpt.optional__c = boolean.valueof(p1.optional);
                                            pkgprpt.Type__c = p1.type;

                                            lstPkgPrdtPrpts.add(pkgprpt);
                                        }
                                    }
                                    System.debug('************ProductProperties************' + lstPkgPrdts);
                                }
                            }
                        }
                    }

                    if (lstPkgPrdtPrpts.size() > 0) {
                        System.debug('************PackageProductProperties************' + lstPkgPrdts);
                        insert lstPkgPrdtPrpts;
                        CommonUtility.msgInfo(CommonUtility.updatedPackage);
                    }
                }
            }
            }else{
                CommonUtility.msgError(CommonUtility.selectPackage);   
            }


        } catch (exception e) {
            CommonUtility.msgError(String.valueof(e.getMessage()));
        }
    }
    
    public void flmntInfo(){
        try{
        if(String.isNotBlank(selectedUrl)){
            rspnBody = TalusRequestUtilityGET.initiateRequest(selectedUrl);
            Map<String, Object> m = (Map<String, Object>)JSON.deserializeUntyped(rspnBody);
            FnlJSON.putAll(m);
            CommonUtility.msgConfirm(rspnBody);
            selectedUrl = '';
        }else{
            CommonUtility.msgError(CommonUtility.endPointUrl);
        }            
        }Catch(exception e){
            CommonUtility.msgError(String.valueof('Exception: ' + e.getMessage()));   
        }                         
    }

}