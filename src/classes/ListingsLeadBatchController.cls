global class ListingsLeadBatchController implements Database.Batchable<sObject>,Database.Stateful{
    
    private set<Id> setLstId = new set<Id>();
    private integer batchcount =0;
    
    global ListingsLeadBatchController(set<Id> setListingId){
        setLstId = setListingId;
    }
    global Database.QueryLocator start(Database.BatchableContext bc){
       
        return database.getQuerylocator('SELECT Id,Name,LST_Last_Name_Business_Name__c,Listing_Type__c,Account__c,Lead__c,Bus_Res_Gov_Indicator__c ,First_Name__c,Phone__c,Listing_Street__c,Listing_City__c,Listing_State__c,Listing_Postal_Code__c,Listing_Country__c,Lead_Account_Source__c,Primary_Canvass__c,Telco_Provider_Account_ID__c from listing__c where ID IN :setLstId');
    }
   
    global void execute(Database.BatchableContext bc, List<listing__c> objlist){
        
    map<string,listing__c> mapLeadActExist = new map<string,listing__c>();
    if(objlist.size()>0){
        batchcount=batchcount+objlist.size();
    }
    for(listing__c iterator : objlist){
       if(iterator.Listing_Type__c == 'Main' && iterator.Account__c == null && string.isBlank(iterator.Lead__c) && iterator.Bus_Res_Gov_Indicator__c == 'Business'){
            if(iterator.Phone__c != null){
            	mapLeadActExist.put(iterator.Phone__c,iterator);
            }
        }
    }
        
    map<Id,listing__c> mapListingLeadAccountUpdate = new map<Id,listing__c>();
    	set<Id> setAccountRTId = new set<Id>();
    	setAccountRTId.add(Label.TestAccountCustomerRT);
    	setAccountRTId.add(Label.TestAccountNationalRT);
	    //Existing Accounts
		for(Account acct : [select Id, Name,Phone,ParentId,Is_Child_Account__c from Account where Phone IN :mapLeadActExist.keyset() and recordtypeId IN : setAccountRTId]){
	        if(mapLeadActExist.get(acct.Phone) != null){
	            Listing__c objL = new Listing__c(Id=mapLeadActExist.get(acct.Phone).Id); 
	                if(acct.Is_Child_Account__c == true) {
	                	objL.Account__c = acct.ParentId;
	                }
	                else {
	                	objL.Account__c = acct.Id;
	                }
	                if(!mapListingLeadAccountUpdate.containskey(objL.Id)){
	                    mapListingLeadAccountUpdate.put(objL.Id,objL);
	                }
	                mapLeadActExist.remove(acct.Phone);
	        }
            
       }
        //Existing Leads 
       if(mapLeadActExist.size()>0){
            list<lead> ldLst = [select Id,Name, Phone from Lead where Phone IN : mapLeadActExist.keyset()];
            if(ldLst.size()>0) {
                for(Lead ld : ldLst) {
                    if(mapLeadActExist.get(ld.Phone) != null){
                        Listing__c objL = new Listing__c(Id=mapLeadActExist.get(ld.Phone).Id); 
                            objL.Lead__c = ld.Id;
                            objL.Is_Lead__c = True;
                            if(!mapListingLeadAccountUpdate.containskey(objL.Id)){
                                mapListingLeadAccountUpdate.put(objL.Id,objL);
                            }
                            mapLeadActExist.remove(ld.Phone); 
                    }
                }
            }
        }
        list<Lead> leadLstInsert = new list<Lead>();
        if(mapLeadActExist.size()>0) {
            for(Listing__c objLstng :mapLeadActExist.values()){
                Lead objLead = new Lead();
                if(string.isNotBlank(objLstng.LST_Last_Name_Business_Name__c)){
                    if(objLstng.Lst_Last_Name_Business_Name__c.length()>80){
                		objLead.LastName = objLstng.LST_Last_Name_Business_Name__c.Left(80);
                	}
                	else{
                		objLead.LastName = objLstng.LST_Last_Name_Business_Name__c;
                	}
                }
                objLead.FirstName = objLstng.First_Name__c;
                objLead.Company =  objLstng.LST_Last_Name_Business_Name__c;
                objLead.Phone = objLstng.Phone__c;
                objLead.Street = objLstng.Listing_Street__c;
                objLead.City = objLstng.Listing_City__c;
                objLead.State = objLstng.Listing_State__c;
                objLead.PostalCode = objLstng.Listing_Postal_Code__c;
                objLead.Country = objLstng.Listing_Country__c;
                objLead.Lead_Account_Source__c = objLstng.Lead_Account_Source__c;
                objLead.Status = 'Unqualified';
                objLead.Create_Listing__c = 'No';
                objLead.Primary_Canvass__c = objLstng.Primary_Canvass__c;
                if(objLstng.Telco_Provider_Account_ID__c != null){
                    objLead.Telco_Partner_Account__c = objLstng.Telco_Provider_Account_ID__c;
                }
               // objLead.RecordtypeId = CommonMethods.getRedordTypeIdByName('Service Order Lead','Lead');
                leadLstInsert.add(objLead);
            }
            if(leadLstInsert.size()>0){
                insert leadLstInsert;
            }
            for(Lead objLd :leadLstInsert ){
                if(mapLeadActExist.get(objLd.Phone) != null){
                    Listing__c objLst = new Listing__c(Id=mapLeadActExist.get(objLd.Phone).Id); 
                    objLst.Lead__c = objLd.Id;
                    objLst.Is_Lead__c = True;
                    if(!mapListingLeadAccountUpdate.containskey(objLst.Id)){
                        mapListingLeadAccountUpdate.put(objLst.Id,objLst);
                    }
                    mapLeadActExist.remove(objLd.Phone);
                }
            }
        }
        if(mapListingLeadAccountUpdate.size()> 0){
			ListingHandlerController.bflag = false;
            Update mapListingLeadAccountUpdate.values();
        }
    }

    global void finish(Database.BatchableContext bc){
        
        String strErrorMessage = '';
        AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed,
        TotalJobItems, CreatedBy.Email, ExtendedStatus from AsyncApexJob where Id =:BC.getJobId()];
        String[] toAddresses = new String[] {a.CreatedBy.Email};
        
        if(a.NumberOfErrors > 0){
            strErrorMessage = a.ExtendedStatus;
        }
        CommonEmailUtils.sendHTMLEmail(toAddresses, 'Listings Records processing status ' + a.Status, 'The batch Apex job Items processed ' + a.JobItemsProcessed+
          ' batches with '+ a.NumberOfErrors + '  & Total number of records ' + batchcount + ' with  '+ a.NumberOfErrors +'  failures '+ strErrorMessage + '. Successfull records');
          
    }
  
}