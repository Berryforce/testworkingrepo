global class PushLeadstoQueueScheduler implements Schedulable
{
   global void execute(SchedulableContext sc) 
   {
        PushLeadstoQueue pushleads = new PushLeadstoQueue();
        database.executebatch(pushleads, 2000);
   }
}