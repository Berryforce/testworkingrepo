@isTest
public class localhost80GsoapSbmappservices72WsdlTest {

    @isTest static void testMockWbSrvs1() {

        Test.startTest();

        Test.setMock(WebServiceMock.class, new localhost80GsoapSbmappservicesMkTst());
        localhost80GsoapSbmappservices72Wsdl.sbmappservices72 invkSrvs = new localhost80GsoapSbmappservices72Wsdl.sbmappservices72();
        sbmappservices72.Auth ath = new sbmappservices72.Auth();
        sbmappservices72.ItemIdentifier idfy = new sbmappservices72.ItemIdentifier();
        sbmappservices72.Options optns = new sbmappservices72.Options();
        sbmappservices72.QueryRange qryRng = new sbmappservices72.QueryRange();
        sbmappservices72.ReportsFilter rptFltr = new sbmappservices72.ReportsFilter();
        sbmappservices72.TableIdentifier tblIdfy = new sbmappservices72.TableIdentifier();
        sbmappservices72.TTItem itm = new sbmappservices72.TTItem();
        sbmappservices72.TransitionIdentifier transn = new sbmappservices72.TransitionIdentifier();
        sbmappservices72.ResponseItemOptions rspOptns = new sbmappservices72.ResponseItemOptions();
        sbmappservices72.MultipleResponseItemOptions mlRspOptn = new sbmappservices72.MultipleResponseItemOptions();
        sbmappservices72.FileAttachmentContents flatchcnt = new sbmappservices72.FileAttachmentContents();
        sbmappservices72.ItemIdentifier[] itmIdfy = new sbmappservices72.ItemIdentifier[] {};
        sbmappservices72.MultipleOptions mltOptns = new sbmappservices72.MultipleOptions();
        sbmappservices72.UserIdentifier[] user_x = new sbmappservices72.UserIdentifier[] {};
        sbmappservices72.UserIdentifier user_y = new sbmappservices72.UserIdentifier();
        sbmappservices72.UserResponseOptions usrRspOptns = new sbmappservices72.UserResponseOptions();
        sbmappservices72.ReportData rptData = new sbmappservices72.ReportData();
        sbmappservices72.ProjectIdentifier prjtIdfy = new sbmappservices72.ProjectIdentifier();
        sbmappservices72.NoteAttachmentContents ntAtchcnt = new sbmappservices72.NoteAttachmentContents();
        sbmappservices72.TTItem[] itm1 = new sbmappservices72.TTItem[] {};
        sbmappservices72.ReportIdentifier rptIdfy = new sbmappservices72.ReportIdentifier();
        sbmappservices72.SolutionIdentifier slnIdfy = new sbmappservices72.SolutionIdentifier();
        ath.userId = 'bsfdc';
        ath.password = 'B3rrySfdc01!';
        String transitionOptions = 'Test';
        String attributeName = 'Test';
        String queryWhereClause = 'Test';
        String orderByClause = 'Test';
        Integer firstRecord = 1234;
        Integer maxReturnSize = 1234;
        Boolean breakLock = true;
        Integer attachmentID = 1234;
        Boolean getCurrentUser = true;
        String searchByName = 'Test';
        String reportCategory = 'Test';
        String reportAccessLevel = 'Test';
        String tableType = 'Test';

        invkSrvs.GetAvailableTransitions(ath, idfy, transitionOptions, attributeName, optns);
        invkSrvs.GetSolutions(ath, optns);
        invkSrvs.GetReports(ath, qryRng, rptFltr, optns);
        invkSrvs.GetItemsByQuery(ath, tblIdfy, queryWhereClause, orderByClause, firstRecord, maxReturnSize, mlRspOptn);
        invkSrvs.TransitionItem(ath, itm, transn, breakLock, rspOptns);
        invkSrvs.GetFileAttachment(ath, idfy, attachmentID, optns);
        invkSrvs.CreateAuxItem(ath, tblIdfy, itm, rspOptns);
        invkSrvs.UpdateFileAttachment(ath, idfy, flatchcnt, optns);
        invkSrvs.GetItem(ath, idfy, rspOptns);
        invkSrvs.DeleteItems(ath, itmIdfy, mltOptns);

        Test.stopTest();

    }

    @isTest static void testMockWbSrvs2() {

        Test.startTest();

        Test.setMock(WebServiceMock.class, new localhost80GsoapSbmappservicesMkTst());
        localhost80GsoapSbmappservices72Wsdl.sbmappservices72 invkSrvs = new localhost80GsoapSbmappservices72Wsdl.sbmappservices72();
        sbmappservices72.Auth ath = new sbmappservices72.Auth();
        sbmappservices72.ItemIdentifier idfy = new sbmappservices72.ItemIdentifier();
        sbmappservices72.Options optns = new sbmappservices72.Options();
        sbmappservices72.QueryRange qryRng = new sbmappservices72.QueryRange();
        sbmappservices72.ReportsFilter rptFltr = new sbmappservices72.ReportsFilter();
        sbmappservices72.TableIdentifier tblIdfy = new sbmappservices72.TableIdentifier();
        sbmappservices72.TTItem itm = new sbmappservices72.TTItem();
        sbmappservices72.TransitionIdentifier transn = new sbmappservices72.TransitionIdentifier();
        sbmappservices72.ResponseItemOptions rspOptns = new sbmappservices72.ResponseItemOptions();
        sbmappservices72.MultipleResponseItemOptions mlRspOptn = new sbmappservices72.MultipleResponseItemOptions();
        sbmappservices72.FileAttachmentContents flatchcnt = new sbmappservices72.FileAttachmentContents();
        sbmappservices72.ItemIdentifier[] itmIdfy = new sbmappservices72.ItemIdentifier[] {};
        sbmappservices72.MultipleOptions mltOptns = new sbmappservices72.MultipleOptions();
        sbmappservices72.UserIdentifier[] user_x = new sbmappservices72.UserIdentifier[] {};
        sbmappservices72.UserIdentifier user_y = new sbmappservices72.UserIdentifier();
        sbmappservices72.UserResponseOptions usrRspOptns = new sbmappservices72.UserResponseOptions();
        sbmappservices72.ReportData rptData = new sbmappservices72.ReportData();
        sbmappservices72.ProjectIdentifier prjtIdfy = new sbmappservices72.ProjectIdentifier();
        sbmappservices72.NoteAttachmentContents ntAtchcnt = new sbmappservices72.NoteAttachmentContents();
        sbmappservices72.TTItem[] itm1 = new sbmappservices72.TTItem[] {};
        sbmappservices72.ReportIdentifier rptIdfy = new sbmappservices72.ReportIdentifier();
        sbmappservices72.SolutionIdentifier slnIdfy = new sbmappservices72.SolutionIdentifier();
        ath.userId = 'bsfdc';
        ath.password = 'B3rrySfdc01!';
        String transitionOptions = 'Test';
        String attributeName = 'Test';
        String queryWhereClause = 'Test';
        String orderByClause = 'Test';
        Integer firstRecord = 1234;
        Integer maxReturnSize = 1234;
        Boolean breakLock = true;
        Integer attachmentID = 1234;
        Boolean getCurrentUser = true;
        String searchByName = 'Test';
        String reportCategory = 'Test';
        String reportAccessLevel = 'Test';
        String tableType = 'Test';

        invkSrvs.GetUsers(ath, getCurrentUser, searchByName, user_x, usrRspOptns);
        invkSrvs.RunReportXml(ath, rptData, optns);
        invkSrvs.LinkSubtask(ath, idfy, idfy, optns);
        invkSrvs.GetItems(ath, ItmIdfy, mlRspOptn);
        invkSrvs.GetStateChangeHistory(ath, idfy, qryRng, optns);
        invkSrvs.GetApplications(ath, optns);
        invkSrvs.CreatePrimaryItem(ath, prjtIdfy, idfy, itm, transn, rspOptns);
        invkSrvs.CreateAuxItems(ath, tblIdfy, itm1, mlRspOptn);
        invkSrvs.CreateNoteAttachment(ath, idfy, user_y, ntAtchcnt);
        invkSrvs.GetVersion();

        Test.stopTest();

    }

    @isTest static void testMockWbSrvs3() {

        Test.startTest();

        Test.setMock(WebServiceMock.class, new localhost80GsoapSbmappservicesMkTst());
        localhost80GsoapSbmappservices72Wsdl.sbmappservices72 invkSrvs = new localhost80GsoapSbmappservices72Wsdl.sbmappservices72();
        sbmappservices72.Auth ath = new sbmappservices72.Auth();
        sbmappservices72.ItemIdentifier idfy = new sbmappservices72.ItemIdentifier();
        sbmappservices72.Options optns = new sbmappservices72.Options();
        sbmappservices72.QueryRange qryRng = new sbmappservices72.QueryRange();
        sbmappservices72.ReportsFilter rptFltr = new sbmappservices72.ReportsFilter();
        sbmappservices72.TableIdentifier tblIdfy = new sbmappservices72.TableIdentifier();
        sbmappservices72.TTItem itm = new sbmappservices72.TTItem();
        sbmappservices72.TransitionIdentifier transn = new sbmappservices72.TransitionIdentifier();
        sbmappservices72.ResponseItemOptions rspOptns = new sbmappservices72.ResponseItemOptions();
        sbmappservices72.MultipleResponseItemOptions mlRspOptn = new sbmappservices72.MultipleResponseItemOptions();
        sbmappservices72.FileAttachmentContents flatchcnt = new sbmappservices72.FileAttachmentContents();
        sbmappservices72.ItemIdentifier[] itmIdfy = new sbmappservices72.ItemIdentifier[] {};
        sbmappservices72.MultipleOptions mltOptns = new sbmappservices72.MultipleOptions();
        sbmappservices72.UserIdentifier[] user_x = new sbmappservices72.UserIdentifier[] {};
        sbmappservices72.UserIdentifier user_y = new sbmappservices72.UserIdentifier();
        sbmappservices72.UserResponseOptions usrRspOptns = new sbmappservices72.UserResponseOptions();
        sbmappservices72.ReportData rptData = new sbmappservices72.ReportData();
        sbmappservices72.ProjectIdentifier prjtIdfy = new sbmappservices72.ProjectIdentifier();
        sbmappservices72.NoteAttachmentContents ntAtchcnt = new sbmappservices72.NoteAttachmentContents();
        sbmappservices72.TTItem[] itm1 = new sbmappservices72.TTItem[] {};
        sbmappservices72.ReportIdentifier rptIdfy = new sbmappservices72.ReportIdentifier();
        sbmappservices72.SolutionIdentifier slnIdfy = new sbmappservices72.SolutionIdentifier();
        ath.userId = 'bsfdc';
        ath.password = 'B3rrySfdc01!';
        String transitionOptions = 'Test';
        String attributeName = 'Test';
        String queryWhereClause = 'Test';
        String orderByClause = 'Test';
        Integer firstRecord = 1234;
        Integer maxReturnSize = 1234;
        Boolean breakLock = true;
        Integer attachmentID = 1234;
        Boolean getCurrentUser = true;
        String searchByName = 'Test';
        String reportCategory = 'Test';
        String reportAccessLevel = 'Test';
        String tableType = 'Test';

        invkSrvs.DeleteItemsByQuery(ath, tblIdfy, queryWhereClause, mltOptns);
        invkSrvs.Logout(ath, optns);
        invkSrvs.IsUserValid(ath, user_y, optns);
        invkSrvs.RunReport(ath, qryRng, rptIdfy, slnIdfy, prjtIdfy, tblIdfy, reportCategory, reportAccessLevel, optns);
        invkSrvs.GetSubmitProjects(ath, tblIdfy, optns);
        invkSrvs.CreateFileAttachment(ath, idfy, flatchcnt, optns);
        invkSrvs.TransitionItems(ath, itm1, transn, breakLock, mlRspOptn);
        invkSrvs.DeleteAttachment(ath, attachmentID, optns);
        invkSrvs.CreatePrimaryItems(ath, prjtIdfy, idfy, itm1, transn, mlRspOptn);
        invkSrvs.GetAvailableSubmitTransitions(ath, prjtIdfy, attributeName, optns);

        Test.stopTest();

    }
  
    @isTest static void testMockWbSrvs4() {

        Test.startTest();

        Test.setMock(WebServiceMock.class, new localhost80GsoapSbmappservicesMkTst());
        localhost80GsoapSbmappservices72Wsdl.sbmappservices72 invkSrvs = new localhost80GsoapSbmappservices72Wsdl.sbmappservices72();
        sbmappservices72.Auth ath = new sbmappservices72.Auth();
        sbmappservices72.ItemIdentifier idfy = new sbmappservices72.ItemIdentifier();
        sbmappservices72.Options optns = new sbmappservices72.Options();
        sbmappservices72.QueryRange qryRng = new sbmappservices72.QueryRange();
        sbmappservices72.ReportsFilter rptFltr = new sbmappservices72.ReportsFilter();
        sbmappservices72.TableIdentifier tblIdfy = new sbmappservices72.TableIdentifier();
        sbmappservices72.TTItem itm = new sbmappservices72.TTItem();
        sbmappservices72.TransitionIdentifier transn = new sbmappservices72.TransitionIdentifier();
        sbmappservices72.ResponseItemOptions rspOptns = new sbmappservices72.ResponseItemOptions();
        sbmappservices72.MultipleResponseItemOptions mlRspOptn = new sbmappservices72.MultipleResponseItemOptions();
        sbmappservices72.FileAttachmentContents flatchcnt = new sbmappservices72.FileAttachmentContents();
        sbmappservices72.ItemIdentifier[] itmIdfy = new sbmappservices72.ItemIdentifier[] {};
        sbmappservices72.MultipleOptions mltOptns = new sbmappservices72.MultipleOptions();
        sbmappservices72.UserIdentifier[] user_x = new sbmappservices72.UserIdentifier[] {};
        sbmappservices72.UserIdentifier user_y = new sbmappservices72.UserIdentifier();
        sbmappservices72.UserResponseOptions usrRspOptns = new sbmappservices72.UserResponseOptions();
        sbmappservices72.ReportData rptData = new sbmappservices72.ReportData();
        sbmappservices72.ProjectIdentifier prjtIdfy = new sbmappservices72.ProjectIdentifier();
        sbmappservices72.NoteAttachmentContents ntAtchcnt = new sbmappservices72.NoteAttachmentContents();
        sbmappservices72.TTItem[] itm1 = new sbmappservices72.TTItem[] {};
        sbmappservices72.ReportIdentifier rptIdfy = new sbmappservices72.ReportIdentifier();
        sbmappservices72.SolutionIdentifier slnIdfy = new sbmappservices72.SolutionIdentifier();
        ath.userId = 'bsfdc';
        ath.password = 'B3rrySfdc01!';
        String transitionOptions = 'Test';
        String attributeName = 'Test';
        String queryWhereClause = 'Test';
        String orderByClause = 'Test';
        Integer firstRecord = 1234;
        Integer maxReturnSize = 1234;
        Boolean breakLock = true;
        Integer attachmentID = 1234;
        Boolean getCurrentUser = true;
        String searchByName = 'Test';
        String reportCategory = 'Test';
        String reportAccessLevel = 'Test';
        String tableType = 'Test';

        invkSrvs.GetNoteLoggerInfo(ath, optns);
        invkSrvs.GetTables(ath, slnIdfy, tableType, optns);

        Test.stopTest();

    }
}