@isTest
public class UploadContentControllerTest {
    static testMethod void testUploadContent() {
        Test.StartTest();
        //User u= [Select id, phone, Email from user where isActive=true and phone!=null and Email!=null and userRole.Name = 'Account Manager' limit 1];
        
        Canvass__c c= new Canvass__c(Name='Test Canvas');
        insert c;       
        
        Directory__c dir = new Directory__c(Name = 'test');
        insert dir;
        
        Directory_Edition__c DE = new Directory_Edition__c(Name = 'Test', Canvass__c = c.Id, Directory__c = dir.Id, Book_Status__c = 'BOTS');
        insert DE;
        
        UploadContentController obj = new UploadContentController(new ApexPages.StandardController(DE));
        obj.uploadContents();
        
        Test.StopTest();
    }
}