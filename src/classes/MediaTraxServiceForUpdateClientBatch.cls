global class MediaTraxServiceForUpdateClientBatch implements Database.Batchable<sObject>, Database.AllowsCallouts{
    global String SOQL = '';
    global String strAccRTId;
    global Database.QueryLocator start(Database.BatchableContext bc){
        strAccRTId = CommonMethods.getRedordTypeIdByName('Customer Account', 'Account');
        SOQL = 'SELECT Name, Account_Number__c, Client_Number__c, BillingCity, BillingCountry, Phone, BillingState, BillingStreet, BillingPostalCode, P4P_Spending_Cap__c, P4P_Spending_Cap_Type__c, Fax, Media_Trax_Client_ID__c, Is_Changed__c FROM Account  where Is_Changed__c = true and Media_Trax_Client_ID__c != null and Account_Number__c != null and Name != null and  BillingCity != null and BillingCountry != null and Phone != null and BillingState != null and  BillingStreet != null and BillingPostalCode != null and P4P_Spending_Cap__c != null and P4P_Spending_Cap_Type__c != null and  RecordTypeId =\''+strAccRTId+'\'';
        return Database.getQueryLocator(SOQL);
    }
    
    global void execute(Database.BatchableContext bc, list<Account> lstAccount){
        
        MediaTraxHeadingCalloutController_V1 objMediaTraxService = new MediaTraxHeadingCalloutController_V1();
        objMediaTraxService.ProcessMediaTraxForClientUpdateScope(lstAccount);
    }
    
    global void finish(Database.BatchableContext bc){
    }
}