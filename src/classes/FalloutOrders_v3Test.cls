@isTest
public class FalloutOrders_v3Test
{
    public static testMethod void testFalloutOrders_v3() 
    {
           Test.startTest(); 
           Canvass__c c=TestMethodsUtility.createCanvass(); 
           Directory__c objDir =new Directory__c(Name = 'testDirectory, KY', Directory_Code__c = '111111',Canvass__c=c.id);
           insert objDir;
           Directory_Section__c dsection=Testmethodsutility.createDirectorySection(objDir);
           FalloutOrders_v3 flo=new FalloutOrders_v3 ();
           flo.getrtypes();
           flo.getrtype();
           flo.setrtype('1');
           flo.setrtype('2');
           flo.gettypes();
           flo.gettype();
           flo.settype('1');
           flo.populateCanvass();
           flo.getCanvass();
           flo.getcanvasses();
           flo.setcanvass(c.Id);
           flo.populateDirectory();
           flo.getdirectory();
           flo.getdirectories();
           flo.setdirectories(objDir.Id);
           flo.populateSection();
           flo.getdirectoriesSection();
           flo.setdirectoriesSection(new String[] {dsection.id});
           flo.getdirectorySection();
           //flo.generateReport();
           Test.stopTest(); 
    }
    
   
   public static testMethod void PositivetestgenerateReportforSAR() 
    {
        Canvass__c c=TestMethodsUtility.generateCanvass();
        c.Billing_Entity__c='Test';
        insert c;
        
        list<Account> lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        lstAccount.add(TestMethodsUtility.generateAccount('customer'));
        lstAccount.add(TestMethodsUtility.generateAccount('publication'));
        insert lstAccount;  
        Account newAccount = new Account();
        Account newPubAccount = new Account();
        Account newTelcoAccount = new Account();
        for(Account iterator : lstAccount) {
            if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
                newAccount = iterator;
            }
        }
        
        Telco__c objTelco =TestMethodsUtility.createTelco(newTelcoAccount.Id);
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
        
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        
        Division__c objDiv = TestMethodsUtility.createDivision();
        
        Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Recives_Electronice_File__c=true;
        objDir.Telco_Provider__c = objTelco.Id;
        objDir.Canvass__c = newAccount.Primary_Canvass__c;        
        objDir.Publication_Company__c = newPubAccount.Id;
        objDir.Division__c = objDiv.Id;
        insert objDir;
        
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
        Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
        insert objSHM;
        
        Directory_Edition__c objDirE = new Directory_Edition__c();
        objDirE.Name = 'Test DirE1';
        objDirE.Directory__c = objDir.Id;
        objDirE.Letter_Renewal_Stage_1__c = system.today();
        objDirE.Sales_Lockout__c=Date.today().addDays(30);
        objDirE.book_status__c='NI';
        objDirE.Pub_Date__c=System.today().addMOnths(1);
        insert objDirE;
               
        Product2 objProd = new Product2();
        objProd.Name = 'Test';
        objProd.Product_Type__c = 'Print';
        objProd.ProductCode = 'WLCSH';
        objProd.Print_Product_Type__c='Display';
        insert  objProd;
                
        Spec_Art_Request__c SAR=new Spec_Art_Request__c ();
        SAR.About_Business__c='TECHNICAL';
        SAR.Account__c=newAccount.Id;
        SAR.Miles_Status__c='In Progress';
        SAR.Advertised_business_email__c='test@test.com';
        SAR.Advertised_Business_Potalcode__c='23242';
        SAR.Advertised_Listed_Address__c='test';
        SAR.Advertised_Listed_City__c='test';
        SAR.Advertised_Listed_Phone_Number__c='2343234340';
        SAR.Advertised_Listed_State__c='US';
        SAR.Appointment_Date__c=Date.today();
        SAR.Contact__c=newContact.id;
        SAR.Date_Graphics_Complete__c =Datetime.now();
        SAR.Directory_Section__c=objDS.Id;
        SAR.Directory__c=Objdir.Id;
        SAR.Division__c=objDiv.Id;
        SAR.Edition__c=objDirE.Id;
        SAR.Migrate_to_Miles_33__c=Datetime.now();
        SAR.Product__c=objProd.Id;
        SAR.Section_Heading_Mapping__c=objSHM.Id;
        SAR.Spec_Art_URL__c='https://google.com'; 
        insert SAR;
        
        System.debug('$$$$$$$$$$$$$$$$$'+SAR);
        
        Note n = new Note();
        n.parentId= SAR.id; 
        n.body='Test'; 
        n.title='ERR'; 
        n.isPrivate=false;  
        insert n;  
           
           FalloutOrders_v3 flo=new FalloutOrders_v3 ();
           flo.setrtype('1');
           flo.settype('2');
           flo.setcanvass(c.Id);
           flo.setdirectories(objDir.Id);
           flo.setdirectoriesSection(new String[] {objDS.id});
           flo.generateReport();
           flo.setcanvass('0');
           flo.generateReport();
    }
    
     public static testMethod void PositivetestgeneratePrintReportforDFF() 
    {
        Test.startTest(); 
        Canvass__c c=TestMethodsUtility.generateCanvass();
        c.Billing_Entity__c='Test';
        insert c;
        
        list<Account> lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        lstAccount.add(TestMethodsUtility.generateAccount('customer'));
        lstAccount.add(TestMethodsUtility.generateAccount('publication'));
        insert lstAccount;  
        Account newAccount = new Account();
        Account newPubAccount = new Account();
        Account newTelcoAccount = new Account();
        for(Account iterator : lstAccount) {
            if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
                newAccount = iterator;
            }
            else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
                newPubAccount = iterator;
            }
            else {
                newTelcoAccount = iterator;
            }
        }
        Telco__c objTelco =TestMethodsUtility.createTelco(newTelcoAccount.Id);
        //Telco__c objTelco = [Select Id, Name from Telco__c where Account__c =:newTelcoAccount.Id Limit 1];
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
        system.assertNotEquals(newTelcoAccount.ID, null);
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        
        Division__c objDiv = TestMethodsUtility.createDivision();
        
        Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Recives_Electronice_File__c=true;
        objDir.Telco_Provider__c = objTelco.Id;
        objDir.Canvass__c = newAccount.Primary_Canvass__c;        
        objDir.Publication_Company__c = newPubAccount.Id;
        objDir.Division__c = objDiv.Id;
        insert objDir;
        
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
        Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
        insert objSHM;
        
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
        objDirEd .New_Print_Bill_Date__c=date.today();
        objDirEd .Bill_Prep__c=date.parse('01/01/2013');
        objDirEd.XML_Output_Total_Amount__c=100;
        objDirEd.Pub_Date__c=System.today().addMOnths(2);
        insert objDirEd;
        
        Directory_Edition__c objDirE = new Directory_Edition__c();
        objDirE.Name = 'Test DirE1';
        objDirE.Directory__c = objDir.Id;
        objDirE.Letter_Renewal_Stage_1__c = system.today();
        objDirE.Sales_Lockout__c=Date.today().addDays(30);
        objDirE.book_status__c='NI';
        objDirE.Pub_Date__c=System.today().addMOnths(3);
        insert objDirE;
        
        Product2 objProd = new Product2();
        objProd.Name = 'Test';
        objProd.Product_Type__c = 'Print';
        objProd.ProductCode = 'WLCSH';
        objProd.Print_Product_Type__c='Display';
        insert  objProd;
        
        Product2 objProd1 = new Product2();
        objProd1.Name = 'Test';
        objProd1.Product_Type__c = 'Print';
        objProd1.ProductCode = 'GC50';
        objProd1.Print_Product_Type__c='Specialty';          
        insert objProd1;
        
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        insert newOpportunity;
        
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
        
        Order_Line_Items__c objOrderLineItem = new Order_Line_Items__c(Billing_Partner__c='Hawain Telecom',
        Account__c=newAccount.Id, Billing_Contact__c=newContact.id, Opportunity__c=newOpportunity.id, 
        Order_Group__c=newOrderSet.id,Order__c=newOrder.id,Product2__c=objProd.Id,is_p4p__c=false,media_type__c='Print',
        Directory_Edition__c = objDirE.Id,Directory__c=Objdir.Id,canvass__c=c.id,UnitPrice__c=200,Payment_Duration__c=12,
        Payment_Method__c='Telco Billing',Package_ID__c='pkgid_12',Payments_Remaining__c=11,Successful_Payments__c=1,
        Directory_Section__c = objDS.id);
        insert objOrderLineItem;
        
        Test.stopTest(); 
        Digital_Product_Requirement__c objDFF=TestMethodsUtility.generateDataFulfillmentForm();
        objDFF.Caption_Indent_Level__c=3;
        objDFF.Caption_Indent_Order__c=1;
        objDFF.URN_text__c='TEST';
        objDFF.OrderLineItemID__c=objOrderLineItem.Id;
        objDFF.DFF_Directory_Section__c=objDS.Id;
        objDFF.Directory_Edition__c=objDirE.id;
        objDFF.URN_number__c=Double.valueof(String.valueof(crypto.getRandomLong()).substring(0,10));
        objDFF.Miles_Status__c='In Progress';
        objDFF.Migrate_to_Miles_33__c=Datetime.now();
        objDFF.Status__c='Complete';
        objDFF.Date_Graphics_Received__c=Datetime.now();
        objDFF.Account__c=newAccount.Id;
        //objDFF.Account_ID__c=newAccount.Id;
        objDFF.Graphics_Complete_Date__c=Datetime.now();
        insert objDFF;
        
        Note n = new Note();
        n.parentId= objDFF.id; 
        n.body='Test'; 
        n.title='ERR'; 
        n.isPrivate=false;  
        insert n;  
        
        Spec_Art_Request__c SAR=new Spec_Art_Request__c ();
        SAR.About_Business__c='TECHNICAL';
        SAR.Account__c=newAccount.Id;
        SAR.Miles_Status__c='In Progress';
        SAR.Advertised_business_email__c='test@test.com';
        SAR.Advertised_Business_Potalcode__c='23242';
        SAR.Advertised_Listed_Address__c='test';
        SAR.Advertised_Listed_City__c='test';
        SAR.Advertised_Listed_Phone_Number__c='2343234340';
        SAR.Advertised_Listed_State__c='US';
        SAR.Appointment_Date__c=Date.today();
        SAR.Contact__c=newContact.id;
        SAR.Date_Graphics_Complete__c =Datetime.now();
        SAR.Directory_Section__c=objDS.Id;
        SAR.Directory__c=Objdir.Id;
        SAR.Division__c=objDiv.Id;
        SAR.Edition__c=objDirE.Id;
        SAR.Migrate_to_Miles_33__c=Datetime.now();
        SAR.Product__c=objProd.Id;
        SAR.Section_Heading_Mapping__c=objSHM.Id;
        SAR.Spec_Art_URL__c='https://google.com'; 
        insert SAR;
        
        System.debug('$$$$$$$$$$$$$$$$$'+SAR);
        
        Note nsar = new Note();
        nsar.parentId= SAR.id; 
        nsar.body='Test'; 
        nsar.title='ERR'; 
        nsar.isPrivate=false;  
        insert nsar; 
          
           FalloutOrders_v3 flo=new FalloutOrders_v3 ();
           flo.setrtype('1');
           flo.settype('1');
           flo.setcanvass(c.Id);
           flo.setdirectories(objDir.Id);
           flo.setdirectoriesSection(new String[] {objDS.id});
           flo.generateReport();
           flo.setcanvass('0');
           flo.generateReport();
           flo.setdirectories('0');
           flo.generateReport();
           flo.setcanvass(c.Id);
           flo.generateReport();
           flo.settype('2');
           flo.generateReport();
           flo.setcanvass('0');
           flo.generateReport();
           
           FalloutOrders_v3 flo1=new FalloutOrders_v3 ();
           flo1.setrtype('1');
           flo1.settype('2');
           flo1.setcanvass(c.Id);
           flo1.setdirectories(objDir.Id);
           flo1.setdirectoriesSection(new String[] {objDS.id});
           flo1.generateReport();
           flo1.setcanvass('0');
           flo1.generateReport();
           
           FalloutOrders_v3.PrintReportWrapper prw=new FalloutOrders_v3.PrintReportWrapper(
           String.valueof(objDFF.Name),objDFF.id,
           String.valueof(objDFF.OrderLineItemID__r.Order_Group__r.Name),String.valueof(objDFF.URN_number__c),
           null,String.valueof(objDFF.Miles_Status__c),String.valueof(objDFF.Account__r.Name),
           String.valueof(objDFF.Directory_Edition__r.name),String.valueof(objDFF.DFF_Directory_Section__r.name),
           String.valueof(objDFF.Migrate_to_Miles_33__c),String.valueof('ERR'),
           String.valueof('Test User'));
                                     
    }
    
     public static testMethod void NegativetestgenerateDigitalReport() 
    {
        Test.startTest(); 
        Canvass__c c=TestMethodsUtility.generateCanvass();
        c.Billing_Entity__c='Test';
        insert c;
        
        list<Account> lstAccount = new list<Account>();
        lstAccount.add(TestMethodsUtility.generateAccount('telco'));
        lstAccount.add(TestMethodsUtility.generateAccount('customer'));
        lstAccount.add(TestMethodsUtility.generateAccount('publication'));
        insert lstAccount;  
        Account newAccount = new Account();
        Account newPubAccount = new Account();
        Account newTelcoAccount = new Account();
        for(Account iterator : lstAccount) {
            if(String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountCustomerRT)) {
                newAccount = iterator;
            }
            else if (String.valueOf(iterator.RecordTypeId).contains(System.Label.TestAccountPubCoRT)) {
                newPubAccount = iterator;
            }
            else {
                newTelcoAccount = iterator;
            }
        }
        Telco__c objTelco =TestMethodsUtility.createTelco(newTelcoAccount.Id);
        //Telco__c objTelco = [Select Id, Name from Telco__c where Account__c =:newTelcoAccount.Id Limit 1];
        objTelco.Telco_Code__c = 'Test';
        update objTelco;
        system.assertNotEquals(newTelcoAccount.ID, null);
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        
        Division__c objDiv = TestMethodsUtility.createDivision();
        
        Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Recives_Electronice_File__c=true;
        objDir.Telco_Provider__c = objTelco.Id;
        objDir.Canvass__c = newAccount.Primary_Canvass__c;        
        objDir.Publication_Company__c = newPubAccount.Id;
        objDir.Division__c = objDiv.Id;
        insert objDir;
        
        Directory_Heading__c objDH = TestMethodsUtility.createDirectoryHeading();
        Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
        Section_Heading_Mapping__c objSHM = TestMethodsUtility.generateSectionHeadingMapping(objDS.Id, objDH.Id);
        insert objSHM;
        
        Directory_Edition__c objDirEd = TestMethodsUtility.generateDirectoryEdition(objDir);
        objDirEd .New_Print_Bill_Date__c=date.today();
        objDirEd .Bill_Prep__c=date.parse('01/01/2013');
        objDirEd.XML_Output_Total_Amount__c=100;
        objDirEd.Pub_Date__c=System.today().addMOnths(4);
        insert objDirEd;
        
        Directory_Edition__c objDirE = new Directory_Edition__c();
        objDirE.Name = 'Test DirE1';
        objDirE.Directory__c = objDir.Id;
        objDirE.Letter_Renewal_Stage_1__c = system.today();
        objDirE.Sales_Lockout__c=Date.today().addDays(30);
        objDirE.book_status__c='NI';
        objDirE.Pub_Date__c=System.today().addMOnths(5);
        insert objDirE;
        
        Directory_Edition__c objDirE1 = new Directory_Edition__c();
        objDirE1.Name = 'Test DirE2';
        objDirE1.Directory__c = objDir.Id;
        objDirE1.Letter_Renewal_Stage_1__c = system.today();
        objDirE1.Sales_Lockout__c=Date.today().addDays(30);
        objDirE1.book_status__c='BOTS';
        objDirE1.Pub_Date__c=System.today().addMOnths(6);
        insert objDirE1;     
        
        Directory__c objDirNew = TestMethodsUtility.generateDirectory();
        objDirNew.Directory_Code__c = '111567';
        objDirNew .Canvass__c = newAccount.Primary_Canvass__c;        
        objDirNew .Publication_Company__c = newPubAccount.Id;
        objDirNew .Division__c = objDiv.Id;
        insert objDirNew ;
        Directory_Edition__c objDirEdNew = TestMethodsUtility.generateDirectoryEdition(objDirNew );
        objDirEdNew.Pub_Date__c=System.today().addMOnths(7);
        objDirEdNew.XML_Output_Total_Amount__c=200;
        insert objDirEdNew;
        
        Product2 newProduct = TestMethodsUtility.generateproduct();
        newProduct.Family = 'Digital';
        insert newProduct;
        
        Product2 objProd = new Product2();
        objProd.Name = 'Test';
        objProd.Product_Type__c = 'Digital';
        objProd.ProductCode = 'WLCSH';
        objProd.Print_Product_Type__c='Display';
        objProd.Vendor__c='YP';
        insert  objProd;
        
        Product2 objProd1 = new Product2();
        objProd1.Name = 'Test';
        objProd1.Product_Type__c = 'Digital';
        objProd1.ProductCode = 'GC50';
        objProd1.Print_Product_Type__c='Specialty';
        objProd1.Vendor__c='YP';          
        insert objProd1;
        
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        insert newOpportunity;
        
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
        
        Order_Line_Items__c objOrderLineItem = new Order_Line_Items__c(Billing_Partner__c='Hawain Telecom',
        Account__c=newAccount.Id, Billing_Contact__c=newContact.id, Opportunity__c=newOpportunity.id,
        Order_Group__c=newOrderSet.id,Order__c=newOrder.id,Product2__c=objProd.Id,is_p4p__c=false,media_type__c='Print',
        Directory_Edition__c = objDirE1.Id,Directory__c=Objdir.Id,canvass__c=c.id,UnitPrice__c=200,Payment_Duration__c=12,
        Payment_Method__c='Telco Billing',Package_ID__c='pkgid_12',Payments_Remaining__c=11,Successful_Payments__c=1,Directory_Section__c = objDS.id);
        insert objOrderLineItem;
        Test.stopTest(); 
        
        Digital_Product_Requirement__c objDFF=TestMethodsUtility.generateDataFulfillmentForm();
        objDFF.Caption_Indent_Level__c=3;
        objDFF.Caption_Indent_Order__c=1;
        objDFF.URN_text__c='TEST';
        objDFF.OrderLineItemID__c=objOrderLineItem.Id;
        objDFF.DFF_Directory_Section__c=objDS.Id;
        objDFF.Directory_Edition__c=objDirE.id;
        objDFF.URN_number__c=Double.valueof(String.valueof(crypto.getRandomLong()).substring(0,10));
        objDFF.Miles_Status__c='In Progress';
        objDFF.Migrate_to_Miles_33__c=Datetime.now();
        objDFF.Status__c='Complete';
        objDFF.Date_Graphics_Received__c=Datetime.now();
        objDFF.Account__c=newAccount.Id;
        //objDFF.Account_ID__c=newAccount.Id;
        objDFF.Graphics_Complete_Date__c=Datetime.now();
        insert objDFF;
        
        Note n = new Note();
        n.parentId= objDFF.id; 
        n.body='Test'; 
        n.title='ERR'; 
        n.isPrivate=false;  
        insert n;  
        
        YPC_Graphics__c YPC=new YPC_Graphics__c();
        YPC.Name='YPC123242';
        YPC.DFF__c=objDFF.Id;
        YPC.Date_Graphics_Complete__c=Datetime.now();
        YPC.Date_Graphics_Received__c=Datetime.now();
        YPC.Date_Submitted__c=Datetime.now();
        YPC.YPC_Graphics_Type__c='Logo';
        YPC.Migrate_to_Miles_33__c=Datetime.now();
        YPC.Miles33_ImportAdvert__c='TEST';
        YPC.Miles_Status__c='In Progress';
        YPC.Production_Notes_for_YPC_Graphic__c='TEST';
        YPC.YPC_Graphics_URL__c='https://www.google.com'; 
        insert YPC;
          
       FalloutOrders_v3 flo=new FalloutOrders_v3 ();
       flo.setrtype('2');
       flo.generateReport();
       
        YPC_Graphics__c YPC1=new YPC_Graphics__c();
        YPC1.Name='YPC123242';
        YPC1.DFF__c=objDFF.Id;
        YPC1.Date_Graphics_Complete__c=Datetime.now();
        YPC1.Date_Graphics_Received__c=Datetime.now();
        YPC1.Date_Submitted__c=Datetime.now();
        YPC1.YPC_Graphics_Type__c='Logo';
        YPC1.Migrate_to_Miles_33__c=Datetime.now();
        YPC1.Miles33_ImportAdvert__c='TEST';
        YPC1.Miles_Status__c='In Progress';
        YPC1.Production_Notes_for_YPC_Graphic__c='TEST';
        YPC1.YPC_Graphics_URL__c='https://www.google.com'; 
        insert YPC1;
       
       FalloutOrders_v3 flo1=new FalloutOrders_v3 ();
       flo1.setrtype('2');
       flo1.generateReport();
     }
}