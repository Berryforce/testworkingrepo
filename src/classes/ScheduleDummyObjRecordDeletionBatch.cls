global class ScheduleDummyObjRecordDeletionBatch   Implements Schedulable {
    global void execute(SchedulableContext sc) {
        DummyObjRecordDeletionBatch   obj = new DummyObjRecordDeletionBatch();
        Database.executeBatch(obj);
    }
}