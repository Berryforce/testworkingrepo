global class DFFReminderNotification implements Database.Batchable<sObject>
{        
   global Database.QueryLocator start(Database.BatchableContext BC)
   {
    return Database.getQueryLocator([SELECT Id,Fulfillment_Submit_Status__c,RecordType.DeveloperName,Miles_Status__c,Account__c,Account__r.Owner.Email,Account__r.Account_Manager__r.Email, OpportunityID__c,OrderLineItemID__c FROM Digital_Product_Requirement__c 
                                    where  Account__c!=Null AND OrderLineItemID__c!=Null AND      
                                    OpportunityID__c!=Null AND Account_Manager__c!=Null]);

   }


   global void execute(Database.BatchableContext info, List<Digital_Product_Requirement__c> Scope)
   {
    system.debug('**********Scope Lst********'+Scope.size());
    set<Id> objdffId = new set<id>();
    map<Id,Digital_Product_Requirement__c> mapDff = new map<Id,Digital_Product_Requirement__c>();

        for(Digital_Product_Requirement__c DFF:Scope)
        {   
            if((DFF.Miles_Status__c=='New' || DFF.Miles_Status__c=='In Progress') && (DFF.RecordType.DeveloperName=='iYP_Bronze' || DFF.RecordType.DeveloperName=='iYP_Bronze_5_Heading' || DFF.RecordType.DeveloperName=='iYP_Silver' || DFF.RecordType.DeveloperName=='iYP_Silver_5_Heading' || DFF.RecordType.DeveloperName=='iYP_Gold' || DFF.RecordType.DeveloperName=='iYP_Gold_5_Heading' || DFF.RecordType.DeveloperName=='iYP_Priority' || DFF.RecordType.DeveloperName=='iYP_Priority'))
            {
                objdffId.add(DFF.Id);
            }
            else
            if(DFF.Fulfillment_Submit_Status__c==Null)
            {
                objdffId.add(DFF.Id);
            }
        }
        system.debug('**********Scope Lst SET ids********'+objdffId);
        list<Digital_Product_Requirement__c> dffLst = new list<Digital_Product_Requirement__c>();
        if(objdffId.size()>0){
            dffLst=[select id,Account__r.Name ,UDAC__c,Account__r.owner.Email,DFFcloseddate__c,UnitPrice__c,Account__r.Account_Manager__r.Email from Digital_Product_Requirement__c where id IN:objdffId];
        }
        system.debug('**********Scope Lst dff lst *******'+dffLst);
        if(dffLst.size()>0){
            for(Digital_Product_Requirement__c objDff : dffLst)
            {
                mapDff.put(objDff.Id,objDff);
            }
        }
        system.debug('**********Scope Lst dff map******'+mapDff);
        if(mapDff.size()>0)
        {
            sendmail(mapDff);
        }
}
        public void sendmail(map<Id,Digital_Product_Requirement__c> mapDffEmail){
       
        List<Messaging.SingleEmailMessage> lstEmail= new list<Messaging.SingleEmailMessage>();

        for(ID dffid:mapDffEmail.keyset())
        {
          Digital_Product_Requirement__c objDFFEmail = mapDffEmail.get(dffid);
          Messaging.SingleEmailMessage email = new Messaging.SingleEmailMessage();
          string [] toaddress;
          string [] ccAddresses;
          system.debug('**********Scope account owner email********'+objDFFEmail.Account__r.Owner.Email);
          system.debug('**********Scope account owner manager email********'+objDFFEmail.Account__r.Account_Manager__r.Email);
          if(objDFFEmail.Account__r.Owner.Email!=Null)
          {
          toaddress= New string[]{objDFFEmail.Account__r.Owner.Email};
          }
          if(objDFFEmail.Account__r.Account_Manager__r.Email!=Null)
          {
          ccAddresses= New string[]{objDFFEmail.Account__r.Account_Manager__r.Email};
          }
          email.setToAddresses(toaddress);
          email.setccAddresses(ccAddresses);
          email.setWhatID(dffid); 
          email.setSubject('Reminder Notification for DFF ' );    
          email.setBccSender(false);
          email.setPlainTextBody('Message – Urgent: The completion of the fulfillment process for the above product requires your immediate attention.');
          email.setHtmlBody('<html> <b><img width="120" src="https://c.cs11.content.force.com/servlet/servlet.ImageServer?id=015Z00000006QIz&oid=00DZ0000001C48o&lastMod=1384862875000"> <br>  Message – Urgent:</b> The completion of the fulfillment process for the Below product requires your immediate attention.<br> <br> <b>Account Name</b>:' + objDFFEmail.Account__r.Name +' <br><br>  ' + '<b>Product Code  </b>:' + objDFFEmail.UDAC__c +' </b> <br><br> ' + '<b>Opportunity closed date </b>:' + objDFFEmail.DFFcloseddate__c +' </b>  <br>'  + '<br> <b> Sold Amount  </b>:' + objDFFEmail.UnitPrice__c +' </b> <br> <br> Please complete the fulfillment process for the above product. Another reminder will be sent 24 hours from today. <br> <img width="120" src="https://c.cs11.content.force.com/servlet/servlet.ImageServer?id=015Z00000006QIz&oid=00DZ0000001C48o&lastMod=1384862875000"></html> ');
          //email.setTemplateId(label.DFFremainder1);
          email.setSaveAsActivity(false);
          lstEmail.add(email);

        }
         Messaging.sendEmail(lstEmail);

        }
  
    global void finish(Database.BatchableContext info)
    {

    }
}