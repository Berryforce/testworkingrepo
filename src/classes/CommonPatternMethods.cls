public class CommonPatternMethods {
	public static String replacePhoneSpecialCharString(string fieldValue) {
            string replacedRCF;
            Pattern nonWordChar = Pattern.compile('[^\\w]'); 
            if(!string.ISBLANK(fieldValue))
            	replacedRCF = nonWordChar.matcher(fieldValue).replaceAll('');
            return replacedRCF;
    }
}