@isTest(seeAllData=true)
public with sharing class NewSalesInvoiceLineItemControllerTest {
    static testMethod void newSILI() {
        Test.startTest();
        Canvass__c c=TestMethodsUtility.createCanvass();
        Account newaccount = TestMethodsUtility.createaccount(c);
        insert newaccount ;
        Contact newContact = TestMethodsUtility.generateContact(newAccount.id);
        insert newContact ;
        Opportunity opp =TestMethodsUtility.createOpportunity(newaccount,newContact );
        insert opp;
        Directory__c dir=new Directory__c(Name = 'testDirectory, KY', Directory_Code__c = '111111',Canvass__c=c.id);
        insert dir;
        Directory_Edition__c objDirE = new Directory_Edition__c();
        objDirE.Name = 'Test DirE1';
        objDirE.Directory__c = dir.Id;
        objDirE.Letter_Renewal_Stage_1__c = system.today();
        objDirE.Sales_Lockout__c=Date.today().addDays(30);
        objDirE.book_status__c='NI';
        insert objDirE;
        Directory_Section__c objDirSec = new Directory_Section__c();
        objDirSec.Directory__c = dir.id;
        objDirSec.Name = 'Test Dir Section';
        objDirSec.Section_Page_Type__c = 'WP';
        objDirSec.Section_Code__c = '111111';
        insert objDirSec; 
        
        Telco__c newTelco = TestMethodsUtility.createTelco(newAccount.Id);
        Pricebook2 newPriceBook = [Select Id from Pricebook2 where isStandard=true limit 1];
        Product2 newProduct = TestMethodsUtility.generateproduct();
        newProduct.Family = 'Print';
        insert newProduct;
        Product2 objProd = new Product2();
        objProd.Name = 'Test';
        objProd.Product_Type__c = 'Print';
        objProd.ProductCode = 'WLCSH';
        objProd.Print_Product_Type__c='Display';
        objProd.Exclude_Dimension_1_3__c = true;
        
        insert  objProd;
        Product2 objProd1 = new Product2();
        objProd1.Name = 'Test';
        objProd1.Product_Type__c = 'Print Graphic';
        objProd1.ProductCode = 'GC50';
        objProd1.Print_Product_Type__c='Specialty';  
        objProd1.Exclude_Dimension_1_3__c = true;
        
        insert objProd1;
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        insert newOpportunity;
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        list<Order_Line_Items__c> lstOrderLI = new list<Order_Line_Items__c>();
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
        // Order_Line_Items__c newOrLI = TestMethodsUtility.createOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet);     
        
        Order_Line_Items__c objOrderLineItem1 = new Order_Line_Items__c(Successful_Payments__c=4,Directory_Section__c=objDirSec.id ,Billing_Partner__c=newAccount.Id,Account__c=newAccount.Id, Billing_Contact__c=newContact.id, Opportunity__c=newOpportunity.id, Order_Group__c=newOrderSet.id,Order__c=newOrder.id,Product2__c=objProd.Id,is_p4p__c=false,media_type__c='Digital',Directory_Edition__c = objDirE.Id,Directory__c=dir.id,canvass__c=c.id,UnitPrice__c=100,Payment_Duration__c=12,Payment_Method__c='Credit Card',Billing_Frequency__c='Single Payment',Package_ID__c='pkgid_12',Payments_Remaining__c=2,Service_End_Date__c=System.today().addMonths(12),Talus_Go_Live_Date__c=System.today(),Service_Start_Date__c=System.today(),Order_Anniversary_Start_Date__c=System.today().addMonths(2));
        objOrderLineItem1.Billing_Partner_Account__c=newaccount.id;
        objOrderLineItem1.Quantity__c = 1;
        objOrderLineItem1.Cancelation_Fee__c=50;
        
        Order_Line_Items__c objOrderLineItem2 = new Order_Line_Items__c(Successful_Payments__c=4,Directory_Section__c=objDirSec.id ,Billing_Partner__c=newAccount.Id,Account__c=newAccount.Id, Billing_Contact__c=newContact.id, Opportunity__c=newOpportunity.id, Order_Group__c=newOrderSet.id,Order__c=newOrder.id,Product2__c=objProd.Id,is_p4p__c=false,media_type__c='Digital',Directory_Edition__c = objDirE.Id,Directory__c=dir.id,canvass__c=c.id,UnitPrice__c=100,Payment_Duration__c=12,Payment_Method__c='Credit Card',Billing_Frequency__c='Single Payment',Package_ID__c='pkgid_12',Payments_Remaining__c=2,Service_End_Date__c=System.today().addMonths(12),Talus_Go_Live_Date__c=System.today(),Service_Start_Date__c=System.today(),Order_Anniversary_Start_Date__c=System.today().addMonths(2));
        objOrderLineItem2.Billing_Partner_Account__c=newaccount.id;
        objOrderLineItem2.Quantity__c = 1;
        objOrderLineItem2.Cancelation_Fee__c=50;                
        
        lstOrderLI.add(objOrderLineItem1);
        lstOrderLI.add(objOrderLineItem2);
        
        insert lstOrderLI;
        c2g__codaDimension1__c  dimension1=TestMethodsUtility.createDimension1(dir);
        c2g__codaDimension2__c  dimension2=TestMethodsUtility.createDimension2(objProd1);
        c2g__codaDimension3__c  dimension3=TestMethodsUtility.createDimension3(objOrderLineItem1);
        
        c2g__codaInvoice__c CodaInvoice = TestMethodsUtility.createSalesInvoice(newAccount, newOpportunity); 
        
        c2g__codaInvoiceLineItem__c newSILI = TestMethodsUtility.generateSalesInvoiceLineItem(CodaInvoice, objProd1);
        
        apexpages.currentpage().getparameters().put('retURL' , CodaInvoice.Id);
        
        NewSalesInvoiceLineItemController obj = new NewSalesInvoiceLineItemController(new ApexPages.StandardController(newSILI));
        obj.newSILI = newSILI;
        obj.insertSILI();
        Test.stopTest();
    }
}