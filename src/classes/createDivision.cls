public with sharing class createDivision {
	public Division__c objDivision {get;set;}
	String retURL {get;set;}
	public createDivision(Apexpages.Standardcontroller controller) {
		objDivision = new Division__c();
		retURL = Apexpages.currentPage().getParameters().get('retURL');
	}
	
	public Pagereference saveData() {
		Group insertGroup;
		map<String, Group> mapGroup = new map<String, Group>();
		if(objDivision.Name != null) {
			list<Group> lstGroup = [Select Id, Name from Group where Type = 'Queue' and Name = :objDivision.Name limit 1];
			
			if(lstGroup.size() <= 0) {
				insertGroup = new Group(Type = 'Queue', Name = objDivision.Name);
				if(insertGroup != null) {
					insert insertGroup;
					QueueSobject insertSObject = new QueueSobject(QueueId = insertGroup.Id, SobjectType = 'Division__c');
					insert insertSObject;					
				}
				objDivision.OwnerId = insertGroup.Id;
				String strDivision = JSON.serialize(objDivision);
				CommonMethods.updateDivisionWithOwnerID(strDivision);
				return new Pagereference(retURL);
			}
			else {
				//TO DO -- Display the message duplicate name should not be
				CommonMethods.addError(CommonMessages.divisionQueueCheck);
				return null;
			}
			CommonMethods.addINFO(CommonMessages.successMessage);
		}
		return null;
	}
	
	public Pagereference cancel() {
		return new Pagereference(retURL);
	}
	
	public List<Schema.FieldSetMember> getFields() {
        return SObjectType.Division__c.FieldSets.CreateDivision.getFields();
    }
}