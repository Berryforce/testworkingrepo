global class SOLstSLDPDeleteBatch implements Database.Batchable<sObject> {
    private string SOBatchId;
    
    global SOLstSLDPDeleteBatch(string batchId){
        SOBatchId=batchId;
    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
        String query = 'SELECT Id,Batch_ID__c,Name,(Select Id, Lst_Batch_Id__c from Listings__r)from service_order_stage__c where Batch_ID__c=:SOBatchId';
        return Database.getQueryLocator(query);
    }
   
    global void execute(Database.BatchableContext BC, List<service_order_stage__c> objSOSLst) {
         set<Id> setSOId = new set<Id>();
         set<Id> setListingId = new set<Id>();
         set<Id> setnotMatchedListingId = new set<Id>();
         list<listing__c> listingLst = new list<listing__c>();
         list<Service_order_stage__c > SODelLst = new list<Service_order_stage__c >();
         list<Directory_listing__c> dlLst = new list<Directory_listing__c>();
         if(objSOSLst.size()> 0) {
             for(Service_order_stage__c iteratorSOS : objSOSLst) {
             system.debug('Listing Size&&&'+iteratorSOS.Listings__r.size());
                 if(iteratorSOS.Listings__r.size()> 0){
                     for(listing__c iteratorLst : iteratorSOS.Listings__r){
                         if(iteratorLst.Lst_Batch_ID__c == SOBatchId) {
	                         setListingId.add(iteratorLst.Id);
	                         listingLst.add(iteratorLst);
                         }
                         else {
                         	setnotMatchedListingId.add(iteratorLst.Id);
                         }
                         
                     }
                 }
                 SODelLst.add(iteratorSOS);
             }
         }
         if(setnotMatchedListingId.size()> 0) {
         	dlLst = [Select Id,Listing__c,SL_Batch_Id__c from Directory_listing__c where Listing__c IN : setnotMatchedListingId AND SL_Batch_Id__c = : SOBatchId];
         	if(dlLst.size()> 0) {
         		for(Directory_listing__c iteratorDL : dlLst){
         			setListingId.add(iteratorDL.listing__c);
         		}
         	}
         }
         if(setListingId.size()> 0){
             list<directory_pagination__c> DPLst = [Select Id from directory_pagination__c where DP_Listing__c IN : setListingId];
             if(DPLst.size()> 0) {
                 delete DPLst;
             }
         }
         if(dlLst.size()> 0) {
         	delete dlLst;
         }
         if(listingLst.size()> 0){
             delete listingLst;
         }
         if(SODelLst.size()> 0){
             delete SODelLst;
         }
    }   
    
    global void finish(Database.BatchableContext BC) {
        String strErrorMessage = '';
        AsyncApexJob a = AsyncApexJobSOQLMethods.getBatchDetails(BC.getJobId());
        //String[] toAddresses = new String[] {a.CreatedBy.Email};
        
        if(a.NumberOfErrors > 0){
            strErrorMessage = a.ExtendedStatus;
        }
    
        CommonEmailUtils.sendHTMLEmailForTargetObject(a.CreatedById, 'Purge Process for Batch '+ SOBatchId +' is '+ a.Status, 'The Batch Apex Job processed ' + a.JobItemsProcessed +
          ' batches with '+ a.NumberOfErrors +' failures.');
    }
}