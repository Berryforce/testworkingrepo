public with sharing class ListingToScopedListingSync {
    public static void syncListingToScopedListing(set<Id> setListingIds,map<Id,Listing__c> mapListing) {
    	list<Directory_Listing__c> slLst = new list<Directory_Listing__c>();
    	if(setListingIds.size()> 0){
    		slLst=DirectoryListingSOQLMethods.getScopedListingsByListingIds(setListingIds);
    	}
        // loop through all the scoped listing records to assign values
        List<Directory_Listing__c> updatedSLs = new List<Directory_Listing__c>();
        for (Directory_Listing__c SL : slLst){
            // check if it needs to be updated
        	if(SL.Follows_Listing_Record2__c == 'true'){
            	listing__c objListing = mapListing.get(SL.Listing__c);
                SL.Bus_Res_Gov_Indicator__c = objListing.Bus_Res_Gov_Indicator__c;
                SL.Cross_Reference_Text__c = objListing.Cross_Reference_Text__c;
                SL.Normalized_Designation__c = objListing.Normalized_Designation__c;
                SL.Disconnect_Reason__c = objListing.Disconnect_Reason__c;
                SL.Disconnected__c = objListing.Disconnected__c;
                SL.Effective_Date__c = objListing.Effective_Date__c;
                SL.First_Name__c = objListing.First_Name__c;
                SL.Honorary_Title__c = objListing.Honorary_Title__c;
                SL.SL_Last_Name_Business_Name__c = objListing.Lst_Last_Name_Business_Name__c;
                SL.Left_Telephone_Phrase__c = objListing.Left_Telephone_Phrase__c;
                SL.Lineage_Title__c = objListing.Lineage_Title__c;
                SL.Bus_Main_or_Additional__c = objListing.Listing_Type__c;
                SL.Manual_Sort_As_Override__c = objListing.Manual_Sort_As_Override__c;
                SL.Normalized_Last_Name_Business_Name__c = objListing.Normalized_Last_Name_Business_Name__c;
                SL.Normalized_First_Name__c = objListing.Normalized_First_Name__c;
                SL.Normalized_Listing_Street_Name__c = objListing.Normalized_Listing_Street_Name__c;
                SL.Normalized_Listing_Street_Number__c = objListing.Normalized_Listing_Street_Number__c;
                SL.Normalized_Phone__c = objListing.Normalized_Phone__c;
                SL.Omit_Address_OAD__c = objListing.Omit_Address_OAD__c;
                SL.Phone_Override__c = objListing.Phone_Override__c;
                SL.Phone_Type__c = objListing.Phone_Type__c;
                SL.Phone_Number__c = objListing.Phone__c;
                SL.Service_Order__c = objListing.Service_Order__c;
                SL.Telco_Provider__c = objListing.Telco_Provider__c;
                SL.Listing_street_number__c = objListing.Listing_Street_Number__c;
                SL.Listing_Street__c = objListing.Listing_Street__c;
                SL.Listing_City__c = objListing.Listing_City__c;
                SL.Listing_PO_Box__c = objListing.Listing_PO_Box__c;
                SL.Listing_State__c = objListing.Listing_State__c;
                SL.Listing_Postal_Code__c = objListing.Listing_Postal_Code__c;
                SL.Listing_Country__c = objListing.Listing_Country__c;
                SL.Right_Telephone_Phrase__c = objListing.Right_Telephone_Phrase__c;
                SL.Designation__c = objListing.Designation__c;
                SL.Telco_Sort_Order__c = objListing.Telco_Sort_Order__c;
                SL.Normalized_Honorary_Title__c = objListing.Normalized_Honorary_Title__c;
                SL.Normalized_Lineage_Title__c = objListing.Normalized_Lineage_Title__c;
                SL.Normalized_Secondary_Surname__c = objListing.Normalized_Secondary_Surname__c;
                SL.Disconnected_Via_BOC_Purge__c = objListing.Disconnected_Via_ABD__c;
                SL.ABD_Pending_Review__c = objListing.ABD_Pending_Review__c;
                updatedSLs.add(SL);
            }
        }
        system.debug('************Directory Listings***************'+updatedSLs);
        if (updatedSLs.size() > 0) {
            update updatedSLs;
        }
    }
    /*public static void syncNormalizedListingToScopedListing(set<Id> setListingIds,map<Id,Listing__c> mapListing) {
    	list<Directory_Listing__c> slLst = new list<Directory_Listing__c>();
    	if(setListingIds.size()> 0){
    		slLst=DirectoryListingSOQLMethods.getCMSlsByListingIdsforNormalization(setListingIds);
    	}
        // loop through all the scoped listing records to assign values
        List<Directory_Listing__c> updatedNormalisedSLs = new List<Directory_Listing__c>();
        for (Directory_Listing__c SL : slLst){
            	listing__c objListing = mapListing.get(SL.Listing__c);
                SL.Normalized_Designation__c = objListing.Normalized_Designation__c;
                SL.Normalized_Last_Name_Business_Name__c = objListing.Normalized_Last_Name_Business_Name__c;
                SL.Normalized_First_Name__c = objListing.Normalized_First_Name__c;
                SL.Normalized_Listing_Street_Name__c = objListing.Normalized_Listing_Street_Name__c;
                SL.Normalized_Listing_Street_Number__c = objListing.Normalized_Listing_Street_Number__c;
                SL.Normalized_Phone__c = objListing.Normalized_Phone__c;
                SL.Normalized_Honorary_Title__c = objListing.Normalized_Honorary_Title__c;
                SL.Normalized_Lineage_Title__c = objListing.Normalized_Lineage_Title__c;
                SL.Normalized_Secondary_Surname__c = objListing.Normalized_Secondary_Surname__c;
                updatedNormalisedSLs.add(SL);
         }
        system.debug('************Directory Listings***************'+updatedNormalisedSLs);
        if (updatedNormalisedSLs.size() > 0) {
            update updatedNormalisedSLs;
        }
    }*/
}