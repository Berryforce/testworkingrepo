@isTest(SeeAllData=true)
private class PortalBalancePay_Test {
	
    public static testmethod void testSiteSalesInvoiceController() {
        
        // --------------- Set up records for test scenarios ------------------
        
        // Check to make sure settings record has been created, and PC is in Simulation Mode (to
        // prevent API callouts from being made during test method execution.
		
        pymt__Settings__c settings = new pymt__Settings__c();
        List<pymt__Settings__c> settingsList = [ select name, pymt__Gateway_Simulation_Mode__c from pymt__Settings__c ];
        if (settingsList.size() > 0) {
        	settings = settingsList[0];
        	settings.pymt__Gateway_Simulation_Mode__c = true;    
        	update settings;   
        }
        else {
        	
        	settings.name = 'ApexTestSettings';
        	settings.pymt__Gateway_Simulation_Mode__c = true;                    
        	insert settings;
        }
        
        // Add processor connections
        pymt__Processor_Connection__c connection1 = new pymt__Processor_Connection__c(
                name = 'ApexTestProcessorConnection1',
                pymt__PaymentConnect_Setup__c = settings.id,
                pymt__Assigned_To_Terminal__c = true,
                pymt__Default_Connection__c = true,
                pymt__Processor_Id__c = 'Authorize.Net',
                pymt__Enabled_Card_Types__c = 'Visa;Mastercard',
                pymt__Authnet_Enable_Echecks__c = true);
            Database.SaveResult sr =    Database.insert(connection1);
            System.assert(sr.isSuccess(), 'Error inserting test processor connection object. '+sr);     
     
      
       
       //non portal user
        user nonPortalUser = [ Select id,  Email, ContactId From User where isPortalEnabled = false and isActive = true limit 1  ];
        system.runAs(nonPortalUser) {
          PortalBalancePayController controller = new PortalBalancePayController();
        }
    	user portalUser = [ Select id,  Email, ContactId From User where isPortalEnabled = true and isActive = true and ContactId <> null limit 1  ];
       system.runAs(portalUser) {
       	 PortalBalancePayController controller = new PortalBalancePayController();
       
       	 controller.payAmount = 0;
       	 controller.submitAmount();
       	 controller.payAmount = 40;
       	 controller.submitAmount();
       	 controller.processor = connection1;
       	 controller.submitAmount();
       	 controller.getCurrentIPAddress();
       	 controller.optIn = true;
       	 controller.acceptTermsandSubmit();
       }
       pymt__Payment_Method__c[] pmList = [select id, pymt__default__c from pymt__Payment_Method__c where pymt__Contact__c =: portalUser.ContactId  ];
      
       if (pmList.size() > 0) {
       	delete pmList;
       }
       		
        
       //assignee Task User id
       
        system.runAs(portalUser) {
        	 pymt__Payment_Method__c pm = new pymt__Payment_Method__c();
       		pm.name = 'ApexTest';
       		pm.pymt__default__c = true;
       		pm.pymt__contact__c = portalUser.ContactId;
       		insert pm;
       		
        	PageReference p = Page.PortalBalancePay;
        	Test.setcurrentPage(p);
        	ApexPages.currentPage().getParameters().put('assigneeUserName', system.userInfo.getUserName());
        	 PortalBalancePayController controller = new PortalBalancePayController();
        	 	 controller.optIn = true;
       	 			controller.acceptTermsandSubmit();
        }
    }
    
    	private static testmethod void testcheckForAutoPayTrigger() {
    		
    			user portalUser = [ Select id,  Email, ContactId From User where isPortalEnabled = true and isActive = true and ContactId <> null limit 1  ];
       			Contact c = [ select agreed_to_autobill__c, (select id from pymt__Payment_Methods__r) from contact where id =: portalUser.contactid ];
       			if (!c.agreed_to_autobill__c) {
       				c.agreed_to_autobill__c = true;
       				update c;
       			}
       			if (c.pymt__Payment_Methods__r.size() == 0) {
       				 pymt__Payment_Method__c pm = new pymt__Payment_Method__c();
       				 pm.name = 'ApexTest';
       				 pm.pymt__default__c = true;
       				 pm.pymt__contact__c = portalUser.ContactId;
       				 insert pm;
       			}
    		
       	 				system.runAs(portalUser) {
       	 					List<pymt__Payment_Method__c> pmListToDelete = [select id from pymt__Payment_Method__c ];
       	 					try {
								delete pmListToDelete;
       	 					}
       	 					catch(system.exception ex) {
       	 						Boolean expectedExceptionThrown =  ex.getMessage().contains('AutoBill') ? true : false;
								System.AssertEquals(expectedExceptionThrown, true);
       	 					}
       	 				}
    	}
        private static testmethod void testAccessControllerWithoutSharing() {
    	AccessControllerWithoutSharing_Berry acwos = new AccessControllerWithoutSharing_Berry();
    	
    	    Contact c = [select id from contact limit 1];
    	    
    	     pymt__Payment_Method__c pm = new pymt__Payment_Method__c();
       		pm.name = 'ApexTest';
       		pm.pymt__default__c = true;
       		pm.pymt__contact__c = c.id;
       		insert pm;
       		
    	  	pymt__Payment_Method__c pm1 = new pymt__Payment_Method__c();
       		pm1.name = 'ApexTest1';
       		pm1.pymt__default__c = false;
       		pm1.pymt__contact__c = c.id;
       		
       		pymt__Payment_Method__c pm2 = new pymt__Payment_Method__c();
       		pm2.name = 'ApexTest2';
       		pm2.pymt__default__c = false;
       		pm2.pymt__contact__c = c.id;
    	
    	pymt__Payment_Method__c[] pmList = new pymt__Payment_Method__c[]{};
    	
    	pmList.add(pm2);
    	

    	acwos.dbInsert(pm1);
    	acwos.dbInsert(pmList);
    	acwos.dbUpdate(pm1);
    	acwos.dbUpdate(pmList);
    	pmList = acwos.dbQuery('Select id from pymt__Payment_Method__c where name = \'ApexTest2\' Limit 50');
    	acwos.dbDelete(pm1);
    	acwos.dbDelete(pmList);
    }		
    
}