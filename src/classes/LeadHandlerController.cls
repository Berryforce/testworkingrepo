public class LeadHandlerController {
    public static void onBeforeInsert(list<Lead> lstNewLead) {
        populateValuesToAccountToIdentifyAccountConvertedFromLead(lstNewLead, null);
        populateLeadTelcoPartner(lstNewLead, null);
    }
    
    public static void onBeforeUpdate(list<Lead> lstNewLead, map<Id, Lead> oldMap) {
        populateValuesToAccountToIdentifyAccountConvertedFromLead(lstNewLead, oldMap);
        populateLeadTelcoPartner(lstNewLead, oldMap);
    }
    
    public static void onAfterUpdate(list<Lead> lstNewLead, map<Id, Lead> oldMap) {
        populateAccountIDonCase(lstNewLead, oldMap);
        if(!CommonVariables.LeadListingAdrRecursive){
        	updateAccountForListingInfo(lstNewLead, oldMap);
        }
    }
    
    private static void populateAccountIDonCase(list<Lead> lstNewLead, map<Id, Lead> oldMap) {
        set<Id> setConvertId = new set<Id>();
        String AccountId;
        for(Lead iterator : lstNewLead) {
            if(oldMap != null) {
                if(iterator.IsConverted && iterator.ConvertedAccountId!=null) {
                    setConvertId.add(iterator.Id);
                    if(String.isBlank(AccountId)) {
                        AccountId = iterator.ConvertedAccountId;
                    }
                }
            }
        }
        
        if(setConvertId.size() > 0) {
            list<Case> lstCases = [Select Id,Lead_Name__c,AccountId from Case where Lead_Name__c IN :setConvertId and AccountId = null];
            if(lstCases != null && lstCases.size() > 0) {
                for(Case cIterator : lstCases) {
                    cIterator.AccountId = AccountId;
                }
                update lstCases;
            }
        }
    }
    
    
    private static void populateValuesToAccountToIdentifyAccountConvertedFromLead(list<Lead> lstNewLead, map<Id, Lead> oldMap) {
        for(Lead iterator : lstNewLead) {
            if(oldMap != null) {
                Lead objOldLead = oldMap.get(iterator.Id);
                if(objOldLead.LeadSource != iterator.LeadSource) {
                    iterator.Lead_Account_Source__c = iterator.LeadSource;
                }
            }
            else {
                if(String.isNotBlank(iterator.LeadSource)) {
                    iterator.Lead_Account_Source__c = iterator.LeadSource;
                }
            }
        }
    }
    
    private static void updateAccountForListingInfo(list<Lead> lstNewLead, map<Id, Lead> oldMap) {
        set<Id> setConvertId = new set<Id>();
        set<Id> setListingID = new set<Id>();
        String AccountId;
        set<Id> setLeadId = new set<Id>();
        for(Lead iterator : lstNewLead){
            if(oldMap != null){
                setLeadId.add(iterator.Id);
            }
        }
        list<lead> lstLead = new list<lead>();
        lstLead = [Select id,IsConverted,ConvertedAccountId,Create_Listing__c,(select id,name from Listings__r) from Lead where Id IN : setLeadId];
        
        for(Lead iteratorlD : lstLead) {
            if(iteratorlD.IsConverted && iteratorlD.ConvertedAccountId!=null) {
                if((iteratorlD.Create_Listing__c == 'NO' || iteratorlD.Create_Listing__c == Null)||(iteratorlD.Create_Listing__c == 'Yes' && iteratorlD.Listings__r.size()> 0)){
                    setConvertId.add(iteratorlD.Id);
                    if(String.isBlank(AccountId)) {
                        AccountId = iteratorlD.ConvertedAccountId;
                    }
                    system.debug('****Create Listing value***'+AccountId+'******Lead Id*******'+setConvertId);
                }
                else if(iteratorlD.Create_Listing__c == 'Yes' && iteratorlD.Listings__r.size()== 0 ){
                    setListingID.add(iteratorlD.Id);
                }
            }
         }
        
        if(setConvertId.size() > 0 && String.IsNotBlank(AccountId)) {
            listingUpdatewithAccount(setConvertId,AccountId);
        }
        
        if(setListingID.size() > 0) {
            CreateNOSOListingfromLead.createNosoListing(setListingID);
        }
    }
    
    private static void populateLeadTelcoPartner(list<Lead> lstNewLead, map<Id, Lead> oldMap){
        map<Id,Id> mapLdCanvassTelcoAct = new map<Id,Id>();
        set<Id> setCanvassId = new set<Id>();
        RecordType Leadrt  = CommonMethods.getRecordTypeDetailsByDeveloperName('Lead',CommonMessages.leadRT);
         
        for(Lead objld : lstNewLead){
            if(oldMap != null){
                Lead oldLd = oldMap.get(objld.Id);
                if(objld.Primary_Canvass__c != oldLd.Primary_Canvass__c && objld.RecordTypeId == Leadrt.Id){
                    setCanvassId.add(objld.Primary_Canvass__c);
                }
            }
            else if(objld.Primary_Canvass__c != null && objld.RecordTypeId == Leadrt.Id ){
                setCanvassId.add(objld.Primary_Canvass__c);
            }
        }
        if(setCanvassId.size()>0){
            list<Canvass__c> lstCanvass = [Select Id,Primary_Telco__c,Primary_Telco__r.Account__c from Canvass__c where ID IN : setCanvassId];
            
            for(Canvass__c objCan :lstCanvass){
                if(!mapLdCanvassTelcoAct.containsKey(objCan.Id)){
                    mapLdCanvassTelcoAct.put(objCan.Id, objCan.Primary_Telco__r.Account__c);
                }
            }
        }
        
        for(lead objL : lstNewLead){
            if(mapLdCanvassTelcoAct.get(objL.Primary_Canvass__c) != null){
                objL.Telco_Partner_Account__c = mapLdCanvassTelcoAct.get(objL.Primary_Canvass__c);
            }
        }
    }
    
    private static void listingUpdatewithAccount(set<Id> ldconvertId, String AccountId) {
        List<Listing__c> objLstng = new List<Listing__c>();
        map<Id, Listing__c> mapLstUpdate = new map<Id, Listing__c>();
            objLstng =[Select Id,Name,Account__c from Listing__c where Lead__c IN : ldconvertId AND Account__c = NULL];
                for(Listing__c objL : objLstng){
                   Listing__c objLstUpdte = new Listing__c(Id = objL.ID);
                   objLstUpdte.Account__c = AccountId;
                   //objLstUpdte.Lead__c = '';
                   if(!mapLstUpdate.containskey(objLstUpdte.Id)){
                       mapLstUpdate.put(objLstUpdte.Id,objLstUpdte);
                   }
                }
                if(mapLstUpdate.size()>0){
                    CommonVariables.LeadListingAdrRecursive=true;
                    update mapLstUpdate.values();
                }
    }
}