Public class NewScopedListingOverrideController{
     public Pagereference onLoad(){
         PageReference newPage = new PageReference('/a2e/e?');
             for(String keyValue : ApexPages.currentPage().getParameters().keySet()){
                 if(!keyValue.contains('override') ){
                    newPage.getParameters().put(keyValue, ApexPages.currentPage().getParameters().get(keyValue));
                 }
             }
             newPage.getParameters().remove('save_new');
             newPage.getParameters().put('Name','DO NOT EDIT-Populate Scoped Listing Last/Business Name field');
             newPage.getParameters().put('nooverride','1');
         return newPage.setredirect(true);
     }
     public NewScopedListingOverrideController(ApexPages.StandardController controller){
        
     }

}