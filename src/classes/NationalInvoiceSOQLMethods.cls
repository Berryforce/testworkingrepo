public class NationalInvoiceSOQLMethods {    
    public static list<NationalInvoice__c> getLastNationalInvoiceBYOrderSetID(set<Id> orderSetID) {
        return [Select OrderSet__c, Id, Name, Invoice_Date__c, ISS__c, ISS__r.Name, IsNew__c From NationalInvoice__c where OrderSet__c IN:orderSetID and IsNew__c = true];
    }
    
    public static list<NationalInvoice__c> getNationalInvoiceForNationalViewByOrderSet(set<Id> orderSetID) {
        return [SELECT Net_Amount__c,TotalADJAmount__c,TotalGrossAmount__c,Tax_Amount_New__c,Total_Amount__c,Trans__c, 
              Trans_Version__c,ISS__r.Name,Invoice_Date__c,
              SourceDirectoryCode__c,directory_edition__r.name,Publication_Company__r.Name, 
              To_C__c,From_P__c, Directory_Code__c,Directory__r.State__c,Directory__r.Name, 
              Publication_Date__c, Directory_Edition__r.Life__c,CMR_Number__c, 
              Client_Number__c, NAT__c, Client_Name__r.Name, NAT_Client_ID__c, 
              (SELECT Line_Number__c, Page_Number__c, Advertising_Data__c,ADJ_Amount__c,UDAC__C,DAT__C,BAS__C,SPINS__C, 
              OrderLineItem__r.Line_Number__c,OrderLineItem__r.ListPrice__c 
              FROM National_Invoice_Line_Items__r) FROM NationalInvoice__c 
              where orderset__c IN:orderSetID  and isnew__c = true];
    }
    
    public static list<NationalInvoice__c> getNatInvByPubCodeEdtncodeDirId(String pubCode, String edtnCode, Id dirId) {
        return [SELECT Id, Name, Total_Amount__c, Net_Amount__c, Client_Name__c, CMR__c, Directory__c, ISS__c, Publication_Date__c, State__c,  
                Directory_Version__c, OrderSet__r.Total_Order_Value__c, CMR_Number__c, Client_Number__c, Tax_Amount_New__c, CMR_Number_Client_Number__c, 
                TotalGrossAmount__c, OrderSet__c FROM NationalInvoice__c WHERE Directory__c = : dirId AND Publication_Code__c = : pubCode AND 
                Directory_Edition_Code__c = : edtnCode AND Send_to_Elite__c = false ORDER BY CMR_Number__c, Client_Number__c];
    }
    
    public static list<NationalInvoice__c> getNatInvByPubCodeEdtncodeWithISSNull(String pubCode, String edtnCode) {
        return [select Net_Amount__c,TotalGrossAmount__c, id, To__c, From__c, Trans__c, Trans_Version__c, Tax_Amount_New__c,
            Publication_Date__c, Directory_Edition__c, Directory_Edition__r.name, directory__c, CMR__c, Directory_Edition__r.National_Billing_Date__c,
            Publication_Company__c, Publication_Company__r.name, Client_Name__c, CMR_Number__c, Publication_Code__c, Tax__c, 
            Directory_Edition__r.Life__c, (select Gross_Amount__c, ADJ_Amount__c from National_Invoice_Line_Items__r ) 
            from NationalInvoice__c where Publication_Code__c =: pubCode and Directory_Edition_Code__c =: edtnCode 
            and ISS__c = null];
    }
    
    public static list<NationalInvoice__c> getNatInvByPubCodeEdtncodeWithISSNotNull(String pubCode, String edtnCode) {
        return [select Net_Amount__c,TotalGrossAmount__c, id, To__c, From__c, Trans__c, Trans_Version__c, 
            Publication_Date__c, Directory_Edition__c, Directory_Edition__r.name, directory__c, CMR__c, ISS__c, Directory_Edition__r.National_Billing_Date__c,
            Publication_Company__c, Publication_Company__r.name, Client_Name__c, CMR_Number__c, Publication_Code__c, Tax_Amount_New__c,
            Directory_Edition__r.Life__c, (select Gross_Amount__c from National_Invoice_Line_Items__r ) 
            from NationalInvoice__c where Publication_Code__c =: pubCode and Directory_Edition_Code__c =: edtnCode 
            and ISS__c != null];
    }
    
    public static list<NationalInvoice__c> getNatInvById(Set<Id> natInvIds) {
        return [SELECT Id, Name, Total_Amount__c, Net_Amount__c, ISS__c, Directory__c, State__c, Publication_Date__c, CMR__c, Tax_Amount_New__c,
                Client_Name__c, Directory_Version__c, OrderSet__c FROM NationalInvoice__c WHERE ID IN: natInvIds];
    }
}