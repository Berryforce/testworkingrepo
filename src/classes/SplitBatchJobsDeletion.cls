global class SplitBatchJobsDeletion implements Database.Batchable <sObject> {
	global Database.QueryLocator start(Database.BatchableContext bc) {
        String SOQL = 'SELECT Id,Name,Start_Number__c,Type__c FROM SplitBatchJobs__c';
        return Database.getQueryLocator(SOQL);
    }
    global void execute(Database.BatchableContext bc, List<SplitBatchJobs__c> listSplitJobs) {
        if(listSplitJobs.size()> 0) {
        	delete listSplitJobs;
        }
    }
    global void finish(Database.BatchableContext bc) {
    }
    
}