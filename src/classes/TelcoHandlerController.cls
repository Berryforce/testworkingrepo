public with sharing class TelcoHandlerController {
	public static void onAfterInsert(list<Telco__c> listTelco) {
	 	callWPSortingBatch(listTelco);
    }
    
    public static void onAfterUpdate(List<Telco__c> listTelco, Map<Id, Telco__c> oldMap) {
    	List<Telco__c> listTelcoForWPSortBatch = new List<Telco__c>();
    	List<Account> listAccountUpdateForTelcoAcctChange = new List<Account>();
    	for(Telco__c telco : listTelco) {
			Telco__c oldTelco = oldMap.get(telco.Id);
			if(telco.Telco_Drives_WP_Sorting__c != oldTelco.Telco_Drives_WP_Sorting__c) {
				listTelcoForWPSortBatch.add(telco);
			}
			if(((telco.Account__c != oldTelco.Account__c && telco.Account__c != null) || (telco.Use_As_Account_Telco_Name_and_Code__c != oldTelco.Use_As_Account_Telco_Name_and_Code__c && telco.Account__c != null)) && telco.Use_As_Account_Telco_Name_and_Code__c) {
				listAccountUpdateForTelcoAcctChange.add(new Account(Id = telco.Account__c, Telco__c = telco.Id));
			}
		}
		if(listTelcoForWPSortBatch.size() > 0) {
			callWPSortingBatch(listTelcoForWPSortBatch);
    	}
    	
    	if(listAccountUpdateForTelcoAcctChange.size() > 0) {
    		update listAccountUpdateForTelcoAcctChange;
    	}
    }
    
    //for ticket CC-1913 by Mythili
    public static void onBeforeInsert(list<Telco__c> telcoList){
    	setDocumentId(telcoList);	
    }
    
    //for ticket CC-1913 by Mythili
    public static void onBeforeUpdate(list<Telco__c> telcoList,Map<Id,Telco__c> oldTelcoMap){
    	List<Telco__c> reqdTelcoList=new List<Telco__c>();
    	for(Telco__c telco:telcoList){
    	  	if(telco.Sold_Letter_Logos__c!=oldTelcoMap.get(telco.Id).Sold_Letter_Logos__c){
    	  		reqdTelcoList.add(telco);
    	  	}
    	}
    	if(reqdTelcoList!=null && reqdTelcoList.size()>0){
    		setDocumentId(reqdTelcoList);
    	}
    }
    
    //for ticket CC-1913 by Mythili
    private static void setDocumentId(list<Telco__c> telcoList){
    	List<Document> docList=null;
    	Document docObj=null;
    	Map<String,Document> docNameMap=new Map<String,Document>();
    	docList=[select id,Name from Document where Folder.DeveloperName='Sold_Letter_Logos'];  
    	for(Document doc:docList){
    		docNameMap.put(doc.Name,doc);
    	}
    	
    	for(Telco__c telObj:telcoList){
    		docObj=docNameMap.get(telObj.Sold_Letter_Logos__c);
    		if(docObj!=null){
    			telObj.Document_Id__c=docObj.Id;
    		}
    	}
    }
    
    private static void callWPSortingBatch(list<Telco__c> listTelco) {    	
    	Set<Id> checkedTelcoIds = new Set<Id>();
    	Set<Id> unCheckedTelcoIds = new Set<Id>();
    	
    	for(Telco__c telco : listTelco) {
			if(telco.Telco_Drives_WP_Sorting__c){
				checkedTelcoIds.add(telco.Id);
			} else {
				unCheckedTelcoIds.add(telco.Id);
			}
    	}
    	
    	if(checkedTelcoIds.size() > 0) {
    		WPSortingOnDirectorySectionBatch WPSortCheck = new WPSortingOnDirectorySectionBatch(checkedTelcoIds, 'checked');
    		Database.executeBatch(WPSortCheck);
    	}
    	
    	if(unCheckedTelcoIds.size() > 0) {
    		WPSortingOnDirectorySectionBatch WPSortUnCheck = new WPSortingOnDirectorySectionBatch(unCheckedTelcoIds, 'unchecked');
    		Database.executeBatch(WPSortUnCheck);
    	}
    }
}