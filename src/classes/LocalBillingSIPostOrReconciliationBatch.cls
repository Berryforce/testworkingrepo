global class LocalBillingSIPostOrReconciliationBatch implements Database.Batchable<sObject> {  
    global Dummy_Object__c objDummy = new Dummy_Object__c();
    global String postOrRecncle;
    global String type=null;
    global String dirEditionCode=null;
    global String directoryCode=null;
    global Integer startNumber=null;
    global Integer endNumber=null;
    global Digital_Telco_Scheduler__c digiTelco=null;
    global Integer lineCountMax = null;
    global Integer lineCountMin = null;
   
    
    //Subsequent Print and Reconcile Statement wizards
    global LocalBillingSIPostOrReconciliationBatch(String type,String postOrRecncle,Dummy_Object__c objDummy,Integer startNum,Integer endNum, Integer lineCountMin, Integer lineCountMax) {
        this.objDummy = objDummy;
        this.postOrRecncle = postOrRecncle;
        this.type=type;
        this.startNumber=startNum;
        this.endNumber=endNum;
        this.lineCountMin = lineCountMin;
        this.lineCountMax = lineCountMax;
     }
    
    //First Month Print
    global LocalBillingSIPostOrReconciliationBatch(String type,String postOrRecncle,String editionCode,String dirCode,Integer startNum,Integer endNum,Integer lineCountMin, Integer lineCountMax){
        this.type=type;
        this.postOrRecncle=postOrRecncle;
        this.dirEditionCode=editionCode;
        this.directoryCode=dirCode;
        this.startNumber=startNum;
        this.endNumber=endNum;
        this.lineCountMin = lineCountMin;
        this.lineCountMax = lineCountMax;  
    }
    
    //Digital Local
    global LocalBillingSIPostOrReconciliationBatch(String type,String postOrRecncle,Digital_Telco_Scheduler__c objDigiTelco,Integer startNum,Integer endNum,Integer lineCountMin, Integer lineCountMax){
        this.type=type;
        this.postOrRecncle=postOrRecncle;
        this.digiTelco=objDigiTelco;
        this.startNumber=startNum;
        this.endNumber=endNum; 
        this.lineCountMin = lineCountMin;
        this.lineCountMax = lineCountMax; 
    }
    
    //Telco Reversal Sales Invoice Posting
    global LocalBillingSIPostOrReconciliationBatch(String type,String postOrRecncle,Integer startNum,Integer endNum){
    	this.type=type;
    	this.postOrRecncle=postOrRecncle;
    	this.startNumber=startNum;
        this.endNumber=endNum; 
    }
    
    
    global Database.QueryLocator start(Database.BatchableContext bc) {
        String strQuery=null;
        set<String> pymtMtds = new Set<String> {'Telco Billing', 'Statement'};
        Date fromDate = objDummy.From_Date__c;
        Date toDate=objDummy.To_Date__c;
        if(type=='Subsequent Print'){
            strQuery = 'SELECT Id, Reconcile__c FROM c2g__codaInvoice__c WHERE SI_P4P__c = false '
                            + 'AND SI_Media_Type__c = \'Print\' AND c2g__InvoiceStatus__c = \'In Progress\' AND SI_Payment_Method__c IN: pymtMtds '
                            + 'AND c2g__InvoiceDate__c =: fromDate AND SI_Successful_Payments__c > 1 AND SI_Payments_Remaining__c < 12 ';
        }else if(type=='First Month Print'){
            strQuery = 'Select Id, SI_Telco__c, SI_Telco__r.Name, Reconcile__c, c2g__Account__c, c2g__Account__r.Name, Customer_Name__c, c2g__NetTotal__c, SI_Payment_Method__c, SI_Billing_Partner__c, (Select Id, Order_Line_Item__r.UnitPrice__c From c2g__InvoiceLineItems__r) '+ 
                        ' From c2g__codaInvoice__c where SI_Successful_Payments__c = 1 AND SI_P4P__c = false AND SI_Media_Type__c = \'Print\' AND c2g__InvoiceStatus__c = \'In Progress\' '+
                        ' AND SI_Directory_Edition_Code__c =:dirEditionCode AND  SI_Directory_Code__c =:directoryCode ';
            if(postOrRecncle == 'post') {
                strQuery +=' AND SI_Payment_Method__c = \'Telco Billing\' ';
            }
            
        }else if(type=='Digital Local'){
            Date DLfromDate=digiTelco.Invoice_From_Date__c;
            Date DLtoDate=digiTelco.Invoice_To_Date__c;
            String billingEntity=digiTelco.Billing_Entity__c;
            strQuery = 'SELECT SI_Billing_Partner__c, Customer_Name__c,Reconcile__c,Name,c2g__NetTotal__c FROM c2g__codaInvoice__c '+ 
                       ' where SI_P4P__c = false AND c2g__InvoiceStatus__c = \'In Progress\' AND Billing_Entity__c=:billingEntity '+
                       ' AND SI_Media_Type__c=\'Digital\' AND  c2g__InvoiceDate__c >=:DLfromDate AND c2g__InvoiceDate__c <=:DLtoDate';
            
            if(postOrRecncle == 'post'){  
                strQuery += ' AND SI_Billing_Partner__c!= \'THE BERRY COMPANY\'';
            }
        }else if(type=='Reconcile Statement'){
            strQuery = 'Select Customer_Name__c, c2g__NetTotal__c, Name, Id, c2g__InvoiceDate__c, Reconcile__c From c2g__codaInvoice__c '+
                        ' WHERE SI_Billing_Partner__c = \'THE BERRY COMPANY\' AND c2g__InvoiceStatus__c = \'In Progress\' AND SI_Payment_Method__c = \'Statement\' '+
                        ' AND c2g__InvoiceDate__c<=:toDate and c2g__InvoiceDate__c>=:fromDate ';
            if(postOrRecncle == 'reconcile') {
                strQuery += 'and Reconcile__c = false ';    
            }
        }else if(type=='Telco Reversal Sales Invoice'){
        	strQuery ='Select id from c2g__codaInvoice__c where c2g__InvoiceStatus__c = \'In Progress\' and Telco_Reversal__c=true and ffps_berry__AutoPost__c=true';
        }
        if(startNumber!=null){
            strQuery +=' AND SI_Posting_Number__c>=:startNumber ';
        }
        if(endNumber!=null){
            strQuery += ' AND SI_Posting_Number__c<=:endNumber';    
        }
        
        if(lineCountMin != null) {
        	strQuery +=' AND SI_Noof_SILI__c >=:lineCountMin ';
        }
        if(lineCountMax != null) {
        	strQuery +=' AND SI_Noof_SILI__c <=:lineCountMax ';
        }
       
            
        System.debug('Testingg strQuery '+strQuery);
        System.debug('Testingg strQuery '+fromDate);  
        System.debug('Testingg strQuery '+pymtMtds);
        return Database.getQueryLocator(strQuery);
    }

    global void execute(Database.BatchableContext bc, List<c2g__codaInvoice__c> listSI) {
        if(postOrRecncle == 'reconcile') {
            list<c2g__codaInvoice__c> lstSIUpdate = new list<c2g__codaInvoice__c>();
            for(c2g__codaInvoice__c iterator: listSI){
                lstSIUpdate.add(new c2g__codaInvoice__c(Id = iterator.Id, Reconcile__c = true));
            }
            update lstSIUpdate;
        } 
        else if(postOrRecncle == 'post') {
            set<Id> setInvoiceID = new set<Id>();
            for(c2g__codaInvoice__c iterator: listSI){
                setInvoiceID.add(iterator.Id);
            }
            BillingWizardCommonController.postSalesInvoice(BillingWizardCommonController.generateCODAAPICommonReference(setInvoiceID));
        }
    }

    global void finish(Database.BatchableContext bc) {
        AsyncApexJob a = [Select Id, Status, NumberOfErrors, JobItemsProcessed, TotalJobItems, CreatedBy.Email, ExtendedStatus from AsyncApexJob where Id =:BC.getJobId()];
        /*Map<String, LocalSISCNPostBatchEmails__c> allemails = LocalSISCNPostBatchEmails__c.getAll();
        Set<String> emailaddrSet=new Set<String>();
        emailaddrSet.add(a.CreatedBy.Email);
        String emailaddr='';*/
         Set<String> recipientIds = new Set<String>();
         recipientIds.addAll(User_Ids_For_Email__c.getInstance('SISCNPost').User_Ids__c.split(';'));
         recipientIds.add(UserInfo.getUserId());
        String errorTxt='';
        /*for(LocalSISCNPostBatchEmails__c iter : allemails.values()) {
            emailaddrSet.add(iter.email__c);
        }
        List<String> emailaddrLst=new List<String>();
        emailaddrLst.addall(emailaddrSet);*/
        String strErrorMessage = '';
        if(a.NumberOfErrors > 0){
            strErrorMessage = a.ExtendedStatus;
            errorTxt='The batch Apex job processed ' + a.TotalJobItems +' batches with '+ a.NumberOfErrors + ' failures.'+strErrorMessage+'. Please check error/exception object for more detaled information.';
        }
        else{
        errorTxt='The batch Apex job processed ' + a.TotalJobItems +' batches with '+ a.NumberOfErrors + ' failures.'+strErrorMessage+'.';
        }
        for(String str : recipientIds) {
            CommonEmailUtils.sendHTMLEmailForTargetObject(str, 'Local Billing Sales Invoice Reconciliation Batch  is ' + a.Status,errorTxt);
        }
    }
}