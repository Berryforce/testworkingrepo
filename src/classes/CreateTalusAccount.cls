/****************************************************
Apex Class to send Account info for Fulfillment
Created By: Reddy(reddy.yellanki@theberrycompany.com)
Updated Date: 03/30/2015
$Id$
*****************************************************/
public with sharing class CreateTalusAccount {

    //Get nigel endpoint url from custom label
    public static final string furl = Label.Talus_Nigel_Url;

    public static String dt;
    public static String newJsonString;
    public static String accntIds;
    public static String newResponsestring;
    public static Map < String, Object > newResps = new Map < String, Object > ();
    public static Map < String, Object > accmngr = new Map < String, Object > ();
    public static List < Object > phnbrs = new List < Object > ();
    public static Map < String, Object > phn1 = new Map < String, Object > ();
    public static List < Fulfillment_Account__c > lstFlmntAccnt = new List < Fulfillment_Account__c > ();

    //Asynchronous method to create or update fulfillment information on Account records    
    @future(callout = true)
    public static void createAccTalus(list < Id > AccountId, Id MngrId) {

        //Query for account information 
        list < Account > lstAcc = [Select a.TalusAccountId__c, a.Phone, a.Berry_ID__c, a.Name, a.Id, a.Enterprise_Customer_ID__c, a.Account_Manager__c, a.Sensitive_Heading__c,
            a.Account_Manager__r.Id, a.Account_Manager__r.Name, a.Account_Manager__r.FirstName, a.Account_Manager__r.LastName, a.Account_Manager__r.Phone,
            a.Account_Manager__r.Email, CreatedBy.FirstName, CreatedBy.LastName, CreatedBy.Username, CreatedBy.Email, CreatedBy.phone
            From Account a where a.Id = : AccountId FOR UPDATE
        ];

        for (Account acc: lstAcc) {

            String extIdUrl = furl + '?external_id=' + acc.Enterprise_Customer_ID__c;
            newResponsestring = TalusRequestUtilityGET.initiateRequest(extIdUrl);

            if (Test.isRunningTest() && acc.Name == 'TestingHttp') {
                newResponsestring = CommonUtility.mockGetRspns();
            }

            if (newResponsestring.contains('external_id')) {

                //Deserialize JSON response body
                newResps = (Map < String, Object > ) JSON.deserializeUntyped(newResponsestring);
                List < Object > lstObj = (List < Object > ) newResps.get('objects');
                Map < String, Object > vals = (Map < String, Object > ) lstObj[0];

                System.debug('************KeySet************' + vals.keySet() + '************Values************' + vals.Values());

                //Update fulfillment values on account record
                acc.TalusAccountId__c = String.valueof(vals.get('id'));
                dt = String.valueof(vals.get('created'));
                acc.TalusCreatedDate__c = datetime.valueof(dt.replace('T', ' '));
                dt = String.valueof(vals.get('last_updated'));
                acc.TalusModifiedDate__c = datetime.valueof(dt.replace('T', ' '));
                acc.Original_Enterprise_Customer_Id__c = acc.Enterprise_Customer_ID__c;

                //Parse account_manager information for phone id value
                accmngr = (Map < String, Object > ) vals.get('account_manager');
                phnbrs = (List < Object > ) accmngr.get('phone_numbers');

                if (phnbrs.size() > 0) {
                    phn1 = (Map < String, Object > ) phnbrs[0];
                    //System.debug('************PhoneId************' + phn1.get('id'));
                    acc.TalusPhoneId__c = String.valueof(phn1.get('id'));
                }

            } else {

                //Instantiate JSON Generator method to form JSON data
                JSONGenerator jsonGen = JSON.createGenerator(true);
                //String newJsonString;

                //Starting JSON Generator
                jsonGen.writeStartObject();

                jsonGen.writeStringField('status', 'active');
                jsonGen.writeBooleanField('privacy_flag', acc.Sensitive_Heading__c);
                jsonGen.writeStringField('name', acc.Name);
                jsonGen.writeStringField('external_id', acc.Enterprise_Customer_ID__c);

                jsonGen.writeFieldName('account_manager');

                jsonGen.writeStartObject();

                if (acc.Account_Manager__c == null) {
                    jsonGen.writeStringField('name', acc.CreatedBy.UserName);
                    jsonGen.writeStringField('first_name', acc.CreatedBy.FirstName);
                    jsonGen.writeStringField('last_name', acc.CreatedBy.LastName);
                    jsonGen.writeStringField('email', acc.CreatedBy.Email);
                    if (acc.CreatedBy.Phone != null) {
                        jsonGen.writeFieldName('phone_numbers');
                        jsonGen.writeStartArray();
                        jsonGen.writeStartObject();
                        jsonGen.writeStringField('phone_type', 'Office');
                        jsonGen.writeStringField('phone_number', acc.CreatedBy.Phone);
                        jsonGen.writeEndObject();
                        jsonGen.writeEndArray();
                    }
                } else {
                    jsonGen.writeStringField('name', acc.Account_Manager__r.Name);
                    jsonGen.writeStringField('first_name', acc.Account_Manager__r.FirstName);
                    jsonGen.writeStringField('last_name', acc.Account_Manager__r.LastName);
                    jsonGen.writeStringField('email', acc.Account_Manager__r.Email);
                    if (acc.Account_Manager__r.Phone != null) {
                        jsonGen.writeFieldName('phone_numbers');
                        jsonGen.writeStartArray();
                        jsonGen.writeStartObject();
                        jsonGen.writeStringField('phone_type', 'Office');
                        jsonGen.writeStringField('phone_number', acc.Account_Manager__r.Phone);
                        jsonGen.writeEndObject();
                        jsonGen.writeEndArray();
                    }
                }

                jsonGen.writeEndObject();

                //Ending JSON Generator
                jsonGen.writeEndObject();

                //Retrieve JSON body as a string
                newJsonString = jsonGen.getAsString();

                System.debug('************JSON Body************' + newJsonString);

                if (String.isNotBlank(newJsonString)) {
                    try {

                        HttpResponse newRes = CommonUtility.postUtil(newJsonString, fUrl, String.valueof('POST'));

                        if (Test.isRunningTest()) {
                            newResponsestring = CommonUtility.mockGetRspns();
                            newRes.setStatusCode(201);
                        }

                        newResponsestring = newRes.getBody();

                        system.debug('************Response************' + newResponsestring + '------------' + newRes.getStatusCode() + '------------' + newRes.getStatus());

                        if (newRes != NULL && newRes.getStatusCode() == 201) {

                            //Deserializing response body
                            if (!Test.isRunningTest()) {
                                newResps = (Map < String, Object > ) JSON.deserializeUntyped(newResponsestring);
                            }

                            //Update fulfillment Ids on account record
                            acc.TalusAccountId__c = String.valueof(newResps.get('id'));
                            dt = String.valueof(newResps.get('created'));
                            acc.TalusCreatedDate__c = datetime.valueof(dt.replace('T', ' '));
                            dt = String.valueof(newResps.get('last_updated'));
                            acc.TalusModifiedDate__c = datetime.valueof(dt.replace('T', ' '));
                            acc.Original_Enterprise_Customer_Id__c = acc.Enterprise_Customer_ID__c;

                            //Parse account_manager information for phone id value
                            accmngr = (Map < String, Object > ) newResps.get('account_manager');
                            phnbrs = (List < Object > ) accmngr.get('phone_numbers');

                            if (phnbrs.size() > 0) {
                                phn1 = (Map < String, Object > ) phnbrs[0];
                                //System.debug('************Phone Id************' + phn1.get('id'));
                                acc.TalusPhoneId__c = String.valueof(phn1.get('id'));
                            }
                            accntIds = 'AccountId: ' + acc.TalusAccountId__c + '------CreatedDate: ' + acc.TalusCreatedDate__c + '------ModifiedDate: ' + acc.TalusModifiedDate__c + '------PhoneId: ' + acc.TalusPhoneId__c;
                            lstFlmntAccnt.add(new Fulfillment_Account__c(Account_Id__c = acc.Id, TalusAccountId__c = acc.TalusAccountId__c, TalusCreatedDate__c = acc.TalusCreatedDate__c, TalusModifiedDate__c = acc.TalusModifiedDate__c, TalusPhoneId__c = acc.TalusPhoneId__c, Sent_JSON__c = newJsonString));

                        } else(ErrorLog.CreateErrorLog(newResponsestring, acc.Id, newRes.getStatusCode(), 'Account'));

                    } catch (exception e) {
                        //Create error log record for any exceptions
                        ErrorLog.CreateErrorLog(e.getmessage(), acc.Id, 501, 'Account');
                    }
                }
            }

        }

        //Insert fulfillment account records for Admin tracking
        if (lstFlmntAccnt.size() > 0) {
            insert lstFlmntAccnt;
        }

        //Update account records
        try {
            update lstAcc;
        } catch (exception e) {
            ErrorLog.CreateErrorLog(e.getmessage(), NULL, 501, 'Failed In CreateAccount');
        }

    }

}