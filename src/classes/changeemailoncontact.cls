global class changeemailoncontact implements Database.batchable<sObject>{

    global final string query='select email from contact';
    
    global changeemailoncontact(){
    }

    global Database.querylocator start(Database.batchablecontext bc){
    return Database.getquerylocator(query);
    }
    
    global void execute(Database.batchablecontext bc, List<user>scope){
        for(user u: scope){
        }
        
        update scope;
    }
    
    global void finish(Database.batchablecontext bc){
    }

}