@isTest
public class SensitiveHeadingUpdateBatchTest {
	static testMethod void sensitiveBatchTest() {
		Test.StartTest();   
		Directory_Heading__c objDH = TestMethodsUtility.generateDirectoryHeading();   
		objDH.Sensitive_Heading__c = true;
		insert objDH;
        Account a = TestMethodsUtility.generateCustomerAccount();
        a.Sensitive_Heading__c = false;
        insert a;             
        Contact cnt = TestMethodsUtility.createContact(a);               
        Opportunity oppty = TestMethodsUtility.createOpportunity('new', a);                      
        Order__c ord = TestMethodsUtility.createOrder(a.Id);                
        Order_Group__c og = TestMethodsUtility.createOrderSet(a, ord, oppty);     
        Product2 product2 = TestMethodsUtility.createproduct2();
        product2.Print_Specialty_Product_Type__c = 'Banner';
        insert product2;   
        Order_Line_Items__c oln = TestMethodsUtility.generateOrderLineItem(a, cnt, oppty, ord, og);
        oln.Product2__c = product2.Id;
        oln.Directory_Heading__c = objDH.Id;
        insert oln;        
                
		Datetime dt = Datetime.now().addMinutes(1);
        String CRON_EXP = '0 '+ dt.minute() + ' * ' + dt.day() + ' ' + dt.month() + ' ? ' + dt.year();
		String jobId = System.schedule('CCExpireSchedule', CRON_EXP, new SensitiveHeadingUpdateBatchSchedule() );
		Test.StopTest(); 
	}
}