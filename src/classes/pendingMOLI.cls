public with sharing class pendingMOLI{

    //ATP-3547

    ID accountID;
    ID CanvassID;
    private String uniqueID = '';
    public Opportunity objOPP=null;
    public ID selectedMOLI{get;set;}
    public Boolean CheckBool{get;set;}
    public list<Modification_Order_Line_Item__c> listMOLI {get;set;}
    public list<MOLISelectionWrapper> listMOLIWrapper{get;set;}
    public pendingMOLI(ApexPages.StandardController controller) {
        accountID = ApexPages.CurrentPage().getParameters().get('accountID');
        CanvassID = ApexPages.CurrentPage().getParameters().get('CanvassId');
        listMOLI = new list<Modification_Order_Line_Item__c>();
        listMOLI = ModificationOrderLineItemSOQLMethods.getMOLIBasedOnCanvass(accountID,CanvassID);
        listMOLIWrapper = new list<MOLISelectionWrapper>();
        for(Modification_Order_Line_Item__c objMOLI:listMOLI)
        {
           listMOLIWrapper.add(new MOLISelectionWrapper(objMOLI));
        }
    }
    public PageReference returnModifyAccountOrder()
    {
         
        string url='/apex/selectformodifyorder_v3?ID='+accountID+'&CanvassId='+CanvassID;
       
        pageReference PagePendingMOLI= new PageReference(url);
        PagePendingMOLI.setRedirect(true);
        return PagePendingMOLI;  
    }
    public class MOLISelectionWrapper{
        public Boolean isSelected{get;set;}
        public Modification_Order_Line_Item__c objMOLI{get;set;}
        public MOLISelectionWrapper(Modification_Order_Line_Item__c MOLI)
        {
            objMOLI = MOLI;
            isSelected=false;
        }
    }
     /* Selects the MOLI  with same package or parent id */  
    public pagereference CheckMOLIBundle()
    { 
        Modification_Order_Line_Item__c tempMOLI=[select id,Package_ID_External__c,Parent_ID__c,Parent_ID_of_Addon__c from Modification_Order_Line_Item__c where id=:selectedMOLI];
        if(CheckBool){
        if(tempMOLI.Parent_ID__c != null)
        {
            for(MOLISelectionWrapper wrapper:listMOLIWrapper)
            {
                if(wrapper.objMOLI.Parent_ID__c != null || wrapper.objMOLI.Parent_ID_of_Addon__c != null)
                {                   
                    if( wrapper.objMOLI.Parent_ID_of_Addon__c == tempMOLI.Parent_ID__c || wrapper.objMOLI.Parent_ID__c == tempMOLI.Parent_ID__c)
                    {
                        wrapper.isSelected = true;
                    }
                }
                
            }
            
        }else if(tempMOLI.Parent_ID_of_Addon__c != null)
        {
            for(MOLISelectionWrapper wrapper:listMOLIWrapper)
            {
                if(wrapper.objMOLI.Parent_ID__c != null || wrapper.objMOLI.Parent_ID_of_Addon__c != null)
                {                   
                    if( wrapper.objMOLI.Parent_ID_of_Addon__c == tempMOLI.Parent_ID_of_Addon__c || wrapper.objMOLI.Parent_ID__c == tempMOLI.Parent_ID_of_Addon__c)
                    {
                        wrapper.isSelected = true;
                    }
                }
                
            }
            
        }
         if(tempMOLI.Package_ID_External__c!=null)
         { 
            for(MOLISelectionWrapper wrapper:listMOLIWrapper)
            {
                if(wrapper.objMOLI.Package_ID_External__c!=null)
                {
                    if(wrapper.objMOLI.Package_ID_External__c==tempMOLI.Package_ID_External__c){
                        wrapper.isSelected=true;
                    }  
                }
            }
         }
        }
        if(!CheckBool)
        {
            if(tempMOLI.Parent_ID__c != null)
        {
            for(MOLISelectionWrapper wrapper:listMOLIWrapper)
            {
                if(wrapper.objMOLI.Parent_ID__c != null || wrapper.objMOLI.Parent_ID_of_Addon__c != null)
                {                   
                    if( wrapper.objMOLI.Parent_ID_of_Addon__c == tempMOLI.Parent_ID__c || wrapper.objMOLI.Parent_ID__c == tempMOLI.Parent_ID__c)
                    {
                        wrapper.isSelected = false;
                    }
                }
                
            }
            
        }else if(tempMOLI.Parent_ID_of_Addon__c != null)
        {
            for(MOLISelectionWrapper wrapper:listMOLIWrapper)
            {
                if(wrapper.objMOLI.Parent_ID__c != null || wrapper.objMOLI.Parent_ID_of_Addon__c != null)
                {                   
                    if( wrapper.objMOLI.Parent_ID_of_Addon__c == tempMOLI.Parent_ID_of_Addon__c || wrapper.objMOLI.Parent_ID__c == tempMOLI.Parent_ID_of_Addon__c)
                    {
                        wrapper.isSelected = false;
                    }
                }
                
            }
            
        }
         if(tempMOLI.Package_ID_External__c!=null)
         { 
            for(MOLISelectionWrapper wrapper:listMOLIWrapper)
            {
                if(wrapper.objMOLI.Package_ID_External__c!=null)
                {
                    if(wrapper.objMOLI.Package_ID_External__c==tempMOLI.Package_ID_External__c){
                        wrapper.isSelected=false;
                    }  
                }
            }
         }
            
        }
       return null;
        
    }
    public pagereference saveOppandMOLIs()
    {
        
        string BookStatus = null;
        list<Modification_Order_Line_Item__c> listSelectedMOLI = new list<Modification_Order_Line_Item__c>();
        set<string> BillingPartnerSet = new set<string>();
        for(MOLISelectionWrapper wrapper:listMOLIWrapper)
        {
            if(wrapper.isSelected == true)
            {
                if(bookStatus == null){
                    bookStatus = wrapper.objMOLI.Order_Line_Item__r.Directory_Edition__r.Book_Status__c;
                    listSelectedMOLI.add(wrapper.objMOLI);
                    BillingPartnerSet.add(wrapper.objMOLI.Billing_Partner__c);
                 }else if(wrapper.objMOLI.Order_Line_Item__r.Directory_Edition__r.Book_Status__c == bookStatus){
                    listSelectedMOLI.add(wrapper.objMOLI);
                    BillingPartnerSet.add(wrapper.objMOLI.Billing_Partner__c);
                  }else                     
                  {
                    CommonMethods.addError('Please check whether you have selected only one edition per directory and the same edition for all the selected directories');                          
                    return null;
                   }
            }
        }
        if(listSelectedMOLI.size()>0)
        {
            list<account> accountList =AccountSOQLMethods.getAccountIDByName(BillingPartnerSet);
            map<String,ID> accNameID = new map<String,ID> ();
            ID BilingPartner ;
            for(account accObj:accountList)
            {
                accNameID.put(accObj.name,accObj.id);
            }           
            map<ID, Account> mapAccount = AccountSOQLMethods.getAccountContactByAccountID(new Set<ID>{accountID});
            Savepoint sp = Database.setSavepoint();
            try
            {              
                uniqueID = Userinfo.getUserId() + AccountID +'-'+DateTime.now().format('yyyyMMdd - HHmmss');
                objOPP = insertOpportunity(mapAccount.get(accountID));                
                list<TmpOpportunityLineItem__c> lstTempOLI = new list<TmpOpportunityLineItem__c>();
                //Loop through all records in the order line item collection
                for(Modification_Order_Line_Item__c iterator : listSelectedMOLI) {                   
                    
                    //Adding temp opportunity line item to collection
                    if(accNameID.containsKey(iterator.Billing_Partner__c))
                    {
                        BilingPartner=accNameID.get(iterator.Billing_Partner__c);
                    }
                    else
                    BilingPartner=null;
                    lstTempOLI.add(generateTempOpportunityLineItem(iterator, objOPP, BilingPartner));
                }
                //Insert temp order line items
               
                if(lstTempOLI.size() > 0) {
                    insert lstTempOLI;
                    
                    return (new ApexPages.StandardController(objOPP)).view();
                }
                
               
            }catch (exception ex)
            {
                CommonMethods.addError('Due to some reason, unable to create opportunity. Please contact support team.');
                Database.rollback(sp);
            }
            
            
        }else
        {
            CommonMethods.addError('Please select atleast one order line item to create an opportunity.');
        }
        return null;    
    }
     //Inserting new opportunity
    @TestVisible private Opportunity insertOpportunity(Account objAccount) {
        Opportunity objOPP = generateOpportunity(objAccount, CommonMethods.getRedordTypeIdByName(CommonMessages.opportunityNewRT, CommonMessages.opportunityObjectName));
        insert objOPP;
        return objOPP;
    }
     //Generating new temp opportunity line ietm
    @TestVisible private TmpOpportunityLineItem__c generateTempOpportunityLineItem(Modification_Order_Line_Item__c MOLI, Opportunity objOPP,ID BillingPartner) {
       
         
        system.debug('-----accountid'+accountID+'--'+canvassId+'--'+MOLI.Package_ID__c);
        system.debug('Accountid__c '+ accountID+'Canvass__c '+canvassId+'UnitPrice__c '+ MOLI.UnitPrice__c+'    Geo_Type__c '+ MOLI.Geo_Type__c+'ListPrice__c '+ MOLI.ListPrice__c);
        system.debug('Digital_Product_Requirement__c '+ MOLI.Digital_Product_Requirement__c+'Geo_Code__c '+ MOLI.Geo_Code__c+'blling_End_Date__c '+ MOLI.Billing_End_Date__c+' Billing_Start_Date__c '+ MOLI.Billing_Start_Date__c);                
        system.debug('Billing_Method__c '+ MOLI.Payment_Method__c+'Parent_Line_Item__c '+ MOLI.Parent_Line_Item__c +'Contact__c '+ MOLI.Billing_Contact__c+'Quantity__c '+ MOLI.Quantity__c+' Disount__c '+ MOLI.Discount__c);
        system.debug('status__c '+ MOLI.Status__c+'Description__c '+ MOLI.Description__c+'  Effective_Date__c '+ MOLI.Effective_Date__c+' heaading__c'+MOLI.Directory_Heading__c+' Billing_Duration__c '+ MOLI.Payment_Duration__c+'Parent_Product_ID__c '+ MOLI.Product2__c); 
        system.debug('Directory_Heading__c '+ MOLI.Directory_Heading__c+'orignalOpportunityid__c '+ MOLI.Opportunity__c+'Templinkid__c '+ uniqueID+' Product_Type__c '+ MOLI.Product_Type__c+'Package_ID__c '+ MOLI.Package_ID__c+'Directory_Section__c '+ MOLI.Listing_Section__c+' package_ID_External__c '+ MOLI.Package_ID_External__c);
        system.debug('Full_Rate__c '+ MOLI.ListPrice__c+'Is_P4P__c '+ MOLI.Is_P4P__c+'Product_Code__c '+ MOLI.Product_Code__c+' Opportunityid__c '+ objOPP.Id+'Billing_Partner__c'+MOLI.Billing_Partner__c+'Billing_Frequency__c'+MOLI.Billing_Frequency__c+'TOPPLI_Original_MOLI__c'+MOLI.id);
    
        return new TmpOpportunityLineItem__c(Accountid__c = accountID, 
                                            Canvass__c = canvassId, 
                                            UnitPrice__c = MOLI.UnitPrice__c, 
                                            Geo_Type__c = MOLI.Geo_Type__c,
                                            ListPrice__c = MOLI.ListPrice__c, 
                                            Digital_Product_Requirement__c = MOLI.Digital_Product_Requirement__c,
                                            Geo_Code__c = MOLI.Geo_Code__c,
                                            Billing_End_Date__c = MOLI.Billing_End_Date__c, 
                                            Billing_Start_Date__c = MOLI.Billing_Start_Date__c,                
                                            Billing_Method__c = MOLI.Payment_Method__c,
                                            Parent_Line_Item__c = (MOLI.Parent_Line_Item__c != Null ? MOLI.Parent_Line_Item__c : MOLI.Id),
                                            Contact__c = MOLI.Billing_Contact__c,  
                                            Quantity__c = MOLI.Quantity__c, 
                                            Discount__c = MOLI.Discount__c, 
                                            Status__c = MOLI.Status__c,
                                            Description__c = MOLI.Description__c, 
                                            Effective_Date__c = MOLI.Effective_Date__c, 
                                            Heading__c=MOLI.Directory_Heading__c, 
                                            Billing_Duration__c = MOLI.Payment_Duration__c, 
                                            Parent_Product_ID__c = MOLI.Product2__c, 
                                            Directory_Heading__c = MOLI.Directory_Heading__c,
                                            orignalOpportunityid__c = MOLI.Opportunity__c,
                                            Templinkid__c = uniqueID, 
                                            Product_Type__c = MOLI.Product_Type__c, 
                                            Package_ID__c = MOLI.Package_ID__c, 
                                            Directory_Section__c = MOLI.Listing_Section__c, 
                                            Package_ID_External__c = MOLI.Package_ID_External__c,
                                            Full_Rate__c = MOLI.ListPrice__c,Is_P4P__c = MOLI.Is_P4P__c, 
                                            Product_Code__c = MOLI.Product_Code__c,
                                            Opportunityid__c = objOPP.Id, 
                                            Billing_Partner__c= BillingPartner, 
                                            Billing_Frequency__c=MOLI.Billing_Frequency__c,
                                            TOPPLI_Original_MOLI__c=MOLI.id,
                                            Original_Line_Item_ID__c = (MOLI.Order_Line_Item__r.id != Null ? MOLI.Order_Line_Item__r.id:Null),
                                            Directory__c = (MOLI.Order_Line_Item__r.Directory__c != Null ? MOLI.Order_Line_Item__r.Directory__c:Null),
                                            Listing__c = (MOLI.Order_Line_Item__r.Listing__c != Null? MOLI.Order_Line_Item__r.Listing__c:Null),
                                            Is_Caption__c = (MOLI.Order_Line_Item__r.Is_Caption__c != Null? MOLI.Order_Line_Item__r.Is_Caption__c:Null)); 
                                            
     
    }
    //Generating new opportunity from account data
    private Opportunity generateOpportunity(Account objAccount, ID oppRTID) {
        Opportunity objOpportunity = new Opportunity(AccountId = objAccount.Id, StageName = CommonMessages.proposalStageOpp, 
        Name = objAccount.Name +' ' + System.today(), CloseDate = System.today(), Pricebook2Id = system.label.PricebookId,
        RecordTypeId = oppRTID, Account_Manager__c = objAccount.Account_Manager__c, Account_Number__c = objAccount.Account_Number__c, 
        Account_Primary_Canvass__c = canvassId, modifyid__c = uniqueID);
        if(objAccount.Contacts.size() > 0) {
                Contact objContact = objAccount.Contacts[0];
                objOpportunity.Billing_Contact__c = objContact.Id;
        }
        return objOpportunity;
    }
    
    public static list<Modification_Order_Line_Item__c> deactiveTOPMOLI(set<ID> OppIdSet)
    {
        list<TmpOpportunityLineItem__c> TmpOPPList = [select id,TOPPLI_Original_MOLI__c from TmpOpportunityLineItem__c where Opportunityid__c IN :OppIdSet];
        set<ID> MOLIIDSet = new set<ID> ();
        list<Modification_Order_Line_Item__c> DeactiveMOLIList = new list<Modification_Order_Line_Item__c> ();
        for(TmpOpportunityLineItem__c tmpObj:TmpOPPList)
        {
            MOLIIDSet.add(tmpObj.TOPPLI_Original_MOLI__c);
        }
        if(MOLIIDSet.size() >0){
            DeactiveMOLIList = ModificationOrderLineItemSOQLMethods.getMOLIBasedOnID(MOLIIDSet);
            for(Modification_Order_Line_Item__c objMOLI:DeactiveMOLIList)
            {               
                objMOLI.Inactive__c = true;             
            }
            system.debug(DeactiveMOLIList);
            if(DeactiveMOLIList.size() > 0)
            {
                upsert DeactiveMOLIList;
            }
        
        }
        return DeactiveMOLIList;
    
    }
    

}