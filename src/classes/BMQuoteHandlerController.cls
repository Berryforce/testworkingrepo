public class BMQuoteHandlerController {
	public static void onAfterInsert(list<BigMachines__Quote__c> lstNewQuote) {
		submitApprovalProcess(lstNewQuote, null);
	}
	
	public static void onAfterUpdate(list<BigMachines__Quote__c> lstNewQuote, map<Id, BigMachines__Quote__c> oldMap) {
		submitApprovalProcess(lstNewQuote, oldMap);
	}
	
	private static void submitApprovalProcess(list<BigMachines__Quote__c> lstNewQuote, map<Id, BigMachines__Quote__c> oldMap) {
		set<Id> opportunityIDs = new set<Id>();
		list<Opportunity> lstUpdateOpp = new list<Opportunity>();
		for(BigMachines__Quote__c iterator : lstNewQuote) {
			if(oldMap != null) {
				BigMachines__Quote__c oldBMIQuote = oldMap.get(iterator.Id);
				if(oldBMIQuote.Submitted_for_Approval__c == false && iterator.Submitted_for_Approval__c) {
					system.debug('BG Opty :'+iterator.Submitted_for_Approval__c);
	                opportunityIDs.add(iterator.BigMachines__Opportunity__c);
	            }
	            if(oldBMIQuote.Submitted_for_Approval__c == true && iterator.Submitted_for_Approval__c == false) {
	            	lstUpdateOpp.add(new Opportunity(Id = iterator.BigMachines__Opportunity__c));
	            }
			}
			else {
				if(iterator.Submitted_for_Approval__c) {
	                opportunityIDs.add(iterator.BigMachines__Opportunity__c);
	            }
			}
		}
		
			
		if(opportunityIDs.size() > 0) {
			list<Task> lstTask = new list<Task>();
		    //Getting sales rep manager
		    list<User> lstUser = UserSOQLMethods.getUserDetailsByID(new set<Id>{UserInfo.getUserId()});
            //Getting Canvass branch manager
    		map<Id, Opportunity> lstOpportunityWithCanvassDetails = OpportunitySOQLMethods.getOpportunityCanvassById(opportunityIDs);
            
            for(BigMachines__Quote__c iterator : lstNewQuote) {
            	boolean submitApproval = false;
            	if(oldMap != null) {
					BigMachines__Quote__c oldBMIQuote = oldMap.get(iterator.Id);
					if(oldBMIQuote.Submitted_for_Approval__c == false && iterator.Submitted_for_Approval__c) {
		                submitApproval = true;
		            }
				}
				else {
					if(iterator.Submitted_for_Approval__c) {
		                submitApproval = true;
		            }
				}
				if(submitApproval) {
		            set<Id> setId = new set<Id>();
		            String appeovalTrigger = iterator.Approval_Triggers__c;		            
		            system.debug(appeovalTrigger);
		            Opportunity objOpportunity = lstOpportunityWithCanvassDetails.get(iterator.BigMachines__Opportunity__c);
		            system.debug('************** Execute Approval : ' + submitApproval);
		            if(String.isNotEmpty(appeovalTrigger) && appeovalTrigger.equals('Exception Pricing')) {
		                system.debug('Manager ID : ' +objOpportunity.Account_Primary_Canvass__r.Branch_Manager__c);
		                setId.add(objOpportunity.Account_Primary_Canvass__r.Branch_Manager__c);
		            }
		            else if(String.isNotEmpty(appeovalTrigger) && appeovalTrigger.equals('Waived Setup Fee')) {
		                setId.add(lstUser[0].ManagerId);
		            }
		            else {
		                setId.add(objOpportunity.Account_Primary_Canvass__r.Branch_Manager__c);
		            }
		            
		            if(setId.size() > 0) {
		                list<Id> lstIds = new list<Id>();
		                lstIds.addAll(setId);
		                system.debug(lstIds);
		                if(CommonMethods.submitApprovalProcess(iterator.Id, lstIds)) {
		                    //TO DO 
		                    //Create task
		                    lstTask.add(CommonMethods.createTask('Waiting for approval', 'Not Started', 'Normal', iterator.Id, lstIds[0]));
		                }
		            }
				}
            }
            if(lstTask.size() > 0) {
            	insert lstTask;
            }
        }
        
        if(lstUpdateOpp.size() > 0) {
        	update lstUpdateOpp;
        }
	}
}