@isTest(SeeAllData=True)
public class SalesCreditNoteSOQLMethodsTest{
@isTest(SeeAllData = true) static void SalesCreditNoteSOQLMethodsTest(){
        Set<Id> setSalesInvoiceIds = new Set<Id>();
        set<Id> setSalesCreditNoteInvoiceIds = new Set<Id>();
        List<c2g__codaDimension1__c> lstDimension1 = new List<c2g__codaDimension1__c>();
        Canvass__c newCanvass = TestMethodsUtility.createCanvass();
        Account newAccount = TestMethodsUtility.generateAccount('customer', false);
        newAccount.Primary_Canvass__c = newCanvass.Id;
        insert newAccount;
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Telco__c newTelco = TestMethodsUtility.createTelco(newAccount.Id);
        Pricebook2 newPriceBook = [Select Id from Pricebook2 where isStandard=true limit 1];
        Product2 newProduct = TestMethodsUtility.generateproduct();
        newProduct.Family = 'Print';
        insert newProduct;
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        insert newOpportunity;
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
        Order_Line_Items__c newOrLI = TestMethodsUtility.createOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet);
        Directory__c newDirectory = TestMethodsUtility.createDirectory();
        Directory_Mapping__c newDirectoryMapping = TestMethodsUtility.createDirectoryMapping();
        Directory_Edition__c newDirectoryEdition = TestMethodsUtility.createDirectoryEdition(newDirectory.Id, newAccount.Primary_Canvass__c, newTelco.Id, newDirectoryMapping.Id);
        Canvass__c soqlCanvass = [Select Id, Name, Canvass_Code__c From Canvass__c Where Id =: newCanvass.Id Limit 1];
        c2g__codaDimension1__c Dimension1ForDirectory = TestMethodsUtility.generateDimension1(newDirectory);
        c2g__codaDimension1__c Dimension1ForDirectoryEdition = TestMethodsUtility.generateDimension1(newDirectoryEdition);
        c2g__codaDimension1__c Dimension1ForAccount = TestMethodsUtility.generateDimension1(newCanvass.Name, soqlCanvass.Canvass_Code__c);
        lstDimension1.add(Dimension1ForDirectory);
        lstDimension1.add(Dimension1ForDirectoryEdition);
        lstDimension1.add(Dimension1ForAccount);
        insert lstDimension1;
        c2g__codaDimension2__c Dimension2ForProduct = TestMethodsUtility.generateDimension2(newProduct);
        c2g__codaDimension3__c Dimension3ForOrLI = TestMethodsUtility.generateDimension3(newOrLI);
        c2g__codaInvoice__c newSalesInvoice = TestMethodsUtility.createSalesInvoice(newAccount, newOpportunity);
        setSalesInvoiceIds.add(newSalesInvoice.Id);
        c2g__codaCreditNote__c newSalesInvoiceCreditNote = TestMethodsUtility.createSalesCreditNote(newSalesInvoice, newAccount);
        setSalesCreditNoteInvoiceIds.add(newSalesInvoiceCreditNote.Id);
        System.assertNotEquals(null, SalesCreditNoteSOQLMethods.getSalesCreditNoteByCreditID(setSalesCreditNoteInvoiceIds));
    }
    }