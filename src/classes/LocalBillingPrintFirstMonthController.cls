public with sharing class LocalBillingPrintFirstMonthController {
    
    public Dummy_Object__c objDummy {get;set;}
    public String strSelectedDirectoryEdition {get;set;}
    public String strNoofAccountAndInvoice {get;set;}
    public list<SelectOption> lstDirectoryEdition {get;set;}
    public list<listofBusinessReport> lstLOB {get;set;}
    public boolean bReconciled {get;set;}
    public boolean bMIRT {get;set;}
    public boolean bPostAllInvoice {get;set;}
    public String strBatchStatus {get;set;}
    public String strBatchStatusTelco {get;set;}
    public boolean bEnabledPoller {get;set;}
    public boolean bEnabledPollerForTelco {get;set;}
    public Decimal dTelcoInvoiceTotal {get;set;}
    public Id LOBMatchingReportId{get;set;}
    public Id printFirstMonthPostingReportId{get;set;}
    public Directory_Edition__c objDEValues{get;set;}
    public boolean bShowPostFF {get;set;}
    public boolean bShowCreateTelco {get;set;}
    set<Id> setSIIdTelco{get;set;}
    public boolean bReconciliationEnabledPoller {get;set;}
    set<Id> setSIId;
    Id batchId;
    Id SIReconciliationbatchId;
    public String strSIReconcileBatchStatus {get;set;}    
    public Id LOBReportId {get;set;}
    public Id UDACSumReportId {get;set;}
    Set<Id> setBatchIds = new Set<id>();
    
    public LocalBillingPrintFirstMonthController() {
        objDummy = new Dummy_Object__c();
        objDEValues = new Directory_Edition__c();
        bReconciled = false;
        bMIRT = false;
        strBatchStatus = '';
        strBatchStatusTelco = '';
        bPostAllInvoice = false;
        bShowPostFF = false;
        bShowCreateTelco = false;
        bEnabledPoller = false;
        bEnabledPollerForTelco = false;
        dTelcoInvoiceTotal = 0.0;
        lstLOB = new list<listofBusinessReport>();
        setSIIdTelco = new set<Id>();
        setSIId = new set<Id>();
        strNoofAccountAndInvoice = concatenateString(0, 0);
        Map<String, Local_Billing_Reports__c> reportIds = Local_Billing_Reports__c.getAll();        
        if(reportIds.get('LOB Matching Report')!=null) LOBMatchingReportId= reportIds.get('LOB Matching Report').Report_Id__c;
        if(reportIds.get('Print First Month Posting')!=null) printFirstMonthPostingReportId= reportIds.get('Print First Month Posting').Report_Id__c;
        LOBReportId = Local_Billing_Reports__c.getInstance('LOBReport').Report_Id__c;
        UDACSumReportId = Local_Billing_Reports__c.getInstance('UDACSumReport').Report_Id__c;
    }
    
    private void clearValues() {
        bReconciled = false;
        bMIRT = false;
        strBatchStatus = '';
        strBatchStatusTelco = '';
        strSIReconcileBatchStatus= '';
        bPostAllInvoice = false;
        bShowPostFF = false;
        bShowCreateTelco = false;
        bEnabledPoller = false;
        bEnabledPollerForTelco = false;
        dTelcoInvoiceTotal = 0.0;
        lstLOB = new list<listofBusinessReport>();
        setSIIdTelco = new set<Id>();
        setSIId = new set<Id>();
         bReconciliationEnabledPoller =false;
    }
    
    private String concatenateString(Integer iAccCount, Integer iSICount) {
        return String.valueOf(iAccCount) + ' Accounts and '+ String.valueOf(iSICount) + ' Invoices for Selections';
    }
    
    public void fetchDirectoryEdition() {
        lstDirectoryEdition = new list<SelectOption>();
        if(objDummy.Date__c != null) {
            list<Directory_Edition__c> lstDE = [Select id,Name,New_Print_Bill_Date__c from Directory_Edition__c where Bill_Prep__c =:objDummy.Date__c Order By Name ASC];
            if(lstDE.size()>0) {
                for(Directory_Edition__c iterator : lstDE) {
                    lstDirectoryEdition.add(new SelectOption(iterator.id, iterator.Name));
                }
            }
        }
    }
    
    public void clickGo() {
        clearValues();
        system.debug(strSelectedDirectoryEdition);
        if(String.isNotBlank(strSelectedDirectoryEdition)) {
            fetchDirectoryEditionForReport();
            list<c2g__codaInvoice__c> lstSalesInvoice = [Select Id, SI_Telco__c, SI_Telco__r.Name, Reconcile__c, c2g__Account__c, c2g__Account__r.Name, Customer_Name__c, c2g__NetTotal__c, SI_Payment_Method__c, SI_Billing_Partner__c, (Select Id, Order_Line_Item__r.UnitPrice__c From c2g__InvoiceLineItems__r) 
                                                        From c2g__codaInvoice__c
                                                        where SI_Successful_Payments__c = 1 AND SI_P4P__c = false AND SI_Media_Type__c = 'Print' AND c2g__InvoiceStatus__c = 'In Progress' 
                                                        //AND SI_Payments_Remaining__c IN (11,0) 
                                                        AND SI_Directory_Edition_Code__c =:objDEValues.Edition_Code__c AND  SI_Directory_Code__c =:objDEValues.Directory_Code__c];
            system.debug(lstSalesInvoice.size());
            if(lstSalesInvoice.size() > 0) {
                lstLOB = new list<listofBusinessReport>();
                setSIId = new set<Id>();
                set<Id> setAccountID = new set<Id>();
                map<Id, set<Id>> mapAccountIdByTelcoId = new map<Id, set<Id>>();
                map<Id, set<Id>> mapSIIdByTelcoId = new map<Id, set<Id>>();
                map<Id, String> mapTelcoNameById = new map<Id, String>();
                map<Id, Decimal> mapOLITotalByTelcoID = new map<Id, Decimal>();
                map<Id, Decimal> mapSITotalByTelcoID = new map<Id, Decimal>();
                setSIIdTelco = new set<Id>();
                set<Id> setAccountIDTelco = new set<Id>();
                set<Id> setSIIdForBerry = new set<Id>();
                set<Id> setAccountIDForBerry = new set<Id>();
                Decimal dOLITotal = 0.0;
                Decimal dSITotal = 0.0;
                dTelcoInvoiceTotal = 0.0;
                listofBusinessReport objLOBSummary = new listofBusinessReport();
                listofBusinessReport objLOBBerry = new listofBusinessReport();
                for(c2g__codaInvoice__c iterator : lstSalesInvoice) {
                    setAccountID.add(iterator.Customer_Name__c);
                    setSIId.add(iterator.Id);
                    for(c2g__codaInvoiceLineItem__c iteratorChild : iterator.c2g__InvoiceLineItems__r) {                        
                        objLOBSummary.dOLITotal = (objLOBSummary.dOLITotal == null ? iteratorChild.Order_Line_Item__r.UnitPrice__c.setscale(2) : objLOBSummary.dOLITotal + iteratorChild.Order_Line_Item__r.UnitPrice__c.setscale(2));
                        if(iterator.SI_Payment_Method__c == 'Telco Billing') {
                            if(!mapOLITotalByTelcoID.containsKey(iterator.c2g__Account__c)) {
                                mapOLITotalByTelcoID.put(iterator.c2g__Account__c, 0);
                            }
                            mapOLITotalByTelcoID.put(iterator.c2g__Account__c, mapOLITotalByTelcoID.get(iterator.c2g__Account__c) + iteratorChild.Order_Line_Item__r.UnitPrice__c.setscale(2));
                        }
                        if(iterator.SI_Billing_Partner__c == 'THE BERRY COMPANY') {
                            objLOBBerry.dOLITotal = (objLOBBerry.dOLITotal == null ? iteratorChild.Order_Line_Item__r.UnitPrice__c.setscale(2) : objLOBBerry.dOLITotal + iteratorChild.Order_Line_Item__r.UnitPrice__c.setscale(2));
                        }
                    }
                    
                    objLOBSummary.dSITotal = (objLOBSummary.dSITotal == null ? iterator.c2g__NetTotal__c.setscale(2) : objLOBSummary.dSITotal + iterator.c2g__NetTotal__c.setscale(2));
                    
                    if(iterator.SI_Payment_Method__c == 'Telco Billing') {
                        setSIIdTelco.add(iterator.Id);
                        if(!mapAccountIdByTelcoId.containsKey(iterator.c2g__Account__c)) {
                            mapAccountIdByTelcoId.put(iterator.c2g__Account__c, new set<Id>());
                            mapSIIdByTelcoId.put(iterator.c2g__Account__c, new set<Id>());
                            mapTelcoNameById.put(iterator.c2g__Account__c, iterator.c2g__Account__r.Name);
                            mapSITotalByTelcoID.put(iterator.c2g__Account__c, 0);
                        }
                        mapAccountIdByTelcoId.get(iterator.c2g__Account__c).add(iterator.Customer_Name__c);
                        mapSIIdByTelcoId.get(iterator.c2g__Account__c).add(iterator.Id);                        
                        mapSITotalByTelcoID.put(iterator.c2g__Account__c, mapSITotalByTelcoID.get(iterator.c2g__Account__c) + iterator.c2g__NetTotal__c.setscale(2));
                    }
                    if(iterator.SI_Billing_Partner__c == 'THE BERRY COMPANY') { 
                        setAccountIDForBerry.add(iterator.Customer_Name__c);
                        setSIIdForBerry.add(iterator.Id);                        
                        objLOBBerry.dSITotal = (objLOBBerry.dSITotal == null ? iterator.c2g__NetTotal__c.setscale(2) : objLOBBerry.dSITotal + iterator.c2g__NetTotal__c.setscale(2));
                    }
                }
                
                if(setAccountID.size() > 0) {
                    objLOBSummary.strName = 'Summary';
                    objLOBSummary.iAccCount = setAccountID.size();
                    objLOBSummary.iSICount = setSIId.size();
                    objLOBSummary.dVariance = objLOBSummary.dOLITotal - objLOBSummary.dSITotal;
                    lstLOB.add(objLOBSummary); 
                }
                
                if(setAccountIDForBerry.size() > 0) {
                    objLOBBerry.strName = 'Berry Billed';
                    objLOBBerry.iAccCount = setAccountIDForBerry.size();
                    objLOBBerry.iSICount = setSIIdForBerry.size();
                    objLOBBerry.dVariance = objLOBBerry.dOLITotal - objLOBBerry.dSITotal;
                    lstLOB.add(objLOBBerry);
                }
                for(Id idTelco : mapAccountIdByTelcoId.keySet()) {
                    listofBusinessReport objLOBTelco = new listofBusinessReport();
                    objLOBTelco.strName = mapTelcoNameById.get(idTelco);
                    objLOBTelco.iAccCount = mapAccountIdByTelcoId.get(idTelco).size();
                    objLOBTelco.iSICount = mapSIIdByTelcoId.get(idTelco).size();
                    objLOBTelco.dOLITotal = mapOLITotalByTelcoID.get(idTelco);
                    objLOBTelco.dSITotal = mapSITotalByTelcoID.get(idTelco);
                    dTelcoInvoiceTotal += objLOBTelco.dSITotal;
                    objLOBTelco.dVariance = objLOBTelco.dOLITotal - objLOBTelco.dSITotal;
                    lstLOB.add(objLOBTelco);
                }
                strNoofAccountAndInvoice = concatenateString(setAccountID.size(), setSIId.size());
            }       
        }
    }
    
    public void updateSIwithReconciledCheckBox() {
    	Batch_Size__c reconcileBatchSize=Batch_Size__c.getValues('WizardSIReconcile');
        Batch_Size__c defaultBatchSize=Batch_Size__c.getValues('WizardSIPostWithoutSplit');
        List<SplitBatchJobs__c> reqdSplitBatchList=CommonMethods.fetchRequiredRecords('First Month Print');
        setBatchIds = new set<Id>();
        if(reqdSplitBatchList!=null && reqdSplitBatchList.size()>0) {
        	for(SplitBatchJobs__c splitBatch:reqdSplitBatchList) {
        		setBatchIds.add(Database.executeBatch(new LocalBillingSIPostOrReconciliationBatch('First Month Print','reconcile',objDEValues.Edition_Code__c,objDEValues.Directory_Code__c,Integer.valueOf(splitBatch.Start_Number__c),Integer.valueOf(splitBatch.End_Number__c),Integer.valueOf(splitBatch.Min_Line_Item_Count__c), Integer.valueOf(splitBatch.Max_Line_Item_Count__c)),Integer.valueOf(reconcileBatchSize.Size__c)));
        	}
        }else {
        	setBatchIds.add(Database.executeBatch(new LocalBillingSIPostOrReconciliationBatch('First Month Print','reconcile',objDEValues.Edition_Code__c,objDEValues.Directory_Code__c,null,null,null,null),Integer.valueOf(defaultBatchSize.Size__c)));
        }
        strSIReconcileBatchStatus= 'Status of Reconciliation Invoice Batch : Queued';
        bReconciliationEnabledPoller =true;
    }
    
    public void apexReconciliationJobStatus() {
    	boolean bStatus = LocalBillingCommonMethods.apexBatchJobStatus(setBatchIds);
    	if(bStatus) {
    		strSIReconcileBatchStatus= 'Status of Reconciliation Invoice Batch : Completed';
            bReconciliationEnabledPoller = false;
            bShowPostFF=true;
        }
        else {
        	strSIReconcileBatchStatus= 'Status of Reconciliation Invoice Batch : InProgress/Queued/Aborted/Hold. Please wait.......';
        }
        /*
        Set<Id> setBatchIds = new Set<id>();
        Boolean bStatus = false;
        if(SIReconciliationbatchId != null) {
            setBatchIds.add(SIReconciliationbatchId);
        }
        Map<id,AsyncApexJob> mapBatchFFJob;
        if(setBatchIds.size() > 0) {
            mapBatchFFJob = new Map<Id,AsyncApexJob>([SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors FROM AsyncApexJob WHERE ID =:setBatchIds]);
            if(mapBatchFFJob.containsKey(SIReconciliationbatchId)) {
             strSIReconcileBatchStatus= 'Status of Reconciliation Invoice Batch : ' + mapBatchFFJob.get(SIReconciliationbatchId).Status;
                if(mapBatchFFJob.get(SIReconciliationbatchId).Status == 'Completed') {
                    bStatus = true;
                }
            }
            else {
                bStatus = true;
            }
            
            if(bStatus) {
                bReconciliationEnabledPoller = false;
                bShowPostFF=true;
            }
        }*/
    }
    public void postFFInvoice() {
    	setBatchIds = new set<Id>();
    	Batch_Size__c defaultBatchSize=Batch_Size__c.getValues('WizardSIPostWithoutSplit');
        List<SplitBatchJobs__c> reqdSplitBatchList=CommonMethods.fetchRequiredRecords('First Month Print');
        if(reqdSplitBatchList!=null && reqdSplitBatchList.size()>0){
        	for(SplitBatchJobs__c splitBatch:reqdSplitBatchList){
        		setBatchIds.add(Database.executeBatch(new LocalBillingSIPostOrReconciliationBatch('First Month Print','post',objDEValues.Edition_Code__c,objDEValues.Directory_Code__c,Integer.valueOf(splitBatch.Start_Number__c),Integer.valueOf(splitBatch.End_Number__c),Integer.valueOf(splitBatch.Min_Line_Item_Count__c), Integer.valueOf(splitBatch.Max_Line_Item_Count__c)), Integer.valueOf(splitBatch.Batch_Size__c)));
        	}
        }else{
        	setBatchIds.add(Database.executeBatch(new LocalBillingSIPostOrReconciliationBatch('First Month Print','post',objDEValues.Edition_Code__c,objDEValues.Directory_Code__c,null,null,null,null), Integer.valueOf(defaultBatchSize.Size__c)));
        }
        strBatchStatus = 'Status of Financial Force Invoice Batch : Queued';
        bEnabledPoller = true;  
    }
    
    public void apexJobStatus() {
    	boolean bStatus = LocalBillingCommonMethods.apexBatchJobStatus(setBatchIds);
    	if(bStatus) {
    		strSIReconcileBatchStatus= 'Status of Financial Force Invoice Batch : Completed';
            bEnabledPoller = false;
            bShowCreateTelco = true;
        }
        else {
        	strSIReconcileBatchStatus= 'Status of Financial Force Invoice Batch : Queue/Aborted/Hold/InProgress. Please wait.......';
        }
        /*
        AsyncApexJob objBatchFFJob = [SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors FROM AsyncApexJob WHERE ID =:batchId];
        strBatchStatus = 'Status of Financial Force Invoice Batch : ' + objBatchFFJob.Status;
        if(objBatchFFJob.Status == 'Completed') {
            bEnabledPoller = false;
            bShowCreateTelco = true;
        }*/
    }
    
    public void createTelcoFile() {
        if(String.isNotBlank(strSelectedDirectoryEdition)) {
            objDEValues.Sent_Telco_File__c = true;
            update objDEValues;
            strBatchStatusTelco = 'XML Telco Invoice Status : Queued';
            bEnabledPollerForTelco = true;
        }
    }
    
    public void checkTelcoXMLOutput() {
        if(String.isNotBlank(strSelectedDirectoryEdition)) {
            strBatchStatusTelco = 'XML Telco Invoice Status : InProgress';
            fetchDirectoryEditionForReport();
        }
    }
    
    public void fetchDirectoryEditionForReport() {
        list<Directory_Edition__c> lstDE= [Select Directory__c,Telco__c,Directory_Code__c,Edition_Code__c,Telco_Recives_Electronice_File__c,XML_Output_Total_Amount__c,Sent_Telco_File__c,
                                            id,Name from Directory_Edition__c where id=:strSelectedDirectoryEdition];
        for(Directory_Edition__c iterator : lstDE) {
            objDEValues = iterator;
            if(objDEValues.XML_Output_Total_Amount__c != null && objDEValues.XML_Output_Total_Amount__c > 0) {
                strBatchStatusTelco = 'XML Telco Invoice Status : Completed';
                bEnabledPollerForTelco = false;
            }
        }
    }
    
    public class listofBusinessReport {
        public String strName {get;set;}
        public Integer iAccCount {get;set;}
        public Integer iSICount {get;set;}
        public Decimal dOLITotal {get;set;}
        public Decimal dSITotal {get;set;}
        public Decimal dVariance {get;set;}
        public Integer iAccCountBerry {get;set;}
        public Integer iSICountBerry {get;set;}
        public Decimal dOLITotalBerry {get;set;}
        public Decimal dSITotalBerry {get;set;}
        public Decimal dVarianceBerry {get;set;} 
    }
}