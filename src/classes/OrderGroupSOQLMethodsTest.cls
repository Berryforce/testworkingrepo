@IsTest
public with sharing class OrderGroupSOQLMethodsTest {
    static testMethod void OrderGroupSOQLMethods() {
        Canvass__c objCanvass = TestMethodsUtility.createCanvass();
        
        Account objAccount = new Account(Name = 'Unit Testing', Phone = '(999) 999-9999', Billing_Phone__c='(999) 999-9999');
        insert objAccount;
        
        Opportunity objOpportunity = new Opportunity(StageName ='Perception Analysis', Name = 'unit test', CloseDate = System.today(), AccountId = objAccount.ID);
        insert objOpportunity;
        
        Contact objContact = TestMethodsUtility.generateContact(objAccount.Id);
        //Contact objContact = new Contact(Phone = '(999) 999-9999', LastName = 'Contac', IsActive__c = true, FirstName = 'Test Unit', Fax = '9999999999', Email = 'test@test.com', Description = 'Test', Contact_Type__c = 'Billing', AccountId = objAccount.Id, Contact_Role__c = '1;2', PrimaryBilling__c = true, MailingStreet = 'Test',MailingCountry = 'US', MailingCity = 'Test');
        insert objContact;        
        
        National_Staging_Order_Set__c NSOS = new National_Staging_Order_Set__c();
        insert NSOS;
        
        //Order_Group__c objOrderGroup = new Order_Group__c (Name = 'Unit Testing', oli_count__c =3.0, selected__c = true, Payment_Method__c = 'Test Payment', Order_Account__c = objAccount.ID, Billing_Partner__c = 'Test Partner', Billing_Frequency__c = 'Monthly', Billing_Contact__c = objContact.ID, Opportunity__c = objOpportunity.ID);
        Order_Group__c objOrderGroup = new Order_Group__c (Name = 'Unit Testing', selected__c = true, Order_Account__c = objAccount.ID, Opportunity__c = objOpportunity.ID);
        insert objOrderGroup ;
        
        Set<ID> setOppId = new Set<ID>();
        setOppId.add(objOpportunity.id);
        Set<ID> setAccId = new Set<ID>();
        setOppId.add(objAccount.id);
        Test.startTest();
        System.assertNotEquals(null, OrderGroupSOQLMethods.getOrderGroupByOpptyID(setOppId));
        System.assertNotEquals(null, OrderGroupSOQLMethods.getOrderGroupByOpptyIDAndAccountId(setOppId, setAccId));
        System.assertNotEquals(null, OrderGroupSOQLMethods.getOrderGroupByOrderSetID(objOrderGroup.id));
        System.assertNotEquals(null, OrderGroupSOQLMethods.getOrderGroupByOrderSetIDs(new Set<ID>{objOrderGroup.id}));
        //System.assertNotEquals(null, OrderGroupSOQLMethods.getOrderGroupByNSOS(new Set<ID>{NSOS.id}));
        System.assertNotEquals(null, OrderGroupSOQLMethods.getOrderGroupByAcctIdOpptyCanvassId(objAccount.id, objCanvass.id));
        Test.stopTest();
    }
}