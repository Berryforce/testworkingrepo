@isTest
public class MediaTraxServiceForAddHeadingTest{
    public static testmethod void MediaTraxAddHeadingBatchTest(){
        //Test.setMock(HttpCalloutMock.class, new MediaTraxMockHttpResponseGenerator());
        List<MediaTrax_API_Configuration__c> lstMedia = new list<MediaTrax_API_Configuration__c>();
        MediaTrax_API_Configuration__c objMedia = new MediaTrax_API_Configuration__c(NAME='GetAuthToken',AUTHTOKEN__C='12345',USERNAME__C='12345',PASSWORD__C='12345',ENDPOINT_URL__C='https://ct.mediatrax.com/api/authentication.cfc',HEADER_SOAPACTION__C='GetAuthentication',CALLOUT_METHOD__C='POST',RESPONSE_METHOD__C='GetAuthenticationResponse',RETURN_METHOD__C='GetAuthenticationReturn',SOAP_URL__C='http://schemas.xmlsoap.org/soap/envelope/',API_URL__C='https://ct.mediatrax.com/api/',CONTENT_TYPE__C='text/xml',RETURN_FIELDS__C='',PREDICATEFIELD__C='',PREDICATEOPERATOR__C='',PREDICATEVALUE__C='',FIELDS_MAPPING__C='',OPERATION__C='');
        lstMedia.add(objMedia);
        objMedia = new MediaTrax_API_Configuration__c(NAME='AddHeadings',AUTHTOKEN__C='12345',USERNAME__C='12345',PASSWORD__C='12345',ENDPOINT_URL__C='https://ct.mediatrax.com/api/headings.cfc',HEADER_SOAPACTION__C='AddHeadings',CALLOUT_METHOD__C='POST',RESPONSE_METHOD__C='AddHeadingsResponse',RETURN_METHOD__C='AddHeadingsReturn',SOAP_URL__C='http://schemas.xmlsoap.org/soap/envelope/',API_URL__C='https://ct.mediatrax.com/api/',CONTENT_TYPE__C='text/xml',RETURN_FIELDS__C='',PREDICATEFIELD__C='',PREDICATEOPERATOR__C='EQUALS',PREDICATEVALUE__C='',FIELDS_MAPPING__C='headingNames',OPERATION__C='Add');
        lstMedia.add(objMedia);
        insert lstMedia;
        
        
        Directory_Heading__c objDH = TestMethodsUtility.generateDirectoryHeading();
        insert objDH;
        
        MediaTraxServiceForAddHeadingBatch objAHCallout = new MediaTraxServiceForAddHeadingBatch();
        database.executebatch(objAHCallout , 500);
    }
}