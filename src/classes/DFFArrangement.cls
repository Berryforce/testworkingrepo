public class DFFArrangement {
    
    public list<wrapperOLIcls> wrapperlst{get;set;}
    public list<wrapperOLIcls> wrapperOLILst{get;set;}
    public string getDFFid{get;set;}
    public boolean editflag{get;set;}
    public boolean displaysave{get;set;}
    public boolean displaycancel{get;set;}
    public set<string> geteslectedid{get;set;}
    public list<apexpage> apexpagelst{get;set;}
    public string vfpageid{get;set;}
    public string divstring{get;set;}
    public list<Digital_Product_Requirement__c> DFFHeaderList{get;set;}
    public list<Digital_Product_Requirement__c> DFFCurrentHeader{get;set;}
    public List<wrapperDLHeader> wDLHeader{get;set;}
    public boolean ShowDFFHeaderTable{get;set;}
    public boolean ShowCurrentHeader{get;set;}
    public Id SelectedHeaderId;
    String CaptionId;
    public boolean ShowMemberBtn{get;set;}
    public DFFArrangement(ApexPages.StandardController controller) 
    {
        apexpagelst=new  list<apexpage>();
        divstring = '<div id="mypopupdiv1"  role="dialog" aria-live="assertive" aria-describedby="chatterFindExistingContentTitle" class="overlayDialog " style="width: 946px; display: block; left: 285.5px; top: 19px; visibility:hidden">hell0</div>';
        editflag=false;
        displaysave=false;
        
        displaycancel=false;
        getDFFid=ApexPages.currentPage().getParameters().get('id');
        List<Digital_Product_Requirement__c> HeadDirList=new List<Digital_Product_Requirement__c>();
        geteslectedid=new set<string>();
        getDFFdata();
    }


public void getDFFdata()
{
    getDFFid=ApexPages.currentPage().getParameters().get('id');
    wrapperOLILst=new list<wrapperOLIcls>();
    list<Order_Line_Items__c> ListOLI=new List<Order_Line_Items__c>();
    list<Digital_Product_Requirement__c> ListDFF=new List<Digital_Product_Requirement__c>();
    String HeaderOLIID;
    Digital_Product_Requirement__c SelectedDFF=[Select Id,name,OrderLineItemID__r.Scoped_Caption_Header__c,OrderLineItemID__r.Anchor_Listing_Caption_Header__c,OrderLineItemID__c,Caption_Address__c,Caption_City__c,Caption_Country__c,Caption_Display_Text__c,Caption_Header__c,Caption_Member__c,Caption_Phone_Area_Code__c,Caption_Phone_Exchange__c,Caption_Phone_Line__c,Caption_Phone_Number__c,Caption_PO_Box__c,Caption_Print_Address__c,Caption_Print_Name__c,Caption_Print_Phone_Number__c,Caption_State__c,Caption_Street_Name__c,Caption_Street_Number__c,Caption_Indent_Level__c,Caption_Indent_Order__c from Digital_Product_Requirement__c where ID=:getDFFid];
    if(SelectedDFF.Caption_Header__c==true)
    {
       HeaderOLIID=SelectedDFF.OrderLineItemID__c;
    }
    else
    {
       if(SelectedDFF.OrderLineItemID__r.Anchor_Listing_Caption_Header__c!=null)
       {
           HeaderOLIID=SelectedDFF.OrderLineItemID__r.Anchor_Listing_Caption_Header__c;
       }
       else
       {
           HeaderOLIID=SelectedDFF.OrderLineItemID__r.Scoped_Caption_Header__c;
       }
    }
    
    if(HeaderOLIID !=null)
    {
           ListOLI=[SELECT ID,Anchor_Listing_Caption_Header__c,Listing_Name__c,Digital_Product_Requirement__c,Digital_Product_Requirement__r.Name,Product2__c,ProductCode__c,Digital_Product_Requirement__r.Caption_Address__c,Digital_Product_Requirement__r.Caption_City__c,Digital_Product_Requirement__r.Caption_Country__c,Digital_Product_Requirement__r.Caption_Display_Text__c,Digital_Product_Requirement__r.Caption_Header__c,Digital_Product_Requirement__r.Caption_Member__c,Digital_Product_Requirement__r.Caption_Phone_Area_Code__c,Digital_Product_Requirement__r.Caption_Phone_Exchange__c,Digital_Product_Requirement__r.Caption_Phone_Line__c,Digital_Product_Requirement__r.Caption_Phone_Number__c,Digital_Product_Requirement__r.Caption_PO_Box__c,Digital_Product_Requirement__r.Caption_Print_Address__c,Digital_Product_Requirement__r.Caption_Print_Name__c,Digital_Product_Requirement__r.Caption_Print_Phone_Number__c,Digital_Product_Requirement__r.Caption_State__c,Digital_Product_Requirement__r.Caption_Street_Name__c,Digital_Product_Requirement__r.Caption_Street_Number__c,Digital_Product_Requirement__r.Caption_Indent_Level__c,Digital_Product_Requirement__r.Caption_Indent_Order__c FROM Order_Line_Items__c where (id =:HeaderOLIID OR Anchor_Listing_Caption_Header__c=:HeaderOLIID OR Scoped_Caption_Header__c=:HeaderOLIID) order by Digital_Product_Requirement__r.Caption_Indent_Order__c,Digital_Product_Requirement__r.Caption_Indent_Level__c];
           ListDFF=[Select Id,name,OrderLineItemID__r.Scoped_Caption_Header__c,OrderLineItemID__r.Anchor_Listing_Caption_Header__c,OrderLineItemID__c,Caption_Address__c,Caption_City__c,Caption_Country__c,Caption_Display_Text__c,Caption_Header__c,Caption_Member__c,Caption_Phone_Area_Code__c,Caption_Phone_Exchange__c,Caption_Phone_Line__c,Caption_Phone_Number__c,Caption_PO_Box__c,Caption_Print_Address__c,Caption_Print_Name__c,Caption_Print_Phone_Number__c,Caption_State__c,Caption_Street_Name__c,Caption_Street_Number__c,Caption_Indent_Level__c,Caption_Indent_Order__c from Digital_Product_Requirement__c where OrderLineItemID__r.Scoped_Caption_Header__c= : HeaderOLIID];
    }
    
    
    for(Order_Line_Items__c OLIIterator : ListOLI)
    {
        wrapperOLIcls tempOLIvar;
        if(geteslectedid.contains(OLIIterator.id))
        {
            tempOLIvar=new wrapperOLIcls(true,OLIIterator);
        }
        else
        {
            tempOLIvar=new wrapperOLIcls(false,OLIIterator);
        }
        wrapperOLILst.add(tempOLIvar);
    }
    
}

public void uplisting_order()
{
    list<Directory_Listing__c> forupdateListing=new  list<Directory_Listing__c>();
    list<Digital_Product_Requirement__c> forupdateDFF=new  list<Digital_Product_Requirement__c>();
    geteslectedid=new set<string>();
    for(integer i=0;i<wrapperOLILst.size();i++)
    {
       System.debug(wrapperOLILst[i].ischecked+'@@@@@@@@'+i);
        if(wrapperOLILst[i].ischecked==true)
        {
            integer tempi=i;
            System.debug('@@@@@@@@'+tempi);
            if((tempi+1)<wrapperOLILst.size())
            {
               System.debug('@@@@@@@@'+tempi);
               if(wrapperOLILst[tempi+1].ischecked==true)
               {
                   
                    integer j=tempi;
                    while(wrapperOLILst[j].ischecked==true)
                    {
                       geteslectedid.add(wrapperOLILst[j].objOLI.Digital_Product_Requirement__r.id);
                       decimal new_indentorder=wrapperOLILst[j-1].objOLI.Digital_Product_Requirement__r.Caption_Indent_Order__c;
                               Digital_Product_Requirement__c uptempDFF=new Digital_Product_Requirement__c(id=wrapperOLILst[j].objOLI.Digital_Product_Requirement__r.id,Caption_Indent_Order__c=new_indentorder) ;
                               forupdateDFF.add(uptempDFF);
                               if((j+1)<wrapperOLILst.size())
                       {
                          j=j+1;
                       }
                       else
                       {
                         j=wrapperOLILst.size();
                         break;
                       }
                    }
                    
                    
                           decimal new_indentorder=wrapperOLILst[j-1].objOLI.Digital_Product_Requirement__r.Caption_Indent_Order__c;
                           Digital_Product_Requirement__c uptempDFF=new Digital_Product_Requirement__c(id=wrapperOLILst[i-1].objOLI.Digital_Product_Requirement__r.id,Caption_Indent_Order__c=new_indentorder) ;
                           forupdateDFF.add(uptempDFF);
                           i=j;
                }
                else
                {
                            System.debug(wrapperOLILst[i].objOLI.Digital_Product_Requirement__r.id+'@@@@@@@@'+tempi);
                            geteslectedid.add(wrapperOLILst[i].objOLI.Digital_Product_Requirement__r.id);
                            decimal indent_order1,indent_order2;
                            indent_order1=wrapperOLILst[i].objOLI.Digital_Product_Requirement__r.Caption_Indent_Order__c;
                            indent_order2=wrapperOLILst[i-1].objOLI.Digital_Product_Requirement__r.Caption_Indent_Order__c;
                            Digital_Product_Requirement__c uptempDFF1=new Digital_Product_Requirement__c(id=wrapperOLILst[i-1].objOLI.Digital_Product_Requirement__r.Id,Caption_Indent_Order__c=indent_order1) ;
                            forupdateDFF.add(uptempDFF1);
                            Digital_Product_Requirement__c uptempDFF2=new Digital_Product_Requirement__c(id=wrapperOLILst[i].objOLI.Digital_Product_Requirement__r.Id,Caption_Indent_Order__c=indent_order2) ;
                            forupdateDFF.add(uptempDFF2);
                    
                }
               
            }
            else
            {
                            System.debug(wrapperOLILst[i].objOLI.Digital_Product_Requirement__r.id+'@@@@@@@@'+tempi);
                            geteslectedid.add(wrapperOLILst[i].objOLI.Digital_Product_Requirement__r.id);
                            decimal indent_order1,indent_order2;
                            indent_order1=wrapperOLILst[i].objOLI.Digital_Product_Requirement__r.Caption_Indent_Order__c;
                            indent_order2=wrapperOLILst[i-1].objOLI.Digital_Product_Requirement__r.Caption_Indent_Order__c;
                            Digital_Product_Requirement__c uptempDFF1=new Digital_Product_Requirement__c(id=wrapperOLILst[i-1].objOLI.Digital_Product_Requirement__r.Id,Caption_Indent_Order__c=indent_order1) ;
                            forupdateDFF.add(uptempDFF1);
                            Digital_Product_Requirement__c uptempDFF2=new Digital_Product_Requirement__c(id=wrapperOLILst[i].objOLI.Digital_Product_Requirement__r.Id,Caption_Indent_Order__c=indent_order2) ;
                            forupdateDFF.add(uptempDFF2);
                    
            
            }
            
        }
    
    }
    if(forupdateDFF.size()>0)
    {
       update forupdateDFF;
    }
    getDFFdata();
}
public void downlisting_order()
{
  geteslectedid=new set<string>();
   list<Digital_Product_Requirement__c> forupdateDFF=new  list<Digital_Product_Requirement__c>();
    for(integer i=0;i<wrapperOLILst.size();i++)
    {
        if(wrapperOLILst[i].ischecked==true)
        {
            if(wrapperOLILst[i+1].ischecked==true)
            {
                integer j=i;
                while(wrapperOLILst[j].ischecked)
                {
                           geteslectedid.add(wrapperOLILst[j].objOLI.Digital_Product_Requirement__r.id);
                           decimal new_indentorder=wrapperOLILst[j+1].objOLI.Digital_Product_Requirement__r.Caption_Indent_Order__c;
                           Digital_Product_Requirement__c uptempDFF2=new Digital_Product_Requirement__c(id=wrapperOLILst[j].objOLI.Digital_Product_Requirement__r.Id,Caption_Indent_Order__c=new_indentorder) ;
                           forupdateDFF.add(uptempDFF2);
                             if((j+1)<wrapperOLILst.size())
                                     j=j+1;
                               else
                               {
                                 j=wrapperOLILst.size();
                                 break;
                               }
                 }
                    
                    decimal new_indentorder=wrapperOLILst[i].objOLI.Digital_Product_Requirement__r.Caption_Indent_Order__c;
                    Digital_Product_Requirement__c uptempDFF2=new Digital_Product_Requirement__c(id=wrapperOLILst[j].objOLI.Digital_Product_Requirement__r.Id,Caption_Indent_Order__c=new_indentorder) ;
                    forupdateDFF.add(uptempDFF2);
                    i=j;
                    
                
            }
            else
            {
                     geteslectedid.add(wrapperOLILst[i].objOLI.Digital_Product_Requirement__r.id);
                     decimal indentlevel1,indentlevel2;
                     indentlevel1=wrapperOLILst[i].objOLI.Digital_Product_Requirement__r.Caption_Indent_Order__c;
                     Digital_Product_Requirement__c uptempDFF1=wrapperOLILst[i].objOLI.Digital_Product_Requirement__r;
                     indentlevel2=wrapperOLILst[i+1].objOLI.Digital_Product_Requirement__r.Caption_Indent_Order__c;
                      Digital_Product_Requirement__c uptempDFF2=wrapperOLILst[i+1].objOLI.Digital_Product_Requirement__r;
                      uptempDFF2.Caption_Indent_Order__c=indentlevel1;
                      forupdateDFF.add(uptempDFF2);
                      uptempDFF1.Caption_Indent_Order__c=indentlevel2;
                      forupdateDFF.add(uptempDFF1);
            }
        }
    }
    if(forupdateDFF.size()>0)
    {
       update forupdateDFF;
    }
    getDFFdata();
}
public void activeedit()
{
    editflag=true;
    displaysave=true;
    displaycancel=true;
  
}
public void increaseindentlevel()
{
    geteslectedid=new set<string>();
    list<Digital_Product_Requirement__c> forupdateDFF=new  list<Digital_Product_Requirement__c>();
    for(integer i=0;i<wrapperOLILst.size();i++)
    {
       if(wrapperOLILst[i].ischecked)
       {
           geteslectedid.add(wrapperOLILst[i].objOLI.Digital_Product_Requirement__c);
           Digital_Product_Requirement__c uptempDFF=new Digital_Product_Requirement__c(id=wrapperOLILst[i].objOLI.Digital_Product_Requirement__c,Caption_Indent_Level__c =wrapperOLILst[i].objOLI.Digital_Product_Requirement__r.Caption_Indent_Level__c+1);
           forupdateDFF.add(uptempDFF);
       }
     }
      if(forupdateDFF.size()>0)
    {
    update forupdateDFF;
    } 
     getDFFdata();
}
public void decreaseindentlevel()
{
   geteslectedid=new set<string>();
    list<Digital_Product_Requirement__c> forupdateDFF=new  list<Digital_Product_Requirement__c>();
    for(integer i=0;i<wrapperOLILst.size();i++)
    {
       if(wrapperOLILst[i].ischecked)
       {
           geteslectedid.add(wrapperOLILst[i].objOLI.Digital_Product_Requirement__c);
           Digital_Product_Requirement__c uptempDFF=new Digital_Product_Requirement__c(id=wrapperOLILst[i].objOLI.Digital_Product_Requirement__c,Caption_Indent_Level__c=((wrapperOLILst[i].objOLI.Digital_Product_Requirement__r.Caption_Indent_Level__c-1)>=0?wrapperOLILst[i].objOLI.Digital_Product_Requirement__r.Caption_Indent_Level__c-1:0));
           forupdateDFF.add(uptempDFF);
       }
     }
      if(forupdateDFF.size()>0)
    {
    update forupdateDFF;
    } 
     getDFFdata();
}
public void cancelbtn()
{
    editflag=false;
    displaysave=false;
    displaycancel=false;
    geteslectedid=new set<string>();
    for(integer i=0;i<wrapperOLILst.size();i++)
    {
        if(wrapperOLILst[i].ischecked==true)
        {
            geteslectedid.add(wrapperOLILst[i].objOLI.Digital_Product_Requirement__c);
        }
    }
    getDFFdata();
}
public void savebtn()
{
    list<Digital_Product_Requirement__c> forupdateDFF=new  list<Digital_Product_Requirement__c>();
    geteslectedid=new set<string>();
    for(integer i=0;i<wrapperOLILst.size();i++)
    {
       if(wrapperOLILst[i].ischecked)
       {
           geteslectedid.add(wrapperOLILst[i].objOLI.Digital_Product_Requirement__c);
           Digital_Product_Requirement__c uptempDFF=new Digital_Product_Requirement__c(id=wrapperOLILst[i].objOLI.Digital_Product_Requirement__c,Caption_Print_Name__c=wrapperOLILst[i].objOLI.Digital_Product_Requirement__r.Caption_Print_Name__c,Caption_Print_Address__c=wrapperOLILst[i].objOLI.Digital_Product_Requirement__r.Caption_Print_Address__c,Caption_Print_Phone_Number__c=wrapperOLILst[i].objOLI.Digital_Product_Requirement__r.Caption_Print_Phone_Number__c);
           forupdateDFF.add(uptempDFF);
       }
     }
     if(forupdateDFF.size()>0)
    {
    update forupdateDFF;
    }
    cancelbtn();
    getDFFdata();
}
public PageReference GenerateNewListing()
{
  PageReference pg=null;
  return pg;
}

public void PopulateDLCaptionHeaders()
{
  list<Digital_Product_Requirement__c> DLHeaderList=new list<Digital_Product_Requirement__c>();
  Digital_Product_Requirement__c SelectedDFF=new Digital_Product_Requirement__c ();
  wDLHeader=new List<wrapperDLHeader> ();
  Set<Id> HeaderSet=new Set<Id>();
  DFFCurrentHeader=new list<Digital_Product_Requirement__c> ();
  string CurrentHeaderId=ApexPages.currentPage().getParameters().get('id');
  if(CurrentHeaderId!=null)
  {
      SelectedDFF=[Select Id,name,OrderLineItemID__r.Anchor_Listing_Caption_Header__r.Digital_Product_Requirement__c,OrderLineItemID__r.Anchor_Listing_Caption_Header__c,OrderLineItemID__c,Caption_Address__c,Caption_City__c,Caption_Country__c,Caption_Display_Text__c,Caption_Header__c,Caption_Member__c,Caption_Phone_Area_Code__c,Caption_Phone_Exchange__c,Caption_Phone_Line__c,Caption_Phone_Number__c,Caption_PO_Box__c,Caption_Print_Address__c,Caption_Print_Name__c,Caption_Print_Phone_Number__c,Caption_State__c,Caption_Street_Name__c,Caption_Street_Number__c,Caption_Indent_Level__c,Caption_Indent_Order__c from Digital_Product_Requirement__c where ID=:CurrentHeaderId];
    
      if(SelectedDFF.Caption_Member__c ==true && SelectedDFF.Caption_Header__c==false)
      {
        HeaderSet.add(SelectedDFF.OrderLineItemID__r.Anchor_Listing_Caption_Header__c);
        CurrentHeaderId=SelectedDFF.OrderLineItemID__r.Anchor_Listing_Caption_Header__r.Digital_Product_Requirement__c;
      }
      else if (SelectedDFF.Caption_Member__c ==false && SelectedDFF.Caption_Header__c==true)
      {
        HeaderSet.add(SelectedDFF.id);
        CurrentHeaderId=SelectedDFF.id;
      }
  }
  if(HeaderSet.size()>0)
  {
      DFFCurrentHeader=[Select Id,name,OrderLineItemID__r.Listing_Name__c,OrderLineItemID__r.Name,OrderLineItemID__r.Anchor_Listing_Caption_Header__c,Caption_Address__c,Caption_City__c,Caption_Country__c,Caption_Display_Text__c,Caption_Header__c,Caption_Member__c,Caption_Phone_Area_Code__c,Caption_Phone_Exchange__c,Caption_Phone_Line__c,Caption_Phone_Number__c,Caption_PO_Box__c,Caption_Print_Address__c,Caption_Print_Name__c,Caption_Print_Phone_Number__c,Caption_State__c,Caption_Street_Name__c,Caption_Street_Number__c,Caption_Indent_Level__c,Caption_Indent_Order__c from Digital_Product_Requirement__c where ID=:CurrentHeaderId];
      ShowCurrentHeader=true;
      DLHeaderList=[Select Id,name,OrderLineItemID__r.Listing_Name__c,OrderLineItemID__r.Name,OrderLineItemID__r.Anchor_Listing_Caption_Header__c,Caption_Address__c,Caption_City__c,Caption_Country__c,Caption_Display_Text__c,Caption_Header__c,Caption_Member__c,Caption_Phone_Area_Code__c,Caption_Phone_Exchange__c,Caption_Phone_Line__c,Caption_Phone_Number__c,Caption_PO_Box__c,Caption_Print_Address__c,Caption_Print_Name__c,Caption_Print_Phone_Number__c,Caption_State__c,Caption_Street_Name__c,Caption_Street_Number__c,Caption_Indent_Level__c,Caption_Indent_Order__c from Digital_Product_Requirement__c where OrderLineItemID__r.Anchor_Listing_Caption_Header__r.Digital_Product_Requirement__c=:CurrentHeaderId order by Caption_Indent_Order__c,Caption_Indent_Level__c];
      
      if(DLHeaderList.size()>0)
      {
        ShowDFFHeaderTable=true;
        for(Digital_Product_Requirement__c   dFFIterator : DLHeaderList)
        {
            wDLHeader.add(new wrapperDLHeader(false,dFFIterator));
        }
      }
      
   }
}

public PageReference getSelected() 
{
SelectedHeaderId=ApexPages.currentPage().getParameters().get('headerid');
system.debug('############'+SelectedHeaderId);
return null;
}


public void ReplaceNewDLHeader()
{
   system.debug('############'+SelectedHeaderId);
   Digital_Product_Requirement__c dlParent=new Digital_Product_Requirement__c();
   Digital_Product_Requirement__c SelectedListing=new Digital_Product_Requirement__c();
   Digital_Product_Requirement__c UpdateHeader=new Digital_Product_Requirement__c();
   List<Digital_Product_Requirement__c> UpdateDFFList=new List<Digital_Product_Requirement__c>();
   List<Order_Line_Items__c> UpdateOLIList=new List<Order_Line_Items__c >();
   if(SelectedHeaderId!=null)
   {
          SelectedListing=[Select Id,name,OrderLineItemID__r.Id,OrderLineItemID__r.Anchor_Listing_Caption_Header__r.Digital_Product_Requirement__c,OrderLineItemID__r.Anchor_Listing_Caption_Header__c,OrderLineItemID__c,Caption_Address__c,Caption_City__c,Caption_Country__c,Caption_Display_Text__c,Caption_Header__c,Caption_Member__c,Caption_Phone_Area_Code__c,Caption_Phone_Exchange__c,Caption_Phone_Line__c,Caption_Phone_Number__c,Caption_PO_Box__c,Caption_Print_Address__c,Caption_Print_Name__c,Caption_Print_Phone_Number__c,Caption_State__c,Caption_Street_Name__c,Caption_Street_Number__c,Caption_Indent_Level__c,Caption_Indent_Order__c from Digital_Product_Requirement__c where id=:SelectedHeaderId Limit 1];
          string CurrentHeaderId=ApexPages.currentPage().getParameters().get('id');
          if(CurrentHeaderId!=null)
          {
                dlParent=[Select Id,name,OrderLineItemID__r.Id,OrderLineItemID__r.Anchor_Listing_Caption_Header__r.Digital_Product_Requirement__c,OrderLineItemID__r.Anchor_Listing_Caption_Header__c,OrderLineItemID__c,Caption_Address__c,Caption_City__c,Caption_Country__c,Caption_Display_Text__c,Caption_Header__c,Caption_Member__c,Caption_Phone_Area_Code__c,Caption_Phone_Exchange__c,Caption_Phone_Line__c,Caption_Phone_Number__c,Caption_PO_Box__c,Caption_Print_Address__c,Caption_Print_Name__c,Caption_Print_Phone_Number__c,Caption_State__c,Caption_Street_Name__c,Caption_Street_Number__c,Caption_Indent_Level__c,Caption_Indent_Order__c from Digital_Product_Requirement__c where ID =: CurrentHeaderId];
               
                if(dlParent.Caption_Member__c ==true && dlParent.Caption_Header__c==false)
                {
                 CurrentHeaderId=dlParent.OrderLineItemID__r.Anchor_Listing_Caption_Header__r.Digital_Product_Requirement__c;
                }
                else if (dlParent.Caption_Member__c ==false && dlParent.Caption_Header__c==true)
                {
                 CurrentHeaderId=dlParent.id;
                }
               // Updated Current Header Details 
               UpdateHeader=[Select Id,name,OrderLineItemID__r.Id,OrderLineItemID__r.Anchor_Listing_Caption_Header__r.Digital_Product_Requirement__c,OrderLineItemID__r.Anchor_Listing_Caption_Header__c,OrderLineItemID__c,Caption_Address__c,Caption_City__c,Caption_Country__c,Caption_Display_Text__c,Caption_Header__c,Caption_Member__c,Caption_Phone_Area_Code__c,Caption_Phone_Exchange__c,Caption_Phone_Line__c,Caption_Phone_Number__c,Caption_PO_Box__c,Caption_Print_Address__c,Caption_Print_Name__c,Caption_Print_Phone_Number__c,Caption_State__c,Caption_Street_Name__c,Caption_Street_Number__c,Caption_Indent_Level__c,Caption_Indent_Order__c from Digital_Product_Requirement__c where ID =: CurrentHeaderId];
               
               
               if(DFFCurrentHeader.size()>0)
                    {
                        for(Digital_Product_Requirement__c dl_Iterator:[Select Id,name,OrderLineItemID__r.Id,OrderLineItemID__c,OrderLineItemID__r.Anchor_Listing_Caption_Header__c,Caption_Display_Text__c,Caption_Header__c,Caption_Member__c,Caption_Indent_Level__c,Caption_Indent_Order__c from Digital_Product_Requirement__c where (OrderLineItemID__r.Anchor_Listing_Caption_Header__r.Digital_Product_Requirement__c=: CurrentHeaderId OR id=:CurrentHeaderId) order by Caption_Indent_Order__c,Caption_Indent_Level__c ])
                        {
                          
                          if(dl_Iterator.id==SelectedHeaderId)
                          {
                               //Converting Selected Header with Existing Header
                               Digital_Product_Requirement__c dffNewHeader=new Digital_Product_Requirement__c (id=dl_Iterator.id);
                               dffNewHeader.Caption_Indent_Level__c=UpdateHeader.Caption_Indent_Level__c;
                               dffNewHeader.Caption_Indent_Order__c =UpdateHeader.Caption_Indent_Order__c ;
                               dffNewHeader.Caption_Header__c=true;
                               dffNewHeader.Caption_Member__c=false;
                               UpdateDFFList.add(dffNewHeader);
                               Order_Line_Items__c oliNewHeader=new Order_Line_Items__c (id=dl_Iterator.OrderLineItemID__r.Id,Anchor_Listing_Caption_Header__c=null);
                               UpdateOLIList.add(oliNewHeader);
                               
                          }
                          else if(dl_Iterator.id==CurrentHeaderId)
                          {
                                Digital_Product_Requirement__c dffOldHeader=new Digital_Product_Requirement__c (id=dl_Iterator.id);
                                dffOldHeader.Caption_Indent_Level__c=SelectedListing.Caption_Indent_Level__c;
                                dffOldHeader.Caption_Indent_Order__c =SelectedListing.Caption_Indent_Order__c ;
                                dffOldHeader.Caption_Header__c=false;
                                dffOldHeader.Caption_Member__c=true;
                                UpdateDFFList.add(dffOldHeader);
                                Order_Line_Items__c oliOldHeader=new Order_Line_Items__c (id=dl_Iterator.OrderLineItemID__r.Id,Anchor_Listing_Caption_Header__c=SelectedListing.OrderLineItemID__c);
                                UpdateOLIList.add(oliOldHeader);
                          }
                          else if(dl_Iterator.id!=SelectedHeaderId &&  dl_Iterator.id!=CurrentHeaderId)
                          {
                                Order_Line_Items__c oliChild=new Order_Line_Items__c (id=dl_Iterator.OrderLineItemID__r.Id,Anchor_Listing_Caption_Header__c=SelectedListing.OrderLineItemID__c);
                                UpdateOLIList.add(oliChild);
                          }
                          else{}
                          
                        }
                         
                        if(UpdateDFFList.size()>0)
                        {
                           update UpdateDFFList;
                        }
                        if(UpdateOLIList.size()>0)
                        {
                          update UpdateOLIList;
                        }
                    }
            }
    }
    else
    {
       ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.Error,'Please select one for the new header.');
       ApexPages.addMessage(myMsg);
    
    }
    PopulateDLCaptionHeaders();   
}

public class wrapperDLHeader
{
    public boolean ischecked{get;set;}
    public Digital_Product_Requirement__c  objDFF {get;set;}
    public wrapperDLHeader(Boolean ischecked, Digital_Product_Requirement__c  objDFF) 
    {
        this.ischecked = ischecked;
        this.objDFF= objDFF;
    }
}

public class wrapperOLIcls
{
    public boolean ischecked{get;set;}
    public Order_Line_Items__c  objOLI {get;set;}
    public string indentorderdisplay{get;set;}
    public wrapperOLIcls(Boolean ischecked, Order_Line_Items__c  objOLI) 
    {
        this.ischecked = ischecked;
        this.objOLI= objOLI;
         string indentorder='';
         for(integer i=0;i<(integer.valueof(objOLI.Digital_Product_Requirement__r.Caption_Indent_Level__c)*2);i++)
         {
             indentorder=indentorder+'&nbsp;';
         }
         this.indentorderdisplay=+'<span>'+indentorder+'</span>';
    }
}

}