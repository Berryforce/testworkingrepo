global class DirectoryPaginationTestBatch implements Database.Batchable<sObject>{
    
    //String userId='005K0000002MSSp'; 
    
    global DirectoryPaginationTestBatch(){
    }
    
    global Database.QueryLocator start(Database.BatchableContext bc){        
        String SOQL = 'SELECT id, Caption_Member__c, DP_Caption_Member_F__c, Caption_Header__c, DP_Caption_Header_F__c, Continuous_Service_Order_Appearance__c,'+
                      'DP_Continuous_Service_Order_Appearance_F__c, Is_Cross_Reference__c, DP_Is_Cross_Reference_F__c, IBUN_Website_One__c, DP_IBUN_Website_One_F__c,'+
                      'IBUN_Website_Two__c, DP_IBUN_Website_Two_F__c, Scoped_Caption_Header__c, DP_Scoped_Caption_Header_F__c, Year__c, DP_Year_F__c, Directory_Edition_Code__c,'+
                      'DP_Directory_Edition_Code_F__c, Directory_Edition_Name__c, DP_Directory_Edition_Name_F__c,Banner__c,DP_Banner_F__c,DP_Billboard_F__c,Billboard__c,Directory_Heading_Code__c,DP_Directory_Heading_Code_F__c,' +
                      'Directory_Heading__c,DP_Directory_Heading_F__c,IBUN_Type__c,DP_IBUN_Type_F__c,Incorrect_Setup_Excluded_from_XML__c,DP_Incorrect_Setup_Excluded_from_XML_F__c,'+
                      'Internet_Bundle_Ad__c,DP_Internet_Bundle_Ad_F__c,UDAC__c,DP_UDAC_F__c,Scoped_Caption_Member__c,DP_Scoped_Caption_Member_F__c,Section_Heading_Mapping_ID__c,DP_Section_Heading_Mapping_ID_F__c,'+
                      'Section_Type__c,DP_Section_Type_F__c,Trademark_Finding_Line__c,DP_Trademark_Finding_Line_F__c,Type__c,DP_Type_F__c FROM Directory_Pagination__c';        
       
        return Database.getQueryLocator(SOQL);
    }
    
    global void execute(Database.BatchableContext bc, List<Directory_Pagination__c> scope){
    
        List<Directory_Pagination__c> lstUpdt = new List<Directory_Pagination__c>();
        id idDP;
        String strErrorMessage = '';
        List<String> lstError = null;
        Boolean bolError = false;
        for(Directory_Pagination__c objDP : scope) {
            bolError = false;
			idDP = objDP.id;
            lstError = new List<String>();
            Directory_Pagination__c objUpdtDP = new Directory_Pagination__c(id=objDP.id);
            if(objDP.Caption_Member__c != objDP.DP_Caption_Member_F__c) {
                bolError = true;
                objUpdtDP.Caption_Member__c = objUpdtDP.DP_Caption_Member_F__c;
                lstError.add('Caption_Member__c $$ '+objDP.Caption_Member__c+' $$ '+objDP.DP_Caption_Member_F__c+',');
            }
            if(objDP.Caption_Header__c != objDP.DP_Caption_Header_F__c) {
                bolError = true;
                objUpdtDP.Caption_Header__c = objUpdtDP.DP_Caption_Header_F__c;
                lstError.add('Caption_Header__c $$ '+objDP.Caption_Header__c +' $$ '+objDP.DP_Caption_Header_F__c+',');
            }
            if(objDP.Continuous_Service_Order_Appearance__c!= objDP.DP_Continuous_Service_Order_Appearance_F__c) {
                bolError = true;
                objUpdtDP.Continuous_Service_Order_Appearance__c= objUpdtDP.DP_Continuous_Service_Order_Appearance_F__c;
                lstError.add('Continuous_Service_Order_Appearance__c $$ '+objDP.Continuous_Service_Order_Appearance__c+' $$ '+objDP.DP_Continuous_Service_Order_Appearance_F__c+',');
            }
            if(objDP.Is_Cross_Reference__c != objDP.DP_Is_Cross_Reference_F__c) {
                bolError = true;
                objUpdtDP.Is_Cross_Reference__c = objUpdtDP.DP_Is_Cross_Reference_F__c;
                lstError.add('Is_Cross_Reference__c $$ '+objDP.Is_Cross_Reference__c +' $$ '+objDP.DP_Is_Cross_Reference_F__c+',');
            }
            if(objDP.IBUN_Website_One__c != objDP.DP_IBUN_Website_One_F__c) {
                bolError = true;
                objUpdtDP.IBUN_Website_One__c = objUpdtDP.DP_IBUN_Website_One_F__c;
                lstError.add('IBUN_Website_One__c $$ '+objDP.IBUN_Website_One__c +' $$ '+objDP.DP_IBUN_Website_One_F__c+',');
            }
            if(objDP.IBUN_Website_Two__c != objDP.DP_IBUN_Website_Two_F__c) {
                bolError = true;
                objUpdtDP.IBUN_Website_Two__c = objUpdtDP.DP_IBUN_Website_Two_F__c;
                lstError.add('IBUN_Website_Two__c $$ '+objDP.IBUN_Website_Two__c +' $$ '+objDP.DP_IBUN_Website_Two_F__c+',');
            }
            if(objDP.Scoped_Caption_Header__c != objDP.DP_Scoped_Caption_Header_F__c) {
                bolError = true;
                objUpdtDP.Scoped_Caption_Header__c = objUpdtDP.DP_Scoped_Caption_Header_F__c;
                lstError.add('Scoped_Caption_Header__c $$ '+objDP.Scoped_Caption_Header__c +' $$ '+objDP.DP_Scoped_Caption_Header_F__c+',');
            }
            if(objDP.Year__c != objDP.DP_Year_F__c) {
                bolError = true;
                objUpdtDP.Year__c = objUpdtDP.DP_Year_F__c;
                lstError.add('Year__c $$ '+objDP.Year__c +' $$ '+objDP.DP_Year_F__c+',');
            }
            if(objDP.Directory_Edition_Code__c != objDP.DP_Directory_Edition_Code_F__c) {
                bolError = true;
                objUpdtDP.Directory_Edition_Code__c = objUpdtDP.DP_Directory_Edition_Code_F__c;
                lstError.add('Directory_Edition_Code__c $$ '+objDP.Directory_Edition_Code__c +' $$ '+objDP.DP_Directory_Edition_Code_F__c+',');
            }
            if(objDP.Directory_Edition_Name__c != objDP.DP_Directory_Edition_Name_F__c) {
                bolError = true;
                objUpdtDP.Directory_Edition_Name__c = objUpdtDP.DP_Directory_Edition_Name_F__c;
                lstError.add('Directory_Edition_Name__c $$ '+objDP.Directory_Edition_Name__c +' $$ '+objDP.DP_Directory_Edition_Name_F__c+',');
            }
            if(objDP.Banner__c != objDP.DP_Banner_F__c) {
                bolError = true;
                objUpdtDP.Banner__c = objUpdtDP.DP_Banner_F__c;
                lstError.add('Banner__c $$ '+objDP.Banner__c +' $$ '+objDP.DP_Banner_F__c+',');
            }
            if(objDP.Billboard__c != objDP.DP_Billboard_F__c) {
                bolError = true;
                objUpdtDP.Billboard__c = objUpdtDP.DP_Billboard_F__c;
                lstError.add('Billboard__c $$ '+objDP.Billboard__c +' $$ '+objDP.DP_Billboard_F__c+',');
            }
            if(objDP.Directory_Heading_Code__c != objDP.DP_Directory_Heading_Code_F__c) {
                bolError = true;
                objUpdtDP.Directory_Heading_Code__c = objUpdtDP.DP_Directory_Heading_Code_F__c;
                lstError.add('Directory_Heading_Code__c $$ '+objDP.Directory_Heading_Code__c +' $$ '+objDP.DP_Directory_Heading_Code_F__c+',');
            }
            if(objDP.Directory_Heading__c != objDP.DP_Directory_Heading_F__c) {
                bolError = true;
                objUpdtDP.Directory_Heading__c = objUpdtDP.DP_Directory_Heading_F__c;
                lstError.add('Directory_Heading__c $$ '+objDP.Directory_Heading__c +' $$ '+objDP.DP_Directory_Heading_F__c+',');
            }
            if(objDP.IBUN_Type__c != objDP.DP_IBUN_Type_F__c) {
                bolError = true;
                objUpdtDP.IBUN_Type__c = objUpdtDP.DP_IBUN_Type_F__c;
                lstError.add('IBUN_Type__c $$ '+objDP.IBUN_Type__c +' $$ '+objDP.DP_IBUN_Type_F__c+',');
            }
            if(objDP.Incorrect_Setup_Excluded_from_XML__c != objDP.DP_Incorrect_Setup_Excluded_from_XML_F__c) {
                bolError = true;
                objUpdtDP.Incorrect_Setup_Excluded_from_XML__c = objUpdtDP.DP_Incorrect_Setup_Excluded_from_XML_F__c;
                lstError.add('Incorrect_Setup_Excluded_from_XML__c $$ '+objDP.Incorrect_Setup_Excluded_from_XML__c +' $$ '+objDP.DP_Incorrect_Setup_Excluded_from_XML_F__c+',');
            }
            if(objDP.Internet_Bundle_Ad__c != objDP.DP_Internet_Bundle_Ad_F__c) {
                bolError = true;
                objUpdtDP.Internet_Bundle_Ad__c = objUpdtDP.DP_Internet_Bundle_Ad_F__c;
                lstError.add('Internet_Bundle_Ad__c $$ '+objDP.Internet_Bundle_Ad__c +' $$ '+objDP.DP_Internet_Bundle_Ad_F__c+',');
            }
            if(objDP.UDAC__c != objDP.DP_UDAC_F__c) {
                bolError = true;
                objUpdtDP.UDAC__c = objUpdtDP.DP_UDAC_F__c;
                lstError.add('UDAC__c $$ '+objDP.UDAC__c +' $$ '+objDP.DP_UDAC_F__c+',');
            }
            if(objDP.Scoped_Caption_Member__c != objDP.DP_Scoped_Caption_Member_F__c) {
                bolError = true;
                objUpdtDP.Scoped_Caption_Member__c = objUpdtDP.DP_Scoped_Caption_Member_F__c;
                lstError.add('Scoped_Caption_Member__c $$ '+objDP.Scoped_Caption_Member__c +' $$ '+objDP.DP_Scoped_Caption_Member_F__c+',');
            }
            if(objDP.Section_Heading_Mapping_ID__c != objDP.DP_Section_Heading_Mapping_ID_F__c) {
                bolError = true;
                objUpdtDP.Section_Heading_Mapping_ID__c = objUpdtDP.DP_Section_Heading_Mapping_ID_F__c;
                lstError.add('Section_Heading_Mapping_ID__c $$ '+objDP.Section_Heading_Mapping_ID__c +' $$ '+objDP.DP_Section_Heading_Mapping_ID_F__c+',');
            }
            if(objDP.Section_Type__c != objDP.DP_Section_Type_F__c) {
                bolError = true;
                objUpdtDP.Section_Type__c = objUpdtDP.DP_Section_Type_F__c;
                lstError.add('Section_Type__c $$ '+objDP.Section_Type__c +' $$ '+objDP.DP_Section_Type_F__c+',');
            }
            if(objDP.Trademark_Finding_Line__c != objDP.DP_Trademark_Finding_Line_F__c) {
                bolError = true;
                objUpdtDP.Trademark_Finding_Line__c = objUpdtDP.DP_Trademark_Finding_Line_F__c;
                lstError.add('Trademark_Finding_Line__c $$ '+objDP.Trademark_Finding_Line__c +' $$ '+objDP.DP_Trademark_Finding_Line_F__c+',');
            }
            if(objDP.Type__c != objDP.DP_Type_F__c) {
                bolError = true;
                objUpdtDP.Type__c = objUpdtDP.DP_Type_F__c;
                lstError.add('Type__c $$ '+objDP.Type__c +' $$ '+objDP.DP_Type_F__c+',');
            }
            
            if(bolError) {
                strErrorMessage += idDP+' $$ ';
                for(String strMsg : lstError) {
                    strErrorMessage += strMsg; 
                }
                strErrorMessage += '---*---'; 
                lstUpdt.add(objUpdtDP);
            }
        }
        if(lstUpdt != null && lstUpdt.size() > 0) {
        	update lstUpdt;
        }
        if(String.isNotBlank(strErrorMessage)) {
            futureCreateErrorLog.createErrorRecordBatch('Mismatching values for formula fields and new fields', strErrorMessage, 'Directory Pagination Field Differences');
        }
    }
    
    global void finish(Database.BatchableContext bc){
        AsyncApexJob a = AsyncApexJobSOQLMethods.getBatchDetails(BC.getJobId());
        String[] toAddresses = new String[] {a.CreatedBy.Email,'Mythreyee.Kumar@theberrycompany.com','m'};
        CommonEmailUtils.sendHTMLEmail(toAddresses, 'Batch Process Status : ' + a.Status, 'The Apex batch job picked '+a.TotalJobItems+' batches and processed ' + a.JobItemsProcessed +
            ' batches with '+ a.NumberOfErrors + ' failures.');
    }
}