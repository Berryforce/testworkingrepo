public class ServiceOrderstageHandlerController{
    public static void onBeforeInsert(list<service_order_stage__c> objSOSLst) {
        SOFalloutLogic(objSOSLst,null);
    }
    
    public static void onBeforeUpdate(list<service_order_stage__c> objSOSLst, map<ID, service_order_stage__c> mapOldSO) {
        SOFalloutLogic(objSOSLst, mapOldSO);
    }
    
    public static void SOFalloutLogic(list<service_order_stage__c> objSOSLst,map<ID,service_order_stage__c> mapOldSO) {
        set<Id> setDirsectionId = new set<Id>();
        set<Id> setDirHeadingId = new set<Id>();
        set<Id> setDirId = new set<Id>();
        map<string, list<service_order_stage__c>> mapSOS = new map<string, list<service_order_stage__c>>();
        map<Id, Directory_section__c> mapDirSec = new map<Id,Directory_section__c>();
        map<Id,list<Service_Order_Stage__c>> mapDirSoRecords = new map<Id,list<Service_Order_Stage__c>>();
        map<Id,Directory_Edition__c> mapDirNIDirEdition = new map<Id,Directory_Edition__c>();
        map<Id,Directory__c> mapDirDates = new map<Id,Directory__c>();
        set<Id> setDirMapId = new set<Id>();
        set<Id> setTelcoId = new set<Id>();
        set<Id> setCLECId = new set<Id>();
        
        Schema.DescribeSObjectResult RService = Service_Order_Stage__c.SObjectType.getDescribe(); 
            List<Schema.RecordTypeInfo> RTService = RService.getRecordTypeInfos(); 
            
            map<String,Id> mapServiceRecordTypes = new map<String,Id>(); 
            for(Schema.RecordTypeInfo R : RTService){ 
                    mapServiceRecordTypes.put(R.getName(),R.getRecordTypeId()); 
            } 

        for(service_order_stage__c objSOS : objSOSLst) {
            if(mapOldSO != null) {
                service_order_stage__c objSOOld = mapOldSO.get(objSOS.Id);
                if(objSOOld.Directory__c != null && string.isBlank(objSOS.Directory__c)){
                    objSOS.Fallout__c = true;
                    objSOS.Fallout_reason__c = 'Directory Not Identified';
                }
                else if(objSOOld.Directory_Section__c != null && string.isBlank(objSOS.Directory_Section__c)){
                    objSOS.Fallout__c = true;
                    objSOS.Fallout_reason__c = 'Directory Section is Not identified';
                }
                else if(objSOOld.Directory_Heading__c != null && string.isBlank(objSOS.Directory_Heading__c) && objsos.Do_Not_Create_YP__c == false){
                    objSOS.Fallout__c = true;
                    objSOS.Fallout_reason__c = 'Directory Heading is Not identified';
                }
                if(objSOS.Directory_section__c != null && objSOS.Directory_Heading__c != null && objsos.Do_Not_Create_YP__c == false){
                    if(objSOOld.Directory_section__c != objSOS.Directory_section__c || objSOOld.Directory_Heading__c != objSOS.Directory_Heading__c){
                        setDirsectionId.add(objSOS.Directory_section__c);
                        setDirHeadingId.add(objSOS.Directory_Heading__c);
                    }
                }
                if(objSOS.Action__c == 'Disconnect' && objSOOld.OUT_PHONE_NUMBER__c != null && objSOS.OUT_PHONE_NUMBER__c == null){
                    objSOS.Fallout__c = true;
                    objSOS.Fallout_Reason__c = 'Phone not identified for Disconnect';
                }
                if(objSOS.Action__c == 'Change' && objSOOld.OUT_LAST_NAME_BUSINESS_NAME__c != null && objSOS.OUT_LAST_NAME_BUSINESS_NAME__c == null){
                    objSOS.Fallout__c = true;
                    objSOS.Fallout_Reason__c = 'Out Last Name Business Name not Identified';
                }
                if(String.isNotBlank(objSOS.Header_text__c) && objSOS.Fallout_Resolved__c == false && objSOS.Fallout__c == false){
                    objSOS.Fallout__c = true;
                    objSOS.Fallout_Reason__c = 'Header Text';
                }
                if(objSOOld.Caption_member__c != objSOS.Caption_member__c){
                    if(objSOS.Caption_member__c == true){
                        if(string.isBlank(objSOS.Under_Caption__c)){
                            objSOS.Fallout__c = true;
                            objSOS.Fallout_Reason__c = 'Not Identified - Caption Member - Under Caption';
                        }
                        if(objSOS.Fallout_Resolved__c == false && objSOS.recordtypeId == mapServiceRecordTypes.get('Daily Service Orders')){
                            objSOS.Fallout__c = true;
                            objSOS.Fallout_Reason__c = 'Caption Member';
                            
                        }
                    }
                }
                if(objSOOld.Caption_Header__c != objSOS.Caption_Header__c){
                    if(objSOS.Caption_Header__c == true){
                        if(objSOS.Fallout_Resolved__c == false &&  objSOS.recordtypeId == mapServiceRecordTypes.get('Daily Service Orders')){
                            objSOS.Fallout__c = true;
                            objSOS.Fallout_Reason__c = 'Caption Header';
                            
                        }
                    }
                }
                if(objSOS.Telco_Provider__c != null && objSOOld.Telco_Provider__c != objSOS.Telco_Provider__c){
                     setTelcoId.add(objSOS.Telco_Provider__c);
                }
                else if(objSOS.CLEC_Provider__c != null && objSOOld.CLEC_Provider__c != objSOS.CLEC_Provider__c){
                    setTelcoId.add(objSOS.CLEC_Provider__c);
                }
            }
            else{
                if(string.isBlank(objSOS.Directory__c)){
                    objSOS.Fallout__c = true;
                    objSOS.Fallout_reason__c = 'Directory Not Identified';
                }
                else if(string.isBlank(objSOS.Directory_Section__c)){
                    objSOS.Fallout__c = true;
                    objSOS.Fallout_reason__c = 'Directory Section is Not identified';
                }
                else if(objsos.Do_Not_Create_YP__c == false && string.isBlank(objSOS.Directory_Heading__c)){
                    objSOS.Fallout__c = true;
                    objSOS.Fallout_reason__c = 'Directory Heading is Not identified';
                }
                if(objSOS.Directory_section__c != null && objSOS.Directory_Heading__c != null && objsos.Do_Not_Create_YP__c == false){
                    setDirsectionId.add(objSOS.Directory_section__c);
                    setDirHeadingId.add(objSOS.Directory_Heading__c);
                }
                if(objSOS.Action__c == 'Disconnect' && objSOS.OUT_PHONE_NUMBER__c == null){
                    objSOS.Fallout__c = true;
                    objSOS.Fallout_Reason__c = 'Phone not identified for Disconnect';
                }
                if(objSOS.Action__c == 'Change' && objSOS.OUT_LAST_NAME_BUSINESS_NAME__c == null){
                    objSOS.Fallout__c = true;
                    objSOS.Fallout_Reason__c = 'Out Last Name Business Name not Identified';
                }
                if(String.isNotBlank(objSOS.Header_text__c) && objSOS.Fallout_Resolved__c == false && objSOS.Fallout__c == false){
                    objSOS.Fallout__c = true;
                    objSOS.Fallout_Reason__c = 'Header Text';
                }
                if(objSOS.Caption_member__c == true){
                    if(string.isBlank(objSOS.Under_Caption__c)){
                        objSOS.Fallout__c = true;
                        objSOS.Fallout_Reason__c = 'Not Identified - Caption Member - Under Caption';
                    }
                    if(objSOS.Fallout_Resolved__c == false &&  objSOS.recordtypeId == mapServiceRecordTypes.get('Daily Service Orders')){
                        objSOS.Fallout__c = true;
                        objSOS.Fallout_Reason__c = 'Caption Member';
                        
                    }
                }
                if(objSOS.Caption_Header__c == true){
                    if(objSOS.Fallout_Resolved__c == false &&  objSOS.recordtypeId == mapServiceRecordTypes.get('Daily Service Orders')){
                        objSOS.Fallout__c = true;
                        objSOS.Fallout_Reason__c = 'Caption Header';
                        
                    }
                }
                if(objSOS.Directory__c != null && objSOS.recordtypeId == mapServiceRecordTypes.get('Daily Service Orders') && objSOS.Effective_date__c != null){
                    setDirId.add(objSOS.Directory__c);
                }
                if(objSOS.Telco_Provider__c != null) {
                	setTelcoId.add(objSOS.Telco_Provider__c);
                }
                else if(objSOS.CLEC_Provider__c != null){
                    setTelcoId.add(objSOS.CLEC_Provider__c);
                    setCLECId.add(objSOS.CLEC_Provider__c);
                }
                if(string.isNotblank(objSOS.Name)){
                	objSOS.Name = EncodingUtil.urlDecode(objSOS.Name, 'UTF-8');
                }
                if(string.isNotblank(objSOS.SOS_LastName_BusinessName__c)){
                	objSOS.SOS_LastName_BusinessName__c = EncodingUtil.urlDecode(objSOS.SOS_LastName_BusinessName__c, 'UTF-8');
                }
            }
            if(objSOS.Directory__c != null) {
            	setDirMapId.add(objSOS.Directory__c);
        	}
        }
        if(setDirHeadingId.size() > 0 && setDirMapId.size()>0){
            DirSectionHeadingmapping(setDirHeadingId,setDirMapId,objSOSLst);
            //DirSectionHeadingmapping(setDirSecDirHeaStr,objSOSLst);
        }
        if(setDirMapId.size()>0 && setTelcoId.size()>0){
            DirTelcomappinng(setDirMapId,setTelcoId,objSOSLst);
        }
        if(setCLECId.size()> 0) {
        	SOCLECFallout(setCLECId,objSOSLst);
        }
    }
    
    public static void DirSectionHeadingmapping(set<Id> setDirHeadingId,set<Id> setDirId,list<service_order_stage__c> objSOSLst) {
        map<string,Section_Heading_Mapping__c> mapSHM = new map<string,Section_Heading_Mapping__c>();
        list<service_order_stage__c> SOSLstUpdate = new list<service_order_stage__c>();
        map<Id, Id> mapYPDirectorysection = new map<Id, Id>();
        set<Id> setYPDirsectionId = new set<Id>();
        list<Directory_section__c> lstYPDS = new list<Directory_section__c>();
        if(setDirId.size()>0) {
        	//map<Id, Directory__c> mapDirectory = new map<Id, Directory__c>([Select Id, (Select Id, Name, Section_Page_Type__c, Directory__c From Directory_Sections__r where Section_Page_Type__c='YP') From Directory__c where ID IN:setDirId]);
          	lstYPDS = [Select Id, Name, Section_Page_Type__c, Directory__c From Directory_Section__c where Section_Page_Type__c='YP' AND Directory__c IN :setDirId];
          	if(lstYPDS.size()> 0){
	            for(Directory_section__c iterator : lstYPDS){
	                setYPDirsectionId.add(iterator.Id);
	                mapYPDirectorysection.put(iterator.Directory__c,iterator.Id);
	            }
          	}
        }
        if(setDirHeadingId.size()>0 && setYPDirsectionId.size()> 0){
          list<Section_Heading_Mapping__c> SHMLst = SectionHeadingMappingSOQLMethods.getSecHeadMapByDirSecDirHeadIds(setYPDirsectionId,setDirHeadingId);
          //list<Section_Heading_Mapping__c> SHMLst = SectionHeadingMappingSOQLMethods.getSecHeadMapByDSDHCombo(setDirSecDirHeaStr);
          for(Section_Heading_Mapping__c objSHM :SHMLst) {
              if(!mapSHM.containsKey(objSHM.strBundleAppearenceCombo__c)) {
              	system.debug('string combination value1'+objSHM.strBundleAppearenceCombo__c);
                  mapSHM.put(objSHM.strBundleAppearenceCombo__c,objSHM);
              }
          }
          system.debug('string combination value2'+mapSHM);
          if(objSOSLst.size()> 0){
              for(service_order_stage__c objSO : objSOSLst){
	              if(mapYPDirectorysection.get(objSO.Directory__c) != null && objSO.Directory_Heading__c != null) {
		              string objSODSHStr = String.valueOf(mapYPDirectorysection.get(objSO.Directory__c)).substring(0, 15) + String.valueOf(objSO.Directory_Heading__c).substring(0, 15);   
		              system.debug('string combination value3'+objSODSHStr);
		              if(mapSHM.get(objSODSHStr) == null){
		                  objSO.Fallout__c = true;
		                  objSO.Fallout_Reason__c = 'Either Directory Section/Heading is not matching';
		              }
	              }
              }
          }
      }
    }
    
    public static void DirTelcomappinng(set<Id> setDirTelId,set<Id> setTelcoId,list<service_order_stage__c> objSOSLst){
        list<directory_mapping__c> lstDirmapping = new list<directory_mapping__c>();
        map<string,directory_mapping__c> mapstrDM = new map<string,directory_mapping__c>();
    
        lstDirmapping = [Select Id,Directory__c,Telco__c from directory_mapping__c where directory__c IN : setDirTelId AND Telco__c IN : setTelcoId];
        for(directory_mapping__c objDirm : lstDirmapping){
            string strDir = objDirm.Directory__c+''+objDirm.Telco__c;
            if(!mapstrDM.containskey(strDir)){
                mapstrDM.put(strDir,objDirm);
            }
        }
        for(service_order_stage__c iteratorSO : objSOSLst){
           string strSOTelco;
           if(iteratorSO.Telco_provider__c != null) {
                strSOTelco = iteratorSO.directory__c+''+iteratorSO.Telco_provider__c;
                if(iteratorSO.Telco_provider__c != iteratorSO.SOS_DirTelcoProvider__c && mapstrDM.get(strSOTelco) == null) {
                	iteratorSO.Fallout__c = true;
                	iteratorSO.Fallout_Reason__c = 'Telco or CLEC Provider not found in Directory Telco Provider or Directory Mapping - Update Directory Reference Data';
                }
	       }
           else if(iteratorSO.CLEC_Provider__c != null){
                strSOTelco = iteratorSO.directory__c+''+iteratorSO.CLEC_Provider__c;
                if(iteratorSO.CLEC_Provider__c != iteratorSO.SOS_DirTelcoProvider__c && mapstrDM.get(strSOTelco) == null) {
                	iteratorSO.Fallout__c = true;
                	iteratorSO.Fallout_Reason__c = 'Telco or CLEC Provider not found in Directory Telco Provider or Directory Mapping - Update Directory Reference Data';
                }
           }
        }
     
    }
    public static void SOCLECFallout(set<Id> setCLECId,list<service_order_stage__c> objSOSLst) {
    	if(setCLECId.size()> 0) {
    		map<Id,Telco__c> mapTelco = TelcoSOQLMethods.getTelco(setCLECId);
    		for(service_order_stage__c iteratorSOS :objSOSLst) {
    			if(iteratorSOS.CLEC_Provider__c != null && mapTelco.get(iteratorSOS.CLEC_Provider__c) == null) {
    				iteratorSOS.Fallout__c = true;
                	iteratorSOS.Fallout_Reason__c = 'Telco value is not of CLEC Provider record type';
    			}
    		}
    	}
    }
}