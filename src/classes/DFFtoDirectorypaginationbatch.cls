global class DFFtoDirectorypaginationbatch implements Database.Batchable<sObject>{
    global Database.QueryLocator start(Database.BatchableContext bc) {        
        
        String soql = 'Select Id,Order_Line_Item__c,Digital_Product_Requirement__c from Directory_Pagination__c where  Digital_Product_Requirement__c=null and Order_Line_Item__c != null';
        system.debug('DP QUERY'+soql);
        return Database.getQueryLocator(soql);
    }
   
    global void execute(Database.BatchableContext bc, List<Directory_Pagination__c > DirPagLst) {
        set<Id> setOliId = new set<Id>();
        map<Id, Digital_Product_Requirement__c> mapOLIIdDFF = new map<Id, Digital_Product_Requirement__c>();
        list<Directory_Pagination__c> dpUpdateLst = new list<Directory_Pagination__c>();
        for(Directory_Pagination__c iteratorDP :DirPagLst){
            if(iteratorDP.Order_Line_Item__c != null){
                setOliId.add(iteratorDP.Order_Line_Item__c);
            }
        }
        if(setOliId.size()> 0){
            list<Digital_Product_Requirement__c> lstDFF = [Select Id,OrderLineItemID__c from Digital_Product_Requirement__c where OrderLineItemID__c IN : setOliId];
            if(lstDFF.size()> 0){
                for(Digital_Product_Requirement__c iteratorDFF :lstDFF){
                    mapOLIIdDFF.put(iteratorDFF.OrderLineItemID__c,iteratorDFF );
                }
            }
        }
        for(Directory_Pagination__c iteratorDP :DirPagLst){
            if(iteratorDP.Order_Line_Item__c != null && mapOLIIdDFF.get(iteratorDP.Order_Line_Item__c) != null){
                iteratorDP.Digital_Product_Requirement__c = mapOLIIdDFF.get(iteratorDP.Order_Line_Item__c).Id;
                dpUpdateLst.add(iteratorDP);
            }
        }
        if(dpUpdateLst.size()> 0){
            update dpUpdateLst;
        }
      
    }
   
    global void finish(Database.BatchableContext bc)
    {
    }
}