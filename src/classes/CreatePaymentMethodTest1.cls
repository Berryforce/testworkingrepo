@isTest(SeeAllData=true)
public class CreatePaymentMethodTest1 
{
    public static testMethod void testCreatePaymentMethod () 
    {
    
    /*Canvass__c canv=TestMethodsUtility.createCanvass();
    if(canv.id == Null){
        insert canv;
    }
    
    Account acc=TestMethodsUtility.createAccount(canv);
    insert acc;
    Contact con=TestMethodsUtility.createContact(acc);
    insert con;*/
    
    Contact con = TestMethodsUtility.createContact(true);
    
    Test.StartTest();
    PageReference pageRef = Page.createPaymentMethod;
    Test.setCurrentPage(pageRef);
    pymt__Payment_Method__c pymtMethod=new pymt__Payment_Method__c(pymt__Card_Type__c='Credit Card');
    ApexPages.StandardController bgtctrl = new ApexPages.standardController(pymtMethod);
    ApexPages.currentPage().getParameters().put('conid',con.id);
    ApexPages.currentPage().getParameters().put('retURL','/home/home.jsp');
    createPaymentMethod cpm=new createPaymentMethod(bgtctrl );
    cpm.getexpYearOptions();
    cpm.getexpYear();
    cpm.setexpYear('2013');
    cpm.getexpMonthOptions();
    cpm.getexpMonth();
    cpm.setexpMonth('01');
    cpm.isValid('Credit Card','2341 1234 3452 9800');
    cpm.isValid('Credit Card','QWER 1234 3452 9800');
    cpm.isValid('Credit Card','2341 1234 3452');
    cpm.cardNumber='2341 1234 3452';
    cpm.makeDefaultMethod=true;
    cpm.addCreditCard();
    cpm.cardNumber='2341 1234 3452 9800';
    cpm.makeDefaultMethod=true;
    cpm.addCreditCard();
    cpm.cardNumber=null;
    cpm.makeDefaultMethod=true;
    cpm.addCreditCard();
    cpm.cardNumber='2341 1234 3452 9800';
    cpm.makeDefaultMethod=false;
    cpm.addCreditCard();
    cpm.cancelFromPage();
    
    Test.StopTest();
    }
}