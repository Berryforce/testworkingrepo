@isTest(SeeAllData=True)
public class NationalChildLinesSOQLMethodsTest {
	@isTest
	static void natchilincls() {	
		set<String> setOpppLineId = new set<string>();
		set<Id> setOrLIId = new set<id>();
		set<String> transactionIds = new set<string>();
	
		//populate the data
		Account newAccount = TestMethodsUtility.createAccount('customer');
		Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Pricebook2 newPriceBook = [Select Id from Pricebook2 where isStandard=true limit 1];
        Product2 newProduct = TestMethodsUtility.createproduct();
        PricebookEntry newPriceBookEntry = [Select Id, UseStandardPrice, UnitPrice, Product2Id, Pricebook2Id, IsActive from PricebookEntry where Product2Id = :newProduct.Id limit 1];
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        insert newOpportunity;
        //setOppId.add(newOpportunity.Id);
        OpportunityLineItem newOLI = TestMethodsUtility.generateOpportunityLineItem();
        newOLI.PricebookEntryId = newPriceBookEntry.Id;
        newOLI.OpportunityId = newOpportunity.Id;
        newOLI.Transaction_ID__c = 'Test1';
        insert newOLI;
       // setTransId.add('Test1');
        setOpppLineId.add(newOLI.id);
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
        Order_Line_Items__c newOrLI = TestMethodsUtility.createOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet);
        setOrLIId.add(newOrLI.Id);
        National_Child_Lines__c ncl = TestMethodsUtility.createnationalreferenceobject();
        transactionIds.add(ncl.Transaction_ID__c);
		system.assertnotequals(null,NationalChildLinesSOQLMethods.getNCLWithOutOmitByOrderLineItemIds(setOrLIId));
	}
}