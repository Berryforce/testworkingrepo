@isTest (SeeAllData=True)
Public class TestPotentialDupeListingLookup{

  Static testMethod void pdCreator(){
    CRMfusionDBR101__Scenario__c scen = new CRMfusionDBR101__Scenario__c();
    scen.Name = 'Test Scenario';
    scen.CRMfusionDBR101__Scenario_Type__c = 'Listing';
    scen.CRMfusionDBR101__Match_on_Insert_Action__c = 'Report Duplicate';
    scen.CRMfusionDBR101__Match_on_Update_Action__c = 'Report Duplicate';
    scen.CRMfusionDBR101__Create_Task__c = 'No';
    scen.CRMfusionDBR101__Rebuild_Needed__c = false;
    insert scen;
  
    CRMfusionDBR101__Potential_Duplicate__c pd = new CRMfusionDBR101__Potential_Duplicate__c();
    pd.CRMfusionDBR101__Scenario_Type__c = 'Listing__c';
    pd.CRMfusionDBR101__Key__c = '1111122222';
    pd.CRMfusionDBR101__Scenario__c = scen.Id;
    insert pd;
  }
}