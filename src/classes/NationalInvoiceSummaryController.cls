public with sharing class NationalInvoiceSummaryController {
    private Id accountCMRId;
    public list<ISS__c> lstISS {get;set;}
    public NationalInvoiceSummaryController(Apexpages.Standardcontroller controller) {
        accountCMRId = Apexpages.currentPage().getParameters().get('Id');
        lstISS = new list<ISS__c>();
    }
    
    public Pagereference onLoadNew() {
    	lstISS = ISSSOQLMethods.getISSByCMRId(new set<ID>{accountCMRId});
    	return null; 
    }
}