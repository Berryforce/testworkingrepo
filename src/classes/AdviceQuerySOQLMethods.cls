public with sharing class AdviceQuerySOQLMethods {
   
    public static list<Advice_Query__c> getAdviceQueryByNSLI(List<National_Staging_Line_Item__c> lstNSLI){
        return [select National_Staging_Line_Item__c , Frequent_Comments_Questions__c, Advice_Query_Options__c, Free_text__c, Send_to_Elite__c from Advice_Query__c where National_Staging_Line_Item__c in :lstNSLI];
    }
}