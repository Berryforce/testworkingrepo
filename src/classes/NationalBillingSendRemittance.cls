public class NationalBillingSendRemittance {
    public Id batchId {get;set;}
    public Boolean polleraction {get;set;}
    public Boolean isBatchProcessed {get;set;}
    public Boolean createRemFFBool {get;set;}
    public String batchStatus {get;set;}
    map<Id, ISS__c> mapISS;
    public String strPubCodeSendRemittance {get;set;}
    public String strEditionCodeSendRemittance {get;set;}
    list<ISS_Line_Item__c> ISSLineItemList;
    map<Id, list<ISS_Line_Item__c>> mapISSIdISSLI;
    public Integer intISSCount {get;set;} 
    public boolean bolFetchDir {get;set;}
    public boolean bolCreateFF {get;set;}
    map<Id, list<c2g__codaInvoiceLineItem__c>> mapFFInvoiceLine;
    list<c2g__codaInvoice__c> lstFFInvoice = new list<c2g__codaInvoice__c>();
    public Decimal FFAmt {get;set;}
    public Decimal FFTaxAmt {get;set;}
    public Decimal FFNetAmt {get;set;}
    public Decimal FFCommission {get;set;}
    public Decimal NetAmt {get;set;}
    public boolean chkRemittanceComplete{get;set;}
    public boolean chkFFInvBalanced {get;set;}
    public boolean boolPopup {get;set;}
    public boolean sendRemComplBool {get;set;}
    public boolean sendRemBool {get;set;}
    public ISS__c objISSPDF {get;set;}
    List<National_Billing_Status__c> listNatBill = new List<National_Billing_Status__c>();
    public ID FFInvValreportId {get;set;}
    public ID ISSToCMRValreportId {get;set;}
    public List<ISS__c> listISS {get;set;}
	National_Billing_Status__c natBilling = new National_Billing_Status__c();
	public Boolean FFPostBool {get;set;}
	public Boolean ISSRemTranBool {get;set;}
    public ID ISSEliteReportId {get;set;}
    public ID ISSSACReportId {get;set;}
    Map<Id, National_Tax__c> mapISSLINatTax = new Map<Id, National_Tax__c>();
    
    public NationalBillingSendRemittance() {
    	FFPostBool = false;
        intISSCount = 0;
        pollerAction = false;
        isBatchProcessed = false;   
        boolPopup = false;      
    	listISS = new List<ISS__c>();       
    	sendRemComplBool = false;
    	ISSRemTranBool = false;
    	FFInvValreportId = National_Billing_Reports__c.getInstance('FFInvValReport').Report_Id__c;
        ISSToCMRValreportId = National_Billing_Reports__c.getInstance('ISSCMRReport').Report_Id__c;
    	ISSEliteReportId = National_Billing_Reports__c.getInstance('ISSEliteReportId').Report_Id__c;
        ISSSACReportId = National_Billing_Reports__c.getInstance('ISSSACReportId').Report_Id__c;
    	createRemFFBool = true;
    	sendRemBool = false;
    }
    
    public void fetchISSForRemittance() {
    	createRemFFBool = true;
    	fetchStatusTable();
        bolCreateFF = false;
        isBatchProcessed = false;
        chkRemittanceComplete = false;
        chkFFInvBalanced = false;
    	sendRemBool = false;
        listISS = new List<ISS__c>();
        if(listNatBill.size() == 0) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.Error, 'No record found in Status Table'));
        	bolCreateFF = false;
        	intISSCount = 0;
        } else {   
	        if(natBilling.RSAP_Remittance_Sheet_Created__c) {
	        	ISSLineItemList = ISSLineItemSOQLMethods.getISSLIForSendRemittanceWithRemSheet(strPubCodeSendRemittance , strEditionCodeSendRemittance );
	        	bolCreateFF = true;
	        	set<Id> ISSIds = new set<Id>();
	        	if(ISSLineItemList.size() > 0) {
		            for(ISS_Line_Item__c objISSLI : ISSLineItemList) {
		                if(objISSLI.ISS__c != null) {
		                	ISSIds.add(objISSLI.ISS__c);
		                }
		            }
	        		lstFFInvoice = SalesInvoiceSOQLMethods.getSalesInvoiceByISSIds(ISSIds);
	        		calculateTotal(lstFFInvoice);
	            }
	            createRemFFBool = true;
	        } else {
	        	ISSLineItemList = ISSLineItemSOQLMethods.getISSLIForSendRemittanceWithoutRemSheet(strPubCodeSendRemittance , strEditionCodeSendRemittance );
	        	createRemFFBool = false;
	        }
	        mapISS = new map<Id, ISS__c>();
	        
	        for(ISS_Line_Item__c objISSLI : ISSLineItemList) {
	            mapISS.put(objISSLI.ISS__c, objISSLI.ISS__r);
	        }
	        
	        if(mapISS.size() > 0 && natBilling.RSAP_Remittance_Sheet_Created__c) {
    			listISS = mapISS.values();
    		}
	        intISSCount = mapISS.size();
	        if(intISSCount == 0 && !createRemFFBool) {
	        	createRemFFBool = true;
	        }
	        bolFetchDir = true;
	        assignValuesFromNatBill();
        }
    }
    
    public void generateRemitPDFandFF() {
        ISSLineItemList = ISSLineItemSOQLMethods.getISSLIForSendRemittanceWithoutRemSheet(strPubCodeSendRemittance , strEditionCodeSendRemittance );
        mapISS = new map<Id, ISS__c>();
        mapISSIdISSLI = new map<Id, list<ISS_Line_Item__c>>();
        Map<Id, National_Tax__c> mapCMROrDirNatTax = new Map<Id, National_Tax__c>();
        
        if(ISSLineItemList.size() > 0) {
            for(ISS_Line_Item__c objISSLI : ISSLineItemList) {
                if(!mapISS.containsKey(objISSLI.ISS__c)) {
                    mapISS.put(objISSLI.ISS__c, objISSLI.ISS__r);
                    mapISSIdISSLI.put(objISSLI.ISS__c, new list<ISS_Line_Item__c> {objISSLI});
                }
                else {
                    mapISSIdISSLI.get(objISSLI.ISS__c).add(objISSLI);
                }
            }
                    
            lstFFInvoice = new list<c2g__codaInvoice__c>();
            bolCreateFF = true;
            mapFFInvoiceLine = new map<Id, list<c2g__codaInvoiceLineItem__c>>();
            list<Attachment> lstAttachment = new list<Attachment>();
            list<ISS__c> lstUpdateISS = new list<ISS__c>();
            
            if(mapISS != null && mapISS.size() > 0  || test.isRunningTest()==true) {
                
                set<String> dirCode = new set<String>();
                
                for(ISS_Line_Item__c ISSLineItem : ISSLineItemList) {
                    dirCode.add(ISSLineItem.Directory_Code__c);
                }
                Map<String, Id> dimensions1Map = new Map<String, Id>();
                dimensions1Map = DimensionsValues.dimension1Values(dirCode);
                
                if(mapISS.size() > 0) {
                	set<Id> dirIds = new set<Id>();
                	set<Id> CMRIds = new set<Id>();
                	
                		
            		for(ISS_Line_Item__c ISSLI : ISSLineItemList) {
						if(ISSLI.Directory__c != null) {
							dirIds.add(ISSLI.Directory__c);
						}   
						if(String.isNotBlank(ISSLI.ISS__r.CMR__c)) {
                			CMRIds.add(ISSLI.ISS__r.CMR__c);
                		}                   
			        }
                	
                	List<National_Tax__c> listNatTax = [SELECT Id, Tax_Code__c, Directory__c, CMR__c, Tax__c FROM National_Tax__c WHERE Directory__c IN: dirIds OR CMR__c IN: CMRIds];
                	
                	if(listNatTax.size() > 0) {
                		for(National_Tax__c NT : listNatTax) {
                			if(String.isNotBlank(NT.CMR__c)) {
                				mapCMROrDirNatTax.put(NT.CMR__c, NT);
                			}
                			if(String.isNotBlank(NT.Directory__c)) {
                				mapCMROrDirNatTax.put(NT.Directory__c, NT);
                			}
                		}
                	}
                	
            		for(ISS_Line_Item__c ISSLI : ISSLineItemList) {
						if(ISSLI.ISS__r.CMR__c != null) {
							if(mapISSLINatTax.containsKey(ISSLI.Id)) {
								if(mapCMROrDirNatTax.containsKey(ISSLI.ISS__r.CMR__c)) {
									if(mapCMROrDirNatTax.get(ISSLI.ISS__r.CMR__c).Tax__c > mapISSLINatTax.get(ISSLI.Id).Tax__c) {
										mapISSLINatTax.put(ISSLI.Id, mapCMROrDirNatTax.get(ISSLI.ISS__r.CMR__c));
									}
								}
							} else {
								if(mapCMROrDirNatTax.containsKey(ISSLI.ISS__r.CMR__c)) {
									mapISSLINatTax.put(ISSLI.Id, mapCMROrDirNatTax.get(ISSLI.ISS__r.CMR__c));
								}
							}
						}    
						if(ISSLI.Directory__c != null) {
							if(mapISSLINatTax.containsKey(ISSLI.Id)) {
								if(mapCMROrDirNatTax.containsKey(ISSLI.Directory__c)) {
									if(mapCMROrDirNatTax.get(ISSLI.Directory__c).Tax__c > mapISSLINatTax.get(ISSLI.Id).Tax__c) {
										mapISSLINatTax.put(ISSLI.Id, mapCMROrDirNatTax.get(ISSLI.Directory__c));
									}
								}
							} else {
								if(mapCMROrDirNatTax.containsKey(ISSLI.Directory__c)) {
									mapISSLINatTax.put(ISSLI.Id, mapCMROrDirNatTax.get(ISSLI.Directory__c));
								}
							}
						}                   
			        }
                	
                    for(ID issID : mapISS.keySet()) {
                        ISS__c objISS = mapISS.get(issID);
                        objISS.Remittance_Sheet_created__c = true;
                        lstUpdateISS.add(objISS);
                        Attachment objAttachment = generatePDF(objISS);
                        if(objAttachment != null) {
                            lstAttachment.add(objAttachment);
                        }
                        generateFFInvoice(objISS, mapISSIdISSLI.get(issID), CommonMessages.accountingCurrencyId, dimensions1Map);
                    }
                }
                
                if(lstAttachment.size() > 0 || test.isRunningTest()==true) {
                    if(!test.isRunningTest()){
                        insert lstAttachment;
                        insert lstFFInvoice;
                    }
                    list<c2g__codaInvoiceLineItem__c> insertSILI = new list<c2g__codaInvoiceLineItem__c>();
                    
                    for(c2g__codaInvoice__c objSI : lstFFInvoice) {
                        list<c2g__codaInvoiceLineItem__c> lstSILI = mapFFInvoiceLine.get(objSI.ISS_Number__c);
                        for(c2g__codaInvoiceLineItem__c objSILI : lstSILI) {
                            objSILI.c2g__Invoice__c = objSI.Id; 
                            if(!test.isRunningTest()){
                                insertSILI.add(objSILI);
                            }
                        }
                    }
                    if(!test.isRunningTest()) {
                        try {
                        	insert insertSILI;
                        } catch(Exception ex) {
	                        system.debug('**********  in catch an error occured');
	                        String error = ex.getMessage() + '. ';
	                        List<String> errorMessages = c2g.CODAExtensionContext.getErrorMessages();
	                        for( String errorMessage : errorMessages ) { 
	                          if( errorMessage != null ) error += '; ' + errorMessage; 
	                        }
	                        system.debug('**********  error is= '+error);
	                        throw new InvoiceGeneratorHelperException(error);
                    	}  
                    }
                }
            }
            
            List<c2g__codaInvoice__c> lstFFInvoiceInserted = SalesInvoiceSOQLMethods.getSalesInvoiceByInvoice(lstFFInvoice);
            calculateTotal(lstFFInvoiceInserted);
            update lstUpdateISS;
        }
        intISSCount = mapISS.size();
        chkRemittanceComplete = true;
        updateStatusTable();
        updateRemCreatedStatusTable();
    }
    
    @TestVisible private Attachment generatePDF(ISS__c objISS) {
        objISSPDF = objISS;
        String pdfcontent = '';
        if(objISS.Publication_Code__c != null) {
            if(objISS.Publication_Code__c.equals(CommonMessages.publicationCode)) {
            //if(true){
                pdfcontent = '<html><body><div style="border:5px solid black;padding-left:10%;padding-right:10%;height:700px"><div style="float:left"><img src="'+System.Label.NationalDocURLFor0921+'" width="520" height="400" /><hr style="color:green;margin-top:120px;height:5px;background-color:green"/><div><table><tr><td>&nbsp;</td></tr></table><table border="1" cellpadding="0" cellspacing="0" width="100%" style="font-size:10px" align="center"> <tr><td bgcolor="#CCE6FF">ISS Number:</td><td>'+objISS.Name+'</td><td bgcolor="#CCE6FF">CMR Number:</td><td>'+objISS.CMR_Number__c+'</td></tr><tr><td bgcolor="#CCE6FF">Invoice Date:</td><td>'+objISS.Invoice_Date__c.format()+'</td><td bgcolor="#CCE6FF">CMR Name:</td><td>'+objISS.CMR__r.Name+'</td></tr><tr><td bgcolor="#CCE6FF">Pub Code:</td><td>'+objISS.Publication_Code__c+'</td><td bgcolor="#CCE6FF">CMR Address:</td> <td>'+objISS.CMR_Address__c+'</td></tr><tr><td bgcolor="#CCE6FF">Publisher:</td><td>'+objISS.Publication_Company__r.Name+'</td><td bgcolor="#CCE6FF">City/State/Zip:</td><td>'+objISS.State_City_Zip__c+'</td></tr></table><br/><table><tr><td>&nbsp;</td></tr></table><table border="1" style="font-size:10px" cellpadding="0" cellspacing="0" width="70%" align="center"><tr><td colspan="2" style="text-align:center;" bgcolor="#CCE6FF"><b>Account Summary</b></td></tr><tr><td bgcolor="#CCE6FF" style="text-align:right">Gross Amount: &nbsp;</td><td style="text-align:center">$'+objISS.Total_Gross_Amount__c.setScale(2)+'</td></tr><tr><td bgcolor="#CCE6FF" style="text-align:right">Adjustments: &nbsp;</td><td style="text-align:center">$'+objISS.Total_ADJ_Amount__c.setScale(2)+'</td></tr><tr><td bgcolor="#CCE6FF" style="text-align:right">Commission: &nbsp;</td><td style="text-align:center">$'+objISS.TotalCommission__c.setScale(2)+'</td></tr><tr><td bgcolor="#CCE6FF" style="text-align:right">Tax: &nbsp;</td><td style="text-align:center">$'+objISS.Total_Tax__c.setScale(2)+'</td></tr><tr><td bgcolor="#CCE6FF" style="text-align:right">Net Amount Due: &nbsp;</td><td style="text-align:center">$'+objISS.NetAmount__c.setScale(2)+'</td></tr></table><br/><table><tr><td>&nbsp;</td></tr></table><table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-size:10px" align="center"><tr><td><span style="color:#0F048D;font-weight:bold;" align="left">PAYMENT DUE ON OR BEFORE:</span></td><td style="text-align:left">'+objISS.Payment_Date__c.format()+'</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td align="left" style="font-weight:bold;">Refer Questions To:</td><td align="left" style="font-weight:bold;">Ann Purcell 513-397-1223</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td></tr></table><br/><table border="1"><tr><td style="font-weight:bold;font-size:8px;">'+System.Label.Instructions1+'</td></tr> </table><br/><table border="0" cellpadding="0" style="font-size:10px" cellspacing="0" width="100%" align="center"><tr><td colspan="4" class="text" style="text-align:center"><span style="color:#0F048D;font-weight:bold;">Please detach and return with payment.</span></td></tr><tr><td colspan="4" style="text-align:center">---------------------------------------------------------------------------------------------------------------------</td></tr><tr><td><span style="color:#0F048D" align="left">ISS Number:</span></td><td align="left"><u>'+addTrailingSpace(objISS.Name)+'</u></td><td colspan="2"></td></tr><tr><td><span style="color:#0F048D" align="left">CMR Number:</span></td><td align="left" style="border-bottom:2px solid #005000;"><u>'+addTrailingSpace(objISS.CMR_Number__c)+'</u></td><td colspan="2"><span style="color:#0F048D" align="right">Amount Enclosed: &nbsp;______________</span></td></tr><tr><td><span style="color:#0F048D" align="left">PUB Code:</span></td><td colspan="3" align="left"><u>'+addTrailingSpace(objISS.Publication_Code__c)+'</u></td></tr><tr><td><span style="color:#0F048D" align="left">Net Amount Due:</span></td><td align="left"><table border-bottom="1px"><tr><td><u>'+addTrailingSpace(+'$'+objISS.NetAmount__c.setScale(2))+'</u></td></tr></table></td><td colspan="2"></td></tr></table><table><tr><td>&nbsp;</td></tr></table><table border="0"><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td colspan="3" align="left"><b>Remit Payment To:</b><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CBD Media Finance LLC<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;c/o Kith Media LLC<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Attn: Ann Purcell<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;312 Plum Street, Suite 750<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Cincinnati, OH 45202</td></tr></table><table><tr><td colspan="2"><span style="color:#0F048D" align="left">PAN #: &nbsp;_______________________</span></td><td colspan="2"></td></tr><tr><td colspan="2"><span style="color:#0F048D" align="left">#1080s Attached: &nbsp;_______________________</span></td><td colspan="2"></td></tr></table></div></div></div></html></body>';
            }
            else {
                pdfcontent='<html><body><div style="border:5px solid black;padding-left:10%;padding-right:10%;height:700px"><div style="float:left"><img src="'+System.Label.NationalDocURLForNon0921+'" width="500" height="400" /><hr style="color:green;margin-top:120px;height:5px;background-color:green"/><div><table><tr><td>&nbsp;</td></tr></table><table border="1" cellpadding="0" cellspacing="0" width="100%" style="font-size:10px"  align="center"> <tr><td bgcolor="#CCE6FF">ISS Number:</td><td>'+objISS.Name+'</td><td bgcolor="#CCE6FF">CMR Number:</td><td>'+objISS.CMR_Number__c+'</td></tr><tr><td bgcolor="#CCE6FF">Invoice Date:</td><td>'+objISS.Invoice_Date__c.format()+'</td><td bgcolor="#CCE6FF">CMR Name:</td><td>'+objISS.CMR__r.Name+'</td></tr><tr><td bgcolor="#CCE6FF">Pub Code:</td><td>'+objISS.Publication_Code__c+'</td><td bgcolor="#CCE6FF">CMR Address:</td>  <td>'+objISS.CMR_Address__c+'</td></tr><tr><td bgcolor="#CCE6FF">Publisher:</td><td>'+objISS.Publication_Company__r.Name+'</td><td bgcolor="#CCE6FF">City/State/Zip:</td><td>'+objISS.State_City_Zip__c+'</td></tr></table><br/><table><tr><td>&nbsp;</td></tr></table><table border="1" style="font-size:10px" cellpadding="0" cellspacing="0" width="70%" align="center"><tr><td colspan="2" style="text-align:center;" bgcolor="#CCE6FF"><b>Account Summary</b></td></tr><tr><td bgcolor="#CCE6FF" style="text-align:right">Gross Amount: &nbsp;</td><td style="text-align:center">$'+objISS.Total_Gross_Amount__c.setScale(2)+'</td></tr><tr><td bgcolor="#CCE6FF" style="text-align:right">Adjustments: &nbsp;</td><td style="text-align:center">$'+objISS.Total_ADJ_Amount__c.setScale(2)+'</td></tr><tr><td bgcolor="#CCE6FF" style="text-align:right">Commission: &nbsp;</td><td style="text-align:center">$'+objISS.TotalCommission__c.setScale(2)+'</td></tr><tr><td bgcolor="#CCE6FF" style="text-align:right">Tax: &nbsp;</td><td style="text-align:center">$'+objISS.Total_Tax__c.setScale(2)+'</td></tr><tr><td bgcolor="#CCE6FF" style="text-align:right">Net Amount Due: &nbsp;</td><td style="text-align:center">$'+objISS.NetAmount__c.setScale(2)+'</td></tr></table><br/><table><tr><td>&nbsp;</td></tr></table><table border="0" cellpadding="0" cellspacing="0" width="100%" style="font-size:10px"  align="center"><tr><td><span style="color:#0F048D;font-weight:bold;" align="left">PAYMENT DUE ON OR BEFORE:</span></td><td style="text-align:left">'+objISS.Payment_Date__c.format()+'</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td></tr><tr><td align="left" style="font-weight:bold;">Refer Questions To:</td><td align="left" style="font-weight:bold;">' + System.Label.NationalClaimEmail + '</td></tr><tr><td>&nbsp;</td><td>&nbsp;</td></tr></table><br/><table border="1"><tr><td style="font-weight:bold;font-size:8px;">'+System.Label.Instructions2+
                '</td></tr></table><br/><table border="0" cellpadding="0" style="font-size:10px"  cellspacing="0" width="100%" align="center"><tr><td colspan="4" class="text" style="text-align:center"><span style="color:#0F048D;font-weight:bold;">Please detach and return with payment.</span></td></tr><tr><td colspan="4"  style="text-align:center">---------------------------------------------------------------------------------------------------------------------</td></tr><tr><td><span style="color:#0F048D" align="left">ISS Number:</span></td><td align="left"><u>'+addTrailingSpace(objISS.Name)+'</u></td><td colspan="2"></td></tr><tr><td><span style="color:#0F048D" align="left">CMR Number:</span></td><td align="left" style="border-bottom:2px solid #005000;"><u>'+addTrailingSpace(objISS.CMR_Number__c)+'</u></td><td colspan="2"><span style="color:#0F048D" align="right">Amount Enclosed: &nbsp;______________</span></td></tr><tr><td><span style="color:#0F048D" align="left">PUB Code:</span></td><td colspan="3" align="left"><u>'+addTrailingSpace(objISS.Publication_Code__c)+'</u></td></tr><tr><td><span style="color:#0F048D" align="left">Net Amount Due:</span></td><td align="left"><table border-bottom="1px"><tr><td><u>'+addTrailingSpace(+'$'+(objISS.NetAmount__c.setScale(2)))+'</u></td></tr></table></td><td colspan="2"></td></tr></table><table><tr><td>&nbsp;</td></tr></table><table border="0"><tr><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td>&nbsp;</td><td colspan="3" align="left"><b>Remit Payment To:</b><br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Berry Agency LLC<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3100 Research Blvd, Suite 250<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;National Accounts Receivable<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dayton, OH 45420</td></tr></table><table><tr><td colspan="2"><span style="color:#0F048D" align="left">PAN #: &nbsp;_______________________</span></td><td colspan="2"></td></tr><tr><td colspan="2"><span style="color:#0F048D" align="left">#1080s Attached: &nbsp;_______________________</span></td><td colspan="2"></td></tr></table></div></div></div></html></body>';
            }
            return attachPDF(objISS, pdfcontent);
        }
        return null;
    }
    
    @TestVisible private Attachment attachPDF(ISS__c ISSobj, String pdfContent) {
        try {
            Attachment attachmentPDF = new Attachment();
            attachmentPDF.parentId = ISSobj.id;
            attachmentPDF.Name = 'ISS_' + ISSobj.Name + '.pdf';
            attachmentPDF.body = Blob.toPDF(pdfContent); 
            attachmentPDF.ContentType = 'application/pdf';
            return attachmentPDF;
        }
        catch(Exception e) {
            ISSobj.addError(e.getMessage());
            return null;
        }
        return null;
    }
    
    @TestVisible private String addTrailingSpace(String strIss) {
        if(strIss == null) {
            strIss = '';
        }
        Integer result = strIss.length();
        if(result < 20){
            for(Integer i=result; i<= 20; i++){
                strIss += '&nbsp;';
            }
        }
        return strIss;
    }
    
    @TestVisible private void generateFFInvoice(ISS__c objISS, list<ISS_Line_Item__c> lstISSLI, String ffCurrency, Map<String, Id> dimensions1Map) {
        lstFFInvoice.add(newNationalFFInvoice(objISS, ffCurrency));
        Decimal intCommission = -(objISS.TotalCommission__c);
        
        if(!mapFFInvoiceLine.containsKey(objISS.id)) {
            mapFFInvoiceLine.put(objISS.id, new list<c2g__codaInvoiceLineItem__c>());
        }
        for(ISS_Line_Item__c iteratorISS : lstISSLI) {
            if(!mapFFInvoiceLine.containsKey(iteratorISS.ISS__c)) {
                mapFFInvoiceLine.put(iteratorISS.ISS__c, new list<c2g__codaInvoiceLineItem__c>());
            }
            mapFFInvoiceLine.get(iteratorISS.ISS__c).add(newNationalFFInvoiceLine(iteratorISS, dimensions1Map));
            
	        if(iteratorISS.Commission_Amount__c != null && iteratorISS.Commission_Amount__c != 0) {          
	            mapFFInvoiceLine.get(iteratorISS.ISS__c).add(newNationalFFInvoiceLineComm(-(iteratorISS.Commission_Amount__c), dimensions1Map, iteratorISS));
	        }
        }
    }
    
    @TestVisible private c2g__codaInvoice__c newNationalFFInvoice(ISS__c objISS, Id ffCurrency) {
        return new c2g__codaInvoice__c(ISS_Number__c = objISS.Id,c2g__InvoiceDate__c = objISS.Invoice_Date__c, c2g__Account__c = objISS.CMR__c, 
                                        ffbilling__DerivePeriod__c = true, ffbilling__DeriveCurrency__c = true, ffbilling__DeriveDueDate__c = true, 
                                        Customer_Name__c = objISS.CMR__c, Transaction_Type__c = 'I - Invoice', c2g__InvoiceCurrency__c = ffCurrency);
    }
    
    @TestVisible private c2g__codaInvoiceLineItem__c newNationalFFInvoiceLine(ISS_Line_Item__c objISSLI, Map<String, Id> dimensions1Map) {
         c2g__codaInvoiceLineItem__c SILI = new c2g__codaInvoiceLineItem__c(ISS_Line_Item__c = objISSLI.Id, ADJ_Amt__c = objISSLI.ADJ_Amount__c, ffbilling__DeriveUnitPriceFromProduct__c = false, 
                                                Comm_Amt__c = objISSLI.Commission_Amount__c, c2g__UnitPrice__c = objISSLI.Gross_Amount__c,
                                                Gross_Amt__c = objISSLI.Gross_Amount__c, c2g__Product__c = objISSLI.Product__c, 
                                                c2g__Dimension1__c = dimensions1Map.get(objISSLI.Directory_Code__c), 
                                                c2g__Dimension2__c = System.Label.Dimension2IdForPrint,
                                                c2g__Dimension3__c = System.Label.Dimension3IdForCMRNational,
                            					Directory__c = objISSLI.Directory__c,
                            					Directory_Edition__c = objISSLI.Directory_Edition__c,
                            					Billing_Frequency__c = CommonMessages.singlePayment);
                            					
		if(mapISSLINatTax.size() > 0) {
			SILI.c2g__TaxValue1__c = objISSLI.Tax_Amount__c;
			if(mapISSLINatTax.containsKey(objISSLI.Id)) {
				SILI.c2g__TaxCode1__c = mapISSLINatTax.get(objISSLI.Id).Tax_Code__c;
			}
			SILI.ffbilling__CalculateTaxValue1FromRate__c = false;
			SILI.ffbilling__DeriveTaxRate1FromCode__c = false;
			SILI.ffbilling__SetTaxCode1ToDefault__c = false;
		}
    	
    	return SILI;
    }
    
    @TestVisible private c2g__codaInvoiceLineItem__c newNationalFFInvoiceLineComm(Decimal intCommission, Map<String, Id> dimensions1Map, ISS_Line_Item__c ISSLineItem) {
        c2g__codaInvoiceLineItem__c SILI = new c2g__codaInvoiceLineItem__c(ffbilling__DeriveUnitPriceFromProduct__c = false, 
                                                c2g__Product__c= label.National_Commission_Product, 
                                                c2g__UnitPrice__c = intCommission, 
                                                c2g__Dimension1__c = dimensions1Map.get(ISSLineItem.Directory_Code__c), 
                                                c2g__Dimension2__c = System.Label.Dimension2IdForPrint,
                                                c2g__Dimension3__c = System.Label.Dimension3IdForCMRNational,
                            					Directory__c = ISSLineItem.Directory__c,
                            					Directory_Edition__c = ISSLineItem.Directory_Edition__c,
                            					Billing_Frequency__c = CommonMessages.singlePayment);
    	
    	return SILI;
    }
    
    public void postInvToFF() {
    	FFPostBool = true;
        Set<Id> saleInvIds = new Set<Id>();
        
        for(c2g__codaInvoice__c objSI : lstFFInvoice) {
            saleInvIds.add(objSI.Id);
        }
        
        if(saleInvIds.size() > 0) {
            FFSalesInvoicePostBatchController obj = new FFSalesInvoicePostBatchController(saleInvIds);
            batchId = Database.executeBatch(obj, 2);       
            pollerAction = true;
            isBatchProcessed = true;        
        }
        updateFFCompleteStatusTable();
    }
    
    public void PollingBatchProcess() {                 
        if(batchId!=null) {
            AsyncApexJob FFjob = [SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors FROM AsyncApexJob WHERE ID =: batchId];
            if(!Test.isRunningTest())
            batchStatus= FFjob.Status;
        }
        if(batchStatus == 'Completed') {
            pollerAction = false;
        }
    }
    
    public void fetchStatusTable() {
        listNatBill = NationalBillingStatusSOQLMethods.getNatBillByPubEditionCode(strPubCodeSendRemittance, strEditionCodeSendRemittance);
        if(listNatBill.size() > 0) {
        	natBilling = listNatBill.get(0);
        }
    }
    
    public void assignValuesFromNatBill() {
    	ISSRemTranBool = natBilling.ISS_Transfer_Completed__c;
    	sendRemComplBool = natBilling.RSAP_COMPLETE_EOJ_FROM_INFORMATICA__c;
    	sendRemBool = natBilling.NB_RSAP_Send_Remittance__c;
        chkRemittanceComplete = natBilling.RSAP_REMITTANCE_COMPLETE__c;
        chkFFInvBalanced = natBilling.RSAP_FF_INVOICES_BALANCED__c;
        FFPostBool = natBilling.RSAP_POST_TO_FINANCIALFORCE__c;
    }    
    
    public void updateStatusTable() {
        if(listNatBill.size() > 0) {
            for(National_Billing_Status__c NBS : listNatBill) {
                NBS.RSAP_REMITTANCE_COMPLETE__c = chkRemittanceComplete;
                NBS.RSAP_FF_INVOICES_BALANCED__c = chkFFInvBalanced;
                NBS.NB_RSAP_Send_Remittance__c = sendRemBool;
                NBS.RSAP_COMPLETE_EOJ_FROM_INFORMATICA__c = sendRemComplBool;
                NBS.ISS_Transfer_Completed__c = ISSRemTranBool;
            }
            update listNatBill;
        }
    }
    
    public void sendToElite() {
    	if(listNatBill.size() > 0) {
    		sendRemBool = true;
            for(National_Billing_Status__c NBS : listNatBill) {
                NBS.RSAP_SEND_TO_ELITE_INITIATE_INFORMATICA__c = true;
                NBS.NB_RSAP_Send_Remittance__c = true;
            }
            updateStatusTable();
        }
    }
    
    public void calculateTotal(List<c2g__codaInvoice__c> lstFFInvoices) {
    	Decimal TaxAmtTemp;
        Decimal CommisionTemp;
        Decimal GrossAmt;
        FFAmt = 0;
        FFCommission = 0;
        FFTaxAmt = 0;
        FFNetAmt = 0;
        for(c2g__codaInvoice__c iterator: lstFFInvoices) {
            TaxAmtTemp = 0;
            CommisionTemp = 0;
            GrossAmt = 0;
            if(iterator.c2g__TaxTotal__c != null) {
                TaxAmtTemp = iterator.c2g__TaxTotal__c;
                FFTaxAmt = FFTaxAmt + TaxAmtTemp;
            }
            if(iterator.Total_Gross_Amnt__c != null) {
                GrossAmt = iterator.Total_Gross_Amnt__c;
                FFAmt = FFAmt + iterator.Total_Gross_Amnt__c;
            }
            if(iterator.Total_Commission__c != null) {
                CommisionTemp = iterator.Total_Commission__c;
                FFCommission = FFCommission + CommisionTemp;
            }
            
            FFNetAmt = FFNetAmt + ((GrossAmt + TaxAmtTemp) - CommisionTemp);
        }
    }                   
    
    public void hideISS() {
    	boolPopup = false;
    }
    
    public void showISS() {
    	boolPopup = true;
    	listISS = new List<ISS__c>();
    	
    	if(mapISS.size() > 0) {
    		listISS = mapISS.values();
    	}
    }
    
    public void updateRemCreatedStatusTable() {
        if(listNatBill.size() > 0) {
            for(National_Billing_Status__c NBS : listNatBill) {
                NBS.RSAP_Remittance_Sheet_Created__c = true;
            }
            update listNatBill;
        }
    }
    
    public void updateFFCompleteStatusTable() {
        if(listNatBill.size() > 0) {
            for(National_Billing_Status__c NBS : listNatBill) {
                NBS.RSAP_POST_TO_FINANCIALFORCE__c = true;
            }
            update listNatBill;
        }
    }
    
    public void updateSenRemStatusTable() {
    	if(!chkRemittanceComplete) {
    		sendRemBool = false;
	        if(listNatBill.size() > 0) {
	            for(National_Billing_Status__c NBS : listNatBill) {
	                NBS.NB_RSAP_Send_Remittance__c = chkRemittanceComplete;
	            }
	            update listNatBill;
	        }
    	}
    }
}