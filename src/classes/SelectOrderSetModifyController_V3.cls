/*
    Author : Mythili P
    Date   : 17-September-2014
    Purpose : This class is used to select the order line items for the account that has to be modified.
*/
public with sharing class SelectOrderSetModifyController_V3 {
    
    //Variable declaration
    private string accountID{get;set;}
    public list<Order_Line_Items__c> OLIs{get; set;}
    public Set<SelectOption> primaryCanvassSet{get; set;}
    public List<SelectOption> primaryCanvassList{get; set;}
    private String uniqueID = '';
    public String canvassId {get;set;}
    public List<DigitalDisplayWrapper> digitalOLIList{get;set;}
    public Opportunity objOPP=null;
    public Boolean CheckBool{get;set;}
    public Id OLIIdforcheck{get;set;}
    private Set<Id> allOliSetToRemove = new Set<Id>();
    public List<OrderSetWrapper> printDisplayList{get;set;}
    public String orderSetId{get;set;}
    public string dirEditionId{get;set;}
    public Boolean check{get;set;}
    
    //for ticket ATP-1964
    public String dirEditionStatus{get;set;}
    public String mainBook{get;set;}
    public String companionDir{get;set;}    
    public list<Modification_Order_Line_Item__c> MOLIPendingList {get;set;}
    
    // constructor
    public SelectOrderSetModifyController_V3(ApexPages.StandardController controller) {
        accountID = controller.getId();
        primaryCanvassSet = new Set<SelectOption>();        
        primaryCanvassList = new List<SelectOption>();       
        List<Opportunity> opptyList = new List<Opportunity>();
        opptyList = OpportunitySOQLMethods.getOpportunityByAcctIds(new set<Id> {accountId});
        system.debug('Oppty List is ' + opptyList);
        primaryCanvassSet.add(new SelectOption('none', '---None---'));
        if(opptyList.size() > 0){
            for(Opportunity oppty : opptyList){
                if(oppty.Account_Primary_Canvass__c != null) {
                    primaryCanvassSet.add(new SelectOption(oppty.Account_Primary_Canvass__c, oppty.Account_Primary_Canvass__r.Name));
                }
            }
        }        
        primaryCanvassList.addAll(primaryCanvassSet);
        Map<String, String> UrlParameterMap = ApexPages.currentPage().getParameters();
        if (UrlParameterMap.containsKey('CanvassId'))   
        
        {
            canvassId = ApexPages.CurrentPage().getParameters().get('CanvassId');
            displayOrderLineItems();
        }
        else{
        
        canvassId = 'none';
        
        }
    }

    //Inserting new opportunity
    @TestVisible private Opportunity insertOpportunity(Account objAccount) {
        Opportunity objOPP = generateOpportunity(objAccount, CommonMethods.getRedordTypeIdByName(CommonMessages.opportunityNewRT, CommonMessages.opportunityObjectName));
        insert objOPP;
        return objOPP;
    }
    
    //Generating new opportunity from account data
    private Opportunity generateOpportunity(Account objAccount, ID oppRTID) {
        Opportunity objOpportunity = new Opportunity(AccountId = objAccount.Id, StageName = CommonMessages.proposalStageOpp, 
        Name = objAccount.Name +' ' + System.today(), CloseDate = System.today(), Pricebook2Id = system.label.PricebookId,
        RecordTypeId = oppRTID, Account_Manager__c = objAccount.Account_Manager__c, Account_Number__c = objAccount.Account_Number__c, 
        Account_Primary_Canvass__c = canvassId, modifyid__c = uniqueID);
        if(objAccount.Contacts.size() > 0) {
                Contact objContact = objAccount.Contacts[0];
                objOpportunity.Billing_Contact__c = objContact.Id;
        }
        return objOpportunity;
    }
    
    //Generating new temp opportunity line ietm
    @TestVisible private TmpOpportunityLineItem__c generateTempOpportunityLineItem(Order_Line_Items__c OLI, Opportunity objOPP) {
        return new TmpOpportunityLineItem__c(Accountid__c = accountID, Canvass__c = canvassId, UnitPrice__c = OLI.UnitPrice__c, Geo_Type__c = OLI.Geo_Type__c,
                                        ListPrice__c = OLI.ListPrice__c, Original_Line_Item_ID__c = OLI.id, Digital_Product_Requirement__c = OLI.Digital_Product_Requirement__c, Geo_Code__c = OLI.Geo_Code__c,
                                        Billing_End_Date__c = OLI.Billing_End_Date__c, Billing_Start_Date__c = OLI.Billing_Start_Date__c,                
                                        Billing_Method__c = OLI.Payment_Method__c,Parent_Line_Item__c = (OLI.Parent_Line_Item__c != Null ? OLI.Parent_Line_Item__c : OLI.Id),
                                        Contact__c = OLI.Billing_Contact__c, Cancel_Date__c = OLI.Cutomer_Cancel_Date__c, Quantity__c = OLI.Quantity__c, Discount__c = OLI.Discount__c, Status__c = OLI.Status__c,
                                        Directory__c = OLI.Directory__c, Description__c = OLI.Description__c, Effective_Date__c = OLI.Effective_Date__c, Heading__c=OLI.Directory_Heading__r.Directory_Heading_Name__c, 
                                        Billing_Duration__c = OLI.Payment_Duration__c, Parent_Product_ID__c = OLI.Product2__c, Directory_Heading__c = OLI.Directory_Heading__c,
                                        orignalOpportunityid__c = OLI.Opportunity__c, Templinkid__c = uniqueID, Product_Type__c = OLI.Product_Type__c, Package_ID__c = OLI.Package_ID__c, 
                                        Directory_Section__c = OLI.Directory_Section__c, Listing__c = OLI.Listing__c, Package_ID_External__c = OLI.Package_ID_External__c,
                                        Full_Rate__c = OLI.ListPrice__c, Is_Caption__c = OLI.Is_Caption__c, Is_Child__c = OLI.Is_Child__c, Is_P4P__c = OLI.Is_P4P__c, 
                                        Product_Code__c = OLI.ProductCode__c, Scope__c = OLI.Scope__c, Locality__c = OLI.Locality__c, Category__c = OLI.Category__c, 
                                        Pricing_Program__c = OLI.Pricing_Program__c, Opportunityid__c = objOPP.Id, Billing_Partner__c=OLI.Billing_Partner_Account__c, 
                                        Billing_Frequency__c = OLI.Billing_Frequency__c, TMP_OLI_Parent_ID__c = OLI.Parent_ID__c, TMP_OLI_ParentID_Addon__c = OLI.Parent_ID_of_Addon__c);
    }

    /* This method generates the order line items to de displayed based on the selected Canvass*/
    public void displayOrderLineItems(){
        printDisplayList = new List<OrderSetWrapper>();
        MOLIPendingList = new list<Modification_Order_Line_Item__c>();         
        digitalOLIList=new List<DigitalDisplayWrapper>();
         allOliSetToRemove = new Set<Id>();
        List<Order_Line_Items__c> printList=null;
        List<Order_Group__c> orderSetList = OrderGroupSOQLMethods.getOrderGroupByAcctIdOpptyCanvassId(accountId, canvassId);
         
        system.debug('Order set list'+orderSetList);
         if(orderSetList!=null && orderSetList.size()>0){
            printList=new List<Order_Line_Items__c>();
                for(Order_Group__c orderSet:orderSetList){
                    for(Order_Line_Items__c oli:orderSet.Order_Line_Items__r){
                        if(oli.Media_Type__c=='Digital' && oli.isCanceled__c==false){
                             populateDigitalDisplayList(oli);
                        }else if(oli.Media_Type__c=='Print'){
                            printList.add(oli);
                        }
                    }
                    
                }
            
         }
        
        //Checks the ship date for BOTS DE 
        if(printList!=null && printList.size()>0){
            checkForShipDateonBOTSDirectoryEdition(printList);
        }
        
        //construct the oli list for print olis for display
        for(Order_Group__c orderSet:orderSetList){
            OrderSetWrapper printOli=new OrderSetWrapper();
            printOli.orderSet=orderSet;
            printOli.oliList=new List<DigitalDisplayWrapper>();
            if(orderSet.Order_Line_Items__r!=null && orderSet.Order_Line_Items__r.size()>0){
                for(Order_Line_Items__c oli:orderSet.Order_Line_Items__r){
                    if(oli.Media_Type__c=='Print' && !allOliSetToRemove.contains(oli.Id)){
                        DigitalDisplayWrapper dis=new DigitalDisplayWrapper();
                        dis.digitalOLI=oli;
                        dis.isSelected = false;
                        printOli.oliList.add(dis);
                    }
                }
            }
            
            if(printOli.oliList!=null && printOli.oliList.size()>0){
                printDisplayList.add(printOli);
            }
        }
        //ATP-3754
         MOLIPendingList = ModificationOrderLineItemSOQLMethods.getMOLIBasedOnCanvass(accountId, canvassId);      
        
        system.debug('Print display list - final'+printDisplayList);
    }
    
      /* Checks for the ship date of Directory edition and removes the ones that shloud not be dispalyed*/
    private void checkForShipDateonBOTSDirectoryEdition(List<Order_Line_Items__c> printList){
        List<Order_Line_Items__c> oliList=null;
        Set<Id> oliSetToRemove=null;
        Map<Id,List<Order_Line_Items__c>> dirOliMap=new Map<Id,List<Order_Line_Items__c>>();
        
        for(Order_Line_Items__c printOli:printList){
            if(!dirOliMap.containsKey(printOli.Directory__c)){
                oliList=new List<Order_Line_Items__c>();
                oliList.add(printOli);
                dirOliMap.put(printOli.Directory__c,oliList);
            }else{
                oliList=dirOliMap.get(printOli.Directory__c);
                oliList.add(printOli);
                dirOliMap.put(printOli.Directory__c,oliList);
            }
        }
        
        if(dirOliMap!=null && dirOliMap.size()>0){
            
            for(Id dirId:dirOliMap.KeySet()){
                oliSetToRemove=new Set<Id>();
                oliList=dirOliMap.get(dirId);
                boolean remove = false;
                for(Order_Line_Items__c oli:oliList){
                    if(oli.Directory_Edition__r.Ship_Date__c<=System.Today() && oli.Directory_Edition__r.Book_Status__c=='BOTS'){
                        remove = true;
                    }
                    if(oli.Directory_Edition__r.Book_Status__c=='BOTS-1'){
                        oliSetToRemove.add(oli.Id);
                    }
                }
                if(remove== true && oliSetToRemove.size()>0){
                    allOliSetToRemove.addAll(oliSetToRemove);
                }
            }
        }
    }
    
    /* This method populates the Wrapper for digital OLIs to be displayed*/
    @Testvisible private void populateDigitalDisplayList(Order_Line_Items__c orderLineItem){
        if(orderLineItem!=null){
            DigitalDisplayWrapper digiWrapperObj=new DigitalDisplayWrapper();
            digiWrapperObj.isSelected=false;
            digiWrapperObj.digitalOLI=orderLineItem;
            digitalOLIList.add(digiWrapperObj);
        }
    }
    

    /* Method that saves selected OLIs and creates Temp OLIs */
    public PageReference saveOppandOLIs(){
        String bookStatus=null;
        String billingPartner=null;
        String billingFrequency=null;
        String paymentMethod=null;
         OLIs = new list<Order_Line_Items__c>();
          list<Modification_Order_Line_Item__c> listMOLI = new list<Modification_Order_Line_Item__c>();
         //ATP-3754
         set <ID> SETDigitalOLIID = new set <ID>();
         
         //populating print olis
        for(OrderSetWrapper orderSet:printDisplayList){
            for(DigitalDisplayWrapper oliWrap:orderSet.oliList){
                if(oliWrap.isSelected == true){
                    if(bookStatus == null && billingPartner==null && billingFrequency==null && paymentMethod==null){
                        bookStatus = oliWrap.digitalOLI.Directory_Edition__r.Book_Status__c;
                        billingPartner=oliWrap.digitalOLI.Billing_Partner__c;
                        billingFrequency=oliWrap.digitalOLI.Billing_Frequency__c;
                        paymentMethod=oliWrap.digitalOLI.Payment_Method__c;
                        OLIs.add(oliWrap.digitalOLI);
                    }else{
                        if(oliWrap.digitalOLI.Directory_Edition__r.Book_Status__c == bookStatus && oliWrap.digitalOLI.Billing_Partner__c==billingPartner &&
                            oliWrap.digitalOLI.Billing_Frequency__c==billingFrequency && oliWrap.digitalOLI.Payment_Method__c==paymentMethod){
                            OLIs.add(oliWrap.digitalOLI);
                        }else{
                            if(oliWrap.digitalOLI.Directory_Edition__r.Book_Status__c != bookStatus){
                                ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Please check whether you have selected only one edition per directory and the same edition for all the selected directories');
                                ApexPages.addMessage(myMsg);
                            }
                            if(oliWrap.digitalOLI.Billing_Partner__c!=billingPartner || oliWrap.digitalOLI.Billing_Frequency__c!=billingFrequency || oliWrap.digitalOLI.Payment_Method__c!=paymentMethod){
                                 ApexPages.Message myMsg1 = new ApexPages.Message(ApexPages.Severity.ERROR, 'Billing Partner, Billing Frequency and Payment method should match for all the selected order line items');
                                ApexPages.addMessage(myMsg1);
                            }
                            return null;
                        }
                    }
                    
                }
            }
        }
        //Populating digital OLIs
        if(digitalOLIList!=null && digitalOLIList.size()>0){
            for(DigitalDisplayWrapper digiWrap:digitalOLIList){
                if(digiWrap.isSelected==true){
                    if(billingPartner==null && billingFrequency==null && paymentMethod==null){
                        billingPartner=digiWrap.digitalOLI.Billing_Partner__c;
                        billingFrequency=digiWrap.digitalOLI.Billing_Frequency__c;
                        paymentMethod=digiWrap.digitalOLI.Payment_Method__c;
                        OLIs.add(digiWrap.digitalOLI); 
                        SETDigitalOLIID.add(digiWrap.digitalOLI.id); 
                    }else{
                        if(digiWrap.digitalOLI.Billing_Partner__c==billingPartner &&
                            digiWrap.digitalOLI.Billing_Frequency__c==billingFrequency && digiWrap.digitalOLI.Payment_Method__c==paymentMethod){
                            OLIs.add(digiWrap.digitalOLI);
                        }else{
                             ApexPages.Message myMsg2 = new ApexPages.Message(ApexPages.Severity.ERROR, 'Billing Partner, Billing Frequency and Payment method should match for all the selected order line items');
                             ApexPages.addMessage(myMsg2);
                             return null;
                        }
                    }             
                }
            }
        }
        //ATP-3754
        /*       
        if(SETDigitalOLIID.size() > 0){
            listMOLI = ModificationOrderLineItemSOQLMethods.getMOLIBYOLI(SETDigitalOLIID,date.today());
           
        }*/
        
       // if(OLIs!=null && OLIs.size()>0 && listMOLI.size() == 0){
            if(OLIs!=null && OLIs.size()>0){
            system.debug('****Selected Order line items********'+OLIs);
            map<ID, Account> mapAccount = AccountSOQLMethods.getAccountContactByAccountID(new Set<ID>{accountID});
            Savepoint sp = Database.setSavepoint();
            try {
                uniqueID = Userinfo.getUserId() + AccountID +'-'+DateTime.now().format('yyyyMMdd - HHmmss');
                objOPP = insertOpportunity(mapAccount.get(accountID));
                list<TmpOpportunityLineItem__c> lstTempOLI = new list<TmpOpportunityLineItem__c>();
                //Loop through all records in the order line item collection
                for(Order_Line_Items__c iterator : OLIs) {
                    //Adding temp opportunity line item to collection
                    lstTempOLI.add(generateTempOpportunityLineItem(iterator, objOPP));
                }
                
                //Insert temp order line items
                if(lstTempOLI.size() > 0) {
                    insert lstTempOLI;
                    system.debug('Inserted');
                    return (new ApexPages.StandardController(objOPP)).view();
                }
               
            }
            catch(Exception ex) {
                CommonMethods.addError('Due to some reason, unable to create opportunity. Please contact support team.');
                Database.rollback(sp);
            }  
            
        }else{
            /*if(listMOLI.size() > 0)               
                CommonMethods.addError('For the selected OLI there are MOLI which are not completed.');
            else */
             CommonMethods.addError('Please select atleast one order line item to create an opportunity.');
        }
        return null;
    }
    
    /*This method selects/deselects the olis automatically from an order set with the same directory edition*/
    public void checkSimilarOlis(){
        system.debug('Parameters from page'+orderSetId+'    '+dirEditionId+'   '+check+' '+dirEditionStatus+' '+mainBook+' '+companionDir);
        String directoryReqd=null;
        if(mainBook!=null && mainBook!=''){
            directoryReqd=mainBook;
        }else if(companionDir!=null && companionDir!=''){
            directoryReqd=companionDir;
        }
        
        if(check){
            for(OrderSetWrapper orderSetWrap:printDisplayList){
                if(orderSetWrap.orderSet.Id==orderSetId){
                    for(DigitalDisplayWrapper oliWrap:orderSetWrap.oliList){
                        if(oliWrap.digitalOLI.Directory_Edition__c == dirEditionId || (oliWrap.digitalOLI.Directory_Edition__r.Book_Status__c==dirEditionStatus && directoryReqd!=null && oliWrap.digitalOLI.Directory__c==directoryReqd)){
                            oliWrap.isSelected = true;
                        }
                    }
                    
                }
            }
        }else if(!check){
            for(OrderSetWrapper orderSetWrap:printDisplayList){
                if(orderSetWrap.orderSet.Id==orderSetId){
                    for(DigitalDisplayWrapper oliWrap:orderSetWrap.oliList){
                        if(oliWrap.digitalOLI.Directory_Edition__c == dirEditionId || (oliWrap.digitalOLI.Directory_Edition__r.Book_Status__c==dirEditionStatus && directoryReqd!=null && oliWrap.digitalOLI.Directory__c==directoryReqd)){
                            oliWrap.isSelected = false;
                        }
                    }
                    
                }
            }
        }
        
    }
    
    /* Selects the order line items with same package or parent id */  
    public pagereference CheckBundle()
    {          
           Order_Line_Items__c tempOLI=[select id,Package_ID_External__c,Parent_ID__c,Parent_ID_of_Addon__c from Order_Line_Items__c where id=:OLIIdforcheck];
           if(CheckBool)
           {
               if(tempOLI.Parent_ID__c!=null)
               {
                    for(DigitalDisplayWrapper ol:digitalOLIList)
                    {
                       if(ol.digitalOLI.Parent_ID__c!=null || ol.digitalOLI.Parent_ID_of_Addon__c!=null)
                       {
                            if(ol.digitalOLI.Parent_ID_of_Addon__c==tempOLI.Parent_ID__c || ol.digitalOLI.Parent_ID__c==tempOLI.Parent_ID__c)
                                ol.isSelected=true;
                       }  
                    }                   
               }
               else if(tempOLI.Parent_ID_of_Addon__c!=null)
               {
                    for(DigitalDisplayWrapper ol:digitalOLIList)
                    {
                       if(ol.digitalOLI.Parent_ID__c!=null || ol.digitalOLI.Parent_ID_of_Addon__c!=null)
                       {
                            if(ol.digitalOLI.Parent_ID_of_Addon__c==tempOLI.Parent_ID_of_Addon__c|| ol.digitalOLI.Parent_ID__c==tempOLI.Parent_ID_of_Addon__c)
                                ol.isSelected=true;
                       }  
                    }  
               }
               
              if(tempOLI.Package_ID_External__c!=null)
              { 
                     for(DigitalDisplayWrapper ol:digitalOLIList)
                    {
                       if(ol.digitalOLI.Package_ID_External__c!=null)
                       {
                            if(ol.digitalOLI.Package_ID_External__c==tempOLI.Package_ID_External__c)
                                ol.isSelected=true;
                       }  
                    }
              }
           }
           if(!CheckBool)
           {
               if(tempOLI.Parent_ID__c!=null)
               {
                    for(DigitalDisplayWrapper ol:digitalOLIList)
                    {
                       if(ol.digitalOLI.Parent_ID__c!=null || ol.digitalOLI.Parent_ID_of_Addon__c!=null)
                       {
                            if(ol.digitalOLI.Parent_ID_of_Addon__c==tempOLI.Parent_ID__c || ol.digitalOLI.Parent_ID__c==tempOLI.Parent_ID__c)
                                ol.isSelected=false;
                       }  
                    }                   
               }
               else if(tempOLI.Parent_ID_of_Addon__c!=null)
               {
                    for(DigitalDisplayWrapper ol:digitalOLIList)
                    {
                       if(ol.digitalOLI.Parent_ID__c!=null || ol.digitalOLI.Parent_ID_of_Addon__c!=null)
                       {
                            if(ol.digitalOLI.Parent_ID_of_Addon__c==tempOLI.Parent_ID_of_Addon__c|| ol.digitalOLI.Parent_ID__c==tempOLI.Parent_ID_of_Addon__c)
                                ol.isSelected=false;
                       }  
                    }  
               }
               
              if(tempOLI.Package_ID_External__c!=null)
              { 
                    for(DigitalDisplayWrapper ol:digitalOLIList)
                    {
                       if(ol.digitalOLI.Package_ID_External__c!=null)
                       {
                            if(ol.digitalOLI.Package_ID_External__c==tempOLI.Package_ID_External__c)
                                ol.isSelected=false;
                       }  
                    }
              }
           }
           return null;
    }
   
/*
    Wrapper Classes
*/
    
    public class DigitalDisplayWrapper{
        public Boolean isSelected{get;set;}
        public Order_Line_Items__c digitalOLI{get;set;}
    }
    
    public class OrderSetWrapper{
        public List<DigitalDisplayWrapper> oliList{get;set;}
        public Order_Group__c orderSet{get;set;}
    }
    
    //ATP-3754-to find the pending  MOLI for the account
    
    public PageReference PendingMOLIPage()
    {
         
        string url='/apex/pendingMOLI?accountID='+accountID+'&CanvassId='+canvassId;
       
        pageReference PagePendingMOLI= new PageReference(url);
        PagePendingMOLI.setRedirect(true);
        return PagePendingMOLI;  
    }
    
}