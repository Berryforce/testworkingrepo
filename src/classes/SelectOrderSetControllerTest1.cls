@IsTest(seealldata=true)
public class SelectOrderSetControllerTest1{

     public static testMethod void SelectOrderSetControllerTest01(){
        Canvass__c c=TestMethodsUtility.createCanvass();
       Test.startTest();
        Account objAccount  = new Account(Primary_Canvass__c = c.Id, Name = 'Test Account', Phone = '(999) 999-9999', Billing_Phone__c='(999) 999-9999', Lead_Source__c = 'Testing', Conversion_Status__c = 'Yes');  
        insert objAccount ;       
        Account objAccount1 = new Account();
        objAccount1.Name = 'Test Account2';
        objAccount1.Phone = '(456)789-3478';
        objAccount1.Open_claim__c = false;

        objAccount1.Delinquency_Indicator__c=false;
        objAccount1.Letter_Renewal_Sequence__c = '1';
        insert objAccount1;
        
        Contact objContact = new contact();
        objContact.LastName = 'Test ';
        objContact.FirstName = 'Contact1';
        objContact.Primary_Contact__c = True;
        objContact.Email = 'test4@csc.com';
        objContact.Contact_Role__c = 'Billing';
        objContact.Phone='(489)456-0987';
        objContact.accountId = objAccount.Id;
        insert objContact;
        
        Opportunity objOpportunity =new Opportunity();
        objOpportunity.Name = 'Test Opportunity1';
        objOpportunity.AccountId = objAccount.Id;
        objOpportunity.Billing_Contact__c = objContact.Id;
        objOpportunity.StageName= 'Proposal Created';
        objOpportunity.CloseDate = System.today();
        objOpportunity.Billing_Partner__c=objAccount.Name;
        objOpportunity.Payment_Method__c='Credit Card';
        objOpportunity.Account_Primary_Canvass__c=c.id;
        insert objOpportunity;
        
        Directory__c objDir = new Directory__c();
        objDir.Name = 'Test Dir1';
        objDir.Directory_Code__c='DIR001';
        insert objDir;
        
        Directory_Edition__c objDirE = new Directory_Edition__c();
        objDirE.Name = 'Test DirE1';
        objDirE.Directory__c = objDir.Id;
        objDirE.Letter_Renewal_Stage_1__c = system.today();
        objDirE.Sales_Lockout__c=Date.today().addDays(30);
        objDirE.book_status__c='NI';
        objDirE.Pub_Date__c =Date.today().addMonths(1);
        insert objDirE;
        Directory_Edition__c objDirE1 = new Directory_Edition__c();
        objDirE1.Name = 'Test DirE1';
        objDirE1.Directory__c = objDir.Id;
        objDirE1.Letter_Renewal_Stage_1__c = system.today();
        objDirE1.Sales_Lockout__c=Date.today().addDays(30);
        objDirE1.Pub_Date__c =Date.today();

        insert objDirE1;        
        
        List<Order_Line_Items__c> oliLst = new List<Order_Line_Items__c>();
        List<Product2> prdList =new List<Product2>();
        Product2 objProd = new Product2();
        objProd.Name = 'Test';
        objProd.Product_Type__c = 'Print';
        objProd.ProductCode = 'WLCSH';
        objProd.Print_Product_Type__c='Display';
        objProd.Family='Print';  
        objProd.RGU__c='Print';     
        insert  objProd;
        
        Product2 objProd1 = new Product2();
        objProd1.Name = 'Test';
        objProd1.Product_Type__c = 'Digital';
        objProd1.ProductCode = 'GC50';
        objProd1.Print_Product_Type__c='Specialty';
         objProd1.RGU__c='Print';          
        insert objProd1;      
        Order__c objOrder = new Order__c(Account__c=objAccount.id,Billing_Anniversary_Date__c=null);
        insert objOrder;
        
        Order_Group__c objOrderGroup = new Order_Group__c(Order_Account__c=objAccount.id, Order__c=objOrder.id, Opportunity__c=objOpportunity.id,selected__c=true,Order_Set_Selected_Cancel__c =true);
        insert objOrderGroup ;
   
        Order_Line_Items__c objOrderLineItem = new Order_Line_Items__c(Contract_End_Date__c=Date.today().addmonths(3),Parent_ID__c='pkg01',checked__c=true,Account__c=objAccount.Id, Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd.Id,is_p4p__c=false,Directory_Edition__c = objDirE.Id,UnitPrice__c=100,Directory__c=objDir.Id,Payments_Remaining__c =2,Talus_Go_Live_Date__c=date.today().addMOnths(4),media_Type__c='Digital');
        Order_Line_Items__c objOrderLineItem1 = new Order_Line_Items__c(Contract_End_Date__c=Date.today().addmonths(3),Parent_ID_of_Addon__c='pkg01',checked__c=true,Account__c=objAccount.Id, Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd1.Id,is_p4p__c=true,Directory_Edition__c = objDirE.Id,UnitPrice__c=100,Last_Billing_Date__c=date.today().addDays(45),Payment_Duration__c=12,Directory__c=objDir.Id,Payments_Remaining__c =2,Talus_Go_Live_Date__c=date.today().addMOnths(2),media_Type__c='Digital');
        Order_Line_Items__c objOrderLineItem2 = new Order_Line_Items__c(Contract_End_Date__c=Date.today().addmonths(3),Parent_ID_of_Addon__c='pkg01',checked__c=true,Account__c=objAccount1.Id, Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd.Id,is_p4p__c=false,Directory_Edition__c = objDirE.Id,UnitPrice__c=100,Directory__c=objDir.Id,Payments_Remaining__c =2,Talus_Go_Live_Date__c=date.today().addMOnths(2),media_Type__c='Digital');
        oliLst.add(objOrderLineItem);
        oliLst.add(objOrderLineItem1);
        oliLst.add(objOrderLineItem2);
        insert oliLst;
       //   Test.stopTest();   
        Case cancelCase=TestMethodsUtility.generateCancellation();
        cancelCase.AccountID=objAccount.id;
        insert cancelCase;
        Id caseID = cancelCase.Id;
        PageReference pageRef1 = new PageReference('/apex/selectordersetforcancel?caseID='+caseID+'&pagenum=1');                
        Test.setCurrentPage(pageRef1);
        ApexPages.StandardController con = new ApexPages.StandardController(new account());                     
        SelectOrderSetController ctl = new SelectOrderSetController(con); 
       
        
       // Test.startTest();
             ctl.caseID = caseID;
             ctl.selectedCanvass=c.id;
             ctl.OLIIdforcheck=oliLst[0].id;
             ctl.CheckBool=true;
             
             ctl.fetchOrderSets();
             ctl.ShowSelectOLI(); 
             ctl.OLIs=oliLst;
             ctl.oliNext();
            ctl.CheckBundle();
             
              for(Order_Line_Items__c temp :oliLst)
              {
                  temp.Cutomer_Cancel_Date__c = date.today().addDays(-2);
              }
              ctl.OLIs = oliLst;           
              ctl.oliCancel();
            //system.assertNotEquals(null,ctl.DoneCancel());
        Test.stopTest();
    }            

   public static testMethod void SelectOrderSetControllerTest02(){
    Canvass__c c=TestMethodsUtility.createCanvass();
        Account objAccount  = new Account(Primary_Canvass__c = c.Id, Name = 'Test Account', Phone = '(999) 999-9999', Billing_Phone__c='(999) 999-9999', Lead_Source__c = 'Testing', Conversion_Status__c = 'Yes');  
        insert objAccount ;
        Account objAccount1 = new Account();
        objAccount1.Name = 'Test Account2';
        objAccount1.Phone = '(456)789-3478';
        objAccount1.Open_claim__c = false;

        objAccount1.Delinquency_Indicator__c=false;
        objAccount1.Letter_Renewal_Sequence__c = '1';
        insert objAccount1;
        
        Contact objContact = new contact();
        objContact.LastName = 'Test ';
        objContact.FirstName = 'Contact1';
        objContact.Primary_Contact__c = True;
        objContact.Email = 'test4@csc.com';
        objContact.Contact_Role__c = 'Billing';
        objContact.Phone='(489)456-0987';
        objContact.accountId = objAccount.Id;
        insert objContact;
        
        Opportunity objOpportunity =new Opportunity();
        objOpportunity.Name = 'Test Opportunity1';
        objOpportunity.AccountId = objAccount.Id;
        objOpportunity.Billing_Contact__c = objContact.Id;
        objOpportunity.StageName= 'Proposal Created';
        objOpportunity.CloseDate = System.today();
        objOpportunity.Billing_Partner__c=objAccount.Name;
        objOpportunity.Payment_Method__c='Credit Card';
        objOpportunity.Account_Primary_Canvass__c=c.id;
        insert objOpportunity;
        
        Directory__c objDir = new Directory__c();
        objDir.Name = 'Test Dir1';
        objDir.Directory_Code__c='DIR001';
        insert objDir;
        
        Directory_Edition__c objDirE = new Directory_Edition__c();
        objDirE.Name = 'Test DirE1';
        objDirE.Directory__c = objDir.Id;
        objDirE.Letter_Renewal_Stage_1__c = system.today();
        objDirE.Sales_Lockout__c=Date.today().addDays(30);
         objDirE.Pub_Date__c =Date.today().addMonths(2);
        objDirE.book_status__c='NI';
        insert objDirE;
        Directory_Edition__c objDirE1 = new Directory_Edition__c();
        objDirE1.Name = 'Test DirE1';
        objDirE1.Directory__c = objDir.Id;
        objDirE1.Letter_Renewal_Stage_1__c = system.today();
        objDirE1.Sales_Lockout__c=Date.today().addDays(30);
        objDirE1.Pub_Date__c =Date.today().addMonths(3);
        insert objDirE1;        
        
        List<Order_Line_Items__c> oliLst = new List<Order_Line_Items__c>();
        List<Product2> prdList =new List<Product2>();
        Product2 objProd = new Product2();
        objProd.Name = 'Test';
        objProd.Product_Type__c = 'Print';
        objProd.ProductCode = 'WLCSH';
        objProd.Print_Product_Type__c='Display';
        objProd.Family='Print'; 
         objProd.RGU__c='Print';       

         
        insert  objProd;
        Product2 objProd1 = new Product2();
        objProd1.Name = 'Test';
        objProd1.Product_Type__c = 'Digital';
        objProd1.ProductCode = 'GC50';
        objProd1.Print_Product_Type__c='Specialty';
         objProd1.RGU__c='Print';          
        
        insert objProd1;

        
        Order__c objOrder = new Order__c(Account__c=objAccount.id,Billing_Anniversary_Date__c=null);
        insert objOrder ;
    
    
        Order_Group__c objOrderGroup = new Order_Group__c(Order_Account__c=objAccount.id, Order__c=objOrder.id, Opportunity__c=objOpportunity.id,selected__c=true,Order_Set_Selected_Cancel__c =true);
        insert objOrderGroup ;
    
       Order_Line_Items__c objOrderLineItem = new Order_Line_Items__c(Contract_End_Date__c=Date.today().addmonths(3),Billing_Partner_Account__c=objAccount.id,Payment_Method__c='Telco Billing',Parent_ID_of_Addon__c='pkg01',checked__c=true,Account__c=objAccount.Id, Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd.Id,is_p4p__c=false,Directory_Edition__c = objDirE.Id,UnitPrice__c=100,Directory__c=objDir.Id,Payments_Remaining__c =2,Talus_Go_Live_Date__c=date.today(),media_Type__c='Digital');
        Order_Line_Items__c objOrderLineItem1 = new Order_Line_Items__c(Contract_End_Date__c=Date.today().addmonths(3),Billing_Partner_Account__c=objAccount.id,Payment_Method__c='Statement',Parent_ID__c='pkg01',checked__c=true,Account__c=objAccount.Id, Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd1.Id,is_p4p__c=true,Directory_Edition__c = objDirE.Id,UnitPrice__c=100,Last_Billing_Date__c=date.today().addDays(45),Payment_Duration__c=12,Directory__c=objDir.Id,Payments_Remaining__c =2,Talus_Go_Live_Date__c=date.today(),media_Type__c='Digital');
        Order_Line_Items__c objOrderLineItem2 = new Order_Line_Items__c(Contract_End_Date__c=Date.today().addmonths(3),Billing_Partner_Account__c=objAccount.id,Payment_Method__c='Telco Billing',Package_ID__c='pkg01',checked__c=true,Account__c=objAccount1.Id, Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd.Id,is_p4p__c=false,Directory_Edition__c = objDirE.Id,UnitPrice__c=100,Directory__c=objDir.Id,Payments_Remaining__c =2,Talus_Go_Live_Date__c=date.today().addMOnths(2),media_Type__c='NoMedia');
        oliLst.add(objOrderLineItem);
        oliLst.add(objOrderLineItem1);
        oliLst.add(objOrderLineItem2);
        insert oliLst;
            
        Case cancelCase=TestMethodsUtility.generateCancellation();
        cancelCase.AccountID=objAccount.id;
        insert cancelCase;
        Id caseID = cancelCase.Id;
        PageReference pageRef1 = new PageReference('/apex/selectordersetforcancel?caseID='+caseID);                
        Test.setCurrentPage(pageRef1);
        ApexPages.StandardController con = new ApexPages.StandardController(new account());                     
        SelectOrderSetController ctl = new SelectOrderSetController(con); 
        
        Test.startTest();
             ctl.caseID = caseID;
             ctl.selectedCanvass=c.id;
             ctl.OLIIdforcheck=oliLst[0].id;
             ctl.CheckBool=true;
             ctl.fetchOrderSets();
             ctl.ShowSelectOLI(); 
             ctl.OLIs=oliLst;
             ctl.oliNext();
             ctl.CheckBundle();
             //system.assertNotEquals(null,ctl.DoneCancel());
        Test.stopTest();
    } 
    
              
            public static testMethod void SelectOrderSetControllerTest11(){
       Canvass__c c=TestMethodsUtility.createCanvass();
       Test.startTest();
        Account objAccount  = new Account(Primary_Canvass__c = c.Id, Name = 'Test Account', Phone = '(999) 999-9999', Billing_Phone__c='(999) 999-9999', Lead_Source__c = 'Testing', Conversion_Status__c = 'Yes');  
        insert objAccount ;       
        Account objAccount1 = new Account();
        objAccount1.Name = 'Test Account2';
        objAccount1.Phone = '(456)789-3478';
        objAccount1.Open_claim__c = false;

        objAccount1.Delinquency_Indicator__c=false;
        objAccount1.Letter_Renewal_Sequence__c = '1';
        insert objAccount1;
        
        Contact objContact = new contact();
        objContact.LastName = 'Test ';
        objContact.FirstName = 'Contact1';
        objContact.Primary_Contact__c = True;
        objContact.Email = 'test4@csc.com';
        objContact.Contact_Role__c = 'Billing';
        objContact.Phone='(489)456-0987';
        objContact.accountId = objAccount.Id;
        insert objContact;
        
        Opportunity objOpportunity =new Opportunity();
        objOpportunity.Name = 'Test Opportunity1';
        objOpportunity.AccountId = objAccount.Id;
        objOpportunity.Billing_Contact__c = objContact.Id;
        objOpportunity.StageName= 'Proposal Created';
        objOpportunity.CloseDate = System.today();
        objOpportunity.Billing_Partner__c=objAccount.Name;
        objOpportunity.Payment_Method__c='Credit Card';
        objOpportunity.Account_Primary_Canvass__c=c.id;
        insert objOpportunity;
        
        Directory__c objDir = new Directory__c();
        objDir.Name = 'Test Dir1';
        objDir.Directory_Code__c='DIR001';
        insert objDir;
        
        Directory_Edition__c objDirE = new Directory_Edition__c();
        objDirE.Name = 'Test DirE1';
        objDirE.Directory__c = objDir.Id;
        objDirE.Letter_Renewal_Stage_1__c = system.today();
        objDirE.Sales_Lockout__c=Date.today().addDays(30);
        objDirE.book_status__c='NI';
        objDirE.Pub_Date__c =Date.today().addMonths(1);
        insert objDirE;
        Directory_Edition__c objDirE1 = new Directory_Edition__c();
        objDirE1.Name = 'Test DirE1';
        objDirE1.Directory__c = objDir.Id;
        objDirE1.Letter_Renewal_Stage_1__c = system.today();
        objDirE1.Sales_Lockout__c=Date.today().addDays(30);
        objDirE1.Pub_Date__c =Date.today();

        insert objDirE1;        
        
        List<Order_Line_Items__c> oliLst = new List<Order_Line_Items__c>();
        List<Product2> prdList =new List<Product2>();
        Product2 objProd = new Product2();
        objProd.Name = 'Test';
        objProd.Product_Type__c = 'Print';
        objProd.ProductCode = 'WLCSH';
        objProd.Print_Product_Type__c='Display';
        objProd.Family='Print';  
        objProd.RGU__c='Print';     
        insert  objProd;
        
        Product2 objProd1 = new Product2();
        objProd1.Name = 'Test';
        objProd1.Product_Type__c = 'Digital';
        objProd1.ProductCode = 'GC50';
        objProd1.Print_Product_Type__c='Specialty';
         objProd1.RGU__c='Print';          
        insert objProd1;      
        Order__c objOrder = new Order__c(Account__c=objAccount.id,Billing_Anniversary_Date__c=null);
        insert objOrder;
        
        Order_Group__c objOrderGroup = new Order_Group__c(Order_Account__c=objAccount.id, Order__c=objOrder.id, Opportunity__c=objOpportunity.id,selected__c=true,Order_Set_Selected_Cancel__c =true);
        insert objOrderGroup ;
   
        Order_Line_Items__c objOrderLineItem = new Order_Line_Items__c(Contract_End_Date__c=Date.today().addmonths(3),Parent_ID__c='pkg01',checked__c=true,Account__c=objAccount.Id, Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd.Id,is_p4p__c=false,Directory_Edition__c = objDirE.Id,UnitPrice__c=100,Directory__c=objDir.Id,Payments_Remaining__c =2,Talus_Go_Live_Date__c=date.today().addMOnths(4),media_Type__c='Digital',Selected_Cancel__c=false,Package_ID_External__c='pkg45');
        Order_Line_Items__c objOrderLineItem1 = new Order_Line_Items__c(Contract_End_Date__c=Date.today().addmonths(3),Parent_ID_of_Addon__c='pkg01',checked__c=true,Account__c=objAccount.Id, Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd1.Id,is_p4p__c=true,Directory_Edition__c = objDirE.Id,UnitPrice__c=100,Last_Billing_Date__c=date.today().addDays(45),Payment_Duration__c=12,Directory__c=objDir.Id,Payments_Remaining__c =2,Talus_Go_Live_Date__c=date.today().addMOnths(2),media_Type__c='Digital',Selected_Cancel__c=false,Package_ID_External__c='pkg45');
        Order_Line_Items__c objOrderLineItem2 = new Order_Line_Items__c(Contract_End_Date__c=Date.today().addmonths(3),Parent_ID_of_Addon__c='pkg01',checked__c=true,Account__c=objAccount1.Id, Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd.Id,is_p4p__c=false,Directory_Edition__c = objDirE.Id,UnitPrice__c=100,Directory__c=objDir.Id,Payments_Remaining__c =2,Talus_Go_Live_Date__c=date.today().addMOnths(2),media_Type__c='Digital',Selected_Cancel__c=false,Package_ID_External__c='pkg45');
        Order_Line_Items__c objOrderLineItem3 = new Order_Line_Items__c(Contract_End_Date__c=Date.today().addmonths(3),Parent_ID_of_Addon__c='pkg01',checked__c=true,Account__c=objAccount.Id, Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd1.Id,is_p4p__c=true,Directory_Edition__c = objDirE.Id,UnitPrice__c=100,Last_Billing_Date__c=date.today().addDays(45),Payment_Duration__c=12,Directory__c=objDir.Id,Payments_Remaining__c =2,Talus_Go_Live_Date__c=date.today().addMOnths(2),media_Type__c='Print',Selected_Cancel__c=false,Package_ID_External__c='pkg45');
        Order_Line_Items__c objOrderLineItem4 = new Order_Line_Items__c(Contract_End_Date__c=Date.today().addmonths(3),Parent_ID_of_Addon__c='pkg01',checked__c=true,Account__c=objAccount1.Id, Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd.Id,is_p4p__c=false,Directory_Edition__c = objDirE.Id,UnitPrice__c=100,Directory__c=objDir.Id,Payments_Remaining__c =2,Talus_Go_Live_Date__c=date.today().addMOnths(2),media_Type__c='Print',Selected_Cancel__c=false,Package_ID_External__c='pkg45');                
        oliLst.add(objOrderLineItem);
        oliLst.add(objOrderLineItem1);
        oliLst.add(objOrderLineItem2);
        oliLst.add(objOrderLineItem3);
        oliLst.add(objOrderLineItem4);
        insert oliLst;
       //   Test.stopTest();   
        Case cancelCase=TestMethodsUtility.generateCancellation();
        cancelCase.AccountID=objAccount.id;
                
        insert cancelCase;
        Id caseID = cancelCase.Id;
                cancelCase.CheckNewlyCreatedCancellationCase__c=false;
                update cancelCase;
        PageReference pageRef1 = new PageReference('/apex/selectordersetforcancel?caseID='+caseID+'&pagenum=1');                
        Test.setCurrentPage(pageRef1);
        ApexPages.StandardController con = new ApexPages.StandardController(new account());                     
        SelectOrderSetController ctl = new SelectOrderSetController(con); 
       
        
       // Test.startTest();
             ctl.caseID = caseID;
             ctl.selectedCanvass=c.id;
             ctl.OLIIdforcheck=oliLst[2].id;
             ctl.CheckBool=false;
             
             ctl.fetchOrderSets();
             ctl.ShowSelectOLI(); 
             ctl.OLIs=oliLst;
             ctl.oliNext();
            ctl.CheckBundle();
             
              for(Order_Line_Items__c temp :oliLst)
              {
                  temp.Cutomer_Cancel_Date__c = date.today().addDays(2);
              }
              ctl.OLIs = oliLst;           
              ctl.oliCancel();
            //system.assertNotEquals(null,ctl.DoneCancel());
        Test.stopTest();
    }                   
    public static testMethod void SelectOrderSetControllerTest12(){
        Canvass__c c=TestMethodsUtility.createCanvass();
       Test.startTest();
        Account objAccount  = new Account(Primary_Canvass__c = c.Id, Name = 'Test Account', Phone = '(999) 999-9999', Billing_Phone__c='(999) 999-9999', Lead_Source__c = 'Testing', Conversion_Status__c = 'Yes');  
        insert objAccount ;       
        Account objAccount1 = new Account();
        objAccount1.Name = 'Test Account2';
        objAccount1.Phone = '(456)789-3478';
        objAccount1.Open_claim__c = false;

        objAccount1.Delinquency_Indicator__c=false;
        objAccount1.Letter_Renewal_Sequence__c = '1';
        insert objAccount1;
        
        Contact objContact = new contact();
        objContact.LastName = 'Test ';
        objContact.FirstName = 'Contact1';
        objContact.Primary_Contact__c = True;
        objContact.Email = 'test4@csc.com';
        objContact.Contact_Role__c = 'Billing';
        objContact.Phone='(489)456-0987';
        objContact.accountId = objAccount.Id;
        insert objContact;
        
        Opportunity objOpportunity =new Opportunity();
        objOpportunity.Name = 'Test Opportunity1';
        objOpportunity.AccountId = objAccount.Id;
        objOpportunity.Billing_Contact__c = objContact.Id;
        objOpportunity.StageName= 'Proposal Created';
        objOpportunity.CloseDate = System.today();
        objOpportunity.Billing_Partner__c=objAccount.Name;
        objOpportunity.Payment_Method__c='Credit Card';
        objOpportunity.Account_Primary_Canvass__c=c.id;
        insert objOpportunity;
        
        Directory__c objDir = new Directory__c();
        objDir.Name = 'Test Dir1';
        objDir.Directory_Code__c='DIR001';
        insert objDir;
        
        Directory_Edition__c objDirE = new Directory_Edition__c();
        objDirE.Name = 'Test DirE1';
        objDirE.Directory__c = objDir.Id;
        objDirE.Letter_Renewal_Stage_1__c = system.today();
        objDirE.Sales_Lockout__c=Date.today().addDays(30);
        objDirE.book_status__c='NI';
        objDirE.Pub_Date__c =Date.today().addMonths(1);
        insert objDirE;
        Directory_Edition__c objDirE1 = new Directory_Edition__c();
        objDirE1.Name = 'Test DirE1';
        objDirE1.Directory__c = objDir.Id;
        objDirE1.Letter_Renewal_Stage_1__c = system.today();
        objDirE1.Sales_Lockout__c=Date.today().addDays(30);
        objDirE1.Pub_Date__c =Date.today();

        insert objDirE1;        
        
        List<Order_Line_Items__c> oliLst = new List<Order_Line_Items__c>();
        List<Product2> prdList =new List<Product2>();
        Product2 objProd = new Product2();
        objProd.Name = 'Test';
        objProd.Product_Type__c = 'Print';
        objProd.ProductCode = 'WLCSH';
        objProd.Print_Product_Type__c='Display';
        objProd.Family='Print';  
        objProd.RGU__c='Print';     
        insert  objProd;
        
        Product2 objProd1 = new Product2();
        objProd1.Name = 'Test';
        objProd1.Product_Type__c = 'Digital';
        objProd1.ProductCode = 'GC50';
        objProd1.Print_Product_Type__c='Specialty';
         objProd1.RGU__c='Print';          
        insert objProd1;      
        Order__c objOrder = new Order__c(Account__c=objAccount.id,Billing_Anniversary_Date__c=null);
        insert objOrder;
        
        Order_Group__c objOrderGroup = new Order_Group__c(Order_Account__c=objAccount.id, Order__c=objOrder.id, Opportunity__c=objOpportunity.id,selected__c=true,Order_Set_Selected_Cancel__c =true);
        insert objOrderGroup ;
   
        Order_Line_Items__c objOrderLineItem = new Order_Line_Items__c(Contract_End_Date__c=Date.today().addmonths(3),Parent_ID__c='pkg01',checked__c=true,Account__c=objAccount.Id, Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd.Id,is_p4p__c=false,Directory_Edition__c = objDirE.Id,UnitPrice__c=100,Directory__c=objDir.Id,Payments_Remaining__c =2,Talus_Go_Live_Date__c=date.today().addMOnths(4),media_Type__c='Digital',Selected_Cancel__c=false,Package_ID_External__c='pkg45');
        Order_Line_Items__c objOrderLineItem1 = new Order_Line_Items__c(Contract_End_Date__c=Date.today().addmonths(3),Parent_ID_of_Addon__c='pkg01',checked__c=true,Account__c=objAccount.Id, Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd1.Id,is_p4p__c=true,Directory_Edition__c = objDirE.Id,UnitPrice__c=100,Last_Billing_Date__c=date.today().addDays(45),Payment_Duration__c=12,Directory__c=objDir.Id,Payments_Remaining__c =2,Talus_Go_Live_Date__c=date.today().addMOnths(2),media_Type__c='Digital',Selected_Cancel__c=false,Package_ID_External__c='pkg45');
        Order_Line_Items__c objOrderLineItem2 = new Order_Line_Items__c(Contract_End_Date__c=Date.today().addmonths(3),Parent_ID_of_Addon__c='pkg01',checked__c=true,Account__c=objAccount1.Id, Billing_Contact__c=objContact.id, Opportunity__c=objOpportunity.id, Order_Group__c=objOrderGroup .id,Order__c=objOrder .id,Product2__c=objProd.Id,is_p4p__c=false,Directory_Edition__c = objDirE.Id,UnitPrice__c=100,Directory__c=objDir.Id,Payments_Remaining__c =2,Talus_Go_Live_Date__c=date.today().addMOnths(2),media_Type__c='Digital',Selected_Cancel__c=false,Package_ID_External__c='pkg45');              
        oliLst.add(objOrderLineItem);
        oliLst.add(objOrderLineItem1);
        oliLst.add(objOrderLineItem2);
        insert oliLst;
       //   Test.stopTest();   
        Case cancelCase=TestMethodsUtility.generateCancellation();
        cancelCase.AccountID=objAccount.id;
                
        insert cancelCase;
        Id caseID = cancelCase.Id;
                cancelCase.CheckNewlyCreatedCancellationCase__c=false;
                update cancelCase;
        PageReference pageRef1 = new PageReference('/apex/selectordersetforcancel?caseID='+caseID+'&pagenum=1');                
        Test.setCurrentPage(pageRef1);
        ApexPages.StandardController con = new ApexPages.StandardController(new account());                     
        SelectOrderSetController ctl = new SelectOrderSetController(con); 
       
        
       // Test.startTest();
             ctl.caseID = caseID;
             ctl.selectedCanvass=c.id;
             ctl.OLIIdforcheck=oliLst[0].id;
             ctl.CheckBool=false;
             
             ctl.fetchOrderSets();
             ctl.ShowSelectOLI(); 
             ctl.OLIs=oliLst;
             ctl.oliNext();
            ctl.CheckBundle();
             
              for(Order_Line_Items__c temp :oliLst)
              {
                  temp.Cutomer_Cancel_Date__c = date.today().addDays(2);
              }
              ctl.OLIs = oliLst;           
              ctl.oliCancel();
            //system.assertNotEquals(null,ctl.DoneCancel());
        Test.stopTest();
    }       
}