global class AccountValuesPopulationBatch implements Database.Batchable<sObject>{
	
	global Database.QueryLocator start(Database.BatchableContext bc) {
        String migrationUserID = '\''+System.Label.MigrationUserID+'\'';
        String SOQL = 'SELECT Id,Name,Highest_Spend_Canvass__c,(select id,Name,Total_Order_Value__c,Canvass__c from Order_Sets__r) from Account '+
       				   'WHERE CreatedById =  '+ MigrationUserID;
        return Database.getQueryLocator(SOQL);
    }
    
    global void execute(Database.BatchableContext bc, List<Account> accList) {
    	try{
	    	for(Account acc:accList){
	    		//Highest Spend Canvass value population on Account
	    		if(acc.Order_Sets__r!=null && acc.Order_Sets__r.size()>0){
	    			Decimal highestTotal=0;
	    			String reqdCanvassCode=null;
		    		for(Order_Group__c ordSet:acc.Order_Sets__r){
		    			if(ordSet.Total_Order_Value__c>highestTotal){
	    					highestTotal=ordSet.Total_Order_Value__c;
	    					reqdCanvassCode=ordSet.Canvass__c;
	 					}
		    		}
		    		if(reqdCanvassCode!=null){
	    				acc.Highest_Spend_Canvass__c=reqdCanvassCode;
	    			}
	    		}
	    	}
	    	update accList;
    	}catch(Exception e){
            System.debug('The exception is'+e);
            futureCreateErrorLog.createErrorRecordBatch('Error Type : '+e.getTypename()+'. Error Message : '+e.getMessage(), e.getStackTraceString(), 'Batch update of migrated Account');
        }
    }
    
    global void finish(Database.BatchableContext bc) {
        
    }

}