/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class Testvertex_NationalRateCardController_v3 {

    static testMethod void TestGraphicDisplay_Local() 
    {
    Product2 objPGDisp=new Product2 ();
    objPGDisp.Name='GraphincDisp';
    objPGDisp.ProductCode='pro-001';
    objPGDisp.AllowX10Discount__c=true;
    objPGDisp.AllowX15Discount__c=true;
    objPGDisp.Allow_X20_Discount__c=true;
    objPGDisp.AllowX25Discount__c=true;
    objPGDisp.AllowX30Discount__c=true;
    objPGDisp.AllowX35Discount__c=true;
    objPGDisp.AllowX40Discount__c=true;
    objPGDisp.AllowX45Discount__c=true;
    objPGDisp.AllowX50Discount__c=true;
    objPGDisp.AllowX55Discount__c=true;
    objPGDisp.AllowX60Discount__c=true;
    objPGDisp.AllowX65Discount__c=true;
    objPGDisp.AllowX70Discount__c=true;
    objPGDisp.AllowX75Discount__c=true;
    objPGDisp.AllowX80Discount__c=true; 
    objPGDisp.AllowX85Discount__c=true;
    objPGDisp.AllowX90Discount__c=true;
    objPGDisp.AllowX95Discount__c=true;
    objPGDisp.LOY__c=true;
    objPGDisp.Product_Type__c ='Print Graphic';
    objPGDisp.Print_Product_Type__c='Display';
    objPGDisp.Description='WP';
    insert objPGDisp;
    
    Directory__c  objDir=new Directory__c();
    objDir.Name='TestVertex';
    objDir.Directory_Code__c='100';
    objDir.IsCompanion__c=true;
    objDir.National_Rate_Up_for_Companion__c=10.00;
    insert objDir;
    
    Directory_Product_Mapping__c objDPM=new Directory_Product_Mapping__c();
    objDPM.Directory__c=objDir.id;
    objDPM.Product2__c=objPGDisp.id;
    objDPM.is_Active__c=true;
    objDPM.Local_Only__c=true;
    objDPM.National_Only__c=false;
    //objDPM.Grandfathered__c=true;
    objDPM.FullRate__c=500;
    insert objDPM;
    
    vertex_NationalRateCardController_v3  objVNRC=new vertex_NationalRateCardController_v3(new ApexPages.StandardController(objDir));
       
    Apexpages.currentPage().getParameters().put('type','Local');
    objVNRC.decupdateRate=decimal.valueOf('10.00');
    objVNRC.strprintprdtype='Display';
    objVNRC.onLoad(); 
    objVNRC.onLoad_Forpdf();
    objVNRC.AddNewDirectoryProductMapping();
    objVNRC.changeType();
    objVNRC.save();
    objVNRC.Iriccsvfile();
    objVNRC.NationalExport();
    objVNRC.localpdf();
    objVNRC.Nationalpdf();
    objVNRC.exportXLS();
    objVNRC.doCalculaion();
    objVNRC.doCalculaionPerProduct();
     objVNRC.cancel();
     vertex_NationalRateCardController_v3.NationalPriceClass  objNP=new vertex_NationalRateCardController_v3.NationalPriceClass();
    //YP
    
    }
    
    
    
    static testMethod void TestGraphicDisplay_National() 
    {
    Product2 objPGDisp=new Product2 ();
    objPGDisp.Name='GraphincDispl';
    objPGDisp.ProductCode='pro-002';    
    objPGDisp.Product_Type__c ='Print Graphic';
    objPGDisp.Print_Product_Type__c='Display';
    objPGDisp.Description='WP';
    insert objPGDisp;
    
    Directory__c  objDir=new Directory__c();
    objDir.Name='TestVertex';
    objDir.Directory_Code__c='100';
    objDir.IsCompanion__c=true;
    objDir.National_Rate_Up_for_Companion__c=10.00;
    insert objDir;
    
    Directory_Product_Mapping__c objDPM=new Directory_Product_Mapping__c();
    objDPM.Directory__c=objDir.id;
    objDPM.Product2__c=objPGDisp.id;
    objDPM.is_Active__c=true;
    objDPM.Local_Only__c=false;
    objDPM.National_Only__c=true;
    //objDPM.Grandfathered__c=true;
    objDPM.FullRate__c=500;
    insert objDPM;
    
    vertex_NationalRateCardController_v3  objVNRC=new vertex_NationalRateCardController_v3(new ApexPages.StandardController(objDir));
     Apexpages.currentPage().getParameters().put('type','National');
    objVNRC.strprintprdtype='Display';
    objVNRC.decupdateRate=decimal.valueOf('10.00');
    objVNRC.onLoad(); 
    objVNRC.onLoad_Forpdf();
    objVNRC.AddNewDirectoryProductMapping();
    objVNRC.changeType();
    objVNRC.save();
    objVNRC.Iriccsvfile();
    objVNRC.NationalExport();
    objVNRC.localpdf();
    objVNRC.Nationalpdf();
    objVNRC.exportXLS();
    objVNRC.doCalculaion();
    objVNRC.doCalculaionPerProduct();
    objVNRC.cancel();
    
    }
    
  
 
 
  static testMethod void TestGraphicSpeciality_Local() 
    {
    Product2  objPGDSpe=new Product2();
    objPGDspe.Name='Speciality';
    objPGDSpe.ProductCode='pro-003';
    objPGDSpe.Product_Type__c ='Print Graphic';
    objPGDSpe.Print_Product_Type__c='Specialty';
    
    insert objPGDSpe;
    
    Directory__c  objDir=new Directory__c();
    objDir.Name='TestVertex';
    objDir.Directory_Code__c='100';
    objDir.IsCompanion__c=true;
    objDir.National_Rate_Up_for_Companion__c=10.00;
    insert objDir;
    
    Directory_Product_Mapping__c objDPM=new Directory_Product_Mapping__c();
    objDPM.Directory__c=objDir.id;
    objDPM.Product2__c=objPGDspe.id;
    objDPM.is_Active__c=true;
    objDPM.Local_Only__c=true;
    objDPM.National_Only__c=false;
    
    objDPM.FullRate__c=500;
    insert objDPM;
    
    vertex_NationalRateCardController_v3  objVNRC=new vertex_NationalRateCardController_v3(new ApexPages.StandardController(objDir));
     
    Apexpages.currentPage().getParameters().put('type','Local');
     objVNRC.strprintprdtype='Specialty';
     objVNRC.decupdateRate=decimal.valueOf('10.00');
      objVNRC.onLoad();  
    objVNRC.onLoad_Forpdf();
    objVNRC.AddNewDirectoryProductMapping();
    objVNRC.changeType();
    objVNRC.save();
    objVNRC.Iriccsvfile();
    objVNRC.NationalExport();
    objVNRC.localpdf();
    objVNRC.Nationalpdf();
    objVNRC.exportXLS();
    objVNRC.doCalculaion();
    objVNRC.doCalculaionPerProduct();
     objVNRC.cancel();
    //YP
    
    }
     static testMethod void TestGraphicSpeciality_National() 
    {
    Product2  objPGDSpe=new Product2();
    objPGDspe.Name='Speciality';
    objPGDSpe.ProductCode='pro-004';
    objPGDSpe.Product_Type__c ='Print Graphic';
    objPGDSpe.Print_Product_Type__c='Specialty';
    
    insert objPGDSpe;
    
    Directory__c  objDir=new Directory__c();
    objDir.Name='TestVertex';
    objDir.Directory_Code__c='100';
    objDir.IsCompanion__c=true;
    objDir.National_Rate_Up_for_Companion__c=10.00;
    insert objDir;
    
    Directory_Product_Mapping__c objDPM=new Directory_Product_Mapping__c();
    objDPM.Directory__c=objDir.id;
    objDPM.Product2__c=objPGDspe.id;
    objDPM.is_Active__c=true;
    objDPM.Local_Only__c=false;
    objDPM.National_Only__c=true;
    
    objDPM.FullRate__c=500;
    insert objDPM;
    
    vertex_NationalRateCardController_v3  objVNRC=new vertex_NationalRateCardController_v3(new ApexPages.StandardController(objDir));
    objVNRC.onLoad();    
    Apexpages.currentPage().getParameters().put('type','National');
    objVNRC.decupdateRate=decimal.valueOf('10.00');
    objVNRC.onLoad_Forpdf();
    objVNRC.AddNewDirectoryProductMapping();
    objVNRC.changeType();
    objVNRC.save();
    objVNRC.Iriccsvfile();
    objVNRC.NationalExport();
    objVNRC.localpdf();
    objVNRC.Nationalpdf();
    objVNRC.exportXLS();
   objVNRC.doCalculaion();
    objVNRC.doCalculaionPerProduct();
     objVNRC.cancel();
    //YP
    
    }
    
    static testMethod void TestGraphicListing_National() 
    {
     Product2  objPGDSpe=new Product2();
    objPGDspe.Name='Speciality';
    objPGDSpe.ProductCode='pro-005';
    objPGDSpe.Product_Type__c ='Print Graphic';
    objPGDSpe.Print_Product_Type__c='Listing';
    objPGDspe.Description='YP';
    insert objPGDSpe;
    
    Directory__c  objDir=new Directory__c();
    objDir.Name='TestVertex';
    objDir.Directory_Code__c='100';
    objDir.IsCompanion__c=true;
    objDir.National_Rate_Up_for_Companion__c=10.00;
    insert objDir;
    
    Directory_Product_Mapping__c objDPM=new Directory_Product_Mapping__c();
    objDPM.Directory__c=objDir.id;
    objDPM.Product2__c=objPGDspe.id;
    objDPM.is_Active__c=true;
    objDPM.Local_Only__c=false;
    objDPM.National_Only__c=true;
    
    objDPM.FullRate__c=500;
    insert objDPM;
    
    vertex_NationalRateCardController_v3  objVNRC=new vertex_NationalRateCardController_v3(new ApexPages.StandardController(objDir));
    objVNRC.onLoad();    
    Apexpages.currentPage().getParameters().put('type','National');
    objVNRC.decupdateRate=decimal.valueOf('10.00');
    objVNRC.onLoad_Forpdf();
    objVNRC.AddNewDirectoryProductMapping();
    objVNRC.changeType();
    objVNRC.save();
    objVNRC.Iriccsvfile();
    objVNRC.NationalExport();
    objVNRC.localpdf();
    objVNRC.Nationalpdf();
    objVNRC.exportXLS();
   objVNRC.doCalculaion();
    objVNRC.doCalculaionPerProduct();
     objVNRC.cancel();
    //YP
    
    }
    static testMethod void TestGraphicListing_Local() 
    {
     Product2  objPGDSpe=new Product2();
    objPGDspe.Name='Listing';
    objPGDSpe.ProductCode='pro-006';
    objPGDSpe.Product_Type__c ='Print Graphic';
    objPGDSpe.Print_Product_Type__c='Listing';
    objPGDSpe.Description='YP';
    insert objPGDSpe;
    
    Directory__c  objDir=new Directory__c();
    objDir.Name='TestVertex';
    objDir.Directory_Code__c='100';
    objDir.IsCompanion__c=true;
    objDir.National_Rate_Up_for_Companion__c=10.00;
    insert objDir;
    
    Directory_Product_Mapping__c objDPM=new Directory_Product_Mapping__c();
    objDPM.Directory__c=objDir.id;
    objDPM.Product2__c=objPGDspe.id;
    objDPM.is_Active__c=true;
    objDPM.Local_Only__c=true;
    objDPM.National_Only__c=false;
    
    objDPM.FullRate__c=500;
    insert objDPM;
    
    vertex_NationalRateCardController_v3  objVNRC=new vertex_NationalRateCardController_v3(new ApexPages.StandardController(objDir));
    objVNRC.onLoad();  
      
    Apexpages.currentPage().getParameters().put('type','Local');
    objVNRC.decupdateRate=decimal.valueOf('10.00');
    objVNRC.onLoad_Forpdf();
    objVNRC.AddNewDirectoryProductMapping();
    objVNRC.changeType();
    objVNRC.save();
    objVNRC.Iriccsvfile();
    objVNRC.NationalExport();
    objVNRC.localpdf();
    objVNRC.Nationalpdf();
    objVNRC.exportXLS();
   objVNRC.doCalculaion();
    objVNRC.doCalculaionPerProduct();
     objVNRC.cancel();
    //YP
    
    }
    
    static testMethod void TestGraphicInColumn_Local() 
    {
     Product2  objPGDSpe=new Product2();
    objPGDspe.Name='Speciality';
    objPGDSpe.ProductCode='pro-006';
    objPGDSpe.Product_Type__c ='Print Graphic';
    objPGDSpe.Print_Product_Type__c=' In-Column';
    objPGDSpe.Description='YP';
    insert objPGDSpe;
    
    Directory__c  objDir=new Directory__c();
    objDir.Name='TestVertex';
    objDir.Directory_Code__c='100';
    objDir.IsCompanion__c=true;
    objDir.National_Rate_Up_for_Companion__c=10.00;
    insert objDir;
    
    Directory_Product_Mapping__c objDPM=new Directory_Product_Mapping__c();
    objDPM.Directory__c=objDir.id;
    objDPM.Product2__c=objPGDspe.id;
    objDPM.is_Active__c=true;
    objDPM.Local_Only__c=true;
    objDPM.National_Only__c=false;
    
    objDPM.FullRate__c=500;
    insert objDPM;
    
    vertex_NationalRateCardController_v3  objVNRC=new vertex_NationalRateCardController_v3(new ApexPages.StandardController(objDir));
    objVNRC.onLoad();    
    Apexpages.currentPage().getParameters().put('type','Local');
    objVNRC.decupdateRate=decimal.valueOf('10.00');
    objVNRC.onLoad_Forpdf();
    objVNRC.AddNewDirectoryProductMapping();
    objVNRC.changeType();
    objVNRC.save();
    objVNRC.Iriccsvfile();
    objVNRC.NationalExport();
    objVNRC.localpdf();
    objVNRC.Nationalpdf();
    objVNRC.exportXLS();
    objVNRC.doCalculaion();
    objVNRC.doCalculaionPerProduct();
     objVNRC.cancel();
    //YP
    
    }
   static testMethod void TestGraphicInColumn_Natinal() 
    {
     Product2  objPGDSpe=new Product2();
    objPGDspe.Name='Speciality';
    objPGDSpe.ProductCode='pro-006';
    objPGDSpe.Product_Type__c ='Print Graphic';
    objPGDSpe.Print_Product_Type__c=' In-Column';
    objPGDSpe.Description='WP';
    insert objPGDSpe;
    
    Directory__c  objDir=new Directory__c();
    objDir.Name='TestVertex';
    objDir.Directory_Code__c='100';
    objDir.IsCompanion__c=true;
    objDir.National_Rate_Up_for_Companion__c=10.00;
    insert objDir;
    
    Directory_Product_Mapping__c objDPM=new Directory_Product_Mapping__c();
    objDPM.Directory__c=objDir.id;
    objDPM.Product2__c=objPGDspe.id;
    objDPM.is_Active__c=true;
    objDPM.Local_Only__c=false;
    objDPM.National_Only__c=true;
    
    objDPM.FullRate__c=500;
    insert objDPM;
    
    vertex_NationalRateCardController_v3  objVNRC=new vertex_NationalRateCardController_v3(new ApexPages.StandardController(objDir));
    objVNRC.onLoad();    
    Apexpages.currentPage().getParameters().put('type','Local');
    objVNRC.decupdateRate=decimal.valueOf('10.00');
    objVNRC.onLoad_Forpdf();
    objVNRC.AddNewDirectoryProductMapping();
    objVNRC.changeType();
    objVNRC.save();
    objVNRC.Iriccsvfile();
    objVNRC.NationalExport();
    objVNRC.localpdf();
    objVNRC.Nationalpdf();
    objVNRC.exportXLS();
   objVNRC.doCalculaion();
    objVNRC.doCalculaionPerProduct();
     objVNRC.cancel();
    //YP
    
    }
    static testMethod void TestGraphicInColumntra_Natinal() 
    {
     Product2  objPGDSpe=new Product2();
    objPGDspe.Name='Speciality';
    objPGDSpe.ProductCode='pro-006';
    objPGDSpe.Trademark_Product__c ='test';
    objPGDSpe.Print_Product_Type__c=' In-Column';
    objPGDSpe.Description='YP';
    insert objPGDSpe;
    
    Directory__c  objDir=new Directory__c();
    objDir.Name='TestVertex';
    objDir.Directory_Code__c='100';
    objDir.IsCompanion__c=true;
    objDir.National_Rate_Up_for_Companion__c=10.00;
    insert objDir;
    
    Directory_Product_Mapping__c objDPM=new Directory_Product_Mapping__c();
    objDPM.Directory__c=objDir.id;
    objDPM.Product2__c=objPGDspe.id;
    objDPM.is_Active__c=true;
    objDPM.Local_Only__c=false;
    objDPM.National_Only__c=true;
    
    objDPM.FullRate__c=500;
    insert objDPM;
    
    vertex_NationalRateCardController_v3  objVNRC=new vertex_NationalRateCardController_v3(new ApexPages.StandardController(objDir));
    objVNRC.onLoad();    
    Apexpages.currentPage().getParameters().put('type','Local');
    objVNRC.decupdateRate=decimal.valueOf('10.00');
    objVNRC.onLoad_Forpdf();
    objVNRC.AddNewDirectoryProductMapping();
    objVNRC.changeType();
    objVNRC.save();
    objVNRC.Iriccsvfile();
    objVNRC.NationalExport();
    objVNRC.localpdf();
    objVNRC.Nationalpdf();
    objVNRC.exportXLS();
   objVNRC.doCalculaion();
    objVNRC.doCalculaionPerProduct();
     objVNRC.cancel();
    //YP
    
    }
    static testMethod void TestGraphicPLNC_Natinal() 
    {
     Product2  objPGDSpe=new Product2();
    objPGDspe.Name='Speciality';
    objPGDSpe.ProductCode='pro-007';
     objPGDSpe.Product_Type__c = 'Print Listing No Content';
    objPGDSpe.Description='WP';
    insert objPGDSpe;
    
    Directory__c  objDir=new Directory__c();
    objDir.Name='TestVertex';
    objDir.Directory_Code__c='100';
    objDir.IsCompanion__c=true;
    objDir.National_Rate_Up_for_Companion__c=10.00;
    insert objDir;
    
    Directory_Product_Mapping__c objDPM=new Directory_Product_Mapping__c();
    objDPM.Directory__c=objDir.id;
    objDPM.Product2__c=objPGDspe.id;
    objDPM.is_Active__c=true;
    objDPM.Local_Only__c=false;
    objDPM.National_Only__c=true;
    
    objDPM.FullRate__c=500;
    insert objDPM;
    
    vertex_NationalRateCardController_v3  objVNRC=new vertex_NationalRateCardController_v3(new ApexPages.StandardController(objDir));
    objVNRC.onLoad();    
    Apexpages.currentPage().getParameters().put('type','National');
    objVNRC.decupdateRate=decimal.valueOf('10.00');
    objVNRC.onLoad_Forpdf();
    objVNRC.AddNewDirectoryProductMapping();
    objVNRC.changeType();
    objVNRC.save();
    objVNRC.Iriccsvfile();
    objVNRC.NationalExport();
    objVNRC.localpdf();
    objVNRC.Nationalpdf();
    objVNRC.exportXLS();
   objVNRC.doCalculaion();
    objVNRC.doCalculaionPerProduct();
     objVNRC.cancel();
    //YP
    
    }
     static testMethod void TestGraphicPLNC1_Local() 
    {
     Product2  objPGDSpe=new Product2();
    objPGDspe.Name='Speciality';
    objPGDSpe.ProductCode='pro-007';
     objPGDSpe.Product_Type__c = 'Print Listing No Content';
    objPGDSpe.Description='WP';
    insert objPGDSpe;
    
    Directory__c  objDir=new Directory__c();
    objDir.Name='TestVertex';
    objDir.Directory_Code__c='100';
    objDir.IsCompanion__c=true;
    objDir.National_Rate_Up_for_Companion__c=10.00;
    insert objDir;
    
    Directory_Product_Mapping__c objDPM=new Directory_Product_Mapping__c();
    objDPM.Directory__c=objDir.id;
    objDPM.Product2__c=objPGDspe.id;
    objDPM.is_Active__c=true;
    objDPM.Local_Only__c=true;
    objDPM.National_Only__c=false;
    
    objDPM.FullRate__c=500;
    insert objDPM;
    
    vertex_NationalRateCardController_v3  objVNRC=new vertex_NationalRateCardController_v3(new ApexPages.StandardController(objDir));
    objVNRC.onLoad();    
    Apexpages.currentPage().getParameters().put('type','local');
    objVNRC.decupdateRate=decimal.valueOf('10.00');
    objVNRC.onLoad_Forpdf();
    objVNRC.AddNewDirectoryProductMapping();
    objVNRC.changeType();
    objVNRC.save();
    objVNRC.Iriccsvfile();
    objVNRC.NationalExport();
    objVNRC.localpdf();
    objVNRC.Nationalpdf();
    objVNRC.exportXLS();
    objVNRC.doCalculaion();
    objVNRC.doCalculaionPerProduct();
     objVNRC.cancel();
    //YP
    
    }
    
static testMethod void TestGraphicDISPYP_Local() 
    {
     Product2  objPGDSpe=new Product2();
    objPGDspe.Name='Speciality';
    objPGDSpe.ProductCode='pro-007';
    objPGDSpe.Print_Product_Type__c = 'Display';
    objPGDSpe.Trademark_Product__c=null;          
    objPGDSpe.Description='YP';
    insert objPGDSpe;
    
    Directory__c  objDir=new Directory__c();
    objDir.Name='TestVertex';
    objDir.Directory_Code__c='100';
    objDir.IsCompanion__c=true;
    objDir.National_Rate_Up_for_Companion__c=10.00;
    insert objDir;
    
    Directory_Product_Mapping__c objDPM=new Directory_Product_Mapping__c();
    objDPM.Directory__c=objDir.id;
    objDPM.Product2__c=objPGDspe.id;
    objDPM.is_Active__c=true;
    objDPM.Local_Only__c=true;
    objDPM.National_Only__c=false;
    
    objDPM.FullRate__c=500;
    insert objDPM;
    
    vertex_NationalRateCardController_v3  objVNRC=new vertex_NationalRateCardController_v3(new ApexPages.StandardController(objDir));
    objVNRC.onLoad();    
    Apexpages.currentPage().getParameters().put('type','local');
    objVNRC.decupdateRate=decimal.valueOf('10.00');
    objVNRC.onLoad_Forpdf();
    objVNRC.AddNewDirectoryProductMapping();
    objVNRC.changeType();
    objVNRC.save();
    objVNRC.Iriccsvfile();
    objVNRC.NationalExport();
    objVNRC.localpdf();
    objVNRC.Nationalpdf();
    objVNRC.exportXLS();
   objVNRC.doCalculaion();
    objVNRC.doCalculaionPerProduct();
     objVNRC.cancel();
    //YP
    
    }   
    static testMethod void TestGraphicDISPYP_National() 
    {
     Product2  objPGDSpe=new Product2();
    objPGDspe.Name='Speciality';
    objPGDSpe.ProductCode='pro-007';
    objPGDSpe.Print_Product_Type__c = 'Display';
    objPGDSpe.Trademark_Product__c=null;          
    objPGDSpe.Description='YP';
    insert objPGDSpe;
    
    Directory__c  objDir=new Directory__c();
    objDir.Name='TestVertex';
    objDir.Directory_Code__c='100';
    objDir.IsCompanion__c=true;
    objDir.National_Rate_Up_for_Companion__c=10.00;
    insert objDir;
    
    Directory_Product_Mapping__c objDPM=new Directory_Product_Mapping__c();
    objDPM.Directory__c=objDir.id;
    objDPM.Product2__c=objPGDspe.id;
    objDPM.is_Active__c=true;
    objDPM.Local_Only__c=false;
    objDPM.National_Only__c=true;
    
    objDPM.FullRate__c=500;
    insert objDPM;
    
    vertex_NationalRateCardController_v3  objVNRC=new vertex_NationalRateCardController_v3(new ApexPages.StandardController(objDir));
    objVNRC.onLoad();    
    Apexpages.currentPage().getParameters().put('type','National');
    objVNRC.decupdateRate=decimal.valueOf('10.00');
    objVNRC.onLoad_Forpdf();
    objVNRC.AddNewDirectoryProductMapping();
    objVNRC.changeType();
    objVNRC.save();
    objVNRC.Iriccsvfile();
    objVNRC.NationalExport();
    objVNRC.localpdf();
    objVNRC.Nationalpdf();
    objVNRC.exportXLS();
   objVNRC.doCalculaion();
    objVNRC.doCalculaionPerProduct();
     objVNRC.cancel();
    //YP
    
    }   
    
    static testMethod void TestGraphicDISPYPtra_Local() 
    {
     Product2  objPGDSpe=new Product2();
    objPGDspe.Name='Speciality';
    objPGDSpe.ProductCode='pro-007';
    objPGDSpe.Print_Product_Type__c = 'listing';
    objPGDSpe.Trademark_Product__c='trade';
    objPGDSpe.Description='YP';
    insert objPGDSpe;
    
    Directory__c  objDir=new Directory__c();
    objDir.Name='TestVertex';
    objDir.Directory_Code__c='100';
    objDir.IsCompanion__c=true;
    objDir.National_Rate_Up_for_Companion__c=10.00;
    insert objDir;
    
    Directory_Product_Mapping__c objDPM=new Directory_Product_Mapping__c();
    objDPM.Directory__c=objDir.id;
    objDPM.Product2__c=objPGDspe.id;
    objDPM.is_Active__c=true;
    objDPM.Local_Only__c=true;
    objDPM.National_Only__c=false;
    
    objDPM.FullRate__c=500;
    insert objDPM;
    
    vertex_NationalRateCardController_v3  objVNRC=new vertex_NationalRateCardController_v3(new ApexPages.StandardController(objDir));
    objVNRC.onLoad();    
    Apexpages.currentPage().getParameters().put('type','Local');
    objVNRC.decupdateRate=decimal.valueOf('10.00');
    objVNRC.onLoad_Forpdf();
    objVNRC.AddNewDirectoryProductMapping();
    objVNRC.changeType();
    objVNRC.save();
    objVNRC.Iriccsvfile();
    objVNRC.NationalExport();
    objVNRC.localpdf();
    objVNRC.Nationalpdf();
    objVNRC.exportXLS();
   objVNRC.doCalculaion();
    objVNRC.doCalculaionPerProduct();
     objVNRC.cancel();
    //YP
    
    }   
                    
            
     static testMethod void TestGraphicPLNC_Local() 
    {
     Product2  objPGDSpe=new Product2();
    objPGDspe.Name='Speciality';
    objPGDSpe.ProductCode='pro-007';
    objPGDSpe.Product_Type__c = 'Print Listing No Content' ;
    objPGDSpe.Print_Product_Type__c = 'Listing';
    objPGDSpe.Description='WP';
    insert objPGDSpe;
    
    Directory__c  objDir=new Directory__c();
    objDir.Name='TestVertex';
    objDir.Directory_Code__c='100';
    objDir.IsCompanion__c=true;
    objDir.National_Rate_Up_for_Companion__c=10.00;
    insert objDir;
    
    Directory_Product_Mapping__c objDPM=new Directory_Product_Mapping__c();
    objDPM.Directory__c=objDir.id;
    objDPM.Product2__c=objPGDspe.id;
    objDPM.is_Active__c=true;
    objDPM.Local_Only__c=true;
    objDPM.National_Only__c=false;
    
    objDPM.FullRate__c=500;
    insert objDPM;
    
    vertex_NationalRateCardController_v3  objVNRC=new vertex_NationalRateCardController_v3(new ApexPages.StandardController(objDir));
    objVNRC.onLoad();    
    Apexpages.currentPage().getParameters().put('type','Local');
    objVNRC.decupdateRate=decimal.valueOf('10.00');
    objVNRC.onLoad_Forpdf();
    objVNRC.AddNewDirectoryProductMapping();
    objVNRC.changeType();
    objVNRC.save();
    objVNRC.Iriccsvfile();
    objVNRC.NationalExport();
    objVNRC.localpdf();
    objVNRC.Nationalpdf();
    objVNRC.exportXLS();
   objVNRC.doCalculaion();
    objVNRC.doCalculaionPerProduct();
     objVNRC.cancel();
    //YP
    
    }   
    
    static testMethod void TestGraphicPLNC_National() 
    {
     Product2  objPGDSpe=new Product2();
    objPGDspe.Name='Speciality';
    objPGDSpe.ProductCode='pro-007';
    objPGDSpe.Product_Type__c = 'Print Listing No Content' ;
    objPGDSpe.Print_Product_Type__c = 'Listing';
    objPGDSpe.Description='WP';
    insert objPGDSpe;
    
    Directory__c  objDir=new Directory__c();
    objDir.Name='TestVertex';
    objDir.Directory_Code__c='100';
    objDir.IsCompanion__c=true;
    objDir.National_Rate_Up_for_Companion__c=10.00;
    insert objDir;
    
    Directory_Product_Mapping__c objDPM=new Directory_Product_Mapping__c();
    objDPM.Directory__c=objDir.id;
    objDPM.Product2__c=objPGDspe.id;
    objDPM.is_Active__c=true;
    objDPM.Local_Only__c=false;
    objDPM.National_Only__c=true;
    
    objDPM.FullRate__c=500;
    insert objDPM;
    
    vertex_NationalRateCardController_v3  objVNRC=new vertex_NationalRateCardController_v3(new ApexPages.StandardController(objDir));
    objVNRC.onLoad();    
    Apexpages.currentPage().getParameters().put('type','National');
    objVNRC.decupdateRate=decimal.valueOf('10.00');
    objVNRC.onLoad_Forpdf();
    objVNRC.AddNewDirectoryProductMapping();
    objVNRC.changeType();
    objVNRC.save();
    objVNRC.Iriccsvfile();
    objVNRC.NationalExport();
    objVNRC.localpdf();
    objVNRC.Nationalpdf();
    objVNRC.exportXLS();
   objVNRC.doCalculaion();
    objVNRC.doCalculaionPerProduct();
     objVNRC.cancel();
    //YP
    
    }   
    
    static testMethod void TestGraphicTrade_National() 
    {
     Product2  objPGDSpe=new Product2();
    objPGDspe.Name='Speciality';
    objPGDSpe.ProductCode='pro-007';
    objPGDSpe.Print_Product_Type__c ='In-Column';
    objPGDSpe.Trademark_Product__c = null;
    objPGDSpe.Description='YP';
    insert objPGDSpe;
    
    Directory__c  objDir=new Directory__c();
    objDir.Name='TestVertex';
    objDir.Directory_Code__c='100';
    objDir.IsCompanion__c=true;
    objDir.National_Rate_Up_for_Companion__c=10.00;
    insert objDir;
    
    Directory_Product_Mapping__c objDPM=new Directory_Product_Mapping__c();
    objDPM.Directory__c=objDir.id;
    objDPM.Product2__c=objPGDspe.id;
    objDPM.is_Active__c=true;
    objDPM.Local_Only__c=false;
    objDPM.National_Only__c=true;
    
    objDPM.FullRate__c=500;
    insert objDPM;
    
    vertex_NationalRateCardController_v3  objVNRC=new vertex_NationalRateCardController_v3(new ApexPages.StandardController(objDir));
    objVNRC.onLoad();    
    Apexpages.currentPage().getParameters().put('type','National');
    objVNRC.decupdateRate=decimal.valueOf('10.00');
    objVNRC.onLoad_Forpdf();
    objVNRC.AddNewDirectoryProductMapping();
    objVNRC.changeType();
    objVNRC.save();
    objVNRC.Iriccsvfile();
    objVNRC.NationalExport();
    objVNRC.localpdf();
    objVNRC.Nationalpdf();
    objVNRC.exportXLS();
   objVNRC.doCalculaion();
    objVNRC.doCalculaionPerProduct();
     objVNRC.cancel();
    //YP
    
    }    
     
     static testMethod void TestGraphicTradenotnull_National() 
    {
     Product2  objPGDSpe=new Product2();
    objPGDspe.Name='Speciality';
    objPGDSpe.ProductCode='pro-007';
    objPGDSpe.Print_Product_Type__c ='listing';
    objPGDSpe.Trademark_Product__c = 'test';
    objPGDSpe.Description='YP';
    insert objPGDSpe;
    
    Directory__c  objDir=new Directory__c();
    objDir.Name='TestVertex';
    objDir.Directory_Code__c='100';
    objDir.IsCompanion__c=true;
    objDir.National_Rate_Up_for_Companion__c=10.00;
    insert objDir;
    
    Directory_Product_Mapping__c objDPM=new Directory_Product_Mapping__c();
    objDPM.Directory__c=objDir.id;
    objDPM.Product2__c=objPGDspe.id;
    objDPM.is_Active__c=true;
    objDPM.Local_Only__c=false;
    objDPM.National_Only__c=true;
    
    objDPM.FullRate__c=500;
    insert objDPM;
    
    vertex_NationalRateCardController_v3  objVNRC=new vertex_NationalRateCardController_v3(new ApexPages.StandardController(objDir));
    objVNRC.onLoad();    
    Apexpages.currentPage().getParameters().put('type','National');
    objVNRC.decupdateRate=decimal.valueOf('10.00');
    objVNRC.onLoad_Forpdf();
    objVNRC.AddNewDirectoryProductMapping();
    objVNRC.changeType();
    objVNRC.save();
    objVNRC.Iriccsvfile();
    objVNRC.NationalExport();
    objVNRC.localpdf();
    objVNRC.Nationalpdf();
    objVNRC.exportXLS();
   objVNRC.doCalculaion();
    objVNRC.doCalculaionPerProduct();
     objVNRC.cancel();
    //YP
    
    }     
      
               
    
    }