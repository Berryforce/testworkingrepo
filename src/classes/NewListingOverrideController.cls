Public class NewListingOverrideController{
    public Pagereference onLoad(){
         Set<ID> accID = new Set<ID>();
         PageReference newPage = new PageReference('/a52/e?');
             for(String keyValue : ApexPages.currentPage().getParameters().keySet()){
                 if(!keyValue.contains('override') ){
                    newPage.getParameters().put(keyValue, ApexPages.currentPage().getParameters().get(keyValue));
                 }
             }
             ID accountID = ApexPages.currentPage().getParameters().get('CF00NG000000D3Vcd_lkid');
             if(accountID != null){
                 accID.add(accountID);
                 map<ID, Account> mapAccount = AccountSOQLMethods.getAccountContactByAccountID(accID);
                 newPage.getParameters().put('00NG000000FBzTx',mapAccount.get(accountID).Name);
             }
             newPage.getParameters().remove('save_new');
             newPage.getParameters().put('Name','DO NOT EDIT-Populate Full Last Name/Business Name field');
             newPage.getParameters().put('nooverride','1');
         return newPage.setredirect(true);
     }
    public NewListingOverrideController(ApexPages.StandardController controller){
        
    }

}