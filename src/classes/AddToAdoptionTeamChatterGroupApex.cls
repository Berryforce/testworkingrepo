global class AddToAdoptionTeamChatterGroupApex {
    @future
    public static void AddUserToGroup(List<ID> UserIds) {
        try {
            List<CollaborationGroupMember> cgm = new List<CollaborationGroupMember>();                      
            Id cgID = [ Select Id 
                        FROM CollaborationGroup 
                        WHERE Name = 'Adoption Team' LIMIT 1 ].ID;
           for ( Id UserId : UserIds ) {
               cgm.add(new CollaborationGroupMember (CollaborationGroupId = cgID, MemberId = UserId));   
           }
           insert cgm;
        } catch (QueryException qe) {
            System.debug('QueryException in AddToAdoptionTeamChatterGroupApex.AddUserToGroup is :' + qe);  
        } catch (Exception ex) {
            System.debug('Exception in AddToAdoptionTeamChatterGroupApex.AddUserToGroup is :' + ex);
        }    
    }
}