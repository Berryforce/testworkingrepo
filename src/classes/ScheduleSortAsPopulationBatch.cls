global class ScheduleSortAsPopulationBatch implements Schedulable {
    global void execute(SchedulableContext SC) {
        SortAsPopulationBatch obj = new SortAsPopulationBatch(null,null,null,null);
        Database.executeBatch(obj);
    }
}