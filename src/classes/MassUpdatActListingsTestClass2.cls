@IsTest
public class MassUpdatActListingsTestClass2 {
    public static testmethod void MassUpdatListings2Test (){
        
        Canvass__c objCanvass = TestMethodsUtility.createCanvass();
       
        Account objAccount = TestMethodsUtility.generateAccount();
        insert objAccount;
        
        Telco__c objTelco = TestMethodsUtility.createTelco(objAccount.Id);
        Listing__c objListing = new Listing__c();
            objListing.Name = 'test listing name121';
            objListing.Bus_Res_Gov_Indicator__c = 'Business';
            objListing.Phone__c = '(414) 068-1589';
            objListing.Listing_Street__c = 'Test st123';
            objListing.Listing_City__c = 'Texas';
            objListing.Listing_State__c = 'TX';
            objListing.Listing_Country__c = 'USA';
            objListing.Account__c = objAccount.Id;
            objListing.Listing_Street_Number__c = '12345';
            objListing.Listing_PO_Box__c = '123456';
            objListing.Changes_Authorized_By__c = 'John Doe';
            objListing.Customer_Advised_Charges_May_Apply__c = false;
            objListing.Service_Order__c= '9087';
            insert objListing;
            
            objListing.Listing_Street__c = 'Test st1234';
            objListing.Listing_City__c = 'Texas';
            objListing.Listing_State__c = 'CA';
            objListing.Listing_Country__c = 'USA';
            objListing.Changes_Authorized_By__c = 'Jane Doe';
            objListing.Customer_Advised_Charges_May_Apply__c = true;
            //objListing.Telco_Provider__c = objTelco.Id;
            update objListing;
       
       Listing__c objListing1 = new Listing__c();
            objListing1.Name = 'test listing name0307';
            objListing1.Bus_Res_Gov_Indicator__c = 'Business';
            objListing1.Phone__c = '(441) 068-1908';
            objListing1.Listing_Street__c = 'Test st1234';
            objListing1.Listing_City__c = 'Texas';
            objListing1.Listing_State__c = 'TX';
            objListing1.Listing_Country__c = 'USA';
            objListing1.Account__c = objAccount.Id;
            objListing1.Listing_Street_Number__c = '12897';
            objListing1.Listing_PO_Box__c = '12908';
            objListing1.Changes_Authorized_By__c = 'John Doe';
            objListing1.Customer_Advised_Charges_May_Apply__c = false;
            objListing.Service_Order__c= '9088';
            insert objListing1;
            
            objListing1.Listing_Street__c = '';
            objListing1.Listing_City__c = '';
            objListing1.Listing_State__c = '';
            objListing1.Listing_Country__c = '';
            //objListing1.Telco_Provider__c = objTelco.Id;
        
        Test.startTest(); 
       
        ApexPages.StandardController sc = new ApexPages.standardController(new Account());    
        ApexPages.currentPage().getParameters().put('Id',objAccount.Id);
        
        MassUpdateActListings2 mupLst = new MassUpdateActListings2(sc);
        MassUpdateActListings2.WrapData wrap = new MassUpdateActListings2.WrapData();
        wrap.ischecked = true;
        wrap.objL = objListing;
        List<MassUpdateActListings2.WrapData> lstW = new List<MassUpdateActListings2.WrapData>();
        lstW.add(wrap);
        
        mupLst.listingActLst = new List<MassUpdateActListings2.WrapData>();
        mupLst.listingActLst.addall(lstW);
        mupLst.listing = new Listing__c();
        mupLst.listing = objListing;
        mupLst.listingsData();
        try{
            mupLst.SaveSelected();
        }
        catch(Exception e){}
        mupLst.redirect();
        
        Test.stopTest();
    }
}