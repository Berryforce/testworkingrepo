@isTest(seealldata=true)
public with sharing class MultiScopingBatchTest {
    static testMethod void testMultiScoping() {
        Account acct = TestMethodsUtility.createAccount('telco');
        Telco__c telco = TestMethodsUtility.createTelco(acct.Id);
        Directory__c objDir = TestMethodsUtility.generateDirectory();
        objDir.Telco_Provider__c = telco.Id;
        insert objDir;

        Directory_Section__c objDS = TestMethodsUtility.createDirectorySection(objDir);
        MultiScopingBatchCallController obj = new MultiScopingBatchCallController();
        obj.callMultiScopeBatch();
        obj.pollingBatchProcess();
        Multi_Scoping_Header__c objMSH = TestMethodsUtility.createMultiHead(objDir.Id, objDS.Id);
        objMSH = [SELECT Id, MSH_Area_Code__c, MSH_Exchange_Code__c FROM Multi_Scoping_Header__c WHERE Id =: objMSH.Id];
        Multi_Scoping_Child__c objMSC = TestMethodsUtility.createMultiChild(objDir.Id, objDS.Id, objMSH.Id);
        Listing__c listing = TestMethodsUtility.createListing('Main Listing');
        Directory_Listing__c objSL = TestMethodsUtility.generateDirectoryListing('Address Override RT');
        objSL.Listing__c = listing.Id;
        objSL.Phone_Number__c = '(' + objMSH.MSH_Area_Code__c + ') ' + objMSH.MSH_Exchange_Code__c + '-8888';
        objSL.Bus_Res_Gov_Indicator__c = 'Business';
        objSL.Disconnected__c = false;
        objSL.Directory_Section__c = objDS.Id;
        objSL.Directory__c = objDir.Id;
        objSL.Telco_Provider__c = telco.Id;
        insert objSL;
        MultiScopingBasedOnDirectory.callMultiScopBatch(objDir.Id);
    }
}