@IsTest
public class CreateNOSOListingfromLeadTestClass{
	private static Id LeadId;
	  
	static testmethod void CreateNOSOListingfromLead(){
		set<Id> setLeadId = new set<Id>();
		Canvass__c objCanvass = new Canvass__c(Name = '123678');
		insert objCanvass;
		    
		Lead objLead = new Lead(FirstName = 'Test24', LastName = 'Test Unit12', Company = 'Test Berry12', Primary_Canvass__c = objCanvass.Id, Phone = '9998877009', Status = 'Open',City='Texas',Country='US',State='TX',Create_Listing__c='Yes',street='st 12');
		insert objLead;
		LeadId = objLead.Id;
		set<Id> convertedLeadId = new set<Id>();
		Database.LeadConvert lc = new database.LeadConvert();
		lc.setLeadId(LeadId);
		lc.setDoNotCreateOpportunity(true);
		lc.setConvertedStatus('Qualified');
		       
		//LeadStatus convertStatus = [Select Id, MasterLabel from LeadStatus where IsConverted=true limit 1];
		//lc.setConvertedStatus(convertStatus.MasterLabel);
		        
		Database.LeadConvertResult lcr = Database.convertLead(lc);
		system.debug('***********Converted Lead Id***********'+lcr.getLeadID());
		System.assert(lcr.isSuccess());
		convertedLeadId.add(lcr.getLeadID());
		        
		//convertedLeadId.add(lc.Id);
		system.debug('***********Converted Lead Id***********'+convertedLeadId);
		CreateNOSOListingfromLead.createNosoListing(convertedLeadId);
	     
	}
	//CreateNOSOListingfromLead.createNosoListing(convertedLeadId);
}