@isTest(seeAllData=True)
private class BugsandEnhancementsSearchContv3_Test {

    
    static testMethod void testSearch() {
    
        RecordType r = [select id from recordtype where name='Bug' limit 1];
        case cs= new case();
        cs.RecordTypeId=r.id;
        insert cs;
        BugsandEnhancementsSearchContv3 bugAndEnhanceSearch = new BugsandEnhancementsSearchContv3(new ApexPages.StandardController(cs));
        bugAndEnhanceSearch.searchstring='1';
        string searchquery='select Id, CaseNumber, Status, RecordType.Name, JIRA_Assigned_To__c, CreatedBy.Alias, CreatedDate, Priority, JIRA_Link__c, Environment_and_Application__c, Case_Category__c, Subject, Description, Opened_By_Person_s_Email__c from Case where RecordType.Name IN (\'Bug\', \'Enhancement\') AND ((CaseNumber like \'%' + String.escapeSingleQuotes(bugAndEnhanceSearch.searchstring) + '%\') OR (Opened_By_Person_s_Email__c like \'%' + String.escapeSingleQuotes(bugAndEnhanceSearch.searchstring) + '%\')) Order By CaseNumber desc Limit 1';  
        cs = Database.query(searchquery);
        bugAndEnhanceSearch.search(); 
        bugAndEnhanceSearch.clear();
    }
}