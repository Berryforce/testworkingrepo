global class M33StatusUpdateWebService_V1 {

    //Webservice returned data
    global class M33StatusUpdateResponse {
        webservice  Boolean success;
        webservice  String warning;
        webservice  String error;
        
        public M33StatusUpdateResponse() {
            success = false;
            warning = '';
            error = '';
        }
    }
	
	//Modified by Sathish on 3/23/2015 - ATP-3992
    webService static M33StatusUpdateResponse updateStatus(String sfID, String m33UrnText, String newStatus, String newMilesURL ) {
        M33StatusUpdateResponse response = New M33StatusUpdateResponse();
        try {
			String strObject = '';
			String strPrefix = String.valueOf(sfID).substring(0,3);
			if(strPrefix.equals(system.label.New_Spec_Art)) {
				list<Spec_Art_Request__c> lstSpecArt = [Select Id, Miles_Status__c, Spec_Art_URL__c, Date_Received__c, Date_Graphics_Complete__c,Name from Spec_Art_Request__c where Id =:sfID OR Name =:m33UrnText];
				if(lstSpecArt.size() > 0) {
					if(lstSpecArt.size() > 1) {
						response.warning = 'More than one Spec Art Request returned for urn [' +  m33UrnText + ']';
					}
					for (Spec_Art_Request__c iterator : lstSpecArt) {
						iterator.Miles_Status__c = newStatus;
						if(iterator.Miles_Status__c.equals('In Progress')) {
							iterator.Date_Received__c = system.today();
						}
						if(iterator.Miles_Status__c.equals('Complete')) {
							iterator.Date_Graphics_Complete__c = system.today();
							iterator.Spec_Art_URL__c = 'http://'+system.label.Graphics_IP_Address+'/Prod_Output/'+iterator.Name.Right(4)+'/'+iterator.Name+'.pdf';
						}
					}
					update lstSpecArt;
					response.success = true;
				}
				else {
					response.error = 'Spec Art Request not found by id [' + sfID + '] or urn [' + m33UrnText + ']';
				}
			}
			else if(strPrefix.equals(system.label.YPC_Prefix)) {
				map<ID, Digital_Product_Requirement__c> mapDFF = new map<ID, Digital_Product_Requirement__c>();
				list<YPC_Graphics__c> lstYPC = [Select Id, Miles_Status__c, YPC_Graphics_URL__c, DFF__c, LoResCopy__c, DFF__r.DFF_Cancel_Status__c from YPC_Graphics__c where Id =:sfID OR Name =:m33UrnText];
				if(lstYPC.size() > 0) {
					if(lstYPC.size() > 1) {
						response.warning = 'More than one YPC Graphics art returned for id [' +  sfID + ']';
					}
					set<Id> setDFFId = new set<Id>();
					list<YPC_Graphics__c> lstUpdateYPC = new list<YPC_Graphics__c>();
					for (YPC_Graphics__c iterator : lstYPC) {
						if(iterator.DFF__r.DFF_Cancel_Status__c == false) {
							iterator.Miles_Status__c = newStatus;
							if(iterator.Miles_Status__c == 'Complete') {
								iterator.YPC_Graphics_URL__c = iterator.LoResCopy__c;
								setDFFId.add(iterator.DFF__c);
							}
							lstUpdateYPC.add(iterator);
							system.debug('YPC URL : ' + iterator.YPC_Graphics_URL__c);
							system.debug('YPC URL : ' + iterator.LoResCopy__c);
						}
					}
					update lstUpdateYPC;
					if(setDFFId.size() > 0) {
						system.debug('WS : '+ setDFFId);
						list<Digital_Product_Requirement__c> lstDFF = [Select Id, Miles_Status__c, Graphic_Pao_Url__c, Logo_Graphic_Url__c, Diamond_Graphic_URL__c, Banner_Url__c, Diamond_Graphic_Dist_URL__c, Banner_Dist_Url__c, Logo_Graphic_Dist_Url__c , (Select Id, DFF__c, Miles_Status__c, YPC_Graphics_Type__c, YPC_Graphics_URL__c From YPC_Graphics__r) From Digital_Product_Requirement__c where ID IN:setDFFId];
						if(lstDFF.size() > 0) {
							boolean bFlag = false;
							system.debug('WS : '+ lstDFF.size());
							for(Digital_Product_Requirement__c iterator : lstDFF) {
								set<Id> setYPCCount = new set<Id>();
								for(YPC_Graphics__c iteratorYPC : iterator.YPC_Graphics__r) {
									system.debug('WS : '+ iteratorYPC.Miles_Status__c);
									if(iteratorYPC.Miles_Status__c == 'Complete') {
										setYPCCount.add(iteratorYPC.Id);
										bFlag = true;
										if(iteratorYPC.YPC_Graphics_Type__c != null && iteratorYPC.YPC_Graphics_Type__c.contains('PAO'))
					                    	iterator.Graphic_Pao_Url__c = iteratorYPC.YPC_Graphics_URL__c;
					                    if(iteratorYPC.YPC_Graphics_Type__c != null && iteratorYPC.YPC_Graphics_Type__c =='Logo' )
					                    	iterator.Logo_Graphic_Url__c = iteratorYPC.YPC_Graphics_URL__c;
					                    if(iteratorYPC.YPC_Graphics_Type__c != null && iteratorYPC.YPC_Graphics_Type__c =='Diamond Logo')    
					                    	iterator.Diamond_Graphic_URL__c = iteratorYPC.YPC_Graphics_URL__c;
					                    if(iteratorYPC.YPC_Graphics_Type__c != null && iteratorYPC.YPC_Graphics_Type__c =='Banner') 
					                    	iterator.Banner_Url__c = iteratorYPC.YPC_Graphics_URL__c;
					                    if(iteratorYPC.YPC_Graphics_Type__c != null && iteratorYPC.YPC_Graphics_Type__c.contains('Diamond Distribution'))
					                    	iterator.Diamond_Graphic_Dist_URL__c = iteratorYPC.YPC_Graphics_URL__c;    
					                    if(iteratorYPC.YPC_Graphics_Type__c != null && iteratorYPC.YPC_Graphics_Type__c == 'Banner Distribution')
					                    	iterator.Banner_Dist_Url__c = iteratorYPC.YPC_Graphics_URL__c;
					                    if(iteratorYPC.YPC_Graphics_Type__c != null && iteratorYPC.YPC_Graphics_Type__c =='Logo Distribution')
					                    	iterator.Logo_Graphic_Dist_Url__c = iteratorYPC.YPC_Graphics_URL__c;
									}
								}
								if(iterator.YPC_Graphics__r.size() == setYPCCount.size()) {
									iterator.Miles_Status__c = 'Complete';
								}
								else {
									iterator.Miles_Status__c = 'In Progress';
								}
							}
							system.debug('WS : '+ bFlag);
							if(bFlag) {
								update lstDFF;
							}
						}
					}
					response.success = true;
				}
				else {
					response.error = 'YPC Graphics art not found by id [' + sfID + '] or urn [' + m33UrnText + ']';
				}
			}
			else {
				list<String> lstURN = String.valueOf(sfID).split(';');
				set<String> setEditionCode = new set<String>();
				set<String> setURNNumber = new set<String>();
				for(String iterator : lstURN) {
					String[] strSplit = iterator.split('-');
					setEditionCode.add(strSplit[0]);
					setURNNumber.add(strSplit[1]);
				}
				
				if(setEditionCode.size() > 0) {
					list<Digital_Product_Requirement__c> lstDFF = [Select Id, URN_Number__c, Miles_Status__c, Print_Ad_URL__c, DFF_Cancel_Status__c from Digital_Product_Requirement__c where Directory_Edition__r.Edition_Code__c IN:setEditionCode AND Order_URN__c IN:setURNNumber];
					if(lstDFF.size() > 0) {
						list<Digital_Product_Requirement__c> lstUpdateDFF = new list<Digital_Product_Requirement__c>();
						for(Digital_Product_Requirement__c iterator : lstDFF) {
							if(iterator.DFF_Cancel_Status__c == false) {
								iterator.Miles_Status__c = newStatus;
								if(iterator.Miles_Status__c.equals('Complete') || iterator.Miles_Status__c.equals('Paper Proof') || iterator.Miles_Status__c.equals('Web Proof')) {
									iterator.Graphics_Complete_Date__c = System.today();
								}
								if(iterator.Miles_Status__c.equals('In Progress')) {
									iterator.Date_Graphics_Received__c = System.today();
								}
								String strURN = String.valueOf(iterator.URN_Number__c);
								iterator.Print_Ad_URL__c = 'http://'+system.label.Graphics_IP_Address+'/Prod_Output/'+strURN.Right(4)+'/'+strURN+'.pdf';
								lstUpdateDFF.add(iterator);
							}
						}
						update lstUpdateDFF;
						response.success = true;
					}
					else {
						response.error = 'Product not found by id [' + sfID + '] or urn [' + m33UrnText + ']';
					}
				}
				else {
					response.error = 'Product not found by id [' + sfID + '] or urn [' + m33UrnText + ']';
				}
			}
        }
        catch(Exception e) {
            response.error = 'Exception error: ' + e.getMessage() + ' Line number: ' + e.getLineNumber() + ' Stack trace: ' + e.getStackTraceString();           
        }       
        return response;
    }
}