public with sharing class OrderGroupSOQLMethods {    
    
    public static list<Order_Group__c> getOrderGroupByOpptyID(Set<Id> opptyIDs) {
        return [Select Id, Name, oli_count__c, CreatedDate, selected__c, Order_Account__c,  
                Opportunity__c From Order_Group__c where Opportunity__c IN:opptyIDs];
    }
    
 public static list<Order_Group__c> getOrderGroupByOpptyIDAndAccountId(Set<Id> opptyIDs, Set<Id> acctIds) {
        return [SELECT Id, Name, oli_count__c, CreatedDate, selected__c, Order_Account__c,  
                Opportunity__c,OS_Directory_Editions__c FROM Order_Group__c WHERE Opportunity__c IN:opptyIDs AND Order_Account__c IN:acctIds];
    }

    
    public static Order_Group__c getOrderGroupByOrderSetID(Id orderSetId) {
        return [Select Id, Name, oli_count__c, CreatedDate, selected__c, Order_Account__c,  
                Opportunity__c From Order_Group__c where Id =: orderSetId];
    }
    
    public static List<Order_Group__c> getOrderGroupByOrderSetIDs(Set<Id> setOrderGroupIds) {
        return [SELECT Id, Name, oli_count__c, CreatedDate, selected__c, Order_Account__c, OS_Directory_Editions__c,   
                Opportunity__c FROM Order_Group__c WHERE Id =: setOrderGroupIds];
    }
    public static Map<Id,Order_Group__c> getOrderGroupByAcctIdOpptyCanvassId(Id acctId, String canvassId) {
        return new Map<Id,Order_Group__c>([SELECT InvoiceGenerationStatus__c, Id, Type__c, CreatedDate, oli_count__c, Opportunity__c, 
                (SELECT Id,Billing_Partner__c,Billing_Partner_Account__c,Billing_Frequency__c,Parent_ID__c,Parent_ID_of_Addon__c,Order_Group__c,Name,UnitPrice__c,isCanceled__c,UDAC__c,Edition_Code__c, Directory_Edition_Name__c,Media_Type__c,Directory__c,Directory_Edition__c, 
                Canvass__c, Account__c,Directory_Edition__r.Name,Product2__r.Name,Directory_Edition__r.Book_Status__c,Directory_Edition__r.Ship_Date__c,Cutomer_Cancel_Date__c,
                Geo_Type__c,ListPrice__c,Digital_Product_Requirement__c,Geo_Code__c,Billing_End_Date__c,Billing_Start_Date__c,
                Payment_Method__c,Parent_Line_Item__c,Billing_Contact__c,Quantity__c,Discount__c,Status__c,Description__c,
                Effective_Date__c,Directory_Heading__r.Directory_Heading_Name__c,Payment_Duration__c,Product2__c,Directory_Heading__c,Opportunity__c,Product_Type__c,
                Package_ID__c,Directory_Section__c,Listing__c,Package_ID_External__c,Is_Caption__c,Is_Child__c,Is_P4P__c,
                ProductCode__c,Scope__c,Locality__c,Category__c,Pricing_Program__c,Directory__r.Companion_Book__c,Directory__r.Companion_Directory__c From Order_Line_Items__r),
                (select id,Parent_ID__c,Parent_ID_of_Addon__c from Modification_Order_Line_Items__r where Inactive__c=false and Completed__c=false) 
                From Order_Group__c WHERE Order_Account__c = : acctId AND Opportunity__r.Account_Primary_Canvass__c = : canvassId]);  
    }
    public static list<Order_Group__c> getOrderGroupByAcctIdOpptyCanvassIdForBillingTransfer(Id acctId, String canvassId) {
        return [SELECT InvoiceGenerationStatus__c, Id, Type__c, CreatedDate, oli_count__c, Opportunity__c, 
                (SELECT Id,Canvass__r.Billing_Entity__c, Order_Group__c, Edition_Code__c, Directory_Edition_Name__c, Canvass__r.Name, Canvass__r.Canvass_Code__c,Directory_Edition__r.directory__c,Media_Type__c From Order_Line_Items__r where isCanceled__c=False) 
                From Order_Group__c WHERE Order_Account__c = : acctId AND Opportunity__r.Account_Primary_Canvass__c = : canvassId];
    }    
     
    /*public static list<Order_Group__c> getOrderGroupWithOLIByAccountIds(set<Id> setAccIds) {
        return [SELECT Id, Name, Order_Account__c, (SELECT Id,Name, Order__c, Order__r.Account__c, Canvass__c, Description__c , Directory_Code__c, 
                Directory__c, Directory_Edition__c,Discount__c,Fulfilled_on__c, FulfillmentDate__c, ListPrice__c, P4P_Current_Billing_Clicks_Leads__c, 
                Canvass__r.Canvass_Code__c, Package_ID__c, Order_Group__r.Order_Account__c,
                Opportunity__c, Opportunity__r.Name, Product2__c, Product2__r.Family, Product2__r.RGU__c, Media_Type__c, P4P_Price_Per_Click_Lead__c, 
                ProductCode__c, Quantity__c, Sent_to_fulfillment_on__c, UnitPrice__c, Billing_Contact__c, BillingChangesPayment__c, Account__c,                
                Order_Group__r.Order_Account__r.Digital_Product_Involuntary_Cancelled__c, Last_Billing_Date__c, Next_Billing_Date__c, Cancelation_Fee__c,
                Edition_Code__c, Order_Anniversary_Start_Date__c,Billing_End_Date__c, Billing_Frequency__c, Total_Billing_Amount_Transfered__c,
                Billing_Partner__c, Billing_Start_Date__c, Telco_Invoice_Date__c, Print_First_Bill_Date__c, OriginalBillingDate__c, 
                Digital_Product_Requirement__c,Payment_Method__c, Payments_Remaining__c, Successful_Payments__c, Payment_Duration__c,Service_Start_Date__c,
                Service_End_Date__c, IsCanceled__c, Talus_Go_Live_Date__c, Talus_Fulfillment_Date__c,Current_Daily_Prorate__c,Quote_Signed_Date__c,
                Quote_signing_method__c, Total_Prorated_Days_for_contract__c, Cutomer_Cancel_Date__c, Talus_Cancel_Date__c, Order_Group__c, Product_Type__c, 
                Billing_Close_Date__c, P4P_Billing__c, Is_P4P__c, P4P_Current_Months_Clicks_Leads__c,Billing_Partner_Account__c, Order_Billing_Date_Changed__c,
                Contract_End_Date__c, Line_Status__c,Continious_Billing__c, Prorate_Stored_Value__c, Cancellation__c, Total_Prorate__c,
                Digital_Product_Requirement__r.Advertised_Phone_Number__c,
                Product2__r.Name, Product2__r.Product_Type__c,Prorate_Credit__c, Prorate_Credit_Days__c, Current_Billing_Period_Days__c,Order_Line_Total__c,
                BillingChangeNoofDays__c, CMR_Name__c, Billing_Change_Prorate_Credit__c, BillingChangeProrateCreditDays__c FROM Order_Line_Items__r) 
                FROM Order_Group__c WHERE Order_Account__c IN :setAccIds];
    }*/
    
    public static list<Order_Group__c> getOrderGroupWithOLIByAccountIds(set<Id> setAccIds) {
        return [SELECT Id, Name, Order_Account__c, (SELECT Id,Name, Order__c, Order__r.Account__c, Canvass__c, Description__c , Directory_Code__c, 
                Directory__c, Directory_Edition__c,Discount__c,Fulfilled_on__c, FulfillmentDate__c, ListPrice__c, P4P_Current_Billing_Clicks_Leads__c, 
                Canvass__r.Canvass_Code__c, Package_ID__c, Order_Group__r.Order_Account__c,
                Opportunity__c, Opportunity__r.Name, Product2__c, Product2__r.Family, Product2__r.RGU__c, Media_Type__c, P4P_Price_Per_Click_Lead__c, 
                ProductCode__c, Quantity__c, Sent_to_fulfillment_on__c, UnitPrice__c, Billing_Contact__c, BillingChangesPayment__c, Account__c,                
                Order_Group__r.Order_Account__r.Digital_Product_Involuntary_Cancelled__c, Last_Billing_Date__c, Next_Billing_Date__c, Cancelation_Fee__c,
                Edition_Code__c, Order_Anniversary_Start_Date__c,Billing_End_Date__c, Billing_Frequency__c, Total_Billing_Amount_Transfered__c,
                Billing_Partner__c, Billing_Start_Date__c, Telco_Invoice_Date__c, Print_First_Bill_Date__c, OriginalBillingDate__c, 
                Digital_Product_Requirement__c,Payment_Method__c, Payments_Remaining__c, Successful_Payments__c, Payment_Duration__c,Service_Start_Date__c,
                Service_End_Date__c, IsCanceled__c, Talus_Go_Live_Date__c, Talus_Fulfillment_Date__c,Current_Daily_Prorate__c,Quote_Signed_Date__c,
                Quote_signing_method__c, Total_Prorated_Days_for_contract__c, Cutomer_Cancel_Date__c, Talus_Cancel_Date__c, Order_Group__c, Product_Type__c, 
                Billing_Close_Date__c, P4P_Billing__c, Is_P4P__c, P4P_Current_Months_Clicks_Leads__c,Billing_Partner_Account__c, Order_Billing_Date_Changed__c,
                Contract_End_Date__c, Line_Status__c,Continious_Billing__c, Prorate_Stored_Value__c, Cancellation__c, Total_Prorate__c,
                Digital_Product_Requirement__r.business_phone_number_office__c,
                Product2__r.Name, Product2__r.Product_Type__c,Prorate_Credit__c, Prorate_Credit_Days__c, Current_Billing_Period_Days__c,Order_Line_Total__c,
                BillingChangeNoofDays__c, CMR_Name__c, Billing_Change_Prorate_Credit__c, BillingChangeProrateCreditDays__c,Telco__r.Accepts_Cancellation_Fee__c FROM Order_Line_Items__r where Is_P4P__c = false),  
                (select id, Inactive__c, Completed__c from Modification_Order_Line_Items__r where Inactive__c = false and Completed__c = false) 
                FROM Order_Group__c WHERE Order_Account__c IN :setAccIds];
    }
}