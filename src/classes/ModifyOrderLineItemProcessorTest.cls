@isTest
public with sharing class ModifyOrderLineItemProcessorTest {
    static testmethod void modifyOLIProctest(){
        Test.starttest();
        
        Account newAccount = TestMethodsUtility.createAccount('customer');
        Contact newContact = TestMethodsUtility.createContact(newAccount.Id);
        Pricebook2 newPriceBook = new Pricebook2(Id = System.Label.PricebookId);
        Product2 newProduct = TestMethodsUtility.generateproduct();
        newProduct.Product_type__c = 'Print Graphic';
        insert newProduct;
        Opportunity newOpportunity = TestMethodsUtility.generateOpportunity('new');
        newOpportunity.AccountId = newAccount.Id;
        newOpportunity.Pricebook2Id = newPriceBook.Id;
        insert newOpportunity;
        Order__c newOrder = TestMethodsUtility.createOrder(newAccount.Id);
        Order_Group__c newOrderSet = TestMethodsUtility.createOrderSet(newAccount, newOrder, newOpportunity);
        Order_Line_Items__c newOrLI = TestMethodsUtility.createOrderLineItem(newAccount, newContact, newOpportunity, newOrder, newOrderSet);
        Directory__c directory = TestMethodsUtility.createDirectory();
        Directory_Section__c dirSection = TestMethodsUtility.createDirectorySection(directory);
        
        Digital_Product_Requirement__c DFF = TestMethodsUtility.createDataFulfillmentForm('Print Graphic');
         DFF.OrderLineItemID__c=newOrLI.Id;
         DFF.UDAC__c = 'WPFP';
         update DFF;
         newOrLI.Digital_Product_Requirement__c=DFf.Id;
         newOrLI.Media_Type__c = 'Digital';
        update newOrLI;
        
         String recType=[select Id from RecordType where RecordType.Name like 'New_Order_Line_Item'].Id;
        List<Modification_Order_Line_Item__c> MOLIList = new List<Modification_Order_Line_Item__c>();
        Modification_Order_Line_Item__c mol = new Modification_Order_Line_Item__c(Billing_Frequency__c='Monthly',Order_Line_Item__c=newOrLI.id);
       // mol.Directory_Section__c = dirSection.Id;
        mol.Order_Group__c = newOrderSet.Id;
        mol.Action_Code__c ='Renew';
        mol.Effective_Date__c=System.Today();
        mol.IsUDACChange__c=false;
        mol.Completed__c=false;
        mol.product2__c = newProduct.id;
        mol.Inactive__c=false;
        mol.Media_Type__c = 'Print';
        mol.Payment_Duration__c = 5;
        insert mol;
               
        MOLIList.add(mol);
        
        List<Modification_Order_Line_Item__c> moliQueryList=ModificationOrderLineItemSOQLMethods.getMOLIBYEffectiveDate(System.Today());
        System.debug('Testingg moliQueryList '+moliQueryList);
        ModifyOrderLineItemProcessor_v1.ProcessTimedMOLI(moliQueryList,null);
             
        Test.stoptest();
    }
}