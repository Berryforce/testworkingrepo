public class BatchOptionsPage {

   public service_order_stage__c objSO{get;set;}
   
   public List<string> leftselected{get;set;}
   public List<string> rightselected{get;set;}
   public List<string> leftBusselected{get;set;}
   public List<string> rightBusselected{get;set;}
   public List<string> leftDFselected{get;set;}
   public List<string> rightDFselected{get;set;}
   
   public string batchOptions{get;set;}
   public string LocalForeign{get;set;}
   public string runalldata{get;set;}
   public list <SelectOption> batchOptionsPickList{get;set;}
   public list <SelectOption> LocalForeignPickList{get;set;}
   public list <SelectOption> runalldataPickList{get;set;}
   
   public string userinputAreaCode{get;set;}
   public string userinputExchangeCode{get;set;}
   public ID SOrecordId{get;set;}
   public boolean runalldatadir{get;set;}
   public list<string> acLst{get;set;}
   public string outputAreaCode{get;set;}
   public list<string> outputAreaLst{get;set;}
  
   public boolean userinputEAS{get;set;}
   public boolean userinputCATS{get;set;}
   public boolean submitbutton{get;set;}
   
   public Id batchId {get;set;}
   public Boolean polleraction {get;set;}
   public Boolean isBatchProcessed {get;set;}
   public String batchStatus {get;set;}
   
   public Id ABDbatchId {get;set;}
   public Boolean ABDpolleraction {get;set;}
   public Boolean isABDBatchProcessed {get;set;}
   public String ABDbatchStatus {get;set;}
   
   
   private set<string> setarea;
   private set<string> setExchange;
   private list<directory_Section__c> dirsecLst;
   private Set<string> leftvalues;
   private Set<string> rightvalues;
   private Set<string> leftBusvalues;
   private Set<string> rightBusvalues;
   private Set<string> leftDFvalues;
   private Set<string> rightDFvalues;
   private map<string,string> mapAreaExchange = new map<string,string>();
   
    
   public BatchOptionsPage(){
      objSO = new service_order_stage__c();
      batchOptionsPicklist = new list <SelectOption>();
      batchOptionsPickList.add(new SelectOption('Standard','Standard'));
      batchOptionsPickList.add(new SelectOption('Need Options','Need Options'));
      
      LocalForeignPicklist = new list<SelectOption>();
      LocalForeignPicklist.add(new SelectOption('None','None'));
      LocalForeignPicklist.add(new SelectOption('Local','Local'));
      LocalForeignPicklist.add(new SelectOption('Foreign','Foreign'));
      
      leftselected = new List<string>();
      rightselected = new List<string>();
      leftBusselected = new List<String>();
      rightBusselected = new List<string>();
      leftDFselected = new List<String>();
      rightDFselected = new List<string>();
      
      acLst= new list<string>();
      outputAreaLst = new list<string>();
      rightvalues = new set<string>();
      leftvalues= new set<string>();
      rightBusvalues = new set<string>();
      leftBusvalues= new set<string>();
      rightDFvalues = new set<string>();
      leftDFvalues= new set<string>();
      
      setarea = new set<string>();
      setExchange = new set<string>();
     
   }
  
   public PageReference fetchDirectorysectionValues(){
   dirsecLst = [Select Id,Name,Directory__c from Directory_Section__c where Directory__c =:objSO.Directory__c and Section_Page_Type__c='WP'];
        for(directory_Section__c objdirs : dirsecLst){
            leftvalues.add(objdirs.Id);
        }
        submitbutton= false;
        if(batchOptions=='Standard'){
            rightvalues = new set<string>();
            rightBusvalues = new set<string>();
            rightDFvalues = new set<string>();
            outputAreaLst = new list<string>();
        }
        return null;
    }
    public PageReference selectclick(){
        rightselected.clear();
        for(String s : leftselected){
            leftvalues.remove(s);
            rightvalues.add(s);
        }
        return null;
    }
    public PageReference selectBusclick(){
        rightBusselected.clear();
        for(String s : leftBusselected){
            leftBusvalues.remove(s);
            rightBusvalues.add(s);
        }
        return null;
    }
    public PageReference selectDFclick(){
        rightDFselected.clear();
        for(String s : leftDFselected){
            leftDFvalues.remove(s);
            rightDFvalues.add(s);
        }
        return null;
    }
    public PageReference unselectclick(){
        leftselected.clear();
        for(string s : rightselected){
            rightvalues.remove(s);
            leftvalues.add(s);
        }
        return null;
    }
    public PageReference unselectBusclick(){
        leftBusselected.clear();
        for(string s : rightBusselected){
            rightBusvalues.remove(s);
            leftBusvalues.add(s);
        }
        return null;
    }
    public PageReference unselectDFclick(){
        leftDFselected.clear();
        for(string s : rightDFselected){
            rightDFvalues.remove(s);
            leftDFvalues.add(s);
        }
        return null;
    }
    
   
    public List<SelectOption> getunSelectedValues(){
        List<SelectOption> options = new List<SelectOption>();
        List<directory_Section__c> tempList = new List<directory_Section__c>();
        tempList = [Select Id,Name from Directory_Section__c where Id IN:leftvalues];
        tempList.sort();
        if(tempList.size()>0){
            for(directory_Section__c s : tempList){
                options.add(new SelectOption(s.Id,s.Name));
            }
        }
        return options;
    }
    public List<SelectOption> getSelectedValues(){
        List<SelectOption> options1 = new List<SelectOption>();
        List<Directory_section__c> tempList = new List<Directory_section__c>();
        if(rightvalues.size()>0){
        tempList = [Select Id,Name  from Directory_Section__c where Id IN :rightvalues]; 
        }
        tempList.sort();
        if(tempList.size()>0){
            for(directory_section__c s : tempList){
                options1.add(new SelectOption(s.Id,s.Name));
            }
        }
        return options1;
    }
    
    public List<SelectOption> getunSelectedBusValues(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('Business','Business'));
        options.add(new SelectOption('Residential','Residential'));
        options.add(new SelectOption('Government','Government'));
        options.add(new SelectOption('Combination','Combination'));
        
        return options;
    }
    public List<SelectOption> getSelectedBusValues(){
        List<SelectOption> options1 = new List<SelectOption>();
        if(rightBusvalues.size()>0){
            for(string strBus : rightBusvalues){
                options1.add(new SelectOption(strBus,strBus));
            }
        }
        return options1;
    }
    
    public List<SelectOption> getunSelectedDataFeedValues(){
      List<SelectOption> DataFeedTypePickList = new list<SelectOption>();
      DataFeedTypePickList.add(new SelectOption('Annual','Annual'));
      DataFeedTypePickList.add(new SelectOption('CATS','CATS'));
      DataFeedTypePickList.add(new SelectOption('EAS','EAS'));
      DataFeedTypePickList.add(new SelectOption('Daily Service Order','Daily Service Order'));
      DataFeedTypePickList.add(new SelectOption('Manual','Manual'));
      DataFeedTypePickList.add(new SelectOption('SODE','SODE'));
        
      return DataFeedTypePickList ;
    }
    public List<SelectOption> getSelectedDataFeedValues(){
        List<SelectOption> options1 = new List<SelectOption>();
        if(rightDFvalues.size()>0){
            for(string strDF : rightDFvalues){
                options1.add(new SelectOption(strDF,strDF));
            }
        }
        return options1;
    }
    public void addAreaCode(){
        if(!mapAreaExchange.containskey(userinputAreaCode)){
            mapAreaExchange.put(userinputAreaCode,userinputAreaCode);
        }
        //outputAreaLst.add(userinputAreaCode);
        outputAreaLst=mapAreaExchange.values();
        userinputAreaCode='';
        //return null;
    }
    public void deleteAreaCode(){
        if(mapAreaExchange.containskey(userinputAreaCode)){
            mapAreaExchange.remove(userinputAreaCode);
        }
        outputAreaLst= mapAreaExchange.values();
        userinputAreaCode='';
    }
    
    public pagereference ABDBatch(){
        Id DirectoryId = objSO.Directory__c;
        string telcoId;
        if(objSO.Telco_Provider__c != null){
            telcoId = objSO.Telco_Provider__c;
        }
        if(mapAreaExchange.size()>0){
            for(string strarea : mapAreaExchange.values()){
                 setarea.add(strarea);
            }
        }
        set<string> dirsecValues = new set<string>();
        set<string> setBusResValues = new set<string>();
        set<string> setDfvalues = new set<string>();
        if(runalldatadir == true){
            dirsecValues = new set<string>();
        }
        else{
            dirsecValues.addall(rightvalues);
        }
        if(dirsecValues.size()> 0){
        	list<Directory_Section__c> objDirsecYPlst =[Select Id,Name from Directory_Section__c where Section_Page_Type__c='YP' AND Directory__c =:DirectoryId LIMIT 1];
        	if(objDirsecYPlst.size()> 0){
        		dirsecValues.add(objDirsecYPlst[0].Id);
        	}
        }
        setBusResValues.addall(rightBusvalues);
        setDfvalues.addall(rightDFvalues);
        String strCurrentUserId = Userinfo.getUserId();
         if(strCurrentUserId.length() > 15) {
            strCurrentUserId = strCurrentUserId.substring(0, 15);
        }
        SkipTriggerExecution__c objSkip1 = SkipTriggerExecution__c.getvalues(CommonMessages.LObjectName);
        objSkip1.User_IDs__c = strCurrentUserId ;
        update objSkip1;
      
        if(!Test.isRunningTest()){
            ScopedListingABDPendingBatchController soAbd = new ScopedListingABDPendingBatchController(DirectoryId,telcoId,dirsecValues,setarea,LocalForeign,setDfvalues,setBusResValues,false);
            ABDbatchId=Database.executeBatch(soAbd);
      		ABDpollerAction = true;
        	isABDBatchProcessed = true;
        }
        //return(new pagereference('/apex/SOBocBatchOptionsPage').setredirect(true));
        return null;
    }
    public pagereference ABDPendingBackoutBatch(){
        Id DirId = objSO.Directory__c;
        string telcoId;
        if(objSO.Telco_Provider__c != null){
            telcoId = objSO.Telco_Provider__c;
        }
        if(mapAreaExchange.size()>0){
            for(string strarea : mapAreaExchange.values()){
                 setarea.add(strarea);
            }
        }
        
        set<string> dirsecValues = new set<string>();
        set<string> setBusResValues = new set<string>();
        set<string> setDfvalues = new set<string>();
        if(runalldatadir == true){
            dirsecValues = new set<string>();
        }
        else{
            dirsecValues.addall(rightvalues);
        }
        if(rightBusvalues.size()>0){
            setBusResValues.addall(rightBusvalues);
        }
        if(rightDFvalues.size()>0){
            setDfvalues.addall(rightDFvalues);
        }
        String strCurrentUserId = Userinfo.getUserId();
         if(strCurrentUserId.length() > 15) {
            strCurrentUserId = strCurrentUserId.substring(0, 15);
        }
        SkipTriggerExecution__c objSkipL = SkipTriggerExecution__c.getvalues(CommonMessages.LObjectName);
        objSkipL.User_IDs__c = strCurrentUserId ;
        update objSkipL;
       
        ScopedListingABDPendingBatchController soBOAbd = new ScopedListingABDPendingBatchController(DirId,telcoId,dirsecValues,setarea,LocalForeign,setDfvalues,setBusResValues,true);
        ABDbatchId=Database.executeBatch(soBOAbd);
      	ABDpollerAction = true;
        isABDBatchProcessed = true;
        //return(new pagereference('/apex/SOBocBatchOptionsPage').setredirect(true)) ;
        return null;
    }
     public pagereference Bocbatch(){
        Id DirectoryId = objSO.Directory__c;
        if(!Test.isRunningTest()){
            ServiceOrderAnnualBOCBatchController_v3 soAbc = new ServiceOrderAnnualBOCBatchController_v3(DirectoryId);
            batchId=Database.executeBatch(soAbc);
            pollerAction = true;
            isBatchProcessed = true;
        }
        return null;
    }
    
    public void pollingBatchProcess() {                 
        if(batchId!=null) {
            AsyncApexJob FFjob = [SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors FROM AsyncApexJob WHERE ID =: batchId];
            if(!Test.isRunningTest())
            batchStatus = FFjob.Status;
        }
        if(batchStatus == 'Completed') {
            pollerAction = false;
        }
    }
     public void pollingABDBatchProcess() {                 
        if(ABDbatchId!=null) {
            AsyncApexJob FFjob1 = [SELECT Id, Status, JobItemsProcessed, TotalJobItems, NumberOfErrors FROM AsyncApexJob WHERE ID =: ABDbatchId];
            if(!Test.isRunningTest())
            ABDbatchStatus = FFjob1.Status;
        }
        if(ABDbatchStatus == 'Completed') {
            ABDpollerAction = false;
        }
    }
  
}