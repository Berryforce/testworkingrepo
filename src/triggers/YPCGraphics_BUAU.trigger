trigger YPCGraphics_BUAU on YPC_Graphics__c (before update, after update) {
	if(Test.isRunningtest() || CommonMethods.skipTriggerLogic(CommonMessages.ypcGraphicsObjectName)) {
	    if(trigger.isBefore) {
	        YPCDFFHandler.onBeforeUpdate(trigger.new, trigger.oldMap);
	    } else if(trigger.isAfter) {
	        YPCDFFHandler.onAfterUpdate(trigger.new, trigger.oldMap);
	    }
	}
}