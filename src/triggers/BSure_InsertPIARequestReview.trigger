trigger BSure_InsertPIARequestReview on Vertex_Berry__PIA_Credit_Review__c (after Insert,after Update) {
    String setpiarobjId  ='';
    String strOppId =''; 
    String strCustId ='';
    
    list<PIA_Credit_Review_Opp__c> lstsobj = new list<PIA_Credit_Review_Opp__c>();
    list<Opportunity> lstopp = new list<Opportunity>();
    list<Vertex_Berry__BSureC_Customer_Basic_Info__c> lstCust = new list<Vertex_Berry__BSureC_Customer_Basic_Info__c>();
    for(Vertex_Berry__PIA_Credit_Review__c piaobj : Trigger.new)
    {
        setpiarobjId = piaobj.Id;
        strOppId = piaobj.Vertex_Berry__Opportunity_ID__c;
        strCustId = piaobj.Vertex_Berry__Customer_Information__c;
    }
    
    if(Trigger.isInsert)
    {
        if(setpiarobjId != null && setpiarobjId != '')
        {
            lstsobj = new list<PIA_Credit_Review_Opp__c>();
            for(Vertex_Berry__PIA_Credit_Review__c piaobj2 : Trigger.new) 
            {
                PIA_Credit_Review_Opp__c oppcr = new PIA_Credit_Review_Opp__c();
                oppcr.Analyst__c = piaobj2.Vertex_Berry__Analyst__c;
                oppcr.Current_PIA__c = piaobj2.Vertex_Berry__Current_PIA__c;
                oppcr.Former_PIA__c =  piaobj2.Vertex_Berry__Former_PIA__c;
                oppcr.Status__c =  piaobj2.Vertex_Berry__Status__c;
                oppcr.Opportunity__c  = piaobj2.Vertex_Berry__Opportunity_ID__c;
                oppcr.ExternalId__c = setpiarobjId.substring(0,15);
                lstsobj.add(oppcr);
            }
        }   
        if(lstsobj != null && lstsobj.size() > 0)
        {
            insert lstsobj;
        }
    }
    if(Trigger.isUpdate)
    {
        Integer strcurrPIA ;
        if((setpiarobjId != null && setpiarobjId != '') && (strOppId != null && strOppId!= ''))
        {
            lstsobj = [Select Id,Name from PIA_Credit_Review_Opp__c where ExternalId__c =: setpiarobjId.substring(0,15) and Opportunity__c =: strOppId.substring(0,15)];
            System.debug('lstsobj========'+lstsobj);
        }   
        if(lstsobj != null && lstsobj.size() > 0)
        {
            for(Vertex_Berry__PIA_Credit_Review__c piaobj3 : Trigger.new)
            {
                for(PIA_Credit_Review_Opp__c croppobj : lstsobj)
                {
                    croppobj.Analyst__c = piaobj3.Vertex_Berry__Analyst__c;
                    croppobj.Current_PIA__c = piaobj3.Vertex_Berry__Current_PIA__c;
                    strcurrPIA = Integer.valueOf(piaobj3.Vertex_Berry__Current_PIA__c);
                    croppobj.Former_PIA__c =  piaobj3.Vertex_Berry__Former_PIA__c;
                    croppobj.Status__c =  piaobj3.Vertex_Berry__Status__c;
                }
                
            }
            update lstsobj;
        }
        if(strOppId != null && strOppId != '')
        {
            if(strcurrPIA != null)
            {
                lstopp = [Select Id,PIA__c from Opportunity where Id =: strOppId];
                if(lstopp != null && lstopp.size() > 0)
                {
                    for(Opportunity oup : lstopp)
                    {
                        oup.PIA__c = strcurrPIA;
                    }
                    update lstopp;
                }
            }   
        }
        if(strCustId != null && strCustId !='')
        {
            if(strcurrPIA != null )
            {
                lstCust = [Select Id,Vertex_Berry__PIA__c from Vertex_Berry__BSureC_Customer_Basic_Info__c where Id =: strCustId];
                if(lstCust != null && lstCust.size() > 0)
                {
                    for(Vertex_Berry__BSureC_Customer_Basic_Info__c cupdate : lstCust)
                    {
                        cupdate.Vertex_Berry__PIA__c = strcurrPIA;
                        cupdate.Vertex_Berry__Reviewed_By__c = UserInfo.getUserId();
                        cupdate.Vertex_Berry__Review_Complete_Date__c = System.today();
                    }
                    update lstCust;
                }
            }   
        }
        
    }
}