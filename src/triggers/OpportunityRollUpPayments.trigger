trigger OpportunityRollUpPayments on TmpOpportunityLineItem__c (after delete,after insert, 
after update, after undelete) {
     
    if(trigger.isInsert || trigger.isUpdate || trigger.isUnDelete){
         
        list<RollUpSummaryUtility.fieldDefinition> fieldDefinitions = 
        new list<RollUpSummaryUtility.fieldDefinition> {
            new RollUpSummaryUtility.fieldDefinition('SUM', 'Order_Line_Total__c', 
            'TmpOppty_Total_Price__c')            
        };
         
        RollUpSummaryUtility.rollUpTrigger(fieldDefinitions, trigger.new, 
        'TmpOpportunityLineItem__c', 'Opportunityid__c', 'Opportunity', '');
         
    }
     
    if(trigger.isDelete){
         
        list<RollUpSummaryUtility.fieldDefinition> fieldDefinitions = 
        new list<RollUpSummaryUtility.fieldDefinition> {
            new RollUpSummaryUtility.fieldDefinition('SUM', 'Order_Line_Total__c', 
            'TmpOppty_Total_Price__c')

        };
         
        RollUpSummaryUtility.rollUpTrigger(fieldDefinitions, trigger.old, 
        'TmpOpportunityLineItem__c', 'Opportunityid__c', 'Opportunity', '');
         
    }
     
}