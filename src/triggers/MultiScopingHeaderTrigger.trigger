trigger MultiScopingHeaderTrigger on Multi_Scoping_Header__c (before insert, before update) {
	for(Multi_Scoping_Header__c MSH : trigger.New) {
		MSH.MSH_Area_Exchange_Dir_Dir_Sec_Combo__c = MSH.MSH_Area_Code__c + MSH.MSH_Exchange_Code__c + MSH.MSH_Directory__c + MSH.MSH_Directory_Section__c;
	}
}