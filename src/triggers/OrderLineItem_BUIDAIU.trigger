trigger OrderLineItem_BUIDAIU on Order_Line_Items__c (before update, before insert, before delete, after insert, after update) {
    if(Test.isRunningtest() || !CommonMethods.skipTriggerLogic(CommonMessages.OLIObjectName)) { 
        if(trigger.isBefore) {
            if(trigger.isInsert) {
                OrderLineItemHandlerController.onBeforeInsert(trigger.new);             
            } else if(trigger.isUpdate) {
                OrderLineItemHandlerController.onBeforeUpdate(trigger.new, trigger.oldMap);      
            }
        }
        if (trigger.isAfter) {
            if (trigger.isInsert) {
                OrderLineItemHandlerController.onAfterInsert(trigger.new);
            } else if (trigger.isUpdate) {
                OrderLineItemHandlerController.onAfterUpdate(trigger.new, trigger.oldMap);
            }
            // commented by vikas because someoe removed this method from orderlineitemhandlercontroller and it is blocking for other tickets Dated:- 12/03/2014
          /*  else if (trigger.isDelete) {
              OrderLineItemHandlerController.onAfterDelete(trigger.new, trigger.oldMap);
            }*/
        }
    }
    
}