trigger ProductInventory_AIUD on Product_Inventory__c (after insert, after update) {
    if(trigger.isInsert){
        ProductInventoryHandlerController.onAfterInsert(trigger.New, trigger.OldMap);
    }
}