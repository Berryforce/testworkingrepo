trigger Listing_BDAIU_UD on Listing__c (before insert, before delete, before update,after insert, after update, after undelete )
{
    if(Test.isRunningtest() || !CommonMethods.skipTriggerLogic(CommonMessages.LObjectName)) {
        if(trigger.isBefore) {
            if(trigger.isInsert) {
                ListingHandlerController.onBeforeInsert(trigger.new);
            }
            if(trigger.isUpdate) {
                ListingHandlerController.onBeforeUpdate(trigger.new, trigger.oldMap);
            }
        }
        if(trigger.isAfter){
            if(trigger.isInsert){
                ListingHandlerController.onAfterInsert(trigger.new);
            }
            else if(trigger.isUpdate){
                ListingHandlerController.onAfterUpdate(trigger.new, trigger.oldMap,trigger.newMap);
            }
        }
        try{
        if ( Test.isRunningTest() && CRMfusionDBR101.DB_Globals.generateCustomTriggerException )
        {
            throw new CRMfusionDBR101.DB_Globals.TestException( 'Test exception.');
        }
        else if ( trigger.isAfter && ( trigger.isInsert || trigger.isUpdate || trigger.isUndelete ) )
        {
            CRMfusionDBR101.DB_TriggerHandler.processAfterInsertUpdateUndelete( trigger.New, trigger.Old, trigger.isInsert,
                trigger.isUpdate, trigger.isUndelete );
        }
        else if ( trigger.isBefore && trigger.isDelete )
        {
            CRMfusionDBR101.DB_TriggerHandler.processBeforeDelete( trigger.Old );
        }
        }
        catch ( Exception ex )
        {
            CRMfusionDBR101.DB_TriggerHandler.handleTriggerException( ex, 'DB_Custom_Listing_c' );
        }
    }
}