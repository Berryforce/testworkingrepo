trigger ProductInventory_BIUD on Product_Inventory__c (before insert, before update) {
    if(trigger.isInsert){
        ProductInventoryHandlerController.onBeforeInsert(trigger.New);
    }
}