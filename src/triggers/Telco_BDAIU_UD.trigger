trigger Telco_BDAIU_UD on Telco__c ( before delete, after insert, after update, after undelete )
{
    try
    {
        if ( Test.isRunningTest() && CRMfusionDBR101.DB_Globals.generateCustomTriggerException )
        {
            throw new CRMfusionDBR101.DB_Globals.TestException( 'Test exception.');
        }
        else if ( trigger.isAfter && ( trigger.isInsert || trigger.isUpdate || trigger.isUndelete ) )
        {
            CRMfusionDBR101.DB_TriggerHandler.processAfterInsertUpdateUndelete( trigger.New, trigger.Old, trigger.isInsert,
                trigger.isUpdate, trigger.isUndelete );
        }
        else if ( trigger.isBefore && trigger.isDelete )
        {
            CRMfusionDBR101.DB_TriggerHandler.processBeforeDelete( trigger.Old );
        }
    }
    catch ( Exception ex )
    {
        CRMfusionDBR101.DB_TriggerHandler.handleTriggerException( ex, 'Telco_BDAIU_UD' );
    }
    
    if(trigger.isAfter) { 
        if(trigger.isInsert) {
            TelcoHandlerController.onAfterInsert(trigger.new);
        }
        
        if(trigger.isUpdate) {
            TelcoHandlerController.onAfterUpdate(trigger.new, trigger.oldMap);
        }
    }
    
     //Added by Mythili for ticket CC-1913
    if(trigger.isBefore){
        if(trigger.isInsert){
            TelcoHandlerController.onBeforeInsert(trigger.new);
        }
        if(trigger.isUpdate){
            TelcoHandlerController.onBeforeUpdate(trigger.new,trigger.OldMap);  
        }
    }
}