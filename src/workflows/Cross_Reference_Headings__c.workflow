<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Source_and_Cross_Reference_Id_s</fullName>
        <field>Source_Cross_Reference_Id_s__c</field>
        <formula>Cross_Reference_Heading__c  &amp;  Source_Heading__c</formula>
        <name>Source and Cross Reference Id&apos;s</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Source and Cross Reference Id%27s</fullName>
        <actions>
            <name>Source_and_Cross_Reference_Id_s</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>ISNEW() || ISCHANGED( Cross_Reference_Heading__c )  ||  ISCHANGED( Source_Heading__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
