<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_External_ID</fullName>
        <field>Multi_Scoping_External_ID__c</field>
        <formula>Area_Code__c &amp; &quot;-&quot; &amp; Exchange__c &amp; &quot;-&quot; &amp;  Directory_Section_Code__c</formula>
        <name>Set External ID</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_NAME</fullName>
        <field>Name</field>
        <formula>Area_Code__c &amp; &quot;-&quot; &amp; Exchange__c &amp; &quot;-&quot; &amp;  Directory_Section_Code__c</formula>
        <name>Set NAME</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Set NAME and EXTERNAL ID</fullName>
        <actions>
            <name>Set_External_ID</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Set_NAME</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Used to set and format a Multi Scoping Combination record&apos;s Name and External ID</description>
        <formula>OR( ISNEW(),
    ISCHANGED(Area_Code__c),
    ISCHANGED(Exchange__c),
    ISCHANGED(Name),
    ISCHANGED(Multi_Scoping_External_ID__c),
    ISCHANGED(Directory_Section__c)
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
