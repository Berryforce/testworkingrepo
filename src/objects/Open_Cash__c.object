<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <description>Staging object for open cash entries</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Account_Number__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Open Cash</relationshipLabel>
        <relationshipName>Open_Cash</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Bank_Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Bank Account</label>
        <referenceTo>c2g__codaBankAccount__c</referenceTo>
        <relationshipLabel>Open Cash</relationshipLabel>
        <relationshipName>Open_Cash</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>CE_ID__c</fullName>
        <externalId>false</externalId>
        <label>Cash Entry ID</label>
        <length>18</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Cash_Entry_Currency__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Cash Entry Currency</label>
        <referenceTo>c2g__codaAccountingCurrency__c</referenceTo>
        <relationshipLabel>Open Cash</relationshipLabel>
        <relationshipName>Open_Cash</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Cash_Entry_Number__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Cash Entry Number</label>
        <referenceTo>c2g__codaCashEntry__c</referenceTo>
        <relationshipLabel>Open Cash</relationshipLabel>
        <relationshipName>Open_Cash</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Cash_Entry_Processed__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Cash Entry Processed</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Cash_Entry_Value__c</fullName>
        <externalId>false</externalId>
        <label>Cash Entry Value</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Company__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Company</label>
        <referenceTo>c2g__codaCompany__c</referenceTo>
        <relationshipLabel>Open Cash</relationshipLabel>
        <relationshipName>Open_Cash_del</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Date__c</fullName>
        <externalId>false</externalId>
        <label>Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <externalId>false</externalId>
        <label>Description</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>Edition_Code__c</fullName>
        <externalId>false</externalId>
        <label>Edition Code</label>
        <length>18</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Historical_Transaction__c</fullName>
        <defaultValue>false</defaultValue>
        <externalId>false</externalId>
        <label>Historical Transaction</label>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Payment_Method__c</fullName>
        <externalId>false</externalId>
        <label>Payment Method</label>
        <picklist>
            <picklistValues>
                <fullName>Lockbox</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Electronic - EPay</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Manual</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Cash</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Check</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Payment_Type__c</fullName>
        <externalId>false</externalId>
        <label>Payment Type</label>
        <picklist>
            <picklistValues>
                <fullName>Check</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Money Order</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>OCA Payment</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Credit Card</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>ACH Electronic</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Wire Transfer</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Period__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Period</label>
        <referenceTo>c2g__codaPeriod__c</referenceTo>
        <relationshipLabel>Open Cash</relationshipLabel>
        <relationshipName>Open_Cash</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Reference__c</fullName>
        <externalId>false</externalId>
        <label>Reference</label>
        <length>80</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Transaction_ID__c</fullName>
        <externalId>false</externalId>
        <label>Transaction ID</label>
        <length>18</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Transaction_Type__c</fullName>
        <externalId>false</externalId>
        <label>Transaction Type</label>
        <picklist>
            <picklistValues>
                <fullName>P - Payment</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>D - Advance Payment</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>PP - Prepayment</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>DPP - Digital Prepayment</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>RP - Recovery Payment OCA</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>TASP - Telco Settlement Payment</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>RFD - Refund</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>MRD - Misapply Debit</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>MRC - Reapply Credit</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>OCA - OCA Payment</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>NSF - Returned Check NSF</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>CHB - Chargeback</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>TBB - Telco Billbacks</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>WP - Write Off Of Payment</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>WPR - Recovery of Payment</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>Type__c</fullName>
        <externalId>false</externalId>
        <label>Type</label>
        <picklist>
            <picklistValues>
                <fullName>Receipt</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Refund</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Returned Payment</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <label>Open Cash</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>CE_ID__c</columns>
        <columns>Date__c</columns>
        <columns>Company__c</columns>
        <columns>Cash_Entry_Currency__c</columns>
        <columns>Cash_Entry_Processed__c</columns>
        <columns>Historical_Transaction__c</columns>
        <columns>Type__c</columns>
        <columns>Cash_Entry_Value__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <listViews>
        <fullName>Open_Cash_Processed</fullName>
        <columns>NAME</columns>
        <columns>Bank_Account__c</columns>
        <columns>Cash_Entry_Currency__c</columns>
        <columns>CE_ID__c</columns>
        <columns>Cash_Entry_Number__c</columns>
        <columns>Cash_Entry_Processed__c</columns>
        <columns>Cash_Entry_Value__c</columns>
        <columns>Type__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Cash_Entry_Processed__c</field>
            <operation>equals</operation>
            <value>1</value>
        </filters>
        <label>Open Cash Processed</label>
    </listViews>
    <listViews>
        <fullName>Process_Open_Cash</fullName>
        <columns>NAME</columns>
        <columns>Bank_Account__c</columns>
        <columns>Cash_Entry_Currency__c</columns>
        <columns>CE_ID__c</columns>
        <columns>Cash_Entry_Number__c</columns>
        <columns>Cash_Entry_Processed__c</columns>
        <columns>Cash_Entry_Value__c</columns>
        <columns>Type__c</columns>
        <filterScope>Everything</filterScope>
        <filters>
            <field>Cash_Entry_Processed__c</field>
            <operation>equals</operation>
            <value>0</value>
        </filters>
        <label>Process Open Cash</label>
    </listViews>
    <nameField>
        <displayFormat>OCSH-{0000000000}</displayFormat>
        <label>Open Cash Entry</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Open Cash</pluralLabel>
    <searchLayouts>
        <listViewButtons>Convert_to_Cash_Entry_List</listViewButtons>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <startsWith>Vowel</startsWith>
    <webLinks>
        <fullName>Convert_to_Cash_Entry_Detail</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Convert to Cash Entry</masterLabel>
        <openType>sidebar</openType>
        <page>ConvertCashtoCashEntryDetail</page>
        <protected>false</protected>
    </webLinks>
    <webLinks>
        <fullName>Convert_to_Cash_Entry_List</fullName>
        <availability>online</availability>
        <displayType>massActionButton</displayType>
        <height>600</height>
        <linkType>page</linkType>
        <masterLabel>Convert to Cash Entry</masterLabel>
        <openType>sidebar</openType>
        <page>ConvertCashtoCshEntryList</page>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
    </webLinks>
</CustomObject>
