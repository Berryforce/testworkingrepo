<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>ADJ_Amount__c</fullName>
        <externalId>false</externalId>
        <label>ADJ Amount</label>
        <precision>7</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>CMR_Number_Client_Number__c</fullName>
        <externalId>false</externalId>
        <formula>Client_Name__r.CMR_Number_Client_Number__c</formula>
        <label>CMR Number - Client Number</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CMR_to_ISS_Line_Item_Validation__c</fullName>
        <description>Formula to verify that ISS Line Item&apos;s CMR-Client Number matches the CMR number from the ISS.</description>
        <externalId>false</externalId>
        <formula>IF(LEFT(Client_Name__r.CMR_Number_Client_Number__c, 4) = ISS__r.CMR_Number__c, &apos;Match&apos;, &apos;No Match&apos;)</formula>
        <label>CMR to ISS Line Item Validation</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Client_Name__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Client Name</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>ISS Line Items</relationshipLabel>
        <relationshipName>ISS_Line_Items</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Client_Number__c</fullName>
        <externalId>false</externalId>
        <formula>Client_Name__r.AccountNumber</formula>
        <label>Client Number</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Commission_Amount__c</fullName>
        <externalId>false</externalId>
        <formula>(Gross_Amount__c *  Commission__c )</formula>
        <label>Commission Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Commission__c</fullName>
        <externalId>false</externalId>
        <label>Commission(%)</label>
        <precision>4</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Directory_Code__c</fullName>
        <externalId>false</externalId>
        <formula>Directory__r.Directory_Code__c</formula>
        <label>Directory Code</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Directory_Edition_Code__c</fullName>
        <externalId>false</externalId>
        <formula>Directory_Edition__r.Edition_Code__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Directory Edition Code</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Directory_Edition__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Directory Edition</label>
        <referenceTo>Directory_Edition__c</referenceTo>
        <relationshipLabel>ISS Line Items</relationshipLabel>
        <relationshipName>ISS_Line_Items</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Directory__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Directory</label>
        <referenceTo>Directory__c</referenceTo>
        <relationshipLabel>ISS Line Items</relationshipLabel>
        <relationshipName>ISS_Line_Items</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Gross_Amount__c</fullName>
        <externalId>false</externalId>
        <label>Gross Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>ISS__c</fullName>
        <externalId>false</externalId>
        <label>ISS</label>
        <referenceTo>ISS__c</referenceTo>
        <relationshipLabel>ISS Line Items</relationshipLabel>
        <relationshipName>ISS_Line_Items</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Inv_Type__c</fullName>
        <externalId>false</externalId>
        <label>Inv Type</label>
        <length>10</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>LSA_Directory_Version__c</fullName>
        <externalId>false</externalId>
        <formula>Directory_Edition__r.LSA_Directory_Version__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>LSA Directory Version</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Life__c</fullName>
        <externalId>false</externalId>
        <label>Life</label>
        <length>4</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Net_Amount__c</fullName>
        <externalId>false</externalId>
        <formula>(Gross_Amount__c + ADJ_Amount__c - Commission_Amount__c + Tax_Amount__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Net Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Product__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <externalId>false</externalId>
        <label>Product</label>
        <referenceTo>Product2</referenceTo>
        <relationshipLabel>ISS Line Items</relationshipLabel>
        <relationshipName>ISS_Line_Items</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Pubco_date__c</fullName>
        <externalId>false</externalId>
        <label>Pubco date</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>Rate_for_Commission__c</fullName>
        <externalId>false</externalId>
        <formula>IF( Client_Name__r.National_Commission__c != null, 
Client_Name__r.National_Commission__r.Rate__c , 
IF( Directory__r.National_Commission__c != null, Directory__r.National_Commission__r.Rate__c , 
IF( ISS__r.CMR__r.National_Commission__c != null, 
ISS__r.CMR__r.National_Commission__r.Rate__c, 
IF( ISS__r.Publication_Company__r.National_Commission__c != null, ISS__r.Publication_Company__r.National_Commission__r.Rate__c, 0.00 
) 
) 
) 
)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Rate for Commission</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <fields>
        <fullName>Tax_Amount__c</fullName>
        <externalId>false</externalId>
        <formula>(Gross_Amount__c * Tax__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Tax Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Tax__c</fullName>
        <externalId>false</externalId>
        <label>Tax</label>
        <precision>8</precision>
        <required>false</required>
        <scale>5</scale>
        <trackTrending>false</trackTrending>
        <type>Percent</type>
    </fields>
    <label>ISS Line Item</label>
    <nameField>
        <displayFormat>ISS-{000000}</displayFormat>
        <label>ISS Line Item Name</label>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>ISS Line Items</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
