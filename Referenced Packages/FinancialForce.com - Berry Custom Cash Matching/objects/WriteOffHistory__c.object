<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>false</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Account__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Write Off Histories</relationshipLabel>
        <relationshipName>Write_Off_Histories</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Amount__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Amount</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>CashMatchingHistory__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Matching Reference</label>
        <referenceTo>c2g__codaCashMatchingHistory__c</referenceTo>
        <relationshipLabel>Write Off Histories</relationshipLabel>
        <relationshipName>Write_Off_Histories</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>DocumentName__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Document Name</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>MatchingReference__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Matching Reference</label>
        <referenceTo>c2g__codaMatchingReference__c</referenceTo>
        <relationshipLabel>Write Off Histories</relationshipLabel>
        <relationshipName>Write_Off_Histories</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>TransactionLine__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Transaction Line Item</label>
        <referenceTo>c2g__codaTransactionLineItem__c</referenceTo>
        <relationshipLabel>Write Off Histories</relationshipLabel>
        <relationshipName>Write_Off_Histories</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Transaction__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Transaction</label>
        <referenceTo>c2g__codaTransaction__c</referenceTo>
        <relationshipLabel>Write Off Histories</relationshipLabel>
        <relationshipName>Write_Off_Histories</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>UndoMatchingReference__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Undo Matching Reference</label>
        <referenceTo>c2g__codaMatchingReference__c</referenceTo>
        <relationshipLabel>Write Off Histories (Undo Matching Reference)</relationshipLabel>
        <relationshipName>Write_Off_Histories1</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>WriteOffType__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Write Off Type</label>
        <picklist>
            <picklistValues>
                <fullName>External Write-Off</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Internal Write-Off</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Internal Write-Off - No Fault</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <label>Write Off History</label>
    <listViews>
        <fullName>All</fullName>
        <columns>Account__c</columns>
        <columns>Amount__c</columns>
        <columns>NAME</columns>
        <columns>DocumentName__c</columns>
        <columns>Transaction__c</columns>
        <columns>TransactionLine__c</columns>
        <columns>WriteOffType__c</columns>
        <columns>CashMatchingHistory__c</columns>
        <columns>CREATED_DATE</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
        <language>en_US</language>
    </listViews>
    <nameField>
        <label>Write Off History Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Write Off Histories</pluralLabel>
    <searchLayouts/>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
