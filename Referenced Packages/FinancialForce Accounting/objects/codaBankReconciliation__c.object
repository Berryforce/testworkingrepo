<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <customHelpPage>codahelpbankreconciliation</customHelpPage>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Stores the master details of a bank reconciliation.</description>
    <enableActivities>false</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>BankAccountCurrency__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>BankAccount__r.BankAccountCurrency__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Bank Account Currency</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>BankAccountNumber__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>BankAccount__r.AccountNumber__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Bank Account Number</label>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>BankAccount__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Bank Account</label>
        <referenceTo>codaBankAccount__c</referenceTo>
        <relationshipName>Reconciliations</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>BankChargesAndInterest__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>InterestReceived__c + InterestPaid__c + BankCharges__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Bank Charges and Interest</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>BankCharges__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Bank Charges</label>
        <summarizedField>codaBankReconciliationCharge__c.Value__c</summarizedField>
        <summaryFilterItems>
            <field>codaBankReconciliationCharge__c.Type__c</field>
            <operation>equals</operation>
            <value>Bank Charges</value>
        </summaryFilterItems>
        <summaryForeignKey>codaBankReconciliationCharge__c.BankReconciliation__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>ClearedBalance__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>OpeningBalance__c + TotalReconciled__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Cleared Balance</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ClosingBalance__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Closing Balance</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Difference__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>ClosingBalance__c - ClearedBalance__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Difference</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>InterestPaid__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Interest Paid</label>
        <summarizedField>codaBankReconciliationCharge__c.Value__c</summarizedField>
        <summaryFilterItems>
            <field>codaBankReconciliationCharge__c.Type__c</field>
            <operation>equals</operation>
            <value>Interest Paid</value>
        </summaryFilterItems>
        <summaryForeignKey>codaBankReconciliationCharge__c.BankReconciliation__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>InterestReceived__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Interest Received</label>
        <summarizedField>codaBankReconciliationCharge__c.Value__c</summarizedField>
        <summaryFilterItems>
            <field>codaBankReconciliationCharge__c.Type__c</field>
            <operation>equals</operation>
            <value>Interest Received</value>
        </summaryFilterItems>
        <summaryForeignKey>codaBankReconciliationCharge__c.BankReconciliation__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>OpeningBalance__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>PreviousReconciliation__r.ClosingBalance__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Opening Balance</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>OwnerCompany__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Company</label>
        <referenceTo>codaCompany__c</referenceTo>
        <relationshipName>Reconciliations</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>PreviousReconciliation__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Previous Reconciliation</label>
        <referenceTo>codaBankReconciliation__c</referenceTo>
        <relationshipName>Reconciliations</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>ReconciledItems__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Reconciled Items</label>
        <summarizedField>codaBankReconciliationLineItem__c.Value__c</summarizedField>
        <summaryForeignKey>codaBankReconciliationLineItem__c.BankReconciliation__c</summaryForeignKey>
        <summaryOperation>sum</summaryOperation>
        <trackTrending>false</trackTrending>
        <type>Summary</type>
    </fields>
    <fields>
        <fullName>ReconciliationStatus__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Reconciliation Status</label>
        <picklist>
            <picklistValues>
                <fullName>In Progress</fullName>
                <default>true</default>
            </picklistValues>
            <picklistValues>
                <fullName>Complete</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>StatementDate__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Statement Date</label>
        <required>true</required>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>TotalReconciled__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>ReconciledItems__c + BankChargesAndInterest__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Total Reconciled</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>UnitOfWork__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Unit of Work</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Bank Reconciliation</label>
    <listViews>
        <fullName>All</fullName>
        <columns>NAME</columns>
        <columns>BankAccount__c</columns>
        <columns>BankAccountCurrency__c</columns>
        <columns>BankAccountNumber__c</columns>
        <columns>ClosingBalance__c</columns>
        <columns>OwnerCompany__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Bank Reconciliation Reference</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Bank Reconciliations</pluralLabel>
    <recordTypes>
        <fullName>Uncontrolled</fullName>
        <active>true</active>
        <label>Uncontrolled</label>
        <picklistValues>
            <picklist>ReconciliationStatus__c</picklist>
            <values>
                <fullName>Complete</fullName>
                <default>true</default>
            </values>
            <values>
                <fullName>In Progress</fullName>
                <default>false</default>
            </values>
        </picklistValues>
    </recordTypes>
    <searchLayouts>
        <customTabListAdditionalFields>BankAccount__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>BankAccountNumber__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>StatementDate__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>OpeningBalance__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>ClosingBalance__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>OwnerCompany__c</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>BankAccount__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>BankAccountNumber__c</lookupDialogsAdditionalFields>
    </searchLayouts>
    <sharingModel>Private</sharingModel>
    <webLinks>
        <fullName>Commit</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Commit</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <url>{!REQUIRESCRIPT(&quot;/soap/ajax/15.0/connection.js&quot;)}
{!REQUIRESCRIPT(&quot;/soap/ajax/15.0/apex.js&quot;)}

var thisId= &quot;{!codaBankReconciliation__c.Id}&quot;;

var result = sforce.apex.execute(&quot;c2g.CODABankReconciliationWebService&quot;, &quot;commitBankReconciliation&quot;, { bankRecId : thisId } );

// convert result to text and split it into success&gt;&gt;messagetext
var resulttext = result.toString();
var rtnval = resulttext.split(&quot;&gt;&gt;&quot;);

// get the success status and output the messages from the web service
success=rtnval[0];

// concat the remaining elements in the return value to construct the message
msg = &quot;&quot;;
for (i=1; i&lt;rtnval.length; i++)
    msg += rtnval[i];
alert(msg);

if (success == &quot;true&quot;)
{
    window.location.reload(true);
}</url>
    </webLinks>
    <webLinks>
        <fullName>Reopen</fullName>
        <availability>online</availability>
        <displayType>button</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Reopen</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <url>{!REQUIRESCRIPT(&quot;/soap/ajax/15.0/connection.js&quot;)}
{!REQUIRESCRIPT(&quot;/soap/ajax/15.0/apex.js&quot;)}

var thisId= &quot;{!codaBankReconciliation__c.Id}&quot;;

var result = sforce.apex.execute(&quot;c2g.CODABankReconciliationWebService&quot;, &quot;reopenBankReconciliation&quot;, { bankRecId : thisId } );

// convert result to text and split it into success&gt;&gt;messagetext
var resulttext = result.toString();
var rtnval = resulttext.split(&quot;&gt;&gt;&quot;);

// get the success status and output the messages from the web service
success=rtnval[0];

// concat the remaining elements in the return value to construct the message
msg = &quot;&quot;;
for (i=1; i&lt;rtnval.length; i++)
    msg += rtnval[i];
alert(msg);

if (success == &quot;true&quot;)
{
    window.location.reload(true);
}</url>
    </webLinks>
</CustomObject>
