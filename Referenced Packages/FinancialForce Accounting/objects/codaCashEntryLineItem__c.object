<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <customHelpPage>codahelpcashentry</customHelpPage>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Stores the line items on cash entry.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>true</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>AccountDimension1__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Account Dimension 1</label>
        <referenceTo>codaDimension1__c</referenceTo>
        <relationshipLabel>Cash Entry Line Items</relationshipLabel>
        <relationshipName>CashEntryLineItemsAcDim1</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>AccountDimension2__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Account Dimension 2</label>
        <referenceTo>codaDimension2__c</referenceTo>
        <relationshipLabel>Cash Entry Line Items</relationshipLabel>
        <relationshipName>CashEntryLineItemsAcDim2</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>AccountDimension3__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Account Dimension 3</label>
        <referenceTo>codaDimension3__c</referenceTo>
        <relationshipLabel>Cash Entry Line Items</relationshipLabel>
        <relationshipName>CashEntryLineItemsAcDim3</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>AccountDimension4__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Account Dimension 4</label>
        <referenceTo>codaDimension4__c</referenceTo>
        <relationshipLabel>Cash Entry Line Items</relationshipLabel>
        <relationshipName>CashEntryLineItemsAcDim4</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>AccountPaymentMethod__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Account Payment Method</label>
        <picklist>
            <picklistValues>
                <fullName>Electronic - Epay</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Lockbox</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Manual</fullName>
                <default>false</default>
            </picklistValues>
            <picklistValues>
                <fullName>Migrated Open AR</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Picklist</type>
    </fields>
    <fields>
        <fullName>AccountReference__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Account Reference</label>
        <length>20</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Account__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Account</label>
        <referenceTo>Account</referenceTo>
        <relationshipLabel>Cash Entry Line Items</relationshipLabel>
        <relationshipName>CashEntryLineItems</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>BankAccountValue__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Bank Account Value</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CashEntryCurrency__c</fullName>
        <deprecated>false</deprecated>
        <description>Cash entry currency field for layouts</description>
        <externalId>false</externalId>
        <formula>CashEntry__r.CashEntryCurrency__r.Name</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Cash Entry Currency</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CashEntryDate__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>CashEntry__r.Date__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Date</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Date</type>
    </fields>
    <fields>
        <fullName>CashEntryValue__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Cash Entry Value</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>CashEntry__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Cash Entry</label>
        <referenceTo>codaCashEntry__c</referenceTo>
        <relationshipLabel>Cash Entry Line Items</relationshipLabel>
        <relationshipName>CashEntryLineItems</relationshipName>
        <relationshipOrder>0</relationshipOrder>
        <reparentableMasterDetail>false</reparentableMasterDetail>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>MasterDetail</type>
        <writeRequiresMasterRead>false</writeRequiresMasterRead>
    </fields>
    <fields>
        <fullName>Charges__c</fullName>
        <defaultValue>0</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Line Charges</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>ExternalId__c</fullName>
        <caseSensitive>false</caseSensitive>
        <deprecated>false</deprecated>
        <externalId>true</externalId>
        <label>External Id</label>
        <length>32</length>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>true</unique>
    </fields>
    <fields>
        <fullName>LineDescription__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Check number or confirmation number</inlineHelpText>
        <label>Line Description</label>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>TextArea</type>
    </fields>
    <fields>
        <fullName>LineNumber__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Line Number</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>NetValue__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>BankAccountValue__c  -  Charges__c</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Net Value</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>OwnerCompany__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Company</label>
        <referenceTo>codaCompany__c</referenceTo>
        <relationshipLabel>Cash Entry Line Items</relationshipLabel>
        <relationshipName>Cash_Entry_Line_Items</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>PaymentCurrency__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>FOR DEPRECATION:Payment Currency</label>
        <referenceTo>codaAccountingCurrency__c</referenceTo>
        <relationshipLabel>Cash Entry Line Items</relationshipLabel>
        <relationshipName>R00N20000001gr8zEAA</relationshipName>
        <required>false</required>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>PaymentValue__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>FOR DEPRECATION:Payment Value</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SignedLineCharges__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF( text(CashEntry__r.Type__c) = &quot;Refund&quot;, Charges__c  *-1, Charges__c  )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Signed Line Charges</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SignedNetValue__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF( text(CashEntry__r.Type__c) = &quot;Refund&quot;, NetValue__c *-1, NetValue__c )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Signed Net Value</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>SignedValue__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>IF( text(CashEntry__r.Type__c) = &quot;Refund&quot;, CashEntryValue__c *-1, CashEntryValue__c )</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Signed Value</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>UnitOfWork__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Unit of Work</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackHistory>false</trackHistory>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <label>Cash Entry Line Item</label>
    <nameField>
        <displayFormat>{0}</displayFormat>
        <label>Cash Entry Line Item ID</label>
        <trackHistory>false</trackHistory>
        <type>AutoNumber</type>
    </nameField>
    <pluralLabel>Cash Entry Line Items</pluralLabel>
    <searchLayouts>
        <lookupDialogsAdditionalFields>Original_Account_Number__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Original_Amount__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Account__c</lookupDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Account__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>AccountReference__c</lookupPhoneDialogsAdditionalFields>
        <lookupPhoneDialogsAdditionalFields>Cash_Entry_Line_Item__c</lookupPhoneDialogsAdditionalFields>
        <searchFilterFields>Account__c</searchFilterFields>
        <searchFilterFields>AccountReference__c</searchFilterFields>
        <searchFilterFields>CashEntryValue__c</searchFilterFields>
    </searchLayouts>
    <sharingModel>ControlledByParent</sharingModel>
</CustomObject>
