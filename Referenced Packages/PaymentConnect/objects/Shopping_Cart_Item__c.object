<?xml version="1.0" encoding="UTF-8"?>
<CustomObject xmlns="http://soap.sforce.com/2006/04/metadata">
    <actionOverrides>
        <actionName>Accept</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>CancelEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Clone</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Delete</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Edit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Follow</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>List</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>New</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>SaveEdit</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>Tab</actionName>
        <type>Default</type>
    </actionOverrides>
    <actionOverrides>
        <actionName>View</actionName>
        <type>Default</type>
    </actionOverrides>
    <compactLayoutAssignment>SYSTEM</compactLayoutAssignment>
    <customHelpPage>Help</customHelpPage>
    <deploymentStatus>Deployed</deploymentStatus>
    <deprecated>false</deprecated>
    <description>Stores line-item details for payments recorded in Salesforce through PaymentConnect.</description>
    <enableActivities>true</enableActivities>
    <enableBulkApi>true</enableBulkApi>
    <enableFeeds>false</enableFeeds>
    <enableHistory>false</enableHistory>
    <enableReports>true</enableReports>
    <enableSharing>true</enableSharing>
    <enableStreamingApi>true</enableStreamingApi>
    <fields>
        <fullName>Contact__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Contact</label>
        <referenceTo>Contact</referenceTo>
        <relationshipLabel>Shopping Cart Items</relationshipLabel>
        <relationshipName>R00N40000001tGO4EAM</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Description__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>A detailed description of an item (color, size, other options)</inlineHelpText>
        <label>Description</label>
        <length>1000</length>
        <trackTrending>false</trackTrending>
        <type>LongTextArea</type>
        <visibleLines>4</visibleLines>
    </fields>
    <fields>
        <fullName>Lead__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Lead</label>
        <referenceTo>Lead</referenceTo>
        <relationshipLabel>Shopping Cart Items</relationshipLabel>
        <relationshipName>R00N40000001tGO3EAM</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Line_Number__c</fullName>
        <deprecated>false</deprecated>
        <description>Sort order for shopping cart items.</description>
        <externalId>false</externalId>
        <inlineHelpText>Sort order for shopping cart items.</inlineHelpText>
        <label>Line Number</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>On_Payment_Completed__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>On Payment Completed</label>
        <picklist>
            <picklistValues>
                <fullName>Sample Action</fullName>
                <default>false</default>
            </picklistValues>
            <sorted>false</sorted>
        </picklist>
        <trackTrending>false</trackTrending>
        <type>MultiselectPicklist</type>
        <visibleLines>3</visibleLines>
    </fields>
    <fields>
        <fullName>Payment_Completed__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <inlineHelpText>Checked automatically when the parent Payment record status is set to &apos;Completed&apos;.</inlineHelpText>
        <label>Payment Completed</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Payment__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Payment</label>
        <referenceTo>PaymentX__c</referenceTo>
        <relationshipLabel>Shopping Cart Items</relationshipLabel>
        <relationshipName>R00N40000001tGNtEAM</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Product_Code__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Product Code</label>
        <length>255</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Product__c</fullName>
        <deleteConstraint>SetNull</deleteConstraint>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Product</label>
        <referenceTo>Product2</referenceTo>
        <relationshipLabel>Shopping Cart Items</relationshipLabel>
        <relationshipName>R00N40000001tGNZEA2</relationshipName>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Lookup</type>
    </fields>
    <fields>
        <fullName>Quantity__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Quantity</label>
        <precision>18</precision>
        <required>false</required>
        <scale>0</scale>
        <trackTrending>false</trackTrending>
        <type>Number</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Shipped__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Check if this item has been shipped.</description>
        <externalId>false</externalId>
        <inlineHelpText>Check if this item has been shipped.</inlineHelpText>
        <label>Shipped</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Tangible__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Check if the item is a physical product (ie may require shipping)</description>
        <externalId>false</externalId>
        <inlineHelpText>Check if the item is a physical product (ie may require shipping)</inlineHelpText>
        <label>Tangible</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Taxable__c</fullName>
        <defaultValue>false</defaultValue>
        <deprecated>false</deprecated>
        <description>Check if the item is taxable.</description>
        <externalId>false</externalId>
        <inlineHelpText>Check if the item is taxable.</inlineHelpText>
        <label>Taxable</label>
        <trackTrending>false</trackTrending>
        <type>Checkbox</type>
    </fields>
    <fields>
        <fullName>Total__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <formula>if( OR( ISNULL(Quantity__c), ISNULL( Unit_Price__c)), 0,Quantity__c  * Unit_Price__c)</formula>
        <formulaTreatBlanksAs>BlankAsZero</formulaTreatBlanksAs>
        <label>Total</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <fields>
        <fullName>Tracking_Number__c</fullName>
        <deprecated>false</deprecated>
        <description>Tracking number assigned when shipping this item.</description>
        <externalId>false</externalId>
        <inlineHelpText>Tracking number assigned when shipping this item.</inlineHelpText>
        <label>Tracking Number</label>
        <length>100</length>
        <required>false</required>
        <trackTrending>false</trackTrending>
        <type>Text</type>
        <unique>false</unique>
    </fields>
    <fields>
        <fullName>Unit_Price__c</fullName>
        <deprecated>false</deprecated>
        <externalId>false</externalId>
        <label>Unit Price</label>
        <precision>18</precision>
        <required>false</required>
        <scale>2</scale>
        <trackTrending>false</trackTrending>
        <type>Currency</type>
    </fields>
    <label>Shopping Cart Item</label>
    <listViews>
        <fullName>All</fullName>
        <columns>Payment__c</columns>
        <columns>Contact__c</columns>
        <columns>Quantity__c</columns>
        <columns>NAME</columns>
        <columns>Unit_Price__c</columns>
        <columns>Total__c</columns>
        <filterScope>Everything</filterScope>
        <label>All</label>
    </listViews>
    <nameField>
        <label>Item Name</label>
        <type>Text</type>
    </nameField>
    <pluralLabel>Shopping Cart Items</pluralLabel>
    <searchLayouts>
        <customTabListAdditionalFields>Contact__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Payment__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Product__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Product_Code__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Quantity__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Total__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>Payment_Completed__c</customTabListAdditionalFields>
        <customTabListAdditionalFields>CREATED_DATE</customTabListAdditionalFields>
        <lookupDialogsAdditionalFields>Contact__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Product__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>Payment_Completed__c</lookupDialogsAdditionalFields>
        <lookupDialogsAdditionalFields>CREATED_DATE</lookupDialogsAdditionalFields>
        <searchFilterFields>NAME</searchFilterFields>
        <searchFilterFields>Contact__c</searchFilterFields>
        <searchFilterFields>Product__c</searchFilterFields>
        <searchFilterFields>Product_Code__c</searchFilterFields>
        <searchFilterFields>Unit_Price__c</searchFilterFields>
        <searchFilterFields>Payment__c</searchFilterFields>
        <searchResultsAdditionalFields>Contact__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Product__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Product_Code__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Quantity__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Total__c</searchResultsAdditionalFields>
        <searchResultsAdditionalFields>Payment_Completed__c</searchResultsAdditionalFields>
    </searchLayouts>
    <sharingModel>ReadWrite</sharingModel>
    <webLinks>
        <fullName>Mark_Items_Shipped</fullName>
        <availability>online</availability>
        <description>Marks selected Shopping Cart Items as &quot;shipped&quot;</description>
        <displayType>massActionButton</displayType>
        <linkType>javascript</linkType>
        <masterLabel>Mark Items Shipped</masterLabel>
        <openType>onClickJavaScript</openType>
        <protected>false</protected>
        <requireRowSelection>true</requireRowSelection>
        <url>{!REQUIRESCRIPT(&quot;/soap/ajax/25.0/connection.js&quot;)} 

var records = {!GETRECORDIDS( $ObjectType.Shopping_Cart_Item__c)}; 


if (records[0] == null) { 
alert(&quot;Please select at least one record.&quot;) } 
else { 

var updatedRecords = new Array();
for (var i = 0; i &lt; records.length; i++) {
  var item = new sforce.SObject(&quot;pymt__Shopping_Cart_Item__c&quot;);
  item.set(&quot;Id&quot;, records[i]);
  item.set(&quot;pymt__Shipped__c&quot;, true);
  updatedRecords.push(item);
}

var errors = []; 
var result = sforce.connection.update(updatedRecords); 
if (result &amp;&amp; result.length){ 
var numFailed = 0; 
var numSucceeded = 0; 
for (var i = 0; i &lt; result.length; i++){ 
var res = result[i]; 
if (res &amp;&amp; res.success == &apos;true&apos;){ 
numSucceeded++; 
} else { 
var es = res.getArray(&quot;errors&quot;); 
if (es.length &gt; 0) { 
errors.push(es[0].message); 
} 
numFailed++; 
} 
} 
if (numFailed &gt; 0){ 
alert(&quot;Failed: &quot; + numFailed + &quot;\nSucceeded: &quot; + numSucceeded + &quot; \n Due to: &quot; + errors.join(&quot;\n&quot;)); 
} else { 
alert(&quot;Number of records updated: &quot; + numSucceeded); 
} 
} 
window.location.reload(); 
}</url>
    </webLinks>
</CustomObject>
